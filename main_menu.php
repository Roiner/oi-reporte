<?php
// Define el menú principal

$mainMenuItems[] = array('level' => 0, 'label' => 'Inicio', 'url' => 'index.php');

############################Planificacion####################################

//-------------------------Modulo de Planificacion-----------------------------//
$mainMenuItems[] = array('level' => 0, 'label' => 'Planificacion', 'submenu' => 't', 'url' => '#');
	$mainMenuItems[] = array('level' => 1, 'label' => 'Nueva Planificacion', 'url' => '#',
		'script' => "sysfwk.loadPage('task/tsk_planificaciones(mc_add1).php', 'mainContent');");	
	$mainMenuItems[] = array('level' => 1, 'label' => 'Ver Planificaciones', 'url' => '#',
		'script' => "sysfwk.loadPage('task/tsk_planificaciones(mc_list1).php', 'mainContent');");

// //---------------------------Modulo de Recurso---------------------------------//
// $mainMenuItems[] = array('level' => 0, 'label' => 'Recursos', 'submenu' => 't', 'url' => '#');
// 	$mainMenuItems[] = array('level' => 1, 'label' => 'Materiales', 'url' => '#',
// 		'script' => "sysfwk.loadPage('task/tsk_materiales(mc_list1).php', 'mainContent');");
// 	$mainMenuItems[] = array('level' => 1, 'label' => 'Vehiculos', 'url' => '#',
// 		'script' => "sysfwk.loadPage('task/tsk_vehiculos(mc_list1).php', 'mainContent');");

//--------------------------Modulo de Reportes--------------------------------//
$mainMenuItems[] = array('level' => 0, 'label' => 'Reportes', 'submenu' => 't', 'url' => '#');
	$mainMenuItems[] = array('level' => 1, 'label' => 'Ver Reportes', 'url' => '#',
		'script' => "sysfwk.loadPage('task/tsk_actividadesDiarias(mc_list1).php', 'mainContent');");
	$mainMenuItems[] = array('level' => 1, 'label' => 'Resumen de Reporte', 'url' => '#',
		'script' => "sysfwk.loadPage('task/tsk_resumenDiario(mc_list1).php', 'mainContent')");

//------------------------Modulo de indicadores-------------------------------//
$mainMenuItems[] = array('level' => 0, 'label' => 'Indicadores', 'submenu' => 't', 'url' => '#');
	$mainMenuItems[] = array('level' => 1, 'label' => 'Ver Indicadores', 'url' => '#',
		'script' => "sysfwk.loadPage('task/tsk_indicadores(mc_list1).php', 'mainContent')");
	$mainMenuItems[] = array('level' => 1, 'label' => 'Indicadores de Asistencias', 'url' => '#',
		'script' => "sysfwk.loadPage('task/tsk_indicadoresAsistencias(mc_list1).php', 'mainContent')");

############################Supervisor#########################################
//------------------------Modulo de actividades-------------------------------//	
$mainMenuItems[] = array('level' => 0, 'label' => 'Actividades', 'url' => '#',
	'script' => "sysfwk.loadPage('task/tsk_actividadesDiarias(mc_add1).php', 'mainContent');");


#############################Supervisor#######################################

//--------------------------Tipos de asistencias------------------------------//
$mainMenuItems[] = array('level' => 0, 'label' => 'Asistencias', 'submenu' => 't','url' => '#');
	$mainMenuItems[] = array('level' => 1, 'label' => 'Tipos de Asistencias', 'url' => '#',
		'script' => "sysfwk.loadPage('task/tsk_asistenciaTipos(mc_list1).php', 'mainContent');");
	$mainMenuItems[] = array('level' => 1, 'label' => 'Trabajadores Disponibles', 'url' => '#',
		'script' => "sysfwk.loadPage('task/tsk_personalesAsignados(mc_list1).php', 'mainContent');");


//-----------------------------------Pruebas------------------------------------//
// $mainMenuItems[] = array('level' => 0, 'label' => 'Pruebas', 'url' => '#',
// 	'script' => "sysfwk.loadPage('task/tsk_pruebas.php', 'mainContent');");
/*$mainMenuItems[] = array('level' => 0, 'label' => 'Administrar', 'submenu' => 't', 'url' => '#');
	$mainMenuItems[] = array('level' => 1, 'label' => 'Tablas Maestras', 'submenu' => 't', 'url' => '#');
		$mainMenuItems[] = array('level' => 2, 'label' => 'Sistema', 'url' => '#',
			'script' => "sysfwk.loadPage('task/tsk_sistema(mc_list1).php', 'mainContent');");*/

$mainMenuItems[] = array('level' => 0, 'label' => 'Cerrar Sesión', 'url' => '#', 'script' => "sysfwk.logOut();");
?>