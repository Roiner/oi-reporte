<?php
// ---- Constantes del Sistema ----
define('SYS_ID', '1038'); // REQUEDIDA
define('SYS_TITLE', 'Sistema de Servicios Generales de Reportes en Linea'); // OPCIONAL
define('SYS_SIGLAS', 'sisgrel'); // OPCIONAL
define('SYS_ID_ASISTENCIA', '1');


// ---- Version de jquery y plugins (OPCIONAL) ----
// Usar sólo en caso de requerir una versión específica por conflictos con la versión actual
// define('V_JQUERY', '1.12.4');
// define('V_JQUERY_UI', '1.11.4');
// define('V_TIMEPICKER', '1.6.3');
// define('V_VALIDATION', '1.15.0');


// ---- Tema de jqueryui (OPCIONAL) ----
// Se puede usar para definir un tema JQUERYUI distinto al tema por defecto (redmond)
// define('JQUERY_UI_THEME', 'redmond');


// ---- Constantes de Configuración (OPCIONAL) ----
// define('AUTHENTICATION_CLASS', 'MyAuthClass'); // Para definir una clase de autenticación propia
// define('LOG_LOGIN', true); // Para habilitar el registro de acceso
// define('LOG_ERROR', true); // Para habilitar el registro de errores
// define('LOG_ACCION', true); // Para habilitar el registro de acciones


// ---- Cadena de Conexión ----
define('CONN_STR', 'pgsql:host=orinoconetdbdev;port=5432;dbname=db_oi;user=sisgrel_admin;password=sisgrel_admin');

// ---- Constantes de DB ----
define('DB_INST', 'pgsql');
define('DB_SCHEMA', 'sch_sisgrel');
?>