<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'TratamientoMC';

// Estructura del contenido
$structure['title'] = 'Agregar Diagnóstico y Tratamiento';
// $structure['container'] = 'divDialog';

// Cargar configuración del screen
include('../screen/scr_tratamiento_mc(abm1).php');

/*
// Configuración de pantalla para la vista dlg.jsfrm1
// $structure['form_id'] = 'frm_tratamientos_mc_jsfrm1';
// $structure['msg_id'] = 'msg_tratamientos_mc_jsfrm1';

// Opciones del dialogo
$structure['dlg_options'] = array(
	'width' => 850
);

// -> Estructura del contenido del form
$structure['form']['type'] = 'fieldsets';
$structure['form']['fs'][0]['type'] = 'fieldset';
$structure['form']['fs'][0]['legend'] = 'Diagnótico y Tratamiento';
$structure['form']['fs'][0]['table'] = 'y';

$structure['form']['fs'][0]['fields'][0][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][0][0][] = array('control' => 'label', 'field' => 'id_diagnostico');
$structure['form']['fs'][0]['fields'][0][1]['td'] = array('class' => 'field', 'colspan' => 3);
$structure['form']['fs'][0]['fields'][0][1][] = array('control' => 'dropdown', 'field' => 'id_diagnostico');

$structure['form']['fs'][0]['fields'][1][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][1][0][] = array('control' => 'label', 'field' => 'id_medicamento');
$structure['form']['fs'][0]['fields'][1][1]['td'] = array('class' => 'field', 'colspan' => 3);
$structure['form']['fs'][0]['fields'][1][1][] = array('control' => 'dropdown', 'field' => 'id_medicamento');

$structure['form']['fs'][0]['fields'][2][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][2][0][] = array('control' => 'label', 'field' => 'concentracion_medi');
$structure['form']['fs'][0]['fields'][2][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][2][1][] = array('control' => 'textbox', 'field' => 'concentracion_medi');
$structure['form']['fs'][0]['fields'][2][2]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][2][2][] = array('control' => 'label', 'field' => 'presentacion_medi');
$structure['form']['fs'][0]['fields'][2][3]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][2][3][] = array('control' => 'textbox', 'field' => 'presentacion_medi');

$structure['form']['fs'][0]['fields'][3][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][3][0][] = array('control' => 'label', 'field' => 'cantidad_medi');
$structure['form']['fs'][0]['fields'][3][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][3][1][] = array('control' => 'textbox', 'field' => 'cantidad_medi');
$structure['form']['fs'][0]['fields'][3][2]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][3][2][] = array('control' => 'label', 'field' => 'frecuencia_medi');
$structure['form']['fs'][0]['fields'][3][3]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][3][3][] = array('control' => 'textbox', 'field' => 'frecuencia_medi');

$structure['form']['fs'][0]['fields'][4][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][4][0][] = array('control' => 'label', 'field' => 'fecha_inicio_t');
$structure['form']['fs'][0]['fields'][4][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][4][1][] = array('control' => 'textbox', 'field' => 'fecha_inicio_t', 'readonly' => 'y', 'class' => 'fecha');
$structure['form']['fs'][0]['fields'][4][2]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][4][2][] = array('control' => 'label', 'field' => 'fecha_fin_t');
$structure['form']['fs'][0]['fields'][4][3]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][4][3][] = array('control' => 'textbox', 'field' => 'fecha_fin_t', 'readonly' => 'y', 'class' => 'fecha');

$structure['form']['fs'][0]['fields'][5][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][5][0][] = array('control' => 'label', 'field' => 'activo_t');
$structure['form']['fs'][0]['fields'][5][1]['td'] = array('class' => 'field', 'colspan' => 3);
$structure['form']['fs'][0]['fields'][5][1][] = array('control' => 'checkbox', 'field' => 'activo_t');
*/
// Antes de retornar el resultado
function beforeOk() {
	// Agregar el texto del diagnostico
	echo "\ndata['diagnostico'] = $('#id_diagnostico option:selected').text();";
	echo "\ndata['medicamento'] = $('#id_medicamento option:selected').text();";
}
/*
// Javascript
$javascript['exec'] = <<<EOS
// Calendarios
$('#fecha_inicio_t').datepicker({
	changeMonth: true,
	numberOfMonths: 3,
	onClose: function( selectedDate ) {
		$('#fecha_fin_t').datepicker( "option", "minDate", selectedDate );
	}
});
$('#fecha_fin_t').datepicker({
	changeMonth: true,
	numberOfMonths: 3,
	onClose: function( selectedDate ) {
		$('#fecha_inicio_t').datepicker( "option", "maxDate", selectedDate );
	}
});

// Reglas de validación
$('#cantidad_medi').rules('add', { digits: true });
EOS;
*/
// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'dlg_jsfrm1.php');
?>