<?php
// Configuración de pantalla para la vista mc.abm1
$structure['form_id'] = 'frm_solicitud_mc_abm1';
$structure['msg_id'] = 'msg_solicitud_mc_abm1';

// -> Estructura del contenido del form
$structure['form']['type'] = 'fieldsets';
$structure['form']['fs'][0]['type'] = 'fieldset';
$structure['form']['fs'][0]['legend'] = 'Datos de Solicitud';
$structure['form']['fs'][0]['table'] = 'y';

$structure['form']['fs'][0]['fields'][0][0]['td'] = array('class' => 'label');
$structure['form']['fs'][1]['fields'][0][0][] = array('control' => 'hidden', 'field' => 'id_mc_solicitud');
$structure['form']['fs'][0]['fields'][0][0][] = array('control' => 'label', 'field' => 'fecha_solicitud');
$structure['form']['fs'][0]['fields'][0][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][0][1][] = array('control' => 'textbox', 'field' => 'fecha_solicitud', 'readonly' => 'y',
	'class' => 'fecha pointer');
$structure['form']['fs'][0]['fields'][0][2]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][0][2][] = array('control' => 'label', 'field' => 'fecha_envio_prov');
$structure['form']['fs'][0]['fields'][0][3]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][0][3][] = array('control' => 'textbox', 'field' => 'fecha_envio_prov', 'readonly' => 'y',
	'class' => 'fecha pointer');

$structure['form']['fs'][1]['type'] = 'fieldset';
$structure['form']['fs'][1]['legend'] = 'Datos del Titular';
$structure['form']['fs'][1]['table'] = 'y';

$structure['form']['fs'][1]['fields'][0][0]['td'] = array('class' => 'label');
$structure['form']['fs'][1]['fields'][0][0][] = array('control' => 'label', 'field' => 'id_personal');
$structure['form']['fs'][1]['fields'][0][1]['td'] = array('class' => 'field');
$structure['form']['fs'][1]['fields'][0][1][] = array('control' => 'textbox', 'field' => 'id_personal', 'readonly' => 'y');
$structure['form']['fs'][1]['fields'][0][2]['td'] = array('class' => 'label');
$structure['form']['fs'][1]['fields'][0][2][] = array('control' => 'label', 'field' => 'nomb_pers', 'label' => 'Nombre y Apellido:');
$structure['form']['fs'][1]['fields'][0][3]['td'] = array('class' => 'field');
$structure['form']['fs'][1]['fields'][0][3][] = array('control' => 'textbox', 'field' => 'nomb_pers', 'disabled' => 'y', 'class' => 'nombre');

$structure['form']['fs'][1]['fields'][1][0]['td'] = array('class' => 'label');
$structure['form']['fs'][1]['fields'][1][0][] = array('control' => 'label', 'field' => 'ci_pers', 'label' => 'Cédula Identidad:');
$structure['form']['fs'][1]['fields'][1][1]['td'] = array('class' => 'field');
$structure['form']['fs'][1]['fields'][1][1][] = array('control' => 'textbox', 'field' => 'ci_pers', 'disabled' => 'y', 'class' => 'ci');
$structure['form']['fs'][1]['fields'][1][2]['td'] = array('class' => 'label');
$structure['form']['fs'][1]['fields'][1][2][] = array('control' => 'label', 'field' => 'planta', 'label' => 'Empresa:');
$structure['form']['fs'][1]['fields'][1][3]['td'] = array('class' => 'field');
$structure['form']['fs'][1]['fields'][1][3][] = array('control' => 'textbox', 'field' => 'planta', 'disabled' => 'y');

$structure['form']['fs'][1]['fields'][2][0]['td'] = array('class' => 'label');
$structure['form']['fs'][1]['fields'][2][0][] = array('control' => 'label', 'field' => 'tlf_celular_pers', 'label' => 'Tlf. Celular:');
$structure['form']['fs'][1]['fields'][2][1]['td'] = array('class' => 'field');
$structure['form']['fs'][1]['fields'][2][1][] = array('control' => 'textbox', 'field' => 'tlf_celular_pers', 'disabled' => 'y', 'class' => 'tlf');
$structure['form']['fs'][1]['fields'][2][2]['td'] = array('class' => 'label');
$structure['form']['fs'][1]['fields'][2][2][] = array('control' => 'label', 'field' => 'email_pers', 'label' => 'Correo Electrónico:');
$structure['form']['fs'][1]['fields'][2][3]['td'] = array('class' => 'field');
$structure['form']['fs'][1]['fields'][2][3][] = array('control' => 'textbox', 'field' => 'email_pers', 'disabled' => 'y', 'class' => 'nombre');

$structure['form']['fs'][1]['fields'][3][0]['td'] = array('class' => 'label');
$structure['form']['fs'][1]['fields'][3][0][] = array('control' => 'label', 'field' => 'tlf_habitacion_pers', 'label' => 'Tlf. Habitación:');
$structure['form']['fs'][1]['fields'][3][1]['td'] = array('class' => 'field');
$structure['form']['fs'][1]['fields'][3][1][] = array('control' => 'textbox', 'field' => 'tlf_habitacion_pers', 'disabled' => 'y', 'class' => 'tlf');
$structure['form']['fs'][1]['fields'][3][2]['td'] = array('class' => 'label');
$structure['form']['fs'][1]['fields'][3][2][] = array('control' => 'label', 'field' => 'tipo_nomina', 'label' => 'Tipo de Nómina:');
$structure['form']['fs'][1]['fields'][3][3]['td'] = array('class' => 'field');
$structure['form']['fs'][1]['fields'][3][3][] = array('control' => 'textbox', 'field' => 'tipo_nomina', 'disabled' => 'y');

$structure['form']['fs'][1]['fields'][4][0]['td'] = array('class' => 'label');
$structure['form']['fs'][1]['fields'][4][0][] = array('control' => 'label', 'field' => 'ci_pers_autorizada', 'label' => 'CI Pers. Autorizada:');
$structure['form']['fs'][1]['fields'][4][1]['td'] = array('class' => 'field', 'colspan' => 3);
$structure['form']['fs'][1]['fields'][4][1][] = array('control' => 'textbox', 'field' => 'ci_pers_autorizada', 'disabled' => 'y', 'class' => 'ci');

$structure['form']['fs'][1]['fields'][5][0]['td'] = array('class' => 'label');
$structure['form']['fs'][1]['fields'][5][0][] = array('control' => 'label', 'field' => 'tlf_cel_autorizada', 'label' => 'Tlf. Pers. Autorizada:');
$structure['form']['fs'][1]['fields'][5][1]['td'] = array('class' => 'field');
$structure['form']['fs'][1]['fields'][5][1][] = array('control' => 'textbox', 'field' => 'tlf_cel_autorizada', 'disabled' => 'y', 'class' => 'tlf');
$structure['form']['fs'][1]['fields'][5][2]['td'] = array('class' => 'label');
$structure['form']['fs'][1]['fields'][5][2][] = array('control' => 'label', 'field' => 'pers_autorizada', 'label' => 'Pers. Autorizada:');
$structure['form']['fs'][1]['fields'][5][3]['td'] = array('class' => 'field');
$structure['form']['fs'][1]['fields'][5][3][] = array('control' => 'textbox', 'field' => 'pers_autorizada', 'disabled' => 'y', 'class' => 'nombre');

$structure['form']['fs'][1]['fields'][6][0]['td'] = array('class' => 'label');
$structure['form']['fs'][1]['fields'][6][0][] = array('control' => 'label', 'field' => 'tlf_hab_pers_autorizada', 'label' => 'Tlf. Hab. Pers. Autorizada:');
$structure['form']['fs'][1]['fields'][6][1]['td'] = array('class' => 'field');
$structure['form']['fs'][1]['fields'][6][1][] = array('control' => 'textbox', 'field' => 'tlf_hab_pers_autorizada', 'disabled' => 'y', 'class' => 'tlf');
$structure['form']['fs'][1]['fields'][6][2]['td'] = array('class' => 'label');
$structure['form']['fs'][1]['fields'][6][2][] = array('control' => 'label', 'field' => 'parentesco_pers_autorizada', 'label' => 'Parentesco:');
$structure['form']['fs'][1]['fields'][6][3]['td'] = array('class' => 'field');
$structure['form']['fs'][1]['fields'][6][3][] = array('control' => 'textbox', 'field' => 'parentesco_pers_autorizada', 'disabled' => 'y');


$structure['form']['fs'][2]['type'] = 'fieldset';
$structure['form']['fs'][2]['legend'] = 'Datos del Beneficiario';
$structure['form']['fs'][2]['table'] = 'y';

$structure['form']['fs'][2]['fields'][0][0]['td'] = array('class' => 'label');
$structure['form']['fs'][2]['fields'][0][0][] = array('control' => 'label', 'field' => 'id_beneficiario', 'label' => 'Nombre y Apellido:');
$structure['form']['fs'][2]['fields'][0][1]['td'] = array('class' => 'field');
$structure['form']['fs'][2]['fields'][0][1][] = array('control' => 'dropdown', 'field' => 'id_beneficiario');
$structure['form']['fs'][2]['fields'][0][2]['td'] = array('class' => 'label');
$structure['form']['fs'][2]['fields'][0][2][] = array('control' => 'label', 'field' => 'ci_ben', 'label' => 'Cédula Identidad:');
$structure['form']['fs'][2]['fields'][0][3]['td'] = array('class' => 'field');
$structure['form']['fs'][2]['fields'][0][3][] = array('control' => 'textbox', 'field' => 'ci_ben', 'disabled' => 'y', 'class' => 'ci');

$structure['form']['fs'][2]['fields'][1][0]['td'] = array('class' => 'label');
$structure['form']['fs'][2]['fields'][1][0][] = array('control' => 'label', 'field' => 'nomb_parentesco', 'label' => 'Parentesco:');
$structure['form']['fs'][2]['fields'][1][1]['td'] = array('class' => 'field');
$structure['form']['fs'][2]['fields'][1][1][] = array('control' => 'textbox', 'field' => 'nomb_parentesco', 'disabled' => 'y');
$structure['form']['fs'][2]['fields'][1][2]['td'] = array('class' => 'label');
$structure['form']['fs'][2]['fields'][1][2][] = array('control' => 'label', 'field' => 'sexo_ben', 'label' => 'Sexo:');
$structure['form']['fs'][2]['fields'][1][3]['td'] = array('class' => 'field');
$structure['form']['fs'][2]['fields'][1][3][] = array('control' => 'textbox', 'field' => 'sexo_ben', 'disabled' => 'y');

$structure['form']['fs'][2]['fields'][2][0]['td'] = array('class' => 'label');
$structure['form']['fs'][2]['fields'][2][0][] = array('control' => 'label', 'field' => 'fecha_nac_ben', 'label' => 'Fecha Nacimiento:');
$structure['form']['fs'][2]['fields'][2][1]['td'] = array('class' => 'field');
$structure['form']['fs'][2]['fields'][2][1][] = array('control' => 'textbox', 'field' => 'fecha_nac_ben', 'disabled' => 'y', 'class' => 'fecha');
$structure['form']['fs'][2]['fields'][2][2]['td'] = array('class' => 'label');
$structure['form']['fs'][2]['fields'][2][2][] = array('control' => 'label', 'field' => 'edo_civil_ben', 'label' => 'Edo. Civil:');
$structure['form']['fs'][2]['fields'][2][3]['td'] = array('class' => 'field');
$structure['form']['fs'][2]['fields'][2][3][] = array('control' => 'textbox', 'field' => 'edo_civil_ben', 'disabled' => 'y');

$structure['form']['fs'][2]['fields'][3][0]['td'] = array('class' => 'label');
$structure['form']['fs'][2]['fields'][3][0][] = array('control' => 'label', 'field' => 'profesion_ben', 'label' => 'Profesión:');
$structure['form']['fs'][2]['fields'][3][1]['td'] = array('class' => 'field');
$structure['form']['fs'][2]['fields'][3][1][] = array('control' => 'textbox', 'field' => 'profesion_ben', 'disabled' => 'y', 'class' => 'nombre');
$structure['form']['fs'][2]['fields'][3][2]['td'] = array('class' => 'label');
$structure['form']['fs'][2]['fields'][3][2][] = array('control' => 'label', 'field' => 'estado_ben', 'label' => 'Estado:');
$structure['form']['fs'][2]['fields'][3][3]['td'] = array('class' => 'field');
$structure['form']['fs'][2]['fields'][3][3][] = array('control' => 'textbox', 'field' => 'estado_ben', 'disabled' => 'y');

$structure['form']['fs'][2]['fields'][4][0]['td'] = array('class' => 'label');
$structure['form']['fs'][2]['fields'][4][0][] = array('control' => 'label', 'field' => 'ocupacion_ben', 'label' => 'Ocupación:');
$structure['form']['fs'][2]['fields'][4][1]['td'] = array('class' => 'field');
$structure['form']['fs'][2]['fields'][4][1][] = array('control' => 'textbox', 'field' => 'ocupacion_ben', 'disabled' => 'y', 'class' => 'nombre');
$structure['form']['fs'][2]['fields'][4][2]['td'] = array('class' => 'label');
$structure['form']['fs'][2]['fields'][4][2][] = array('control' => 'label', 'field' => 'ciudad_ben', 'label' => 'Ciudad:');
$structure['form']['fs'][2]['fields'][4][3]['td'] = array('class' => 'field');
$structure['form']['fs'][2]['fields'][4][3][] = array('control' => 'textbox', 'field' => 'ciudad_ben', 'disabled' => 'y');

$structure['form']['fs'][2]['fields'][5][0]['td'] = array('class' => 'label');
$structure['form']['fs'][2]['fields'][5][0][] = array('control' => 'label', 'field' => 'direccion_ben', 'label' => 'Dirección:');
$structure['form']['fs'][2]['fields'][5][1]['td'] = array('class' => 'field', 'colspan' => 3);
$structure['form']['fs'][2]['fields'][5][1][] = array('control' => 'textarea', 'field' => 'direccion_ben', 'disabled' => 'y');


$structure['form']['fs'][3]['type'] = 'fieldset';
$structure['form']['fs'][3]['legend'] = 'Datos del Servicio';
$structure['form']['fs'][3]['table'] = 'y';

$structure['form']['fs'][3]['fields'][0][0]['td'] = array('class' => 'label');
$structure['form']['fs'][3]['fields'][0][0][] = array('control' => 'label', 'field' => 'id_medico');
$structure['form']['fs'][3]['fields'][0][1]['td'] = array('class' => 'field');
$structure['form']['fs'][3]['fields'][0][1][] = array('control' => 'dropdown', 'field' => 'id_medico');
$structure['form']['fs'][3]['fields'][0][2]['td'] = array('class' => 'label');
$structure['form']['fs'][3]['fields'][0][2][] = array('control' => 'label', 'field' => 'especialidad', 'label' => 'Especialidad:');
$structure['form']['fs'][3]['fields'][0][3]['td'] = array('class' => 'field');
$structure['form']['fs'][3]['fields'][0][3][] = array('control' => 'textbox', 'field' => 'especialidad', 'disabled' => 'y', 'class' => 'nombre');

$structure['form']['fs'][3]['fields'][1][0]['td'] = array('class' => 'label');
$structure['form']['fs'][3]['fields'][1][0][] = array('control' => 'label', 'field' => 'tlf_cel_medico', 'label' => 'Tlf. Celular:');
$structure['form']['fs'][3]['fields'][1][1]['td'] = array('class' => 'field');
$structure['form']['fs'][3]['fields'][1][1][] = array('control' => 'textbox', 'field' => 'tlf_cel_medico', 'disabled' => 'y', 'class' => 'tlf');
$structure['form']['fs'][3]['fields'][1][2]['td'] = array('class' => 'label');
$structure['form']['fs'][3]['fields'][1][2][] = array('control' => 'label', 'field' => 'correo_medico', 'label' => 'Correo Médico:');
$structure['form']['fs'][3]['fields'][1][3]['td'] = array('class' => 'field');
$structure['form']['fs'][3]['fields'][1][3][] = array('control' => 'textbox', 'field' => 'correo_medico', 'disabled' => 'y', 'class' => 'nombre');

$structure['form']['fs'][3]['fields'][2][0]['td'] = array('class' => 'label');
$structure['form']['fs'][3]['fields'][2][0][] = array('control' => 'label', 'field' => 'id_clinica');
$structure['form']['fs'][3]['fields'][2][1]['td'] = array('class' => 'field');
$structure['form']['fs'][3]['fields'][2][1][] = array('control' => 'dropdown', 'field' => 'id_clinica');
$structure['form']['fs'][3]['fields'][2][2]['td'] = array('class' => 'label');
$structure['form']['fs'][3]['fields'][2][2][] = array('control' => 'label', 'field' => 'tlf_clinica', 'label' => 'Tlf. Clínica:');
$structure['form']['fs'][3]['fields'][2][3]['td'] = array('class' => 'field');
$structure['form']['fs'][3]['fields'][2][3][] = array('control' => 'textbox', 'field' => 'tlf_clinica', 'disabled' => 'y', 'class' => 'tlf');


$structure['form']['fs'][4]['type'] = 'div';
$structure['form']['fs'][4]['table'] = 'y';

$structure['form']['fs'][4]['fields'][0][0][] = array('control' => 'literal', 'content' => '<div id="divTratamientos"></div>');


// Javascript
$javascript['global'] = <<<'EOS'
// Actualizar lista de Beneficiarios
function ActualizarListaBenABM(reset) {
	var id_ben = $('#id_beneficiario').val();
	var param = { extra: { value: '', text: '(Seleccione)' } };
	param['id_personal'] = $('#id_personal').val();
	$('#id_beneficiario').empty().append('<option value="">cargando...</option>');
	if(reset) {
		sysfwk.loadSelect('BeneficiariosSAP', 'getBenList', param, 'id_beneficiario');
		$('#ci_ben').val('');
		$('#nomb_parentesco').val('');
		$('#sexo_ben').val('');
		$('#fecha_nac_ben').val('');
		$('#edo_civil_ben').val('');
		$('#direccion_ben').val('');
		$('#profesion_ben').val('');
		$('#ocupacion_ben').val('');
		$('#ciudad_ben').val('');
		$('#estado_ben').val('');
	}
	else {
		sysfwk.loadSelect('BeneficiariosSAP', 'getBenList', param, 'id_beneficiario', id_ben);
	}
}

// Actualizar Titular a consultar
function ActualizarTitularABM(datos) {
	sysfwk.ajaxRequestJson({ entity: 'PersTipoServ', method: 'getPersonalInfo', params: { id_personal: datos.value }}, function(data) {
		$('#id_personal').val(data.id_personal);
		$('#ci_pers').val(data.ci);
		$('#nomb_pers').val(data.nombre);
		$('#email_pers').val(data.email);
		$('#planta').val(data.planta);
		$('#tipo_nomina').val(data.tipo_nomina);
		$('#tlf_celular_pers').val(data.tlf_celular_pers);
		$('#tlf_habitacion_pers').val(data.tlf_habitacion_pers);
		$('#ci_pers_autorizada').val(data.ci_pers_autorizada);
		$('#pers_autorizada').val(data.pers_autorizada);
		$('#tlf_cel_autorizada').val(data.tlf_cel_autorizada);
		$('#tlf_hab_pers_autorizada').val(data.tlf_hab_pers_autorizada);
		$('#parentesco_pers_autorizada').val(data.parentesco_pers_autorizada);

		// Actualizar lista de beneficiarios
		ActualizarListaBenABM(true);
	});
}
EOS;

$javascript['def'] = <<<EOS
// Obtener datos del beneficiario
function getBeneficiario() {
	sysfwk.ajaxRequestJson({ entity: 'BenTipoServ', method: 'getBeneficiarioInfo', params: { id_beneficiario: $('#id_beneficiario').val() } }, function(data) {
		$('#ci_ben').val(data.ci_beneficiario);
		$('#nomb_parentesco').val(data.nomb_parentesco);
		$('#sexo_ben').val(data.sexo_ben);
		$('#fecha_nac_ben').val(data.fecha_nac_beneficiario);
		$('#edo_civil_ben').val(data.edo_civil_ben);
		$('#direccion_ben').val(data.direccion_ben);
		$('#profesion_ben').val(data.profesion_ben);
		$('#ocupacion_ben').val(data.ocupacion_ben);
		$('#ciudad_ben').val(data.ciudad_ben);
		$('#estado_ben').val(data.estado_ben);
	});
}

// Obtener datos del medico
function getMedico() {
	sysfwk.ajaxRequestJson({ entity: 'Medico', method: 'getMedicoInfo', params: { id_medico: $('#id_medico').val() } }, function(data) {
		$('#especialidad').val(data.especialidad);
		$('#tlf_cel_medico').val(data.tlf_cel_medico);
		$('#correo_medico').val(data.correo_medico);
	});
}

// Obtener datos de la clinica
function getClinica() {
	sysfwk.ajaxRequestJson({ entity: 'Clinica', method: 'getClinicaInfo', params: { id_clinica: $('#id_clinica').val() } }, function(data) {
		$('#tlf_clinica').val(data.tlf_clinica);
	});
}

// Cargar Tabla de Tratamientos
function loadTratamientos() {
	var data = { id_mc_solicitud: $('#id_mc_solicitud').val(), task_mode: '{$structure['task_mode']}' };
	sysfwk.loadPage('task/tsk_tratamiento_mc(list).php', 'divTratamientos', data, sysfwk.POST);
}

// Exportar solicitud
function exportSolicitud() {
	var id_solicitud = $('#id_mc_solicitud').val();
	window.open("task/tsk_solicitud_mc(excel).php?id_mc_solicitud=" + id_solicitud);
	sysfwk.loadPage('task/tsk_solicitud_mc_envio(dlg_add1).php', 'divDialog', { id_mc_solicitud: id_solicitud });
}
EOS;

$javascript['exec'] = <<<'EOS'
// Definir Calendario
$('#fecha_solicitud, #fecha_envio_prov').datepicker();

// Eventos de los selects
$('#id_personal').click(function() {
	sysfwk.loadPage('task/tsk_personal_sel(dlg_acmp).php', 'divDialog1', { container: 'divDialog1', js_func: 'ActualizarTitularABM' });
});
$('#id_beneficiario').change(getBeneficiario);
$('#id_medico').change(getMedico);
$('#id_clinica').change(getClinica);

// Actualizar lista de beneficiarios
if($('#id_personal').val().length > 0) {
	ActualizarListaBenABM(false);
}

// Cargar tabla de diagnóticos y tratamientos
loadTratamientos();

EOS;

// Mostrar botón exportar para mod1, enq1 y el post de add1
if(in_array($structure['task_mode'], array('mod1', 'enq1')) || ($structure['task_mode'] == 'add1' && $_SERVER['REQUEST_METHOD'] == 'POST')) {
	$javascript['exec'] .= <<<'EOS'
// Botón exportar
$('.buttons').append('<button type="button" id="btn_exportar" name="btn_exportar" "value="exportar" title="Exportar a Excel">Exportar</button>');
$('#btn_exportar').button({ icons: { primary: 'ui-icon-print' } });
$('#btn_exportar').click(function() {
	exportSolicitud();
});
EOS;
}
?>