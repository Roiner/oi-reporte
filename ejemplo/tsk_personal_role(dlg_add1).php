<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Roles permitidos
$structure['allowed_roles'] = array(ROLE_ADM);

// Definir la entidad
$structure['entity'] = 'PersonalRole';

// Estructura del contenido
$structure['title'] = 'Asginar Rol a Personal';
$structure['container'] = 'divDialog';
$structure['form_action'] = 'tsk_personal_role(dlg_add1).php';

// Opciones del dialogo
// $structure['dlg_options'] = array(
	// 'width' => 580
// );

// -> Estructura del panel de botones (opcional)
// $structure['buttons'] = array(
	// BTN_SAVE => BTN_SAVE,
	// BTN_CANCEL => BTN_CANCEL,
	// 'MI_BOTON' => array('id' => 'btn_'.MI_BOTON, 'label' => 'Mi Boton', 'title' => 'Botón personalizado', 'icon_p' => 'comment',
		// 'javascript' => "alert($(event.target).button('disable'));"),
	// BTN_COPY => BTN_COPY,
	// BTN_PASTE => BTN_PASTE
// );

// Cargar configuración del screen
include('../screen/scr_personal_role(abm1).php');

// Incluir botón para seleccionar personal
$structure['form']['fs'][0]['fields'][0][1][] = array('control' => 'button', 'id' => 'btnSelPersonalABM', 'content' => '...',
	'title' => 'Seleccionar Personal');

// JS antes del Submit
function beforeSubmit() {
	$js = <<<EOS
	// Validar selección de personal
	if($('#id_personal').val().length == 0) {
		alert('Debe seleccionar un personal');
		$(btn).parent().find("button:contains('Guardar')").button('enable');
		return;
	}
EOS;
echo $js;
}

// Javascript
$javascript['global'] = <<<'EOS'
// Actualizar personal
function ActualizarPersonal_abm(datos) {
	$('#id_personal').val(datos.value);
	$('#personal').val(datos.text);
}
EOS;

$javascript['exec'] = <<<'EOS'
// Reglas de validación
$('#id_sistema').rules('add', { requiredDefault: '' });
$('#id_role').rules('add', { requiredDefault: '' });
	
	// Actualizar lista de roles
$('#id_sistema').change(function(){
	var param = { extra: { value: '', text: '(Seleccione)' } };
	param['id_sistema'] = $('#id_sistema').val();
	$('#id_role').empty().append('<option value="">cargando...</option>');
	sysfwk.loadSelect('Role', 'getRoleList', param, 'id_role');
});

// Botones
$('#btnSelPersonalABM').button({ icons: {primary:'ui-icon-search'}, text: false });
$('#btnSelPersonalABM').click(function() {
	sysfwk.loadPage('task/tsk_personal_sel(dlg_acmp).php', 'divDialog1', { container: 'divDialog1', js_func: 'ActualizarPersonal_abm'});
});
EOS;

// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'dlg_add1.php');
?>