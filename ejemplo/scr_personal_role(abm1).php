<?php
// Configuración de pantalla para la vista dlg.abm1
$structure['form_id'] = 'frm_personalrole_abm1';
$structure['msg_id'] = 'msg_personalrole_abm1';

// Opciones del dialogo
$structure['dlg_options'] = array(
	'width' => 730
);

// -> Estructura del contenido del form
$structure['form']['type'] = 'fieldsets';
$structure['form']['fs'][0]['type'] = 'fieldset';
$structure['form']['fs'][0]['legend'] = 'Datos de la asginación';
$structure['form']['fs'][0]['table'] = 'y';

$structure['form']['fs'][0]['fields'][0][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][0][0][] = array('control' => 'label', 'field' => 'id_personal');
$structure['form']['fs'][0]['fields'][0][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][0][1][] = array('control' => 'hidden', 'field' => 'id_personal');
$structure['form']['fs'][0]['fields'][0][1][] = array('control' => 'textbox', 'field' => 'personal', 'disabled' => 'y');

$structure['form']['fs'][0]['fields'][1][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][1][0][] = array('control' => 'label', 'field' => 'id_sistema');
$structure['form']['fs'][0]['fields'][1][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][1][1][] = array('control' => 'dropdown', 'field' => 'id_sistema');

$structure['form']['fs'][0]['fields'][2][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][2][0][] = array('control' => 'label', 'field' => 'id_role');
$structure['form']['fs'][0]['fields'][2][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][2][1][] = array('control' => 'dropdown', 'field' => 'id_role');

$structure['form']['fs'][0]['fields'][3][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][3][0][] = array('control' => 'label', 'field' => 'is_primary');
$structure['form']['fs'][0]['fields'][3][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][3][1][] = array('control' => 'checkbox', 'field' => 'is_primary');

// No mostrar para agregar
if(strpos($structure['form_action'], '_add1)') === false) {
	$structure['form']['fs'][0]['fields'][4][0]['td'] = array('class' => 'label');
	$structure['form']['fs'][0]['fields'][4][0][] = array('control' => 'label', 'field' => 'ra_add_date');
	$structure['form']['fs'][0]['fields'][4][1]['td'] = array('class' => 'field');
	$structure['form']['fs'][0]['fields'][4][1][] = array('control' => 'literal', 'field' => 'ra_add_date');
	
	$structure['form']['fs'][0]['fields'][5][0]['td'] = array('class' => 'label');
	$structure['form']['fs'][0]['fields'][5][0][] = array('control' => 'label', 'field' => 'ra_add_user', 'for' => 'creado_por');
	$structure['form']['fs'][0]['fields'][5][1]['td'] = array('class' => 'field');
	$structure['form']['fs'][0]['fields'][5][1][] = array('control' => 'literal', 'field' => 'creado_por');

	$structure['form']['fs'][0]['fields'][6][0]['td'] = array('class' => 'label');
	$structure['form']['fs'][0]['fields'][6][0][] = array('control' => 'label', 'field' => 'ra_mod_date');
	$structure['form']['fs'][0]['fields'][6][1]['td'] = array('class' => 'field');
	$structure['form']['fs'][0]['fields'][6][1][] = array('control' => 'literal', 'field' => 'ra_mod_date');
	
	$structure['form']['fs'][0]['fields'][7][0]['td'] = array('class' => 'label');
	$structure['form']['fs'][0]['fields'][7][0][] = array('control' => 'label', 'field' => 'ra_mod_user', 'for' => 'modificado_por');
	$structure['form']['fs'][0]['fields'][7][1]['td'] = array('class' => 'field');
	$structure['form']['fs'][0]['fields'][7][1][] = array('control' => 'literal', 'field' => 'modificado_por');
}
?>