<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'SolicitudMC';

// Estructura del contenido
$structure['location'] = 'SGIS &raquo; SOLICITUDES &raquo; Medicinas Crónicas &raquo; Registro';
$structure['title'] = 'Agregar Solicitud de Medicinas Crónicas';
$structure['form_action'] = 'tsk_solicitud_mc(mc_add1).php';
$structure['task_mode'] = 'add1';

// Cargar configuración del screen
include('../screen/scr_solicitud_mc(abm1).php');

// -> Estructura del panel de botones (opcional)
// $structure['buttons'] = array(
	// BTN_COPY => BTN_COPY,
	// BTN_PASTE => BTN_PASTE,
	// 'EXPORTAR' => array('id' => 'btn_'.MI_BOTON, 'label' => 'Imprmir', 'title' => 'Imprimir Reporte', 'icon_p' => 'print',
		// 'javascript' => "Exportar()"),
	// BTN_SAVE => BTN_SAVE,
	// BTN_CANCEL => BTN_CANCEL
// );

// Javascript
// $javascript['exec'] = <<<'EOS'
	// $('#siglas').datepicker();
// EOS;

// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'mc_add1.php');
?>