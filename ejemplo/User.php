<?php
// La clase PersonalRole es requerida
require_once($GLOBALS['path_fwk_class'] . 'Role.php');
require_once($GLOBALS['path_fwk_class'] . 'PersonalRole.php');

// Representa al usuario del sistema
class User extends DB_Table {

	// **************************************************************************
	// Constructor
	function __construct() {
		// Guardar el nombre del directorio del script actual
		$this->_dirName = dirname(__file__);
		
		// Orden por defecto de las consultas
		$this->defaultOrderBy = array('id_personal' => 'ASC');
		
		/** Ejecutar el constructor padre. Los parámetros del constructor son:
			* $connstr: Cadena de conexión, pueder un string o un array de la forma array(<cadena de conexión>, <usuario>, <password>)
			* $table: Nombre de la tabla en DB
			* $instdbms: (Opcional) Nombre que identifica un instancia en DB, por defecto es null
			* $dbschema: (Opcional) Nombre del esquema en DB, por defecto es null
			* $dbname: (Opcional) Nombre de la DB, por defecto es null
			* LLamada: parent::__construct($connstr, $table, $instdbms = null, $dbschema = null, $dbname = null, $dirNameDict = null);
			* Ejemplo: parent::__construct(array(CONN_STR, CONN_USR, CONN_PWD), 'prueba', null, DB_SCHEMA);
			*/
		parent::__construct(CONN_STR, 'v_personal', DB_INST, SCH_SYSCTRL);
	} // __construct()
	
	
	// **************************************************************************
	// Autenticar Usuario
	// $params: array de parametros del usuario
	// Retorna array con datos de usuario + campos ('access_granted' => [true|false], info => '')
	public function authenticate($params) {
		// echo '<pre>params: '; var_dump($params); echo '</pre>'; //DEBUG
		if(empty($params['ficha']) && empty($params['id_personal']))
			return array('access_granted' => false, 'info' => 'Falta el parámetro ficha o id_personal');
		// Obtener usuario
		$filter = array('activo' => true);
		if(!empty($params['ficha']))
			$filter['ficha'] = $params['ficha'];
		else
			$filter['id_personal'] = $params['id_personal'];
		// echo '<pre>filter: '; var_dump($filter); echo '</pre>'; //DEBUG
		$userData = $this->getRecords($filter);
		// echo '<pre>userData: '; var_dump($userData); echo '</pre>'; //DEBUG
		if($userData === false) {
			$result = array('access_granted' => false, 'info' => $this->getErrorsString());
		}
		elseif(empty($userData)) {
			$result = array('access_granted' => false, 'info' => 'No se encuentra usuario ' . $filter['ficha'] . $filter['id_personal'] . ', o está inactivo');
		}
		else {
			// Datos del usuario en session
			list($lastName, $firstName, $firstName2) = explode(' ', ucwords(strtolower($userData[0]['nombre'])));
			$_SESSION[USER_ID] = $userData[0]['id_personal'];
			$_SESSION[USER_REAL_ID] = $userData[0]['id_personal'];
			$_SESSION[USER_FICHA] = $userData[0]['ficha'];
			$_SESSION[USER_NAME] = $firstName . ' ' . $lastName;
			
			// Obtener roles del usuario
			$roles = $this->getUserRoles($userData[0]['id_personal']);
			if($roles === false) {
				if(empty($this->_errors))
					$info = "El usuario {$_SESSION[USER_ID]} no tiene permiso para ingresar al sistema";
				else
					$info = 'Error:' . $this->getErrorsString();
				$result = array('access_granted' => false, 'info' => $info);
			}
			else {
				$_SESSION[USER_ROLE_NAME] = "[{$roles[0]['role_name']}]";
				$_SESSION[USER_ROLE] = $roles;
				$_SESSION[USER_REAL_ROLE] = $roles;
				$result = array('access_granted' => true, 'info' => "El usuario {$_SESSION[USER_ID]} a ingresado al sistema con rol {$roles[0]['id_role']}");
			}
		}

		return $result;
	} // authenticate()
	
	
	// **************************************************************************
	// Obtener los roles del usuario para el sistema definido por SYS_ID
	// $id_personal: ID del usuario
	// Retorna array con datos de usuario + campos ('access_granted' => [true|false], info => '')
	public function getUserRoles($id_personal) {
		// Obtener Roles del usuario
		$objPersonalRole = new PersonalRole();
		$objPersonalRole->sqlSelect = "t.id_role, role_name, is_primary, is_sys_admin, is_sys_dev";
		$objPersonalRole->sqlOrderBy = array('is_primary' => 'DESC', 'id_role' => 'ASC');
		$roles = $objPersonalRole->getRecords(array('id_personal' => $id_personal, 'id_sistema' => SYS_ID));
		if($roles === false) {
			$this->_errors = $objPersonalRole->getErrors();
			$roles = false;
		}
		elseif(empty($roles)) {
			if(defined('DEFAULT_ROLE')) {
				$objRole = new Role();
				$role = $objRole->getRecords(array('id_role' => DEFAULT_ROLE, 'id_sistema' => SYS_ID));
				if($role === false) {
					$this->_errors = $objRole->getErrors();
					$roles = false;
				}
				elseif(empty($role)) {
					$this->_errors = array('El rol ' . DEFAULT_ROLE . ' no está definido en el sistema ' . SYS_ID);
					$roles = false;
				}
				else {
					$roles = array(
						0 => array(
							'id_role' => $role[0]['id_role'],
							'role_name' => $role[0]['role_name'],
							'is_primary' => true,
							'is_sys_admin' => false,
							'is_sys_dev' => false
						)
					);
				}
			}
			else {
				$roles = false;
			}
		}
		return $roles;
	} // getUserRoles()
	
	
	//***************************************************************************
	// Obtener el personal activo para un control dropdown
	public function getUserList($params) {
		// Prepara where
		// echo '<pre>params: '; var_dump($params); echo '</pre>'; //DEBUG
		$where = array('WHERE' => 'activo = :t', 'PARAMS' => array('t' => true));
		if(isset($params['search']) && $params['search'] != '') {
			$where['WHERE'] .= " AND (ficha::text ilike ('%' || :ficha || '%') OR nombre ilike ('%' || :nombre || '%'))";
			$where['PARAMS']['ficha'] = $params['search'];
			$where['PARAMS']['nombre'] = $params['search'];
		}
		
		// Guardar variables query originales
		$orgQueryVars = $this->getAllQueryVars();
		
		// Obtener gerencias
		$this->sqlSelect = "t.id_personal as value, (ficha || ' - ' || nombre) as text";
		$this->sqlOrderBy = array('value' => 'ASC');
		$result = $this->getRecords($where);
		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . '</pre>'; //DEBUG
		if($result !== false) {
			// Agregar elemento extra si existe
			if(isset($params['extra'])) {
				array_unshift($result, array('value' => $params['extra']['value'], 'text' => $params['extra']['text']));
			}
			// Agregar elemento default si existe
			if($this->getNumRows() == 0 && isset($params['default'])) {
				array_unshift($result, array('value' => $params['default']['value'], 'text' => $params['default']['text']));
			}
		}
		
		// Restaurar variables query originales
		$this->setAllQueryVars($orgQueryVars);

		return $result;
	} // getUserList()
	
	
	//***************************************************************************
	// Obtener las gerencias del personal activo para un control dropdown
	public function getGerenciaList($params) {
		// Prepara where
		// $where = ((isset($params['id_op']) && $params['id_op'] != '') ? array('id_op' => $params['id_op']) : array());
		$where = array('activo' => true);
		
		// Guardar variables query originales
		$orgQueryVars = $this->getAllQueryVars();
		
		// Obtener gerencias
		$this->sqlSelect = "id_gerencia as value, gerencia as text";
		$this->sqlGroupBy = "id_gerencia, gerencia";
		$this->sqlOrderBy = array('text' => 'ASC');
		$result = $this->getRecords($where);
		if($result !== false) {
			// Agregar elemento extra si existe
			if(isset($params['extra'])) {
				array_unshift($result, array('value' => $params['extra']['value'], 'text' => $params['extra']['text']));
			}
			// Agregar elemento default si existe
			if($this->getNumRows() == 0 && isset($params['default'])) {
				array_unshift($result, array('value' => $params['default']['value'], 'text' => $params['default']['text']));
			}
		}
		
		// Restaurar variables query originales
		$this->setAllQueryVars($orgQueryVars);

		return $result;
	} // getGerenciaList()
	
	
	//***************************************************************************
	// Obtener las unidades del personal activo para un control dropdown
	public function getUnidadList($params) {
		// Prepara where
		$where = array('activo' => true);
		if(isset($params['id_gerencia']) && $params['id_gerencia'] != '')
			$where['id_gerencia'] = $params['id_gerencia'];
		
		// Guardar variables query originales
		$orgQueryVars = $this->getAllQueryVars();
		
		// Obtener gerencias
		$this->sqlSelect = "id_unidad as value, unidad as text";
		$this->sqlGroupBy = "id_unidad, unidad";
		$this->sqlOrderBy = array('text' => 'ASC');
		$result = $this->getRecords($where);
		if($result !== false) {
			// Agregar elemento extra si existe
			if(isset($params['extra'])) {
				array_unshift($result, array('value' => $params['extra']['value'], 'text' => $params['extra']['text']));
			}
			// Agregar elemento default si existe
			if($this->getNumRows() == 0 && isset($params['default'])) {
				array_unshift($result, array('value' => $params['default']['value'], 'text' => $params['default']['text']));
			}
		}
		
		// Restaurar variables query originales
		$this->setAllQueryVars($orgQueryVars);

		return $result;
	} // getUnidadList()
	
	
	//***************************************************************************
	// Obtener informacion de un usuario
	// $params: array de filtro
	public function getUserInfo($params) {
		// Obtener datos del usuario
		$result = $this->getRecords($params);
		if(!empty($result)) {
			$result = $result[0]; // Solo el primer registro
		}

		return $result;
	} // getUserInfo()
}
?>
