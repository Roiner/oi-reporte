<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'TratamientoMC';

// Estructura del contenido
// Estructura del contenido
$structure['container'] = 'divTratamientos';
$structure['page_rows'] = 0; // Filas por páginas
$structure['paginator'] = PAGINATOR_NONE;
// $structure['task_enq'] = 'tsk_tratamiento_mc(mc_enq1).php';
if(in_array($_POST['task_mode'], array('mod1'))) {
	$structure['task_mod'] = 'tsk_sv_orden_rep(dlg_mod1).php';
	$structure['task_mod_container'] = 'divDialog';
	$structure['task_del'] = 'tsk_sv_orden_rep(dlg_del1).php';
	$structure['task_del_container'] = 'divDialog';
}
$structure['list_style'] = LST_SPL;

// -> Estructura del WHERE (opcional)
// echo '<pre>_POST: '; var_dump($_POST); echo '</pre>'; // DEBUG
if(empty($_POST['id_mc_solicitud'])) {
	$_POST['id_mc_solicitud'] = 0;
	$structure['where'][0]['fields'] = array('id_mc_solicitud');
	$structure['where'][0]['sql'] = "t.id_mc_solicitud IS NULL";
	$structure['where'][0]['params'] = array();
} 
else {
	$structure['where'][0]['fields'] = array('id_mc_solicitud');
	$structure['where'][0]['sql'] = "t.id_mc_solicitud = :id_mc_solicitud";
	$structure['where'][0]['params'] = array('id_mc_solicitud' => 'id_mc_solicitud');
}

// -> Estructura del contenido del gridview
$structure['gridview']['type'] = 'gridview';
$structure['gridview']['id'] = 'tsk_tratamiento_list';
$structure['gridview']['caption'] = 'Diagnóticos y Tratamiento';
// $structure['gridview']['sorted'] = true;

$structure['gridview']['header'][0][0][] = array('control' => 'literal', 'content' => 'Diagnostico');
$structure['gridview']['header'][0][1][] = array('control' => 'literal', 'content' => 'Medicamento');
$structure['gridview']['header'][0][2][] = array('control' => 'literal', 'content' => 'Concentración');
$structure['gridview']['header'][0][3][] = array('control' => 'literal', 'content' => 'Presentación');
$structure['gridview']['header'][0][4][] = array('control' => 'literal', 'content' => 'Cantidad');
$structure['gridview']['header'][0][5][] = array('control' => 'literal', 'content' => 'Frecuencia');
$structure['gridview']['header'][0][6][] = array('control' => 'literal', 'content' => 'Desde');
$structure['gridview']['header'][0][7][] = array('control' => 'literal', 'content' => 'Hasta');
$structure['gridview']['header'][0][8][] = array('control' => 'literal', 'content' => 'Status');

if(in_array($_POST['task_mode'], array('add1', 'mod1'))) {
	$structure['gridview']['footer'][0][0]['td'] = array('colspan' => 9);
	$structure['gridview']['footer'][0][0][] = array('control' => 'button', 'content' => 'Agregar',
		'id' => 'btnAddTratamiento', 'name' => 'btnAddTratamiento');
}

$structure['gridview']['items'][0][0][] = array('control' => 'hidden', 'field' => 'id_diagnostico', 'id' => 'id_diagnostico[{#}]');
$structure['gridview']['items'][0][0][] = array('control' => 'hidden', 'field' => 'id_mc_solicitud', 'id' => 'id_mc_solicitud[{#}]');
$structure['gridview']['items'][0][0][] = array('control' => 'literal', 'field' => 'diagnostico');
$structure['gridview']['items'][0][1][] = array('control' => 'hidden', 'field' => 'id_medicamento', 'id' => 'id_medicamento[{#}]');
$structure['gridview']['items'][0][1][] = array('control' => 'literal', 'field' => 'medicamento');
$structure['gridview']['items'][0][2][] = array('control' => 'literal', 'field' => 'concentracion_medi');
$structure['gridview']['items'][0][3][] = array('control' => 'literal', 'field' => 'presentacion_medi');
$structure['gridview']['items'][0][4][] = array('control' => 'literal', 'field' => 'cantidad_medi');
$structure['gridview']['items'][0][5][] = array('control' => 'literal', 'field' => 'frecuencia_medi');
$structure['gridview']['items'][0][6][] = array('control' => 'literal', 'field' => 'fecha_inicio_t');
$structure['gridview']['items'][0][7][] = array('control' => 'literal', 'field' => 'fecha_fin_t');
$structure['gridview']['items'][0][8][] = array('control' => 'checkbox', 'field' => 'activo_t', 'disabled' => 'y', 'id' => 'activo_t[{#}]');

// Javascript
$javascript['global'] = <<<'EOS'
// Eliminar Fila
function delRowDiagTrat(btn) {
	if(confirm('Confirme que desea eliminar el tratamiento')) {
		$(btn).closest('tr').remove();
	}
}

// Agregar Fila
function addRowDiagTrat(data) {
	// console.log(data);
	var nFila = $("#tsk_tratamiento_list table tbody tr").length;
	if(nFila > 0) {
		nFila = Number($("#tsk_tratamiento_list table tbody tr").filter(':last').find('input[name=rownum]').val());
		nFila++;
	}
	
	var fila = '<tr>';
	fila += '<td><input type="hidden" name="rownum" value="' + nFila + '"/><input type="hidden" name="id_diagnostico[' + nFila + ']" value="' + data.id_diagnostico + '"/>' + data.diagnostico + '</td>';
	fila += '<td><input type="hidden" name="id_medicamento[' + nFila + ']" value="' + data.id_medicamento + '"/>' + data.medicamento + '</td>';
	fila += '<td><input type="hidden" name="concentracion_medi[' + nFila + ']" value="' + data.concentracion_medi + '"/>' + data.concentracion_medi + '</td>';
	fila += '<td><input type="hidden" name="presentacion_medi[' + nFila + ']" value="' + data.presentacion_medi + '"/>' + data.presentacion_medi + '</td>';
	fila += '<td><input type="hidden" name="cantidad_medi[' + nFila + ']" value="' + data.cantidad_medi + '"/>' + data.cantidad_medi + '</td>';
	fila += '<td><input type="hidden" name="frecuencia_medi[' + nFila + ']" value="' + data.frecuencia_medi + '"/>' + data.frecuencia_medi + '</td>';
	fila += '<td><input type="hidden" name="fecha_inicio_t[' + nFila + ']" value="' + data.fecha_inicio_t + '"/>' + data.fecha_inicio_t + '</td>';
	fila += '<td><input type="hidden" name="fecha_fin_t[' + nFila + ']" value="' + data.fecha_fin_t + '"/>' + data.fecha_fin_t + '</td>';
	fila += '<td><input type="checkbox" name="activo_t[' + nFila + ']" value="' + data.activo_t + '" disabled="disabled" ' + (data.activo_t ? 'checked="checked"' : '') + '/></td>';
	fila += '<td><button type="button" onclick="delRowDiagTrat(this);" "value="Eliminar" title="Eliminar registro">Eliminar</button></td></tr>';
	
	fila = $(fila);
	fila.find('button').button({ icons: { primary: 'ui-icon ui-icon-trash' }, text: false });
	$("#tsk_tratamiento_list table tbody").append(fila);
}
EOS;

$jsAddTratamientoClick = '';
if($_POST['task_mode'] == 'add1') {
	$jsAddTratamientoClick = <<<'EOS'
		var data = { container: 'divDialog', js_func: 'addRowDiagTrat' };
		if($('#tsk_tratamiento_list table tbody tr').length > 0) {
			var tr = $("#tsk_tratamiento_list table tbody tr").filter(':last');
			data['id_diagnostico'] = tr.find('input[name^=id_diagnostico]').val();
			data['fecha_inicio_t'] = tr.find('input[name^=fecha_inicio_t]').val();
			data['fecha_fin_t'] = tr.find('input[name^=fecha_fin_t]').val();
		}
		sysfwk.loadPage('task/tsk_tratamiento_mc(dlg_jsfrm1).php', 'divDialog', data, sysfwk.POST);
EOS;
}
elseif($_POST['task_mode'] == 'mod1') {
	$jsAddTratamientoClick = <<<'EOS'
		var data = { container: 'divDialog', id_mc_solicitud: $('#id_mc_solicitud').val() };
		if($('#tsk_tratamiento_list table tbody tr').length > 0) {
			var tr = $("#tsk_tratamiento_list table tbody tr").filter(':last');
			data['id_diagnostico'] = tr.find('input[name^=id_diagnostico]').val();
			data['fecha_inicio_t'] = tr.find('input[name^=fecha_inicio_t]').val();
			data['fecha_fin_t'] = tr.find('input[name^=fecha_fin_t]').val();
		}
		sysfwk.loadPage('task/tsk_tratamiento_mc(dlg_add1).php', 'divDialog', data);
EOS;
}

$javascript['exec'] = <<<EOS
// Botones
$('#btnAddTratamiento').button({ icons: {primary:'ui-icon-circle-plus'} });
$('#btnAddTratamiento').click(function() {
	$jsAddTratamientoClick
});
EOS;

// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'list.php');
?>