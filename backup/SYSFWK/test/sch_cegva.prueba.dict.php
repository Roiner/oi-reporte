<?php
// Diccionario de Datos de la tabla prueba

// Especificaciones de los campos
$fieldspec['id_prueba'] = array(
	'label' => 'ID',
	'type' => 'integer',
	'size' => 10,
	'required' => 'y',	'autoincrement' => 'y'
);

$fieldspec['descripcion'] = array(
	'label' => 'Descripción:',
	'title' => 'esto es una prueba',
	'type' => 'string',
	'size' => 30
);

$fieldspec['id_prueba_tipo'] = array(
	'label' => 'Tipo Prueba',
	'type' => 'integer',
	'size' => 10,
	'required' => 'y',
	'optionlist' => 'tipo_prueba'
);

$fieldspec['fecha_add'] = array(
	'label' => 'Fecha Agregado',
	'type' => 'timestamp',
	'queryoutput' => "to_char(t.fecha_add, 'DD/MM/YYYY HH24:MI')",
	'autoinsert' => 'now()'
);

$fieldspec['fecha_mod'] = array(
	'label' => 'Fecha Agregado',
	'type' => 'timestamp',
	'queryoutput' => "to_char(t.fecha_mod, 'DD/MM/YYYY HH24:MI')",
	'autoupdate' => 'now()'
);

// Clave primaria
$this->_primaryKey = array('id_prueba');

// Claves Unicas
//$this->_uniqueKeys[] = array();
	
// Relaciones con tablas hijas
$this->_childRelations = array(); // Array vacío en caso de no tener tablas hijas

	
// Relaciones con tablas padres
// $this->_parentRelations = array(); // Array vacío en caso de no tener tablas padres
$this->_parentRelations[] = array(
	'schema' => 'sch_cegva',
	'parent' => 'prueba_tipo',
	'alias' => 'pt',
	'parent_fields' => array('descripcion' => 'tipo_prueba'),
	'fields' => array('id_prueba_tipo' => 'id_prueba_tipo')
);
?>
