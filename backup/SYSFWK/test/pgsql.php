<?php
header('Content-Type: text/html; charset=UTF-8');

$msg = <<< 'EOS'
***********************
* PAGE OUT OF SERVICE *
***********************
EOS;
die("<strong><pre>$msg</pre></strong>");

// Pruebas de la clase pgsql

require_once('../class/dbms.pgsql.php');

// Constantes
define('CONN_STR', 'pgsql:host=orinoconetdb;port=5432;dbname=db_oi;user=cegva_admin;password=cegva_admin');
define('DB_INST', 'pgsql');
define('DB_SCHEMA', 'sch_cegva');
define('TB_PRUEBA_T', DB_SCHEMA . '.prueba_tipo');
define('TB_PRUEBA', DB_SCHEMA . '.prueba');

// Crear objeto
$dbObj = new pgsql(CONN_STR, DB_INST, DB_SCHEMA);

// Mostrar nombre de la DB
echo 'Base de Datos: ' . $dbObj->getDbName() . '<br /><br />';

// Especificacion de prueba_tipo
$specPruebaTipo['id_prueba_tipo'] = array(
	'label' => 'ID:',
	'type' => 'integer',
	'required' => 'y'
);
$specPruebaTipo['descripcion'] = array(
	'label' => 'Descripcion',
	'type' => 'string',
	'size' => 50,
	'required' => 'y'
);

// Especificacion de prueba
$specPrueba['id_prueba'] = array(
	'label' => 'ID:',
	'type' => 'integer',
	'required' => 'y',
	'autoincrement' => 'y'
);
$specPrueba['descripcion'] = array(
	'label' => 'Descripcion',
	'type' => 'string',
	'size' => 100,
	'required' => 'y'
);
$specPrueba['id_prueba_tipo'] = array(
	'label' => 'Tipo Prueba:',
	'type' => 'integer',
	'required' => 'y'
);
$specPrueba['fecha_add'] = array(
	'label' => 'Fecha Creación',
	'type' => 'timestamp',
	'queryoutput' => "to_char(fecha_add, 'DD/MM/YYYY HH24:MI:SS')",
	'autoinsert' => "now()"
);
$specPrueba['fecha_mod'] = array(
	'label' => 'Fecha Modificación',
	'type' => 'timestamp',
	'queryoutput' => "to_char(fecha_mod, 'DD/MM/YYYY HH24:MI:SS')",
	'autoupdate' => "now()"
);


// Iniciar transaction
$result = $dbObj->beginTransaction(DB_SCHEMA);
if($result === false)
	die($dbObj->getErrorsString());
else
	echo '<pre>Iniciando Transacción</pre>';

//**** Pruebas TB_PRUEBA_T ****
$dbObj->fieldSpec = $specPruebaTipo;

// getData
// $result = $dbObj->getData(DB_SCHEMA, TB_PRUEBA_T, '');
// $result = $dbObj->getData(DB_SCHEMA, TB_PRUEBA_T, "SELECT * FROM " . TB_PRUEBA);
// $dbObj->sqlSelect = "descripcion as tipo_prueba";
// $result = $dbObj->getData(DB_SCHEMA, TB_PRUEBA_T, 'id_prueba_tipo < 5');

// Insertar simple
// $pruebaTipo = array('id_prueba_tipo' => 1, 'descripcion' => 'Tipo 1');
// $result = $dbObj->insertRecord(DB_SCHEMA, TB_PRUEBA_T, $pruebaTipo);

// Insertar multiple
// $pruebaTipo[] = array('id_prueba_tipo' => 1, 'descripcion' => 'Tipo 1');
// $pruebaTipo[] = array('id_prueba_tipo' => 2, 'descripcion' => 'Tipo 2');
// $pruebaTipo[] = array('id_prueba_tipo' => 3, 'descripcion' => 'Tipo 3');
// $pruebaTipo[] = array('id_prueba_tipo' => 4, 'descripcion' => 'Tipo 4');
// $pruebaTipo[] = array('id_prueba_tipo' => 5, 'descripcion' => 'Tipo 5');
// $result = $dbObj->insertMultiRecord(DB_SCHEMA, TB_PRUEBA_T, $pruebaTipo);

// Update simple
// $pruebaTipo = array('id_prueba_tipo' => 1, 'descripcion' => 'Prueba de Tipo 1');
// $pruebaTipoKeys = array('id_prueba_tipo' => 1);
// $result = $dbObj->updateRecord(DB_SCHEMA, TB_PRUEBA_T, $pruebaTipo, $pruebaTipoKeys);

// Update multiple
// $pruebaTipo[] = array('descripcion' => 'Prueba Tipo 2'); $pruebaTipoKeys[] = array('id_prueba_tipo' => 2);
// $pruebaTipo[] = array('descripcion' => 'Prueba Tipo 3'); $pruebaTipoKeys[] = array('id_prueba_tipo' => 3);
// $pruebaTipo[] = array('descripcion' => 'Prueba Tipo 4'); $pruebaTipoKeys[] = array('id_prueba_tipo' => 4);
// $pruebaTipo[] = array('descripcion' => 'Prueba Tipo 5'); $pruebaTipoKeys[] = array('id_prueba_tipo' => 5);
// $result = $dbObj->updateMultiRecord(DB_SCHEMA, TB_PRUEBA_T, $pruebaTipo, $pruebaTipoKeys);

// Update data
// $pruebaTipo = array('descripcion' => "Prueba Tipo");
// $where = "descripcion ilike '%replace%'";
// $result = $dbObj->updateData(DB_SCHEMA, TB_PRUEBA_T, $pruebaTipo, $where);

// Delete simple
// $pruebaTipo = array('id_prueba_tipo' => 3);
// $result = $dbObj->deleteRecord(DB_SCHEMA, TB_PRUEBA_T, $pruebaTipo);

// Delete multiple
// $pruebaTipo[] = array('id_prueba_tipo' => 4);
// $pruebaTipo[] = array('id_prueba_tipo' => 5);
// $result = $dbObj->deleteMultiRecord(DB_SCHEMA, TB_PRUEBA_T, $pruebaTipo);

// Delete data
// $where = "descripcion ilike '%prueba%'";
// $result = $dbObj->deleteData(DB_SCHEMA, TB_PRUEBA_T, $where);
//**** Pruebas TB_PRUEBA_T ****


//**** Pruebas TB_PRUEBA ****
$dbObj->fieldSpec = $specPrueba;

// getData
// $result = $dbObj->getData(DB_SCHEMA, TB_PRUEBA, '');
// $dbObj->sqlSelect = "t.id_prueba, t.descripcion as detalle_prueba, t.id_prueba_tipo, p0.descripcion as tipo_prueba, " .
	// "to_char(t.fecha_add, 'DD/MM/YYYY HH24:MI:SS') as fecha_add, to_char(t.fecha_mod, 'DD/MM/YYYY HH24:MI:SS') as fecha_mod";
// $dbObj->sqlSelect = "count(t.id_prueba) as pruebas, t.id_prueba_tipo, p0.descripcion as tipo_prueba";
// $dbObj->sqlFrom = TB_PRUEBA . ' t LEFT JOIN ' . TB_PRUEBA_T . ' p0 ON p0.id_prueba_tipo = t.id_prueba_tipo';
// $where = "t.descripcion ilike '% de %'";
// $dbObj->sqlGroupBy = 't.id_prueba_tipo, p0.descripcion';
// $dbObj->sqlHaving = 't.id_prueba_tipo in (3, 5)';
// $result = $dbObj->getData(DB_SCHEMA, TB_PRUEBA, $where);

// Insertar simple
// $prueba = array('descripcion' => 'Primera prueba', 'id_prueba_tipo' => 1);
// $result = $dbObj->insertRecord(DB_SCHEMA, TB_PRUEBA, $prueba);

// Insertar multiple
// $prueba[] = array('descripcion' => 'Tipo 1', 'id_prueba_tipo' => 1);
// $prueba[] = array('descripcion' => 'Segunda prueba', 'id_prueba_tipo' => 1);
// $prueba[] = array('descripcion' => 'Tercera prueba', 'id_prueba_tipo' => 3);
// $prueba[] = array('descripcion' => 'Cuarta prueba', 'id_prueba_tipo' => 3);
// $prueba[] = array('descripcion' => 'Otra prueba', 'id_prueba_tipo' => 5);
// $result = $dbObj->insertMultiRecord(DB_SCHEMA, TB_PRUEBA, $prueba);

// Update simple
// $prueba = array('descripcion' => 'Primera Prueba de Tipo 1');
// $pruebaKeys = array('id_prueba' => 1);
// $result = $dbObj->updateRecord(DB_SCHEMA, TB_PRUEBA, $prueba, $pruebaKeys);

// Update multiple
// $prueba[] = array('descripcion' => 'Prueba Tipo 2'); $pruebaKeys[] = array('id_prueba' => 5);
// $prueba[] = array('descripcion' => 'Prueba de Tipo 3'); $pruebaKeys[] = array('id_prueba' => 6);
// $prueba[] = array('descripcion' => 'Prueba Tipo 4'); $pruebaKeys[] = array('id_prueba' => 7);
// $prueba[] = array('descripcion' => 'Prueba de Tipo 5'); $pruebaKeys[] = array('id_prueba' => 8);
// $result = $dbObj->updateMultiRecord(DB_SCHEMA, TB_PRUEBA, $prueba, $pruebaKeys);

// Update data
// $prueba = array('descripcion' => "Prueba Tipo");
// $where = "descripcion ilike '%replace%'";
// $result = $dbObj->updateData(DB_SCHEMA, TB_PRUEBA, $prueba, $where);

// Delete simple
// $prueba = array('id_prueba_tipo' => 3);
// $result = $dbObj->deleteRecord(DB_SCHEMA, TB_PRUEBA, $prueba);

// Delete multiple
// $prueba[] = array('id_prueba_tipo' => 4);
// $prueba[] = array('id_prueba_tipo' => 5);
// $result = $dbObj->deleteMultiRecord(DB_SCHEMA, TB_PRUEBA, $prueba);

// Delete data
// $where = "descripcion ilike '%prueba%'";
// $result = $dbObj->deleteData(DB_SCHEMA, TB_PRUEBA, $where);
//**** Pruebas TB_PRUEBA ****

// Mostrar consulta ejecutada
echo "<pre>Ejecutado:\n" . $dbObj->getQuery(); echo "\nRegistros afectados: " . $dbObj->getNumRows() . '</pre>';

// Verificar resultado
if($result === false) {
	echo '<pre>Error: ' . $dbObj->getErrorsString(); echo '</pre>';
	$result = $dbObj->rollBack();
	if($result === false)
		die($dbObj->getErrorsString());
	else
		echo '<pre>Transacción cancelada (RollBack)</pre>';
}
else {
	echo '<pre>Query ejecuado correctamente: '; var_dump($result); echo '</pre>';
	$result = $dbObj->commit();
	if($result === false)
		die($dbObj->getErrorsString());
	else
		echo '<pre>Transacción Ejecutada (Commit)</pre>';
}
?>