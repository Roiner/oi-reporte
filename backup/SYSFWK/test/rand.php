<?php
header('Content-Type: text/html; charset=UTF-8');

$eventos = array('A' => 10, 'B' => 60, 'C' => 30);
$numeros = 100;

$total = array();
$prob = array();
$acum = 0;
foreach($eventos as $key => $val) {
	$acum += $val;
	$prob[$key] = $acum;
	$total[0][$key] = 0;
	$total[1][$key] = 0;
}
// var_dump($prob);

function getEvent($number) {
	global $prob, $total;
	
	$result = array();
	for($i = 0; $i < count($number); $i++) {
		foreach($prob as $key => $val) {
			if($number[$i] < $val) {
				$result[$i] = $key;
				$total[$i][$key] += 1;
				break;
			}
		}
	}
	
	return $result;
}
?>
<table>
	<thead>
		<tr>
			<th colspan="2">rand</th><th colspan="2">mt_rand</th><th colspan="2">random_int</th>
		</tr>
		<tr>
			<th>número</th><th>evento</th><th>número</th><th>evento</th><th>número</th><th>evento</th>
		</tr>
	</thead>
	<tbody>
<?php
for($i = 0; $i < $numeros; $i++) {
	$number[0] = rand(0, $numeros - 1);
	$number[1] = mt_rand(0, $numeros - 1);
	// $number[2] = random_int(0, $numeros - 1); // Solo para PHP 7
	$result = getEvent($number);
	// echo "<tr><td>{$number[0]}</td><td>{$result[0]}</td><td>{$number[1]}</td><td>{$result[1]}</td><td>{$number[2]}</td><td>{$result[2]}</td></tr>";
	echo "<tr><td>{$number[0]}</td><td>{$result[0]}</td><td>{$number[1]}</td><td>{$result[1]}</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
}
?>
	</tbody>
</table>
<pre>
<?php
var_dump($total);
?>
</pre>
