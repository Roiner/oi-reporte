<?php
header('Content-Type: text/html; charset=UTF-8');

$msg = <<<'EOS'
***********************
* PAGE OUT OF SERVICE *
***********************
EOS;
die("<strong><pre>$msg</pre></strong>");

// Pruebas de la clase DB_Table

// Constantes
define('CONN_STR', 'pgsql:host=orinoconetdb;port=5432;dbname=db_oi;user=cegva_admin;password=cegva_admin');
define('DB_INST', 'pgsql');
define('DB_SCHEMA', 'sch_cegva');

// Archivos requeridos
require_once('../class/DB_Table.php');
require_once('../test/Prueba.class.php');
require_once('../test/PruebaTipo.class.php');

// Crear objetos
$objPrueba = new Prueba();
$objPruebaTipo = new PruebaTipo();

// Mostrar datos Iniciales
// echo 'Datos de Prueba: <br />';
// echo 'DB: ' . $objPrueba->getDbName() . '<br />';
// echo 'Tabla: ' . $objPrueba->getFullTableName() . '<br />';
// echo 'RowsPerPage: ' . $objPrueba->getRowsPerPage() . '<br />';
// echo 'PrimaryKey: <pre>'; var_dump($objPrueba->getPrimaryKey()); echo '</pre><br />';
// echo 'FieldSpec: <pre>'; var_dump($objPrueba->getFieldSpec()); echo '</pre><br /><br />';
// echo 'Datos de PruebaTipo: <br />';
// echo 'DB: ' . $objPruebaTipo->getDbName() . '<br />';
// echo 'Tabla: ' . $objPruebaTipo->getFullTableName() . '<br />';
// echo 'RowsPerPage: ' . $objPruebaTipo->getRowsPerPage() . '<br />';
// echo 'PrimaryKey: <pre>'; var_dump($objPruebaTipo->getPrimaryKey()); echo '</pre><br />';
// echo 'FieldSpec: <pre>'; var_dump($objPruebaTipo->getFieldSpec()); echo '</pre><br />';

// Mostrar registro de las tablas
// echo 'Datos de la Tabla: ' . $objPrueba->getFullTableName() . '<br />';
// $dataPrueba = $objPrueba->getRecords();
// echo '<pre>dataPrueba: '; var_dump($dataPrueba); echo '</pre>';
// echo "<pre>Ejecutado:\n" . $objPrueba->getQuery(); echo "\nRegistros afectados: " . $objPrueba->getNumRows() . '</pre>';

// echo 'Datos de la Tabla: ' . $objPruebaTipo->getFullTableName() . '<br />';
// $dataPrueba = $objPruebaTipo->getRecords();
// echo '<pre>dataPrueba: '; var_dump($dataPrueba); echo '</pre>';
// echo "<pre>Ejecutado:\n" . $objPruebaTipo->getQuery(); echo "\nRegistros afectados: " . $objPruebaTipo->getNumRows() . '</pre>';



// Iniciar transaction
$result = $objPrueba->beginTransaction(DB_SCHEMA);
if($result === false)
	die($objPrueba->getErrorsString());
else
	echo '<pre>Iniciando Transacción</pre>';

//**** Pruebas TB_PRUEBA_T ****
// getData
// $result = $dbObj->getData(DB_SCHEMA, TB_PRUEBA_T, '');
// $result = $dbObj->getData(DB_SCHEMA, TB_PRUEBA_T, "SELECT * FROM " . TB_PRUEBA);
// $dbObj->sqlSelect = "descripcion as tipo_prueba";
// $result = $dbObj->getData(DB_SCHEMA, TB_PRUEBA_T, 'id_prueba_tipo < 5');

// Insertar simple
// $pruebaTipo = array('id_prueba_tipo' => 1, 'descripcion' => 'Tipo 1');
// $result = $dbObj->insertRecord(DB_SCHEMA, TB_PRUEBA_T, $pruebaTipo);

// Insertar multiple
// $pruebaTipo[] = array('id_prueba_tipo' => 1, 'descripcion' => 'Tipo 1');
// $pruebaTipo[] = array('id_prueba_tipo' => 2, 'descripcion' => 'Tipo 2');
// $pruebaTipo[] = array('id_prueba_tipo' => 3, 'descripcion' => 'Tipo 3');
// $pruebaTipo[] = array('id_prueba_tipo' => 4, 'descripcion' => 'Tipo 4');
// $pruebaTipo[] = array('id_prueba_tipo' => 5, 'descripcion' => 'Tipo 5');
// $result = $dbObj->insertMultiRecord(DB_SCHEMA, TB_PRUEBA_T, $pruebaTipo);

// Update simple
// $pruebaTipo = array('id_prueba_tipo' => 2, 'descripcion' => 'Tipo 2'); $pruebaTipoOld = array('id_prueba_tipo' => 3);
// $pruebaTipo = array('id_prueba_tipo' => 3); $pruebaTipoOld = array('id_prueba_tipo' => 5);
// $result = $objPruebaTipo->updateRecord($pruebaTipo, $pruebaTipoOld);

// Update multiple
// $pruebaTipo[] = array('id_prueba_tipo' => 1, 'descripcion' => 'Tipo 1');
// $pruebaTipo[] = array('id_prueba_tipo' => 2, 'descripcion' => 'Tipo 2');
// $pruebaTipo[] = array('id_prueba_tipo' => 3, 'descripcion' => 'Tipo 3');
// $result = $objPruebaTipo->updateMultiRecord($pruebaTipo);

// Update data
// $pruebaTipo = array('descripcion' => "Prueba Tipo X");
// $where = "descripcion ilike '%Tipo%'";
// $result = $objPruebaTipo->updateData($pruebaTipo, $where);

// Delete simple
// $pruebaTipo = array('id_prueba_tipo' => 5);
// $result = $objPruebaTipo->deleteRecord($pruebaTipo);

// Delete multiple
// $pruebaTipo[] = array('id_prueba_tipo' => 4);
// $pruebaTipo[] = array('id_prueba_tipo' => 5);
// $result = $objPruebaTipo->deleteMultiRecord($pruebaTipo);

// Delete data
// $where = "descripcion ilike '%prueba%'";
// $result = $objPruebaTipo->deleteData($where);
//**** Pruebas TB_PRUEBA_T ****


//**** Pruebas TB_PRUEBA ****
// $dbObj->fieldSpec = $specPrueba;

// getData
// $result = $dbObj->getData(DB_SCHEMA, TB_PRUEBA, '');
// $dbObj->sqlSelect = "t.id_prueba, t.descripcion as detalle_prueba, t.id_prueba_tipo, p0.descripcion as tipo_prueba, " .
	// "to_char(t.fecha_add, 'DD/MM/YYYY HH24:MI:SS') as fecha_add, to_char(t.fecha_mod, 'DD/MM/YYYY HH24:MI:SS') as fecha_mod";
// $dbObj->sqlSelect = "count(t.id_prueba) as pruebas, t.id_prueba_tipo, p0.descripcion as tipo_prueba";
// $dbObj->sqlFrom = TB_PRUEBA . ' t LEFT JOIN ' . TB_PRUEBA_T . ' p0 ON p0.id_prueba_tipo = t.id_prueba_tipo';
// $where = "t.descripcion ilike '% de %'";
// $dbObj->sqlGroupBy = 't.id_prueba_tipo, p0.descripcion';
// $dbObj->sqlHaving = 't.id_prueba_tipo in (3, 5)';
// $result = $dbObj->getData(DB_SCHEMA, TB_PRUEBA, $where);

// Insertar simple
// $prueba = array('descripcion' => 'Primera prueba', 'id_prueba_tipo' => 1);
// $result = $objPrueba->insertRecord($prueba);

// Insertar multiple
// $prueba[] = array('descripcion' => 'Tipo 1', 'id_prueba_tipo' => 1);
// $prueba[] = array('descripcion' => 'Segunda prueba', 'id_prueba_tipo' => 1);
// $prueba[] = array('descripcion' => 'Tercera prueba', 'id_prueba_tipo' => 2);
// $prueba[] = array('descripcion' => 'Cuarta prueba', 'id_prueba_tipo' => 2);
// $prueba[] = array('descripcion' => 'Otra prueba', 'id_prueba_tipo' => 3);
// $result = $objPrueba->insertMultiRecord($prueba);

// Update simple
// $prueba = array('descripcion' => 'Primera Prueba de Tipo 1'); $pruebaOld = array('id_prueba' => 9);
// $prueba = array('id_prueba' => 9, 'descripcion' => 'Primera Prueba'); $pruebaOld = array();
// $result = $objPrueba->updateRecord($prueba, $pruebaOld);

// Update multiple
// $prueba[] = array('id_prueba' => 15, 'descripcion' => 'Prueba Tipo 2');
// $prueba[] = array('id_prueba' => 16, 'descripcion' => 'Prueba de Tipo 3');
// $prueba[] = array('id_prueba' => 17, 'descripcion' => 'Prueba Tipo 4');
// $prueba[] = array('id_prueba' => 18, 'descripcion' => 'Prueba de Tipo 5');
// $result = $objPrueba->updateMultiRecord($prueba);

// Update data
// $prueba = array('descripcion' => "Prueba Tipo 2");
// $where = "id_prueba_tipo = 2";
// $result = $objPrueba->updateData($prueba, $where);

// Delete simple
// $prueba = array('id_prueba' => 7);
// $result = $objPrueba->deleteRecord($prueba);

// Delete multiple
// $prueba[] = array('id_prueba' => 1);
// $prueba[] = array('id_prueba' => 5);
// $result = $objPrueba->deleteMultiRecord($prueba);

// Delete data
// $where = "descripcion ilike '% de %'";
// $result = $objPrueba->deleteData($where);
//**** Pruebas TB_PRUEBA ****

// Mostrar consulta ejecutada
echo "<pre>Ejecutado con objPrueba:\n" . $objPrueba->getQuery(); echo "\nRegistros afectados: " . $objPrueba->getNumRows() . '</pre>';
echo "<pre>Ejecutado con objPruebaTipo:\n" . $objPruebaTipo->getQuery(); echo "\nRegistros afectados: " . $objPruebaTipo->getNumRows() . '</pre>';

// Verificar resultado
if($result === false) {
	echo '<pre>Error en objPrueba: ' . $objPrueba->getErrorsString(); echo '</pre>';
	echo '<pre>Error en objPruebaTipo: ' . $objPruebaTipo->getErrorsString(); echo '</pre>';
	$result = $objPrueba->rollBack();
	if($result === false)
		die($objPrueba->getErrorsString());
	else
		echo '<pre>Transacción cancelada (RollBack)</pre>';
}
else {
	echo '<pre>Query ejecuado correctamente: '; var_dump($result); echo '</pre>';
	$result = $objPrueba->commit();
	if($result === false)
		die($objPrueba->getErrorsString());
	else
		echo '<pre>Transacción Ejecutada (Commit)</pre>';
}
?>