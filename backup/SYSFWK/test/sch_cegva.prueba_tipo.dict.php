<?php
// Diccionario de Datos de la tabla prueba

// Especificaciones de los campos
$fieldspec['id_prueba_tipo'] = array(
	'label' => 'ID',
	'type' => 'integer',
	'size' => 10,
	'required' => 'y'
);

$fieldspec['descripcion'] = array(
	'label' => 'Descripcion',
	'type' => 'string',
	'size' => 50
);

// Clave primaria
$this->_primaryKey = array('id_prueba_tipo');

// Claves Unicas
//$this->_uniqueKeys[] = array();
	
// Relaciones con tablas hijas
// $this->_childRelations = array(); // Array vacío en caso de no tener tablas hijas
$this->_childRelations[] = array(
	'schema' => 'sch_cegva',
	'child' => 'prueba',
	'fields' => array('id_prueba_tipo' => 'id_prueba_tipo')
);

	
// Relaciones con tablas padres
$this->_parentRelations = array(); // Array vacío en caso de no tener tablas padres
?>
