<?php
header('Content-Type: text/html; charset=UTF-8');

$msg = <<<'EOS'
***********************
* PAGE OUT OF SERVICE *
***********************
EOS;
die("<strong><pre>$msg</pre></strong>");

// Pruebas de la clase DB_Table

// Constantes
define('CONN_STR', 'pgsql:host=orinoconetdb;port=5432;dbname=db_oi;user=cegva_admin;password=cegva_admin');
define('DB_INST', 'pgsql');
define('DB_SCHEMA', 'sch_cegva');

// Archivos requeridos
require_once('../lib/gen_fun_lib.php');
require_once('../class/DB_Table.php');
require_once('../test/Prueba.class.php');
require_once('../test/PruebaTipo.class.php');
require_once('../class/HtmlCtrls.php');
require_once('../class/StdView.php');

// Crear objetos
$objPrueba = new Prueba();
$objPruebaTipo = new PruebaTipo();

// Mostrar datos Iniciales
// echo 'Datos de Prueba: <br />';
// echo 'DB: ' . $objPrueba->getDbName() . '<br />';
// echo 'Tabla: ' . $objPrueba->getFullTableName() . '<br />';
// echo 'RowsPerPage: ' . $objPrueba->getRowsPerPage() . '<br />';
// echo 'PrimaryKey: <pre>'; var_dump($objPrueba->getPrimaryKey()); echo '</pre><br />';
// echo 'FieldSpec: <pre>'; var_dump($objPrueba->getFieldSpec()); echo '</pre><br /><br />';
// echo 'Datos de PruebaTipo: <br />';
// echo 'DB: ' . $objPruebaTipo->getDbName() . '<br />';
// echo 'Tabla: ' . $objPruebaTipo->getFullTableName() . '<br />';
// echo 'RowsPerPage: ' . $objPruebaTipo->getRowsPerPage() . '<br />';
// echo 'PrimaryKey: <pre>'; var_dump($objPruebaTipo->getPrimaryKey()); echo '</pre><br />';
// echo 'FieldSpec: <pre>'; var_dump($objPruebaTipo->getFieldSpec()); echo '</pre><br />';

// FieldData prueba
// $dataPrueba = array(
	// 'id_prueba' => 1,
	// 'descripcion' => 'http://orinoconet/CEGVA/css/img/fondo.jpg',
	// 'id_prueba_tipo' => 3
// );
$dataPrueba = $objPrueba->getRecords();
// echo '<pre>dataPrueba: '; var_dump($dataPrueba); echo '</pre>';

// Obtener datos extra
$objPrueba->getExtraData();

// Crear control
$objCtrl = new HtmlCtrl();
$objCtrl->fieldData = &$dataPrueba;
$objCtrl->fieldSpec = &$objPrueba->getFieldSpec();
$objCtrl->optionList = &$objPrueba->getForeignDataList();

// echo '<pre>optionList: '; var_dump($objCtrl->optionList); echo '</pre><br />';

// $lblDef = array('control' => 'label', 'field' => 'descripcion', 'for' => 'descripcion',
	// 'data-prueba' => 'valor');
// $label = $objCtrl->generateControl($lblDef);
// echo 'label: <pre>'; var_dump($label); echo '</pre><br />';
// echo $label->generateTag();

// $textDef = array('control' => 'textbox', 'field' => 'descripcion', 'pattern' => '[A-Za-z]{3}');
// $textbox = $objCtrl->generateControl($textDef);
// echo '<br />textbox: <pre>'; var_dump($textbox); echo '</pre><br />';
// echo $textbox->generateTag();

// $chkDef = array('control' => 'checkbox', 'field' => 'descripcion');
// $chk = $objCtrl->generateControl($chkDef);
// echo '<br />chk: <pre>'; var_dump($chk); echo '</pre><br />';
// echo $chk->generateTag();

// $chkDef = array('control' => 'radio', 'field' => 'descripcion');
// $chk = $objCtrl->generateControl($chkDef);
// echo '<br />chk: <pre>'; var_dump($chk); echo '</pre><br />';
// echo $chk->generateTag();

// $textAreaDef = array('control' => 'textarea', 'field' => 'descripcion', 'required' => 'y', 'name' => 'prueba');
// $textArea = $objCtrl->generateControl($textAreaDef);
// echo '<br />textArea: <pre>'; var_dump($textArea); echo '</pre><br />';
// echo $textArea->generateTag();

// $litDef = array('control' => 'literal', 'field' => 'id_prueba', 'value' => 'test');
// $lit = $objCtrl->generateControl($litDef);
// echo '<br />lit: <pre>'; var_dump($lit); echo '</pre><br />';
// echo $lit->generateTag();

// $btnDef = array('control' => 'button', 'field' => 'id_prueba', 'field_content' => 'descripcion');
// $btn = $objCtrl->generateControl($btnDef);
// echo '<br />btn: <pre>'; var_dump($btn); echo '</pre><br />';
// echo $btn->generateTag();

// $hiddenDef = array('control' => 'hidden', 'field' => 'id_prueba_tipo', 'name' => 'hola');
// $hidden = $objCtrl->generateControl($hiddenDef);
// echo '<br />hidden: <pre>'; var_dump($hidden); echo '</pre><br />';
// echo $hidden->generateTag();

// $ddlDef = array('control' => 'dropdown', 'field' => 'id_prueba_tipo', 'multiple' => 'y');
// $dropdown = $objCtrl->generateControl($ddlDef);
// echo '<br />dropdown: <pre>'; var_dump($dropdown); echo '</pre><br />';
// echo $dropdown->generateTag();

// $radiosDef = array('control' => 'radiosv', 'field' => 'id_prueba_tipo');
// $radios = $objCtrl->generateControl($radiosDef);
// echo '<br />radios: <pre>'; var_dump($radios); echo '</pre><br />';
// echo $radios->generateTag();

// $imgDef = array('control' => 'img', 'field' => 'descripcion', 'width' => '50%', 'height' => '50%', 'alt' => 'prueba');
// $img = $objCtrl->generateControl($imgDef);
// echo '<br />img: <pre>'; var_dump($img); echo '</pre><br />';
// echo $img->generateTag();

// $tbDef = array('control' => 'table');
// $table = $objCtrl->generateTable($tbDef);
// echo '<br />table: <pre>'; var_dump($table); echo '</pre><br />';
// echo $table->generateTag();

// $table = $objCtrl->generateTable();
// echo '<br />table: <pre>'; var_dump($table); echo '</pre><br />';
// echo $table->generateTag();

// $element['id'] = 'divFieldsets';
// $element['class'] = 'divFieldsets';
// $element['type'] = 'fieldsets';
// $element['fs'][0]['type'] = 'fieldset';
// $element['fs'][0]['legend'] = 'Datos del Registro:';
// $element['fs'][0]['table'] = 'y';
// $element['fs'][0]['fields'][0][0]['td'] = array('style' => 'background-color:blue; color:white');
// $element['fs'][0]['fields'][0][0][] = array('control' => 'label', 'field' => 'descripcion');
// $element['fs'][0]['fields'][0][1][] = array('control' => 'textbox', 'field' => 'descripcion');

// $element['id'] = 'divFieldsets';
// $element['class'] = 'divFieldsets';
// $element['type'] = 'fieldsets';
// $element['fs'][0]['type'] = 'div';
// $element['fs'][0]['table'] = 'n';
// $element['fs'][0]['fields'][0][] = array('control' => 'label', 'field' => 'descripcion');
// $element['fs'][0]['fields'][0][] = array('control' => 'textbox', 'field' => 'descripcion');

$element['id'] = 'divGridView';
$element['class'] = 'divGridView';
$element['type'] = 'gridview';
$element['caption'] = 'Tabla de Prueba';
$element['colgroups'][0]['colgroup'] = array('id' => 'cg_1');
$element['colgroups'][0][0] = 0;
$element['colgroups'][0][1] = array('style' => 'background-color:blue; color:white');
$element['colgroups'][0][2] = 2;
// $element['colgroups'][1]['colgroup'] = array('id' => 'cg_2');
// $element['colgroups'][1][0] = 0;
// $element['colgroups'][1][1] = array('style' => 'background-color:blue; color:white');
// $element['colgroups'][1][2] = null;
// $element['header']['thead'] = array('style' => 'color:red');
// $element['header'][0]['tr'] = array('style' => 'background-color:grey;');
$element['header'][0][0][] = array('control' => 'literal', 'content' => 'ID');
$element['header'][0][1][] = array('control' => 'literal', 'content' => 'Descripcion');
// $element['header'][0][2]['th'] = array('style' => 'font-style:italic;');
$element['header'][0][2][] = array('control' => 'literal', 'content' => 'Tipo');

// $element['footer']['tfoot'] = array('style' => 'color:red');
// $element['footer'][0]['tr'] = array('style' => 'background-color:grey;');
$element['footer'][0][0][] = array('control' => 'literal', 'content' => 'ID');
$element['footer'][0][1][] = array('control' => 'literal', 'content' => 'Descripcion');
// $element['footer'][0][2]['td'] = array('style' => 'font-style:italic;');
$element['footer'][0][2][] = array('control' => 'literal', 'content' => 'Tipo');

$element['items'][0]['condition'] = '$rowData["id_prueba_tipo"] == 2';
$element['items'][0]['tr'] = array('style' => 'background-color:yellow;');
$element['items'][0][0]['td'] = array('colspan' => 2, 'style' => 'text-align:center');
$element['items'][0][0][] = array('control' => 'literal', 'content' => 'prueba');
$element['items'][0][1][] = array('control' => 'literal', 'field' => 'id_prueba_tipo');
$element['items'][0][1][] = array('control' => 'literal', 'content' => 'hola', 'condition' => '$rowData["id_prueba_tipo"] == 2');

$element['items'][1]['condition'] = '$rowData["id_prueba_tipo"] == 3';
$element['items'][1]['tr'] = array('style' => 'font-weight:bold;');
$element['items'][1][0][] = array('control' => 'literal', 'field' => 'id_prueba');
$element['items'][1][1][] = array('control' => 'literal', 'field' => 'descripcion');
$element['items'][1][2][] = array('control' => 'literal', 'field' => 'id_prueba_tipo');

$element['items'][2][0][] = array('control' => 'textbox', 'field' => 'id_prueba', 'id' => 'id_prueba[{#}]');
$element['items'][2][1][] = array('control' => 'textbox', 'field' => 'descripcion', 'id' => 'descripcion[{#}]');
$element['items'][2][2][] = array('control' => 'textbox', 'field' => 'id_prueba_tipo', 'id' => 'id_prueba_tipo[{#}]');

$view = new StdView($element, $dataPrueba, $objPrueba->getFieldSpec(), $objPrueba->getForeignDataList());
$view->generate();
?>