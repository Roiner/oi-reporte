<?php
header('Content-Type: text/html; charset=UTF-8');
// Cargar secuencia de inicio de session general
// require_once('../main.php');

// Variables
$errorFlag = false;
$info = '';
$title = 'Prueba de Datos desde SAP';
$sapUser = 'sapintranet';
$sapPass = 'sapintranet01';
$sapCharSet = 'AL32UTF8';
$sapConStr = <<<'EOS'
(DESCRIPTION =
	(ADDRESS =
		(PROTOCOL = TCP)
		(HOST = SAPPRD)
		(PORT = 1527)
	)
	(CONNECT_DATA =
		(SID = PRD)
	)
)
EOS;

// Definir SQL y data
$datosSAP = array();
$datosSAP['personal_sap']['get'] = false;
$datosSAP['personal_sap']['datos'] = array();
$datosSAP['personal_sap']['sql'] = <<<'EOS'
SELECT * FROM ZHRV003N WHERE MANDT = '600'
EOS;

$datosSAP['unidad_sap']['get'] = false;
$datosSAP['unidad_sap']['datos'] = array();
$datosSAP['unidad_sap']['sql'] = <<<'EOS'
SELECT * FROM ZHRV013
EOS;

$datosSAP['cargo_sap']['get'] = false;
$datosSAP['cargo_sap']['datos'] = array();
$datosSAP['cargo_sap']['sql'] = <<<'EOS'
SELECT * FROM ZHRV013A
EOS;

$datosSAP['funcion_sap']['get'] = false;
$datosSAP['funcion_sap']['datos'] = array();
$datosSAP['funcion_sap']['sql'] = <<<'EOS'
SELECT * FROM ZHRV013B
EOS;

$datosSAP['estructura_sap']['get'] = true;
$datosSAP['estructura_sap']['datos'] = array();
$datosSAP['estructura_sap']['sql'] = <<<'EOS'
SELECT * FROM ZHRV014
EOS;

// Definir extructura del mensaje
$msgFormat = <<<EOS
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>$title</title>
</head>
<body>
[#BODY]
</body>
</html>
EOS;

// ==== Funciones (Inicio) ====
// Ejecuta la sentencia $sql y guarda el resultado en $dataResult
function getDataSap($sql, &$dataResult) {
	global $sapConn, $errorFlag, $info;
	
	$sapStid = oci_parse($sapConn, $sql);
	if(!$sapStid) {
		$errorFlag = true;
		$sapError = oci_error($sapConn);
		$info .= 'Error en Análisis de Sentencia SQL ' . $sapError['code'] . ': ' . $sapError['message'] . '<br />';
		$info .= '<pre>' . $sql . '</pre>';
	}
	else {
		if(!oci_execute($sapStid)) {
			$errorFlag = true;
			$sapError = oci_error($sapStid);
			$info .= 'Error en Ejecución de Sentencia SQL ' . $sapError['code'] . ': ' . $sapError['message'] . '<br />';
			$info .= "\n<pre>\n" . htmlentities($sapError['sqltext']) . sprintf("\n%".($sapError['offset']+1)."s", "^") . "\n</pre>\n";
		}
		else {
			oci_fetch_all($sapStid, $dataResult, null, null, OCI_FETCHSTATEMENT_BY_ROW);
		}
		oci_free_statement($sapStid);
	}
	unset($sapStid);
} // getDataSap()
// ==== Funciones (Fin) ====


// ==== Obterner datos de SAP (Inicio) ====
// Conectar con la DB de SAP
$sapConn = oci_connect($sapUser, $sapPass, $sapConStr, $sapCharSet);
if(!$sapConn) {
	$errorFlag = true;
	$sapError = oci_error();
	$info .= 'Error de Conexión ' . $sapError['code'] . ': ' . $sapError['message'] . '<br />';
}
else {
	foreach($datosSAP as $vista => $datosVista) {
		if($datosVista['get']) {
			getDataSap($datosVista['sql'], $datosVista['datos']);
			echo "<pre>$vista: "; var_dump($datosVista['datos']); echo '</pre>'; // DEBUG
		}
	}
	
	// Cerrar conexión
	oci_close($sapConn);
}
// ==== Obterner datos de SAP (Fin) ====

// Mostrar resultado
$resultInfo = str_replace('[#BODY]', $info, $msgFormat);
echo $resultInfo;
?>