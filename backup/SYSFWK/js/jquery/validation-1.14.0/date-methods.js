(function() {
	/**
		* Convierte una cadena en fecha:
		* str: cadena a convertir
		* format: orden en que aparecen el día, mes y año en str
		* sp: separador de fecha en str
		* Retorna un objeto Date o false en caso de fecha inválida
		*/
	function str2Date(str, format, sp) {
		var fecha = false; // Se asume error por defecto
		
		format = format.toLowerCase();
		var id = format.indexOf('d'); // indice del día
		var im = format.indexOf('m'); // indice del mes
		var iy = format.indexOf('y'); // indice del año
		var formatSp = format.split("");
		
		// Crear RegExp
		var patt = '^' + formatSp.join(sp) + '$';
		patt = patt.replace('d', '\\d{1,2}').replace('m', '\\d{1,2}').replace('y', '\\d{4}');
		var re = new RegExp(patt);
		if(re.test(str)){
			var afecha = str.split(sp);
			var dd = parseInt(afecha[id],10);
			var mm = parseInt(afecha[im],10);
			var yyyy = parseInt(afecha[iy],10);
			var xfecha = new Date(yyyy, mm-1, dd);
			if((xfecha.getFullYear() == yyyy) && ( xfecha.getMonth () == mm - 1 ) && (xfecha.getDate() == dd))
				fecha = xfecha;
		}
		return fecha;
	}
	
	/**
		* Convierte una cadena en fecha/hora:
		* str: cadena a convertir
		* format: orden en que aparecen el día, mes y año en str
		* sp: separador de fecha en str (se asume : como separador de hora)
		* Retorna un objeto Date o false en caso de fecha inválida
		*/
	function str2DateTime(str, format, sp) {
		var fecha = false; // Se asume error por defecto
		
		var aFechaHora = str.split(' '); // Separar fecha y hora
		fecha = str2Date(aFechaHora[0], format, sp); // Obtener la fecha
		if(fecha === false)
			return false;
		var aHora = aFechaHora[1].split(':'); // Separar hora
		
		// Agregar hora a la fecha
		if(aHora.length == 3)
			fecha.setHours(aHora[0], aHora[1], aHora[2]);
		else if(aHora.length == 2)
			fecha.setHours(aHora[0], aHora[1]);
		else
			fecha.setHours(aHora[0]);
			
		return fecha;
	}
	
	/**
		* Retorna true, si el valor es una fecha válida, también verifica que el formato sea dd/mm/aaaa.
		*
		* @example jQuery.validator.methods.date("01/01/1900")
		* @result true
		*
		* @example jQuery.validator.methods.date("01/13/1990")
		* @result false
		*
		* @example jQuery.validator.methods.date("01.01.1900")
		* @result false
		*
		* @example <input name="fecha" class="{dateDMY:true}" />
		* @desc Declara un elemento input opcional cuyo valor debe ser una fecha válida.
		*
		* @name jQuery.validator.methods.dateDMY
		* @type Boolean
		* @cat Plugins/Validate/Methods
		*/
	jQuery.validator.addMethod(
		"dateDMY",
		function(value, element) {
			return this.optional(element) || (str2Date(value, 'dmy', '/') !== false);
		}, 
		"El formato de la fecha debe ser DD/MM/AAAA"
	);

	/**
		* Retorna true, si el valor es una fecha válida, también verifica que el formato sea mm/dd/aaaa.
		*
		* @example jQuery.validator.methods.date("01/01/1900")
		* @result true
		*
		* @example jQuery.validator.methods.date("13/01/1990")
		* @result false
		*
		* @example jQuery.validator.methods.date("01.01.1900")
		* @result false
		*
		* @example <input name="fecha" class="{dateMDY:true}" />
		* @desc Declara un elemento input opcional cuyo valor debe ser una fecha válida.
		*
		* @name jQuery.validator.methods.dateMDY
		* @type Boolean
		* @cat Plugins/Validate/Methods
		*/
	jQuery.validator.addMethod(
		"dateMDY",
		function(value, element) {
			return this.optional(element) || (str2Date(value, 'mdy', '/') !== false);
		}, 
		"El formato de la fecha debe ser MM/DD/AAAA"
	);

	/**
		* Retorna true, si el valor es una fecha válida, también verifica que el formato sea aaaa/mm/dd.
		*
		* @example jQuery.validator.methods.date("1900/01/01")
		* @result true
		*
		* @example jQuery.validator.methods.date("1990/13/01")
		* @result false
		*
		* @example jQuery.validator.methods.date("1900.01.01")
		* @result false
		*
		* @example <input name="fecha" class="{dateYMD:true}" />
		* @desc Declara un elemento input opcional cuyo valor debe ser una fecha válida.
		*
		* @name jQuery.validator.methods.dateYMD
		* @type Boolean
		* @cat Plugins/Validate/Methods
		*/
	jQuery.validator.addMethod(
		"dateYMD",
		function(value, element) {
			return this.optional(element) || (str2Date(value, 'ymd', '/') !== false);
		}, 
		"El formato de la fecha debe ser AAAA/MM/DD"
	);
	
	jQuery.validator.addMethod(
		"dateDMYlt",
		function(value, element, param) {
			var otraFecha = ($(param).length > 0 ? $(param).val() : param);
			//return this.optional(element) || (str2Date(value, 'dmy', '/') < str2Date($(param[0]).val(), 'dmy', '/'));
			return this.optional(element) || (str2Date(value, 'dmy', '/') < str2Date(otraFecha, 'dmy', '/'));
		},
		jQuery.validator.format("La fecha deber ser menor que {0}.")
	);
	
	jQuery.validator.addMethod(
		"dateDMYlit",
		function(value, element, param) {
			var otraFecha = ($(param).length > 0 ? $(param).val() : param);
			//return this.optional(element) || (str2Date(value, 'dmy', '/') < str2Date($(param[0]).val(), 'dmy', '/'));
			return this.optional(element) || (str2Date(value, 'dmy', '/') <= str2Date(otraFecha, 'dmy', '/'));
		},
		jQuery.validator.format("La fecha deber ser menor o igual que {0}.")
	);
	
	jQuery.validator.addMethod(
		"dateDMYgt",
		function(value, element, param) {
			var otraFecha = ($(param).length > 0 ? $(param).val() : param);
			//return this.optional(element) || (str2Date(value, 'dmy', '/') > str2Date($(param[0]).val(), 'dmy', '/'));
			return this.optional(element) || (str2Date(value, 'dmy', '/') > str2Date(otraFecha, 'dmy', '/'));
		}, 
		jQuery.validator.format("La fecha deber ser mayor que {0}.")
	);
	
	jQuery.validator.addMethod(
		"dateDMYgit",
		function(value, element, param) {
			var otraFecha = ($(param).length > 0 ? $(param).val() : param);
			//return this.optional(element) || (str2Date(value, 'dmy', '/') > str2Date($(param[0]).val(), 'dmy', '/'));
			return this.optional(element) || (str2Date(value, 'dmy', '/') >= str2Date(otraFecha, 'dmy', '/'));
		}, 
		jQuery.validator.format("La fecha deber ser mayor que {0}.")
	);
	
	jQuery.validator.addMethod(
		"dateTimeDMYgt",
		function(value, element, param) {
			var otraFecha = ($(param).length > 0 ? $(param).val() : param);
			return this.optional(element) || (str2DateTime(value, 'dmy', '/') > str2DateTime(otraFecha, 'dmy', '/'));
		}, 
		jQuery.validator.format("La fecha deber ser mayor que {0}.")
	);
	
	jQuery.validator.addMethod(
		"requiredDefault",
		function(value, element, param) {
			return this.optional(element) || (value && value != param);
		}, 
		'Debe seleccionar un valor'
	);

})();
