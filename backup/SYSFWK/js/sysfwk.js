// *****************************************************************************
// SYSFWK JavaScript Library v1
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

var sysfwk = (function (window, document, $) {
	// Constantes
	// var ADD = 'ADD';
	// var MOD = 'MOD';
	// var CON = 'CON';
	// var DEL = 'DEL';
	// var SAV = 'SAV';
	var GET = 'GET';
	var POST = 'POST';
	var OK = 'OK';
	var ERROR = 'ERROR';
	var HIDDEN = 'HIDDEN';
	
	// Variables
	var urlAjaxRequestJson, // URL del manejador de peticiones JSON via Ajax
		urlLogOutPage, // URL de la página de cierre de sección
		urlLoadingImg, // URL de la imagen para mostrar en el dialogo de espera en proceso de carga
		dlgLoading, // ID del contenedor para el dialogo de espera en proceso de carga
		msgLoading; // Mensaje para mostrar en el dialogo de espera en proceso de carga
	
	var prueba = function() {
		alert('hola ' + $('#mainContent img').attr('src'));
	};
	
	//--------------------------------------------------------------------------------
	// Convierte una cadena en fecha:
	// str: cadena a convertir
	// format: orden en que aparecen el día, mes y año en str
	// sp: separador de fecha en str
	// Retorna un objeto Date o false en caso de fecha inválida
	var strToDate = function(str, format, sp) {
		var fecha = false; // Se asume error por defecto
		
		format = format.toLowerCase();
		var id = format.indexOf('d'); // indice del día
		var im = format.indexOf('m'); // indice del mes
		var iy = format.indexOf('y'); // indice del año
		var formatSp = format.split("");
		
		// Crear RegExp
		var patt = '^' + formatSp.join(sp) + '$';
		patt = patt.replace('d', '\\d{1,2}').replace('m', '\\d{1,2}').replace('y', '\\d{4}');
		var re = new RegExp(patt);
		if(re.test(str)){
			var afecha = str.split(sp);
			var dd = parseInt(afecha[id],10);
			var mm = parseInt(afecha[im],10);
			var yyyy = parseInt(afecha[iy],10);
			var xfecha = new Date(yyyy, mm-1, dd);
			if((xfecha.getFullYear() == yyyy) && (xfecha.getMonth() == mm - 1) && (xfecha.getDate() == dd))
				fecha = xfecha;
		}
		return fecha;
	}; // strToDate
	
	//--------------------------------------------------------------------------------
	// Convierte una cadena en fecha/hora:
	// str: cadena a convertir
	// format: orden en que aparecen el día, mes y año en str
	// sp: separador de fecha en str (se asume : como separador de hora)
	// Retorna un objeto Date o false en caso de fecha inválida
	var strToDateTime = function(str, format, sp) {
		var fecha = false; // Se asume error por defecto
		
		var aFechaHora = str.split(' '); // Separar fecha y hora
		fecha = strToDate(aFechaHora[0], format, sp); // Obtener la fecha
		if(fecha === false)
			return false;
		var aHora = aFechaHora[1].split(':'); // Separar hora
		
		// Agregar hora a la fecha
		if(aHora.length == 3)
			fecha.setHours(aHora[0], aHora[1], aHora[2]);
		else if(aHora.length == 2)
			fecha.setHours(aHora[0], aHora[1]);
		else
			fecha.setHours(aHora[0]);
			
		return fecha;
	}; // strToDateTime
	
	//--------------------------------------------------------------------------------
	// Definir configuracion
	var setConfig = function(data) {
		if(data.urlAjaxRequestJson !== undefined) urlAjaxRequestJson = data.urlAjaxRequestJson;
		if(data.urlLogOutPage !== undefined) urlLogOutPage = data.urlLogOutPage;
		if(data.urlLoadingImg !== undefined) urlLoadingImg = data.urlLoadingImg;
		if(data.dlgLoading !== undefined) dlgLoading = data.dlgLoading;
		if(data.msgLoading !== undefined) msgLoading = data.msgLoading;
	}; // setConfig
	
	//--------------------------------------------------------------------------------
	// Actualizar panel de mensajes
	var msgPanel = function(data) {
		var $msg = $('#' + data.id);
		var $msgInner = $('#' + data.id + '_inner');
		var $msgIcon = $('#' + data.id + '_icon');
		
		// Actualizar informacion
		if(data.title !== undefined) $('#' + data.id + '_title').html(data.title);
		if(data.info !== undefined) $('#' + data.id + '_info').html(data.info);
		if(data.extra !== undefined) $('#' + data.id + '_extra').html(data.extra);
		
		// Aplicar estilo de acuerdo al estado
		if(data.state == OK) {
			$msgInner.removeClass('ui-state-error');
			$msgInner.addClass('ui-state-highlight');
			$msgIcon.removeClass('ui-icon-alert');
			$msgIcon.addClass('ui-icon-info');
		}
		else if(data.state == ERROR) {
			$msgInner.removeClass('ui-state-highlight');
			$msgInner.addClass('ui-state-error');
			$msgIcon.removeClass('ui-icon-info');
			$msgIcon.addClass('ui-icon-alert');
		}
		
		// Ocultar o Mostrar Información
		if(data.state == HIDDEN)
			$msg.addClass('ui-helper-hidden');
		else
			$msg.removeClass('ui-helper-hidden');
	}; // msgPanel
	
	//--------------------------------------------------------------------------------
	// Obtener los datos de los controles de un contenedor
	var getControlData = function(container) {
		var data = {};
		var $container = {};
		// Obtener contenedor
		if($.type(container) === 'string')
			$container = $("#" + container);
		else if($.type(container) === 'object')
			$container = container;
		else {
			alert('Error en getControlData: container debe ser un string o un objeto jquery');
			return data;
		}
		
		// textbox, select y textarea
		$container.find('input:text, input:hidden, select, textarea').each(function(index){
			data[$(this).attr('name')] = $(this).val();
		});
		
		// radios
		$container.find('input:radio:checked').each(function(index){
			data[$(this).attr('name')] = $(this).val();
		});
		
		// checkbox
		$container.find('input:checkbox').each(function(index){
			// data[$(this).attr('name')] = (this.checked ? 't': 'f');
			data[$(this).attr('name')] = (this.checked ? true : false);
		});
		
		return data;
	}; // getControlData
	
	//--------------------------------------------------------------------------------
	// Cargar paginas con ajax
	var loadPage = function(url, container, data, method) {
		// Obtener contenedor
		var $container = $("#" + container);
		
		// Definir data como un objeto vacio si no esta definido
		if(data === undefined)
			data = {};
		
		// Si el metodo no está definido se asume GET
		if(method === undefined)
			method = GET;
			
		// Cargar la pagina con method
		var jqxhr;
		jqxhr = (method == GET ? $.get(url, data) : $.post(url, data));
		
		// Mostrar de dialogo de espera
		if(dlgLoading !== undefined) {
			showLoadingDlg();
		}
		
		// Cargar resultado on success
		jqxhr.done(function(data) {
			$container.empty().html(data);
			if(dlgLoading !== undefined) {
				hideLoadingDlg();
				$container.find(':focusable:first').focus();
			}
		});
		// En caso de error
		jqxhr.fail(function(xhr, textStatus, errorThrown) {
			$container.html("Error cargando \"" + url + "\": " + textStatus + " " + errorThrown);
			if(dlgLoading !== undefined) {
				hideLoadingDlg();
			}
		});
	}; // loadPage
	
	//--------------------------------------------------------------------------------
	// Mostrar el dialogo de espera
	var showLoadingDlg = function(img, msg, container) {
		if(container === undefined) container = dlgLoading;
		if(img === undefined) img = urlLoadingImg;
		if(msg === undefined) msg = msgLoading;
		
		// Obtener contenedor
		if(container === undefined) { alert('Falta definir "container"'); return; }
		var $container = $("#" + container);
		
		// Mostrar imagen de espera si está definida
		if(img !== undefined) {
			$container.append('<img alt="Loading..." title="Cargando..." src="' + img + '" />');
		}
		
		// Mostrar mensaje de espera si está definida
		if(msg !== undefined) {
			$container.append('<span>' + msg + '</span>');
		}
		
		// Mostrar dialogo
		$container.dialog({
			closeOnEscape: false,
			dialogClass: 'no-title',
			draggable: false,
			minHeight: 60,
			modal: true,
			resizable: false,
			close: function(event, ui){ $(this).dialog('destroy').empty(); }
		});
	}; // showLoadingDlg
	
	//--------------------------------------------------------------------------------
	// Ocultar el dialogo de espera
	var hideLoadingDlg = function(container) {
		if(container === undefined) container = dlgLoading;
		
		// Obtener contenedor
		if(container === undefined) { alert('Falta definir "container"'); return; }
		// Ocultar dialogo
		if($("#" + container).dialog('instance') !== undefined) {
			$("#" + container).dialog('close');
		}
	}; // hideLoadingDlg
	
	//--------------------------------------------------------------------------------
	// Cargar script que retorna un JSON
	var ajaxRequestJson = function(params, process) {
		if(urlAjaxRequestJson === undefined) { alert('Falta definir "urlAjaxRequestJson"'); }
		
		// Cargar el script
		var jqxhr;
		jqxhr = $.post(urlAjaxRequestJson, params, process, "json");
		
		// En caso de error
		jqxhr.fail(function(xhr, textStatus, errorThrown) {
			alert("Error cargando \"" + urlAjaxRequestJson + "\": " + textStatus + " " + errorThrown);
		});
	}; // ajaxRequestJson
	
	//--------------------------------------------------------------------------------
	// Cargar un select
	var loadSelect = function(entity, method, params, container, selValue) {
		// Preparar parametros
		var datos = {"method": method, "params": params};
		if(typeof entity == 'string')
			datos["entity"] = entity;
		else if(entity["class"] != undefined)
			datos["class"] = entity["class"];
		else if(entity["entity"] != undefined)
			datos["entity"] = entity["entity"];
		
		// Obtener datos del select
		ajaxRequestJson(datos, function(data) {
			// Obtener contenedor
			var $container = $("#" + container);
			
			// Limpiar select
			$container.empty();
			
			// Agregar opciones
			$.each(data, function(index, item) {
				$container.append('<option value="' + item.value + '">' + item.text + '</option>');
			});
			
			// Seleccionar el selValue si existe
			if(selValue !== undefined)
				$container.val(selValue);
		});
	}; // loadSelect
	
	//--------------------------------------------------------------------------------
	// Cargar el script anterior
	var scriptPrevious = function(params) {
		// Obtener datos del script anterior
		ajaxRequestJson({"class": "PageStack", "method": "previous", "params": params}, function(data) {
			// Cargar el script anterior
			if(data.container == 'window')
				location.assign(data.url);
			else
				loadPage(data.url, data.container, data.params);
		});
	}; // scriptPrevious
	
	//--------------------------------------------------------------------------------
	// Actualiza la pila con el script anterior sin cargarlo (para dialogos)
	var scriptPreviousClean = function(params) {
		// Obtener datos del script anterior
		ajaxRequestJson({"class": "PageStack", "method": "previous", "params": params}, function(data) { });
	}; // scriptPrevious
	
	//--------------------------------------------------------------------------------
	// Copiar los datos de un contenedor al portapapeles
	var copyToClipBoard = function(container) {
		// Obtener los datos del contenedor
		var data = getControlData(container);
		
		// Guardar los datos en el portapapeles
		ajaxRequestJson({"class":"ClipBoard", "method": "save", "params": data}, $.noop);
	}; // copyToClipBoard
	
	//--------------------------------------------------------------------------------
	// Pegar los datos del portapapeles
	var pageFromClipBoard = function(container) {
		// Obtener los datos del portapapeles
		ajaxRequestJson({"class": "ClipBoard", "method": "getData", "params": {}}, function(data) {
			// Obtener contenedor
			var $container = $("#" + container);
			// Pegar los datos solo en los controles editables del contenedor
			$.each(data, function(key, value) {
				var $ctrl;
				// Verificar existe controles con key como name
				$ctrl = $container.find('input:text[name="' + key + '"]').not(':hidden, :disabled, [readonly]');
				if($ctrl.length == 0) {
					$ctrl = $container.find('textarea[name="' + key + '"]').not(':hidden, :disabled, [readonly]');
					if($ctrl.length == 0) {
						$ctrl = $container.find('select[name="' + key + '"]').not(':hidden, :disabled');
						if($ctrl.length == 0) {
							$ctrl = $container.find('input:radio[name="' + key + '"]').not(':hidden, :disabled');
							if($ctrl.length == 0)
								$ctrl = $container.find('input:checkbox[name="' + key + '"]').not(':hidden, :disabled');
						}
					}
				}
				
				// Pegar el contenido
				if($ctrl.length > 0) {
					$ctrl.val(value);
				}
			});
		});
	}; // pageFromClipBoard
	
	//--------------------------------------------------------------------------------
	// Cargar archivo
	var uploadFile = function(source, task, data, successFn, progressFn) {
		var $source = {};
		// Obtener source
		if($.type(source) === 'string')
			$source = $("#" + source);
		else if($.type(source) === 'object')
			$source = source;
		else {
			alert('Error en uploadFile: source debe ser un string o un objeto jquery');
			return;
		}
		
		// data es opcional
		if(typeof data != "object") {
			progressFn = successFn;
			successFn = data;
		}
		
		// Si $source contiene un archivo
		if($source[0].files[0]) {
			var formData = new FormData();
			formData.append($source.attr("name"), $source[0].files[0]);

			// si hay datos a enviar
			if(typeof data == "object") {
				for(var i in data) {
					formData.append(i, data[i]);
				}
			}

			// Ejecutar ajax
			$.ajax({
				url: task,
				type: 'POST',
				xhr: function() {
					myXhr = $.ajaxSettings.xhr();
					if(myXhr.upload && progressFn) {
						myXhr.upload.addEventListener('progress', function(prog) {
							var value = (prog.loaded / prog.total) * 100;
							// Si progressFn en una funcción
							if(progressFn && typeof progressFn == "function") {
								progressFn(prog, value);
							}
						}, false);
					}
					return myXhr;
				},
				data: formData,
				dataType: 'json',
				cache: false,
				contentType: false,
				processData: false,
				complete: function(res) {
					var json;
					try {
						json = JSON.parse(res.responseText);
					} catch (e) {
						json = res.responseText;
					}
					if(successFn)
						successFn(json);
				}
			});
		}
	}; // uploadFile
	
	//--------------------------------------------------------------------------------
	// Cerrar session
	var logOut = function() {
		if(urlLogOutPage === undefined) { alert('Falta definir "urlLogOutPage"'); }
		if(confirm("Confirme que desea cerrar la sesión?"))
			window.location.href = urlLogOutPage;
	}; // logOut
	
	return {
		prueba: prueba,
		// ADD: ADD, MOD: MOD, CON: CON, DEL: DEL, SAV: SAV,
		GET: GET, POST: POST, OK: OK, ERROR: ERROR, HIDDEN: HIDDEN,
		strToDate: strToDate,
		strToDateTime: strToDateTime,
		setConfig: setConfig,
		msgPanel: msgPanel,
		getControlData: getControlData,
		loadPage: loadPage,
		showLoadingDlg: showLoadingDlg,
		hideLoadingDlg: hideLoadingDlg,
		ajaxRequestJson: ajaxRequestJson,
		loadSelect: loadSelect,
		scriptPrevious: scriptPrevious,
		scriptPreviousClean: scriptPreviousClean,
		copyToClipBoard: copyToClipBoard,
		pageFromClipBoard: pageFromClipBoard,
		uploadFile: uploadFile,
		logOut: logOut
	};
	
})(window, document, jQuery);