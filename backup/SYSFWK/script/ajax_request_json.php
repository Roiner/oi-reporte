<?php
// *****************************************************************************
// Script para ejecutar funciones de clases o entidades con ajax y obtener un json
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

// Variables requeridas
if(!isset($_REQUEST['class']) && !isset($_REQUEST['entity']))
	die('Error: Falta definir la clase (class) o la entidad (entity)');

isset($_REQUEST['method']) or die('Error: Falta definir el método (method) a invocar');

// Obtener clase o entidad, método y parámetros
$method = $_REQUEST['method'];
$params = $_REQUEST['params'];
if(isset($_REQUEST['entity'])) {
	$entity = $_REQUEST['entity'];
	require_once($GLOBALS['path_class'] . "$entity.class.php");
}
elseif(isset($_REQUEST['class'])) {
	$entity = $_REQUEST['class'];
	require_once($GLOBALS['path_fwk_class'] . "$entity.php");
}

// Crear objeto
$objEntity = new $entity();

// Ejecutar metodo
$rfxEntity = new ReflectionClass($entity);
$rfxMethod = $rfxEntity->getMethod($method);
$result = $rfxMethod->invoke($objEntity, $params);

// Resultado en JSON
echo json_encode($result, JSON_FORCE_OBJECT);
?>