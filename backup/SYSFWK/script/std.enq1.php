<?php
// *****************************************************************************
// Script para ver un registro de una tabla
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

// Variables requeridas
isset($structure['entity']) or die('Error: $structure["entity"] debe estar definido');
isset($structure['task_id']) or die('Error: $structure["task_id"] debe estar definido');

// Carga el task en la pila
isset($structure['page_stack']) or $structure['page_stack'] = true;

// Crear objeto entidad
require_once($GLOBALS['path_class'] . "{$structure['entity']}.class.php");
$objEntity = new $structure['entity']();

// Array de mensajes
$messages = array('id' => $structure['msg_id'], 'state' => SC_HIDDEN);

// Array de datos
$fieldData = array();


// ---- Funciones ----
// Configuración de botones inicial
function actionButtonIni() {
	global $actionButtons;
	if(isset($actionButtons[BTN_PASTE]))
		unset($actionButtons[BTN_PASTE]);
	if(isset($actionButtons[BTN_DEL]))
		unset($actionButtons[BTN_DEL]);
	if(isset($actionButtons[BTN_SAVE]))
		unset($actionButtons[BTN_SAVE]);
	if(isset($actionButtons[BTN_CANCEL]))
		unset($actionButtons[BTN_CANCEL]);
}

// Crear mensaje de error
function setErrorMessage($metodo, $info = null) {
	global $messages, $structure, $objEntity;
	
	// Preparar mensajes
	$sql = '';
	$messages['state'] = SC_ERROR;
	$messages['title'] = 'Error:';
	if(is_null($info)) {
		$messages['info'] = "Al ver registro";
		if($_SESSION[USER_ROLE][0]['is_sys_dev']) {
			$messages['info'] .= " en '{$structure['entity']}->$metodo()'";
			$sql = $objEntity->getQuery();
		}
		$messages['extra'] = '<pre>' . $objEntity->getErrorsString() . "\n" . $sql . '</pre>';
	}
	else
		$messages['info'] = $info;
}


// ---- Inicio ----
// Si la llamada es un GET
if($_SERVER['REQUEST_METHOD'] == 'GET') {
	// Actualizar page_stack
	if($structure['page_stack']) {
		PageStack::update(array('task_id' => $structure['task_id'], 'container' => $structure['container'], 'params' => $_GET));
	}
	// ob_start(); // DEBUG
	// var_dump(unserialize($_SESSION[PAGE_STACK]));
	// $salida = ob_get_clean();
	// $messages = array('id' => $structure['msg_id'], 'state' => SC_OK, 'title' => 'Información:', 'info' => "PAGE_STACK:", 'extra' => "<pre>$salida</pre>");
	
	$fieldData = array();
	// Verificar clave primaria
	$primaryKey = $objEntity->getPrimaryKey();
	foreach($primaryKey as $key) {
		if(isset($_GET[$key]))
			$fieldData[$key] = $_GET[$key];
		else {
			setErrorMessage('getPrimaryKey', "Falta el valor del campo '$key' de la clave primaria de la entidad '{$structure['entity']}'");
			foreach($actionButtons as $name => $button) {
				if($name != BTN_OK)
					unset($actionButtons[$name]);
			}
			unset($name, $button);
			return;
		}
	}
	unset($key);
	
	// Obtener datos del registro
	$fieldData = $objEntity->getRecords($fieldData);
	if($fieldData === false) {
		setErrorMessage('getRecords');
	}
	
	// Configuración de botones de acción
	actionButtonIni();
}

// Obtener datos extras
$objEntity->getExtraData(array_merge($fieldData, array(
	'task_id' => $structure['task_id'], 'pattern_id' => $structure['pattern_id'], 'pattern_type' => $structure['pattern_type']
)));
?>