<?php
// *****************************************************************************
// Script para modificar un registro de una tabla
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

// Variables requeridas
isset($structure['entity']) or die('Error: $structure["entity"] debe estar definido');
isset($structure['task_id']) or die('Error: $structure["task_id"] debe estar definido');

// Crear objeto entidad
require_once($GLOBALS['path_class'] . "{$structure['entity']}.class.php");
$objEntity = new $structure['entity']();

// Array de mensajes
$messages = array('id' => $structure['msg_id'], 'state' => SC_HIDDEN);

// Array de datos
$fieldData = array();


// ---- Funciones ----
// Configuración de botones inicial
function actionButtonIni() {
	global $actionButtons;
	if(isset($actionButtons[BTN_PASTE]) && empty($_SESSION[CLIPBOARD]))
		$actionButtons[BTN_PASTE] = array_merge($actionButtons[BTN_PASTE], array('disabled' => 'y'));
	if(isset($actionButtons[BTN_OK]))
		unset($actionButtons[BTN_OK]);
}

// Crear mensaje de error
function setErrorMessage($metodo, $info = null) {
	global $messages, $structure, $objEntity;
	
	// Preparar mensajes
	$sql = '';
	$messages['state'] = SC_ERROR;
	$messages['title'] = 'Error:';
	if(is_null($info)) {
		$messages['info'] = "Al modificar registro";
		if($_SESSION[USER_ROLE][0]['is_sys_dev']) {
			$messages['info'] .= " en '{$structure['entity']}->$metodo()'";
			$sql = $objEntity->getQuery();
		}
		$messages['extra'] = '<pre>' . $objEntity->getErrorsString() . "\n" . $sql . '</pre>';
	}
	else
		$messages['info'] = $info;
}


// ---- Inicio ----
// Si la llamada es un GET
if($_SERVER['REQUEST_METHOD'] == 'GET') {
	// Actualizar page_stack
	PageStack::update(array('task_id' => $structure['task_id'], 'container' => $structure['container'], 'params' => $_GET));
	// ob_start(); // DEBUG
	// var_dump(unserialize($_SESSION[PAGE_STACK]));
	// $salida = ob_get_clean();
	// $messages = array('id' => $structure['msg_id'], 'state' => SC_OK, 'title' => 'Información:', 'info' => "PAGE_STACK:", 'extra' => "<pre>$salida</pre>");
	
	$fieldData = array();
	// Verificar clave primaria
	$primaryKey = $objEntity->getPrimaryKey();
	foreach($primaryKey as $key) {
		if(isset($_GET[$key]))
			$fieldData[$key] = $_GET[$key];
		else {
			setErrorMessage('getPrimaryKey', "Falta el valor del campo '$key' de la clave primaria de la entidad '{$structure['entity']}'");
			foreach($actionButtons as $name => $button) {
				if($name != BTN_CANCEL)
					unset($actionButtons[$name]);
			}
			unset($name, $button);
			return;
		}
	}
	unset($key);
	
	// Obtener datos del registro
	$fieldData = $objEntity->getRecords($fieldData);
	if($fieldData === false) {
		setErrorMessage('getRecords');
	}
	
	// Configuración de botones de acción
	actionButtonIni();
}
elseif($_SERVER['REQUEST_METHOD'] == 'POST') {
	$fieldData = $_POST;
	$fieldDataOrg = $fieldData; // Guardar datos originales
	// Iniciar transacción
	$objEntity->beginTransaction();
	
	// Ejecuar update
	$fieldData = $objEntity->updateRecord($fieldData);
	
	// Cancelar transacción si hay error
	if($fieldData === false) {
		setErrorMessage('updateRecord');
		
		$objEntity->rollBack();
		// Restaurar datos y botones inciales
		$fieldData = $fieldDataOrg;
		actionButtonIni();
	}
	else {
		// Configuración de botones de acción
		foreach($actionButtons as $name => $button) {
			if($name != BTN_OK)
				unset($actionButtons[$name]);
		}
		unset($name, $button);
		
		// Commit transacción
		$objEntity->commit();
		
		// Actualizar $fieldData con la DB
		$fieldDataDB = array();
		$primaryKey = array();
		$primaryKeyList = $objEntity->getPrimaryKey();
		foreach($primaryKeyList as $key) {
			$primaryKey[$key] = $fieldData[$key];
		}
		unset($primaryKeyList, $key);
		$fieldDataDB = $objEntity->getRecords($primaryKey);
		if($fieldDataDB !== false) {
			$fieldData = $fieldDataDB;
			// Preparar mensajes
			$messages['state'] = SC_OK;
			$messages['title'] = 'Registro modificado correctamente';
			$messages['info'] = '';
			$messages['extra'] = '';
		}
		else {
			setErrorMessage('getRecords');
		}
	}
}

// Obtener datos extras
$objEntity->getExtraData(array_merge($fieldData, array(
	'task_id' => $structure['task_id'], 'pattern_id' => $structure['pattern_id'], 'pattern_type' => $structure['pattern_type']
)));
?>