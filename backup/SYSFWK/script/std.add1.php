<?php
// *****************************************************************************
// Script para agregar un registro a una tabla
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

// Variables requeridas
isset($structure['entity']) or die('Error: $structure["entity"] debe estar definido');
isset($structure['task_id']) or die('Error: $structure["task_id"] debe estar definido');

// Crear objeto entidad
require_once($GLOBALS['path_class'] . "{$structure['entity']}.class.php");
$objEntity = new $structure['entity']();

// Array de mensajes
$messages = array('id' => $structure['msg_id'], 'state' => SC_HIDDEN);
// $messages = array('id' => $structure['msg_id'], 'state' => SC_OK, 'title' => 'Información:', 'info' => 'Ingrese los datos del registro');

// Array de datos
$fieldData = array();


// ---- Funciones ----
// Configuración de botones inicial
function actionButtonIni() {
	global $actionButtons;
	if(isset($actionButtons[BTN_PASTE]) && empty($_SESSION[CLIPBOARD]))
		$actionButtons[BTN_PASTE] = array_merge($actionButtons[BTN_PASTE], array('disabled' => 'y'));
	if(isset($actionButtons[BTN_OK]))
		unset($actionButtons[BTN_OK]);
}

// Crear mensaje de error
function setErrorMessage($metodo) {
	global $messages, $structure, $objEntity;
	
	// Preparar mensajes
	$sql = '';
	$messages['state'] = SC_ERROR;
	$messages['title'] = 'Error:';
	$messages['info'] = "Al agregar registro";
	if($_SESSION[USER_ROLE][0]['is_sys_dev']) {
		$messages['info'] .= " en '{$structure['entity']}->$metodo()'";
		$sql = $objEntity->getQuery();
	}
	$messages['extra'] = '<pre>' . $objEntity->getErrorsString() . "\n" . $sql . '</pre>';
}


// ---- Inicio ----
// Si la llamada es un GET
if($_SERVER['REQUEST_METHOD'] == 'GET') {
	// Actualizar page_stack
	PageStack::update(array('task_id' => $structure['task_id'], 'container' => $structure['container'], 'params' => $_GET));
	// ob_start(); // DEBUG
	// var_dump(unserialize($_SESSION[PAGE_STACK]));
	// $salida = ob_get_clean();
	// $messages = array('id' => $structure['msg_id'], 'state' => SC_OK, 'title' => 'Información:', 'info' => "PAGE_STACK:", 'extra' => "<pre>$salida</pre>");
	
	$fieldData = $_GET;
	// Obtener datos iniciales
	$fieldData = $objEntity->getInicialData(array_merge($fieldData, array(
		'task_id' => $structure['task_id'], 'pattern_id' => $structure['pattern_id'], 'pattern_type' => $structure['pattern_type']
	)));
	if($fieldData === false) {
		setErrorMessage('getInicialData');
	}
	
	// Configuración de botones de acción
	actionButtonIni();
}
elseif($_SERVER['REQUEST_METHOD'] == 'POST') {
	$fieldData = $_POST;
	$fieldDataOrg = $fieldData; // Guardar datos originales
	// Iniciar transacción
	$objEntity->beginTransaction();
	
	// Ejecuar insert
	$fieldData = $objEntity->insertRecord($fieldData);
	
	// Cancelar transacción si hay error
	if($fieldData === false) {
		setErrorMessage('insertRecord');
		
		$objEntity->rollBack();
		// Restaurar datos y botones inciales
		$fieldData = $fieldDataOrg;
		actionButtonIni();
	}
	else {
		// Configuración de botones de acción
		foreach($actionButtons as $name => $button) {
			if($name != BTN_OK)
				unset($actionButtons[$name]);
		}
		unset($name, $button);
		
		// Commit transacción
		$objEntity->commit();
		
		// Actualizar $fieldData con la DB
		$fieldDataDB = array();
		$primaryKey = array();
		$primaryKeyList = $objEntity->getPrimaryKey();
		foreach($primaryKeyList as $key) {
			$primaryKey[$key] = $fieldData[$key];
		}
		unset($primaryKeyList, $key);
		$fieldDataDB = $objEntity->getRecords($primaryKey);
		if($fieldDataDB !== false) {
			$fieldData = $fieldDataDB;
			// Preparar mensajes
			$messages['state'] = SC_OK;
			$messages['title'] = 'Registro agregado correctamente';
			$messages['info'] = '';
			$messages['extra'] = '';
		}
		else {
			setErrorMessage('getRecords');
		}
	}
}

// Obtener datos extras
$objEntity->getExtraData(array_merge($fieldData, array(
	'task_id' => $structure['task_id'], 'pattern_id' => $structure['pattern_id'], 'pattern_type' => $structure['pattern_type']
)));
?>