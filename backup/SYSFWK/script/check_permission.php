<?php
// *****************************************************************************
// Script para verificar si el rol del usuario está en los roles permitidos del
// task
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

// El ID del task debe estar definido
isset($structure['task_id']) or die('Error: $structure["task_id"] debe estar definido');

// Verificar si los roles del usuario tiene permiso de acceso al task
if(isset($_SESSION[USER_ROLE])) {
	require_once($GLOBALS['path_fwk_class'] . "RoleTask.php");
	$objRoleTask = new RoleTask();
	$roles = $_SESSION[USER_ROLE];
	$hasAccess = $objRoleTask->checkPermission(SYS_ID, $structure['task_id'], $roles);
	if(!$hasAccess) {
		$chkErros = $objRoleTask->getErrors();
		if(!empty($chkErros)) {
			$errorInfo = ' <span>' . $objRoleTask->getErrorsString() . '</span>';
		}
		include($GLOBALS['path_fwk_script'] . 'acceso_denegado.php');
		return;
	}
	unset($objRoleTask, $roles);
}
?>