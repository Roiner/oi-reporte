<?php
header('Content-Type: text/html; charset=UTF-8');
isset($errorInfo) or $errorInfo = '';
?>
<script>
$(document).ready(function() {
	var msg = '<div class="InfoPanel" style="width:450px; margin-right: auto; margin-left: auto; margin-top: 50px;">';
	msg += '<div class="ui-corner-all ui-state-error" style="padding: 0 .7em; text-align:left">';
	msg += '<p><span class="ui-icon ui-icon-locked" style="float: left; margin-right: .3em;"></span>';
	msg += '<strong>Acceso Denegado:</strong> <span>No tiene permiso para ver esta página</span><?php echo $errorInfo; ?></p>';
	msg += '</div></div>';
	$('#mainContent').html(msg);
});
</script>
