<?php
// Verificar que el usuario est� autenticado
if(!isset($_SESSION[USER_ID]) || $_SESSION[USER_ID] == '') {
	if(isset($_GET['params'])) {
		$_SESSION[INDEX_PARAMS] = $_GET['params'];
	}
	// Redirigir al login
	header("Location: " . $GLOBALS['url_root'] . "login.php");
}
elseif(empty($_SESSION[USER_ROLE])) {
	// Definir clase de autenticacion de usuario
	if(defined('AUTHENTICATION_CLASS')) {
		$AuthenticateClass = AUTHENTICATION_CLASS;
		$pathClass = "{$GLOBALS['path_class']}$AuthenticateClass.class.php";
	}
	else {
		$AuthenticateClass = 'User';
		$pathClass = "{$GLOBALS['path_fwk_class']}$AuthenticateClass.php";
	}
	// Obtener el rol del usuario
	require_once($pathClass);
	$objUser = new $AuthenticateClass();
	$userData = $objUser->authenticate(array('id_personal' => $_SESSION[USER_ID]));
	if($userData['access_granted'] === false)
		header("Location: " . $GLOBALS['url_root'] . "login.php");
	unset($AuthenticateClass, $pathClass, $objUser, $userData);
}
?>