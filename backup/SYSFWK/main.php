<?php
// *****************************************************************************
// Secuencia inicial común de todos los script
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

// Deben estar definidas las constantes SYS_ID y SYS_SIGLAS
(defined('SYS_ID') && defined('SYS_SIGLAS')) or die('Error: SYS_ID y SYS_SIGLAS deben estár definidas');
if(!defined('SYS_DIR')) define('SYS_DIR', '');

// ---- Constantes de Sistema ----
define('SYS_COPYRIGHT', '&copy; Elaborado ' . date('Y') . ' | Sistema Desarrollado por la Gerencia de Tecnología, Telecomunicaciones y Sistemas.');
define('SYSFWK', 'SYSFWK');
define('ROLE_DEV', 'ROT'); // Role de Desarrollador
define('SC_ERROR', 'ERROR');
define('SC_HIDDEN', 'HIDDEN');
define('SC_OK', 'OK');
define('SCH_SYSCTRL', 'sch_sysctrl'); // Esquema de control
define('URL_FOTO_PERSONAL', 'http://acceso2/fotos/'); // Url con las fotos del personal
// define('SC_ADD', 'ADD');
// define('SC_MOD', 'MOD');
// define('SC_CON', 'CON');
// define('SC_DEL', 'DEL');
// define('SC_LOGIN', 'LOGIN');

// ---- Version por defecto de jquery y plugins ----
// defined('V_JQUERY') or define('V_JQUERY', '3.1.1');
defined('V_JQUERY') or define('V_JQUERY', '1.12.4');
// defined('V_JQUERY_UI') or define('V_JQUERY_UI', '1.12.1');
defined('V_JQUERY_UI') or define('V_JQUERY_UI', '1.11.4');
defined('V_TIMEPICKER') or define('V_TIMEPICKER', '1.6.3');
defined('V_VALIDATION') or define('V_VALIDATION', '1.15.0');

define('V_JQUERY_UI_1_12_0', '1.12.0');

// ---- Constantes de estilo de listas
define('LST_SPL', 'SPL'); // Lista de selección simple
define('LST_MLT', 'MLT'); // Lista de selección multiple
define('LST_NOS', 'NOS'); // Lista de sin selección

// ---- Constantes de paginador
define('PAGINATOR_TOP', 0x1); // Paginador superior
define('PAGINATOR_BOTTOM', 0x2); // Paginador inferior
define('PAGINATOR_NONE', 0); // Ningún paginador

// ---- Constantes de botones ----
define('BTN_COPY', 'COPY');
define('BTN_PASTE', 'PASTE');
define('BTN_CANCEL', 'CANCEL');
define('BTN_OK', 'OK');
define('BTN_SAVE', 'SAVE');
define('BTN_DEL', 'DEL');
define('BTN_SEARCH', 'SEARCH');
define('BTN_CLOSE', 'CLOSE');
define('BTN_ADD', 'ADD');
define('BTN_ENQ', 'ENQ');
define('BTN_MOD', 'MOD');
define('BTN_ENQ_SEL', 'ENQ_SEL');
define('BTN_MOD_SEL', 'MOD_SEL');
define('BTN_DEL_SEL', 'DEL_SEL');
define('BTN_BACK', 'BACK');

// ---- Constantes de variables de Session ----
define('USER_ID', 'user_id'); // ID del usuario actual
define('USER_REAL_ID', 'user_real_id'); // ID real del usuario actual
define('USER_FICHA', 'user_ficha'); // Ficha del usuario actual
define('USER_NAME', 'user_name'); // Nombre del usuario actual
define('USER_ROLE', SYS_SIGLAS . '_user_role'); // Rol del usuario actual
define('USER_ROLE_NAME', SYS_SIGLAS . '_user_role_name'); // Nombre del rol del usuario actual
define('USER_REAL_ROLE', SYS_SIGLAS . '_user_real_role'); // Rol real del usuario actual
define('CLIPBOARD', SYS_SIGLAS . '_clipboard'); // Portapapeles del usuario
define('PAGE_STACK', SYS_SIGLAS . '_page_stack'); // Pila de páginas del sistema
define('INDEX_PARAMS', SYS_SIGLAS . '_index_params'); // Parametros del index
define('CURRENT_TASK', SYS_SIGLAS . '_current_task'); // Datos del task que está ejecutandose actualmente

// ---- Constantes de formato de fecha ----
define('F_DMY', 'd/m/Y');
define('F_YMD', 'Y/m/d');
define('F_DMYHM', 'd/m/Y H:i');
define('F_YMDHM', 'Y/m/d H:i');
define('F_DMYHMS', 'd/m/Y H:i:s');
define('F_YMDHMS', 'Y/m/d H:i:s');

// ---- URL de salida ----
defined('URL_ON_EXIT') or define('URL_ON_EXIT', "http://{$_SERVER['HTTP_HOST']}");

// ---- Definir las rutas del framework ----
$GLOBALS['DOCUMENT_ROOT'] = $_SERVER["DOCUMENT_ROOT"] . '/';
$GLOBALS['DOCUMENT_ROOT'] = str_replace('//', '/', $GLOBALS['DOCUMENT_ROOT']);

$GLOBALS['path_fwk'] = $GLOBALS['DOCUMENT_ROOT'] . SYSFWK . '/';
$GLOBALS['path_fwk_class'] = $GLOBALS['path_fwk'] . 'class/';
$GLOBALS['path_fwk_css'] = $GLOBALS['path_fwk'] . 'css/';
$GLOBALS['path_fwk_lib'] = $GLOBALS['path_fwk'] . 'lib/';
$GLOBALS['path_fwk_pattern'] = $GLOBALS['path_fwk'] . 'pattern/';
$GLOBALS['path_fwk_script'] = $GLOBALS['path_fwk'] . 'script/';
$GLOBALS['path_fwk_task'] = $GLOBALS['path_fwk'] . 'task/';
$GLOBALS['path_fwk_view'] = $GLOBALS['path_fwk'] . 'view/';
$GLOBALS['url_fwk'] = 'http://' . $_SERVER["HTTP_HOST"] . '/' . SYSFWK . '/';
$GLOBALS['url_fwk_lib'] = $GLOBALS['url_fwk'] . "lib/";
$GLOBALS['url_fwk_css'] = $GLOBALS['url_fwk'] . "css/";
$GLOBALS['url_fwk_js'] = $GLOBALS['url_fwk'] . "js/";
$GLOBALS['url_fwk_script'] = $GLOBALS['url_fwk'] . "script/";

// ---- Definir las rutas del sistema ----
$GLOBALS['path_root'] = $GLOBALS['DOCUMENT_ROOT'] . SYS_DIR . strtoupper(SYS_SIGLAS) . '/';
$GLOBALS['path_class'] = $GLOBALS['path_root'] . "class/";
$GLOBALS['path_task'] = $GLOBALS['path_root'] . "task/";
$GLOBALS['path_css'] = $GLOBALS['path_root'] . "css/";
$GLOBALS['url_root'] = 'http://' . $_SERVER["HTTP_HOST"] . '/' . SYS_DIR . strtoupper(SYS_SIGLAS) . '/';
$GLOBALS['url_task'] = $GLOBALS['url_root'] . "task/";
$GLOBALS['url_css'] = $GLOBALS['url_root'] . "css/";
$GLOBALS['url_js'] = $GLOBALS['url_root'] . "js/";

// ---- Funciones Generales ----
require($GLOBALS['path_fwk_lib'] . 'gen_fun_lib.php');

// ---- Clases del fwk ----
require_once($GLOBALS['path_fwk_class'] . 'PageStack.php');
require_once($GLOBALS['path_fwk_class'] . 'ClipBoard.php');
require_once($GLOBALS['path_fwk_class'] . 'singleton.php');
require_once($GLOBALS['path_fwk_class'] . 'dbms.php');
require_once($GLOBALS['path_fwk_class'] . 'DB_Table.php');
// if(LOG_LOGIN || LOG_ERROR || LOG_ACCION) {
	// require_once($GLOBALS['path_class'] . 'tables/SystemLog.class.php');
// }

// ---- Iniciar/Continuar session ----
if(!isset($_SESSION)) {
	session_start();
}
?>