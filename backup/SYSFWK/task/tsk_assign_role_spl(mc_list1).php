<?php
// -> Estructura del contenido del form de búsqueda (opcional)
$structure['search']['form']['type'] = 'fieldsets';
$structure['search']['form']['fs'][0]['type'] = 'fieldset';
$structure['search']['form']['fs'][0]['legend'] = 'Filtro de la búsqueda';
$structure['search']['form']['fs'][0]['table'] = 'y';

$structure['search']['form']['fs'][0]['fields'][0][0]['td'] = array('class' => 'label');
$structure['search']['form']['fs'][0]['fields'][0][0][] = array('control' => 'label', 'field' => 'id_personal', 'for' => 'con_id_personal');
$structure['search']['form']['fs'][0]['fields'][0][1]['td'] = array('class' => 'field');
$structure['search']['form']['fs'][0]['fields'][0][1][] = array('control' => 'hidden', 'field' => 'id_personal', 'id' => 'con_id_personal');
$structure['search']['form']['fs'][0]['fields'][0][1][] = array('control' => 'textbox', 'field' => 'personal', 'id' => 'con_personal', 'disabled' => 'y');
$structure['search']['form']['fs'][0]['fields'][0][1][] = array('control' => 'button', 'id' => 'btnSelPersonal',
	'content' => '...', 'title' => 'Seleccionar Personal');
$structure['search']['form']['fs'][0]['fields'][0][1][] = array('control' => 'button', 'id' => 'btnDelSelPersonal',
	'content' => 'Limpiar', 'title' => 'Limpiar Campo');

$structure['search']['form']['fs'][0]['fields'][1][0]['td'] = array('class' => 'label');
$structure['search']['form']['fs'][0]['fields'][1][0][] = array('control' => 'label', 'field' => 'id_role', 'for' => 'con_id_role');
$structure['search']['form']['fs'][0]['fields'][1][1]['td'] = array('class' => 'field');
$structure['search']['form']['fs'][0]['fields'][1][1][] = array('control' => 'dropdown', 'field' => 'id_role', 'id' => 'con_id_role');


// -> Estructura del panel de botones (opcional)
// $structure['buttons'] = array(
	// BTN_SAVE => BTN_SAVE,
	// BTN_CANCEL => BTN_CANCEL,
	// 'MI_BOTON' => array('id' => 'btn_'.MI_BOTON, 'label' => 'Mi Boton', 'title' => 'Botón personalizado', 'icon_p' => 'comment',
		// 'javascript' => "alert('Mi boton propio')"),
	// BTN_COPY => BTN_COPY,
	// BTN_PASTE => BTN_PASTE
// );

// Javascript
$javascript['def'] = <<<'EOS'
// Actualizar personal
window.ActualizarPersonal = function ActualizarPersonal(datos) {
	$('#con_id_personal').val(datos.value);
	$('#con_personal').val(datos.text);
}
EOS;

$javascript['exec'] = <<<'EOS'
// Botones
$('#btnSelPersonal').button({ icons: {primary:'ui-icon-search'}, text: false });
$('#btnSelPersonal').click(function() {
	sysfwk.loadPage('task/tsk_personal_sel(dlg_acmp).php', 'divDialog', { container: 'divDialog', js_func: 'ActualizarPersonal'});
});
$('#btnDelSelPersonal').button({ icons: {primary:'ui-icon-close'}, text: false });
$('#btnDelSelPersonal').click(function() {
	$('#con_id_personal').val('');
	$('#con_personal').val('');
});

EOS;

// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'mc_list1.php');
?>