<?php
// Cargar configuración del screen
include($GLOBALS['path_fwk_task'] . 'scr_assign_role_spl(abm1).php');

// Incluir botón para seleccionar personal
$structure['form']['fs'][0]['fields'][0][1][] = array('control' => 'button', 'id' => 'btnSelPersonalABM', 'content' => '...',
	'title' => 'Seleccionar Personal');

// JS antes del Submit
function beforeSubmit() {
	$js = <<<EOS
	// Validar selección de personal
	if($('#id_personal').val().length == 0) {
		alert('Debe seleccionar un personal');
		$(btn).parent().find("button:contains('Guardar')").button('enable');
		return;
	}
EOS;
echo $js;
}

// Javascript
$javascript['def'] = <<<'EOS'
// Actualizar personal
window.ActualizarPersonal_abm = function ActualizarPersonal_abm(datos) {
	$('#id_personal').val(datos.value);
	$('#personal').val(datos.text);
}
EOS;

$javascript['exec'] = <<<'EOS'
// Reglas de validación
$('#id_role').rules('add', { requiredDefault: '' });
	
// Botones
$('#btnSelPersonalABM').button({ icons: {primary:'ui-icon-search'}, text: false });
$('#btnSelPersonalABM').click(function() {
	sysfwk.loadPage('task/tsk_personal_sel(dlg_acmp).php', 'divDialog1', { container: 'divDialog1', js_func: 'ActualizarPersonal_abm'});
});
EOS;

// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'dlg_add1.php');
?>