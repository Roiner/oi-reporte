<?php
// Configuración por defecto
$structure['task_del_condition'] = '$rowData["id_personal"] != ' . $_SESSION[USER_ID];
$structure['list_style'] = LST_SPL;


// -> Estructura del WHERE (opcional)
$structure['where'][0]['fields'] = array('id_personal');
$structure['where'][0]['sql'] = "t.id_personal = :id_personal";
$structure['where'][0]['params'] = array('id_personal' => 'id_personal');

$structure['where'][1]['fields'] = array('id_role');
$structure['where'][1]['sql'] = "t.id_role = :id_role";
$structure['where'][1]['params'] = array('id_role' => 'id_role');


// -> Estructura del contenido del gridview
$structure['gridview']['type'] = 'gridview';
$structure['gridview']['id'] = 'tsk_assign_role_list';
$structure['gridview']['caption'] = 'Lista de Roles x Usuarios';
$structure['gridview']['sorted'] = true;

$structure['gridview']['header'][0][0]['th'] = array('sort_field' => 'ficha', 'class' => 'personal');
$structure['gridview']['header'][0][0][] = array('control' => 'literal', 'content' => 'Ficha');
$structure['gridview']['header'][0][1]['th'] = array('sort_field' => 'personal', 'class' => 'personal');
$structure['gridview']['header'][0][1][] = array('control' => 'literal', 'content' => 'Usuario');
$structure['gridview']['header'][0][2]['th'] = array('sort_field' => 'role_name', 'class' => 'role');
$structure['gridview']['header'][0][2][] = array('control' => 'literal', 'content' => 'Role');

$structure['gridview']['footer'][0][0]['td'] = array('colspan' => 3);
$structure['gridview']['footer'][0][0][] = array('control' => 'literal', 'content' => '&nbsp;');

$structure['gridview']['items'][0][0]['td'] = array('class' => 'personal');
$structure['gridview']['items'][0][0][] = array('control' => 'literal', 'field' => 'ficha');
$structure['gridview']['items'][0][0][] = array('control' => 'hidden', 'field' => 'id_personal', 'id' => 'id_personal[{#}]');
$structure['gridview']['items'][0][0][] = array('control' => 'hidden', 'field' => 'id_sistema', 'id' => 'id_sistema[{#}]');
$structure['gridview']['items'][0][1]['td'] = array('class' => 'text_left personal');
$structure['gridview']['items'][0][1][] = array('control' => 'literal', 'field' => 'personal');
$structure['gridview']['items'][0][2]['td'] = array('class' => 'text_left role');
$structure['gridview']['items'][0][2][] = array('control' => 'hidden', 'field' => 'id_role', 'id' => 'id_role[{#}]');
$structure['gridview']['items'][0][2][] = array('control' => 'literal', 'field' => 'role_name');


// Javascript
// $javascript['exec'] = <<<'EOS'
// EOS;

// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'list.php');
?>