<?php
// *****************************************************************************
// Obtener sesion LDAP
// Autor: Elvin Calder�n
// Versi�n: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALG�N CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

session_start();

$user = strtolower(str_replace("'","\'",trim($_POST['user'])));
$password = str_replace("'","\'",trim($_POST['pass']));

$dominio = "orinoco-iron";

//Conexion con LDAP
@$ldap_con = ldap_connect("ldap://dc03") or
	die('{"ficha":"0", "info":"No se puede conectar con el servidor. El error es: ' . ldap_error($ldap_con) . '"}');
@ldap_set_option(@$ldap_con, LDAP_OPT_PROTOCOL_VERSION, 3);

//Para validar el usuario en LDAP
@$ldap_bd = ldap_bind(@$ldap_con, $user."@".$dominio.".com", $password) or
	die('{"ficha": "0", "info": "Nombre de Usuario o Contrase�a invalida"}');

//Para obtener la ficha del usuario
$filter = "(&(objectCategory=user)(userPrincipalName=".$user."@".$dominio.".com))";
// $dn = "ou=Usuarios Regulares, ou=Personas, ou=Orinoco Iron, dc=orinoco-iron, dc=com";
$dn = "ou=Personas, ou=Orinoco Iron, dc=orinoco-iron, dc=com";
$result = ldap_search(@$ldap_con,$dn,$filter) or die('{"ficha":"0", "info":"No se encuentra la ficha del usuario ' . $user . '"}');
// echo '<pre>result:'; var_dump($result); echo '</pre>'; // DEBUG
$data = ldap_get_entries($ldap_con, $result);
// echo '<pre>data:'; var_dump($data); echo '</pre>'; // DEBUG
	
$ficha_ldap = $data[0]['description'][0];

ldap_close($ldap_con);	

// Devuelvo la ficha
echo '{"ficha":"' . $ficha_ldap . '"}';
?>