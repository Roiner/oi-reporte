<?php
// *****************************************************************************
// Libreria de funciones generales
// Autor: Elvin Calder�n
// Versi�n: 2.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALG�N CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

// ****************************************************************************
// Prueba si un valor es TRUE o FALSE
if(!function_exists('is_True')) {
	function is_True($value) {
		if(is_null($value)) return false;
		
		if(is_bool($value)) return $value;
		
		// Una cadena puede tener varios valores posibles
		if(preg_match('/^(Y|YES|T|TRUE|ON|1)$/i', $value)) {
			return true;
		}
		
		// Se asume FALSE
		return false;
	} // is_True()
}


// ****************************************************************************
// Cambia el formato de una cadena de representa una fecha por otro
// En caso de error retorna false
if(!function_exists('change_date_format')) {
	function change_date_format($date, $formatIn, $formatOut) {
		$fecha = DateTime::createFromFormat($formatIn, $date);
		if($fecha === false)
			return false;
		else
			return $fecha->format($formatOut);
	} // change_date_format()
}


// ****************************************************************************
// Obtener el nombre (sin ruta ni slashes) del script actual
// Retorna false si no consigue el nombre del script
if(!function_exists('get_script_name')) {
	function get_script_name() {
		// Obtener el nombre del script actual de las variables $_SERVER
		if(!empty($_SERVER['SCRIPT_NAME'])) {
			$scriptName = $_SERVER['SCRIPT_NAME'];
		}
		elseif(!empty($_SERVER['PHP_SELF'])) {
			$scriptName = $_SERVER['PHP_SELF'];
		}
		elseif(!empty($_SERVER['SCRIPT_FILENAME'])) {
			$scriptName = $_SERVER['SCRIPT_FILENAME'];
		}
		else {
			return false;
		}
		
		// Buscar el �ltimo frontslash o backslash
		// $lastSlash = strrpos($scriptName, '/');
		// $lastBackSlash = strrpos($scriptName, "\\");
		// if($lastBackSlash > $lastSlash) {
			// $lastSlash = $lastBackSlash;
		// }
		
		// Retornar el nombre solo del script sin slashes
		// return substr($scriptName, $lastSlash + 1);
		return basename($scriptName);
	} // get_script_name()
}


// ****************************************************************************
// Retorna true si todos las claves existen en el array, false si falta alguna
if(!function_exists('array_keys_exists')) {
	function array_keys_exists(&$keys, &$array) {
		foreach($keys as $k) {
			if(!isset($array[$k]))
				return false;
		}
		return true;
	} // array_keys_exists()
}


// ****************************************************************************
// Codificar los parametros para una url en base al index
// $params: (array asociativo) con los parametros
if(!function_exists('get_index_url')) {
	function get_index_url($params) {
		// Covertir los parametros en un json
		$json = json_encode($params, JSON_FORCE_OBJECT);
		// Codificar json
		$encode = base64_encode($json);
		// Retornar url al index
		return $GLOBALS['url_root'] . 'index.php?params=' . $encode;
	} // get_index_url()
}


// ****************************************************************************
// Decodificar los parametros del index
// $params: (string) parametros codificados
if(!function_exists('params_decode')) {
	function params_decode($params) {
		// Decodificar los parametros
		$json = base64_decode($params);
		// Retornar un array asociativo de los parametros
		return json_decode($json, true);
	} // params_decode()
}
?>