<?php
// *****************************************************************************
// Crear el html del menú principal tomando como fuente un array con la definición
//	del menú. El tag raiz del menú es un <nav>
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

require_once('HtmlCtrls.php');

// Permite construir el menu principal
class StdViewMainMenu {
	// class del submenu
	public $subMenuClass;
	
	/** Array de los elementos del menu.
		* Estructura del array:
		* $Items[<n>][<propiedades>]
		* <n>: Número de Item, inicia en 0;
		* <propiedades>: Puede tener alguno de estos valores:
		*		'level': Obligatorio. Nivel del item en el menú (0 nivel superior).
		*		'label': Obligatorio. Texto que a mostrar en el enlace.
		*		'url': Opcional. Representa a href del enlace.
		*		'desc': Opcional. Descripción a colocar en el title del enlace.
		*		'submenu': Opcional. Si está presenta se asume que el item es un submenu.
		*		'script': Opcional. Script a ejecutar en el evento onclick del enlace.
		*		'target': Opcional. Atributo target del enlace (tag <a>).
		*/
	public $items;
	
	// Constructor
	function __construct($submenuclass, &$items) {
		$this->subMenuClass = $submenuclass;
		$this->items = $items;
	}
	
	// Generar Menu
	public function Generate($nivel = 0) {
		if(!is_array($this->items) || empty($this->items))
			throw new Exception("El array de items del menú no está definido o está vacío");
		
		// Crear contenedor de menu
		$mainmenu = new HtmlTag('nav', array(), false);
		
		// Pila para guardar los submenus
		$pila = array();
		
		// Recorrer los items
		for($i = 0; $i < count($this->items); $i++) {
			// Crear atributos del enlace
			$attrs = array();
			if(isset($this->items[$i]['url']))
				$attrs['href'] = $this->items[$i]['url'];
			if(isset($this->items[$i]['desc']))
				$attrs['title'] = $this->items[$i]['desc'];
			if(isset($this->items[$i]['submenu']) && $this->items[$i]['level'] > 0)
				$attrs['class'] = $this->subMenuClass;
			if(isset($this->items[$i]['script']))
				$attrs['onclick'] = $this->items[$i]['script'];
			if(isset($this->items[$i]['target']))
				$attrs['target'] = $this->items[$i]['target'];
			// Crear enlace
			$lnk = new HtmlTag('a', $attrs, false, $this->items[$i]['label']);
			
			// Crear li
			$li = new HtmlTag('li', array(), false, array());
			$li->innerHtml[] = $lnk; // Agregar lnk
			
			// Procesar elementos en la pila
			while(count($pila) > 0 && $this->items[$i]['level'] <= $pila[count($pila)-1]['level']) {
				//echo "<!-- $i ". $this->items[$i]['level'] . ' - ' . $pila[count($pila)-1]['level'] . ' '; print_r($pila); echo ' -->';
				$ant = array_pop($pila);
				if(count($pila) == 0) {
					// Pila vacía, agregar ant a mainmenu
					$ul = new HtmlTag('ul', array(), false, array());
					$ul->innerHtml[] = $ant['li'];
					$mainmenu->innerHtml[] = $ul;
				}
				else {
					// Agregar ant al padre en la pila
					$pila[count($pila)-1]['li']->innerHtml[1]->innerHtml[] = $ant['li'];
				}
			}
			
			// Procesar item actual
			if(isset($this->items[$i]['submenu'])) {
				// Agregar un <ul> a <li> y agregar <li> a la pila
				$ul = new HtmlTag('ul', array(), false, array());
				$li->innerHtml[] = $ul;
				$pila[] = array('level' => $this->items[$i]['level'], 'li' => $li);
			}
			else {
				$top = count($pila);
				if($top == 0) {
					// Pila vacía, agregar item a mainmenu
					$ul = new HtmlTag('ul', array(), false, array());
					$ul->innerHtml[] = $li;
					$mainmenu->innerHtml[] = $ul;
				}
				else {
					// Agregar item al padre en la pila
					$pila[$top-1]['li']->innerHtml[1]->innerHtml[] = $li;
					//echo '<!-- '; print_r($pila); echo ' -->';
				}
			}
		}
		
		// Procesar elementos restantes en la pila
		//echo '<!-- '; print_r($pila); echo ' -->';
		while(count($pila) > 0) {
			$item = array_pop($pila);
			if(count($pila) == 0) {
				// Pila vacía, agregar item a mainmenu
				$ul = new HtmlTag('ul', array(), false, array());
				$ul->innerHtml[] = $item['li'];
				$mainmenu->innerHtml[] = $ul; break;
			}
			else {
				// Agregar item al padre en la pila
				$pila[count($pila)-1]['li']->innerHtml[1]->innerHtml[] = $item['li'];
			}
		}
		
		// Generar menu
		echo $mainmenu->generateTag($nivel) . "\n";
	}
}
?>
