<?php
// *****************************************************************************
// Clase para el portapapeles del sistema
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

class ClipBoard {
	//***************************************************************************
	// Guardar datos en el portapapeles
	public static function save($data) {
		// Eliminar campos vacíos
		foreach($data as $k => $v) {
			if(empty($v))
				unset($data[$k]);
		}
		
		// Guardar datos
		$_SESSION[CLIPBOARD] = serialize($data);
		
		return $data;
	} // save()
	
	
	//***************************************************************************
	// Obtener datos del portapapeles
	public static function getData() {
		return (isset($_SESSION[CLIPBOARD]) ? unserialize($_SESSION[CLIPBOARD]) : array());
	} // getData()
	
} // ClipBoard
?>