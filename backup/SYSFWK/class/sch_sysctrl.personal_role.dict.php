<?php
// Diccionario de Datos de la tabla sch_sysctrl.personal_role

/** Claves para las especificaciones de los campos de las tablas:
	* autoincrement: Si el campo es de tipo autoincremental, seral o identity
	* autoinsert: Valor para agregar al campo automaticamente al insertar si no está presente
	* autoupdate: Valor para agregar al campo automaticamente al actualizar si no está presente
	* false: Valor FALSE para la DB. Ejemplos: 'f', 0
	* label: Etiqueta que tendrá el campo en las páginas. Ejemplo: 'Nombre'
	* maxvalue: Valor máximo que puede tomar el campo (para validación). Ejemplo: 500
	* minvalue: Valor mínimo que puede tomar el campo (para validación). Ejemplo: 100
	* optionlist: Nombre del array en _ForeignDataList que contiene las opciones posibles para el campo.
	* queryoutput: Expresión que se utilizará como salida en los querys. Ejemplo: "to_char(t.fecha, 'DD/MM/YYYY HH24:MI')"
	* required: Si el campo es requerido (para validación). Ejemplo: 'y'
	* size: Cantidad de caracteres del campo (para validación). Ejemplo: 10
	* title: Descripción del campo para el atributo title en la páginas. Ejemplo: 'Nombre del Usuario'
	* true: Valor TRUE para la DB. Ejemplos: 'y', 1
	* type: Tipo de dato en la DB (para validación). Ejemplo: 'integer'
	* userformat: Formato del campo para el usuario.
	* dbformat: Formato del campo para la DB.
	*		- Para datos tipo timestamp o date, userformat y dbformat deben ser un string soportado por DateTime::createFromFormat
	*		- Para datos tipo double o float, userformat y dbformat deben ser un array asosiativo con los campo:
	*			dec_sep (separador decimal), group_sep (separdor de miles) and decs (cantidad de decimales)
	*/

// Especificaciones de los campos
$fieldspec['id_personal'] = array(
	'label' => 'Personal:',
	'type' => 'integer',
	'required' => 'y'
);

$fieldspec['id_sistema'] = array(
	'label' => 'Sistema:',
	'type' => 'integer',
	'required' => 'y',
	'optionlist' => 'sistemas',
	'opall' => array('value' => '', 'text' => '(Todos)'),
	'opsel' => array('value' => '', 'text' => '(Seleccione)')
);

$fieldspec['id_role'] = array(
	'label' => 'Rol:',
	'type' => 'string',
	'required' => 'y',
	'optionlist' => 'roles',
	'opall' => array('value' => '', 'text' => '(Todos)'),
	'opsel' => array('value' => '', 'text' => '(Seleccione)')
);

$fieldspec['is_primary'] = array(
	'label' => 'Es Primario:',
	'type' => 'boolean',
	'true' => true,
	'false' => false
);

$fieldspec['ra_add_date'] = array(
	'label' => 'Creado:',
	'type' => 'timestamp',
	'class' => 'ra',
	'queryoutput' => "to_char(t.ra_add_date, 'DD/MM/YYYY HH24:MI:SS')",
	'autoinsert' => 'now()'
);

$fieldspec['ra_add_user'] = array(
	'label' => 'Creado por:',
	'type' => 'integer',
	'class' => 'ra',
	'autoinsert' => $_SESSION[USER_REAL_ID]
);

$fieldspec['ra_mod_date'] = array(
	'label' => 'Modificado:',
	'type' => 'timestamp',
	'class' => 'ra',
	'queryoutput' => "to_char(t.ra_mod_date, 'DD/MM/YYYY HH24:MI:SS')",
	'autoupdate' => 'now()'
);

$fieldspec['ra_mod_user'] = array(
	'label' => 'Modificado por:',
	'type' => 'integer',
	'class' => 'ra',
	'autoupdate' => $_SESSION[USER_REAL_ID]
);

// Clave primaria
$this->_primaryKey = array('id_personal', 'id_sistema', 'id_role');

// Claves Unicas
$this->_uniqueKeys = array();


/** Claves para las tablas hijas:
	* schema: Nombre del schema de la tabla hija
	* child: Nombre de la tabla hija
	* fields: Array (campo_local => campo_hijo) de los campos que forman la unión con la tabla hija
	*/

// Relaciones con tablas hijas
$this->_childRelations = array(); // Array vacío en caso de no tener tablas hijas


/** Claves para las tablas padres:
	* schema: Nombre del schema de la tabla padre (opcional)
	* parent: Nombre de la tabla padre
	* alias: Alias de la tabla padre (opcional)
	* join: Cadena que forma el join (opcional). Si no se especifica se asume 'LEFT JOIN' 
	* parent_fields: Array (campo {para expresiones usar parentesis} => alias), (campo) ó string de los campos de la tabla padre a incluir en las consultas (sólo si no se usa optionlist)
	* fields: Array (campo_local => campo_padre) ó string que forman la unión con la tabla padre
	* filter: Array ó string con el where para filtrar los registro que van al optionlist
	*/

// Relaciones con tablas padres
$this->_parentRelations[] = array(
	'schema' => SCH_SYSCTRL,
	'parent' => 'personal',
	'parent_fields' => array('nombre' => 'personal', 'ficha'),
	'fields' => array('id_personal' => 'id_personal')
);

$this->_parentRelations[] = array(
	'schema' => SCH_SYSCTRL,
	'parent' => 'sistema',
	'parent_fields' => array("('(' || t.id_sistema || ' ' || siglas || ') - ' || {P}nombre)" => 'sistema', 'nombre' => 'nombre', 'siglas' => 'siglas'),
	'fields' => array('id_sistema' => 'id_sistema')
);

$this->_parentRelations[] = array(
	'schema' => SCH_SYSCTRL,
	'parent' => 'role',
	'parent_fields' => array("('[' || t.id_sistema || '] ' || {P}id_role || ' - ' || role_name)" => 'role', 'role_name', 'is_sys_admin', 'is_sys_dev'),
	'fields' => array('id_role' => 'id_role', 'id_sistema' => 'id_sistema')
);

$this->_parentRelations[] = array(
	'schema' => SCH_SYSCTRL,
	'parent' => 'personal',
	'parent_fields' => array('nombre' => 'creado_por'),
	'fields' => array('ra_add_user' => 'id_personal')
);

$this->_parentRelations[] = array(
	'schema' => SCH_SYSCTRL,
	'parent' => 'personal',
	'parent_fields' => array('nombre' => 'modificado_por'),
	'fields' => array('ra_mod_user' => 'id_personal')
);
?>
