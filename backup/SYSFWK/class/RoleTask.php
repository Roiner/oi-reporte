<?php
// Representa la tabla role_task
class RoleTask extends DB_Table {

	// **************************************************************************
	// Constructor
	function __construct() {
		// Guardar el nombre del directorio del script actual
		$this->_dirName = dirname(__file__);
		
		// Orden por defecto de las consultas
		$this->defaultOrderBy = array('id_sistema' => 'ASC', 'id_role' => 'ASC', 'id_task' => 'ASC');
		
		/** Ejecutar el constructor padre. Los parámetros del constructor son:
			* $connstr: Cadena de conexión, pueder un string o un array de la forma array(<cadena de conexión>, <usuario>, <password>)
			* $table: Nombre de la tabla en DB
			* $instdbms: (Opcional) Nombre que identifica un instancia en DB, por defecto es null
			* $dbschema: (Opcional) Nombre del esquema en DB, por defecto es null
			* $dbname: (Opcional) Nombre de la DB, por defecto es null
			* LLamada: parent::__construct($connstr, $table, $instdbms = null, $dbschema = null, $dbname = null, $dirNameDict = null);
			* Ejemplo: parent::__construct(array(CONN_STR, CONN_USR, CONN_PWD), 'prueba', null, DB_SCHEMA);
			*/
		parent::__construct(CONN_STR, 'role_task', DB_INST, SCH_SYSCTRL);
	} // __construct()
	
	
	// **************************************************************************
	// Verificar si el role del usuario actual tiene permiso de acceso a un task
	// $idSys: ID del sistema
	// $idTask: ID del task
	// $userRoles: referencia al array de roles del usuario
	// Retorna true si tiene permiso o false en caso contrario
	public function checkPermission($idSys, $idTask, &$userRoles) {
		// if($idSys == 1032) { echo '<pre>userRoles: ' . $idSys . ':' . $idTask; var_dump($userRoles); echo '</pre>'; } // DEBUG
		
		// Si el role primario es administrador, tiene acceso
		if($userRoles[0]['is_primary'] === true && $userRoles[0]['is_sys_admin'] === true) {
			return true;
		}
		
		// Obtener asignación de roles al task
		$dataRoleTask = $this->getRecords(array('id_sistema' => $idSys, 'id_task' => $idTask));
		// if($idSys == 1032) { echo '<pre>dataRoleTask: '; var_dump($dataRoleTask); echo '</pre>'; } // DEBUG
		if($dataRoleTask === false) {
			return false;
		}
		elseif(empty($dataRoleTask)) {
			// Si no hay roles asignados al task se asume que es de libre acceso
			return true;
		}
		
		// Verificar los roles del task con los roles del usuario
		foreach($dataRoleTask as $roleTask) {
			foreach($userRoles as $roleUser) {
				if($roleUser['id_role'] == $roleTask['id_role']) {
					return true; // Tiene permiso
				}
			}
		}
		
		// No tiene permiso
		return false;
	} // checkPermission()
	
	
	/***************************************************************************
	// Realizar proceso personalizado antes de insertar resigtro en DB
	protected function _cm_pre_insertRecord($rowData) {
		// echo '<pre>rowData: '; var_dump($rowData); echo '</pre>'; // DEBUG
		
		// El campo id_role debe ser siempre en uppercase
		if(isset($rowData["id_role"]))
			$rowData["id_role"] = strtoupper($rowData["id_role"]);
		
		return $rowData;
	} // _cm_pre_insertRecord()*/
	
	
	/***************************************************************************
	// Realizar proceso personalizado antes de actualizar resigtro en DB
	protected function _cm_pre_updateRecord($updateData) {
		// echo '<pre>updateData: '; var_dump($updateData); echo '</pre>'; // DEBUG
		
		// El campo id_role debe ser siempre en uppercase
		if(isset($updateData["id_role"]))
			$updateData["id_role"] = strtoupper($updateData["id_role"]);
		
		return $updateData;
	} // _cm_pre_updateRecord()*/
	
	
	/* / ***************************************************************************
	// Para implementar un cambio particular en las especificaciones de una tabla
	protected function _cm_changeConfig($params) {
		// echo '<pre>$params: '; var_dump($params); echo '</pre>'; // DEBUG
		$foreignDataList = $this->_foreignDataList;
		$fieldSpec = $this->_fieldSpec;
		
		// Configuración de acuerdo al task
		switch($params['pattern_type']) {
			case 'list1':
				// Agregar opcion todos
				foreach($foreignDataList as $list => $options) {
					array_unshift($foreignDataList[$list], array('value' => '', 'text' => '(Todos)'));
				}
				break;
			case 'add1':
				// Agregar opcion seleccione
				foreach($foreignDataList as $list => $options) {
					array_unshift($foreignDataList[$list], array('value' => '', 'text' => '(Seleccione)'));
				}
				break;
			case 'mod1':
				// Marcar clave primaria como campos deshabilitados
				foreach($this->_primaryKey as $key) {
					if(isset($fieldSpec[$key]))
						$fieldSpec[$key]['disabled'] = 'y';
				}
				break;
			case 'del1': case 'enq1':
				// Marcar todos los campos como deshabilitados
				foreach($fieldSpec as $field => $spec) {
					$fieldSpec[$field]['disabled'] = 'y';
				}
				break;
		}
		
		$this->_fieldSpec = $fieldSpec;
		$this->_foreignDataList = $foreignDataList;
	} // _cm_changeConfig()
	*/
}
?>
