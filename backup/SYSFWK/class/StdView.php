<?php
// *****************************************************************************
// Define la clase StdView que permite construir controles HTML definidos en un
//	array de estructura
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

require_once('HtmlCtrls.php');

// Permite construir elementos de StdView
class StdView {
	// Estructura del elemento a crear
	public $element;
	
	// Fuente de datos, puede ser una tabla o un vector
	public $dataSource;
		
	// Array de especificación de los campos
	public $fieldSpec;
	
	// Array de con las listas de los controles tipo lista (dropdown, radios, etc)
	public $optionList;
	
	// Tipo de elemento
	public $elementType;
	
	//***************************************************************************
	// Constructor
	function __construct(&$element, &$datasource, &$fieldspec, &$optionlist) {
		$this->elementType = $element['type'];
		$this->element = $element;
		$this->dataSource = $datasource;
		$this->fieldSpec = $fieldspec;
		$this->optionList = $optionlist;
	} // __construct()
	
	
	//***************************************************************************
	// Generar elemento
	public function generate($nivel = 0) {
		switch(strtolower($this->elementType)) {
			case 'gridview':
				$this->generateGridView($nivel);
				break;
			/*case 'gridviews':
				$this->generateGridViews($nivel);
				break;*/
			case 'fieldsets':
				$this->generateFieldsets($nivel);
				break;
			default:
				throw new Exception("El tipo de elemento {$this->elementType} no está definido");
		}
	} // generate()
	
	
	//***************************************************************************
	// Generar gridview
	/** Array de estructura del GridView.
		* Elementos de la estructura gridview:
		* $element[<main_prop>]
		* <main_prop>: Puede tener alguno de estos valores:
		* 	'type': (string) tipo de elemento (siempre es 'gridview' para este caso)
		* 	'id': (string) Atributo id del div que representa el gridview.
		* 	'class': (string) Atributo class del div que representa el gridview.
		* 	'caption': (string) Contenido del caption de la tabla.
		* 	'sorted': (boolean) Indica si la tabla tiene columnas ordenables.
		* 	'colgroups': ['colgroups'][<cg_n>][<col_n>] Array de estructura del colgroup:
		* 		<cg_n>: (0..n) Número de colgroup, inicia en 0
		* 		<col_n>: Puede tener alguno de estos valores:
		* 			'colgroup': Array de atributos del tag colgroup
		* 			0..n: Número de tag col en el colgroup
		* 	'header': ['header'][<row_n>][<col_n>][<ctrl_n>] Array del bloque de encabezado de la tabla:
		* 		<row_n>: Puede tener alguno de estos valores:
		* 			'thead': Array de atributos del tag thead (sort_field para ordenar)
		* 			0..n: Número de fila, inicia en 0
		* 		<col_n>: Puede tenar alguno de estos valores:
		* 			'tr': Array de atributos del tag tr
		* 			0..n: Número de columna, inicia en 0
		* 		<ctrl_n>: Puede tenar alguno de estos valores:
		* 			'th': Array de atributos del tag th
		* 			0..n: Número de control, inicia en 0
		* 	'footer': ['footer'][<row_n>][<col_n>][<ctrl_n>] Array del bloque de pie de la tabla:
		* 		<row_n>: Puede tener alguno de estos valores:
		* 			'tfoot': Array de atributos del tag tfoot
		* 			0..n: Número de fila, inicia en 0
		* 		<col_n>: Puede tenar alguno de estos valores:
		* 			'tr': Array de atributos del tag tr
		* 			0..n: Número de columna, inicia en 0
		* 		<ctrl_n>: Puede tenar alguno de estos valores:
		* 			'td': Array de atributos del tag td
		* 			0..n: Número de control, inicia en 0
		* 	'items': ['items'][<item_n>][<col_n>][<ctrl_n>] Array del bloque de items de la tabla.
		* 		<item_n>: Puede tener alguno de estos valores:
		* 			'tbody': Array de atributos del tag tbody
		* 			0..n: Número de item (pueden haber representaciones alternativas de una fila de datos), inicia en 0
		* 		<col_n>: Puede tener alguno de estos valores:
		* 			'condition': (string) Condición a evaluar para elegir el <item_n> adecuado ($rowData representa la fila de datos actual)
		* 			'tr': Array de atributos del tag tr
		* 			0..n: Número de columna, inicia en 0
		* 		<ctrl_n>: Puede tenar alguno de estos valores:
		* 			'td': Array de atributos del tag td
		* 			0..n: Número de control, inicia en 0
		*/
	public function generateGridView($nivel = 0) {
		// Constructor de controles
		$htmlCtrl = new HtmlCtrl();
		$htmlCtrl->fieldData = $this->dataSource;
		$htmlCtrl->fieldSpec = $this->fieldSpec;
		$htmlCtrl->optionList = $this->optionList;
		
		// Crear contenedor de gridview
		$attr = array();
		if(isset($this->element['id'])) $attr['id'] = $this->element['id'];
		if(isset($this->element['class'])) $attr['class'] = $this->element['class'];
		$gridview = new HtmlTag('div', $attr, false);
		
		// Crear tabla del gridview
		$table = $htmlCtrl->generateTable();
		
		// Crear caption de la tabla
		if(isset($this->element['caption']))
			$table->innerHtml[] = new HtmlTag('caption', array(), false, $this->element['caption']);
			
		// ---- colgroups (inicio) ----
		if(isset($this->element['colgroups'])) {
			// Recorrer colgroups
			$cgs = count($this->element['colgroups']);
			for($cg = 0; $cg < $cgs; $cg++) {
				// Crear colgroup
				$cg_attr = array();
				if(isset($this->element['colgroups'][$cg]['colgroup']))
					$cg_attr = $this->element['colgroups'][$cg]['colgroup'];
				$colgroup = new HtmlTag('colgroup', $cg_attr, false);
				
				// Crear col
				foreach($this->element['colgroups'][$cg] as $kCol => $vCol) {
					if(is_numeric($kCol)) {
						$colgroup->innerHtml[] = new HtmlTag('col', (is_array($vCol) ? $vCol : array()), true);
					}
				}
				
				// Agregar colgroup a la tabla
				$table->innerHtml[] = $colgroup;
			}
		}
		// ---- colgroups (fin) ----
		
		// ---- thead (inicio) ----
		if(isset($this->element['header'])) {
			// Crear encabezado de la tabla
			$thead_attr = array();
			if(isset($this->element['header']['thead']))
				$thead_attr = $this->element['header']['thead'];
			$thead = new HtmlTag('thead', $thead_attr, false);
			
			// Agregar el datasource
			if(isset($this->dataSource[0]))
				$htmlCtrl->fieldData = $this->dataSource[0];
			
			// Crear filas
			foreach($this->element['header'] as $kTr => $vTr) {
				if(is_numeric($kTr)) {
					// Crear fila
					$tr_attr = array();
					if(isset($vTr['tr']))
						$tr_attr = $vTr['tr'];
					$tr = $htmlCtrl->generateTr($tr_attr);
					
					// Crear columnas
					foreach($vTr as $kTh => $vTh) {
						if(is_numeric($kTh)) {
							// Crear columna
							$th_attr = array();
							if(isset($vTh['th'])) {
								$th_attr = $vTh['th'];
								if(is_True($this->element['sorted']) && isset($vTh['th']['sort_field'])) {
									$th_attr['data-sort_field'] = $th_attr['sort_field'];
									$th_attr['class'] = (isset($th_attr['class']) ? $th_attr['class'] . ' sorted' : 'sorted');
								}
							}
							$th = $htmlCtrl->generateTh($th_attr);
							
							// Crear controles
							foreach($vTh as $kCtrl => $vCtrl) {
								if(is_numeric($kCtrl)) {
									$th->innerHtml[] = $htmlCtrl->generateControl($vCtrl);
								}
							}
							
							// Agregar th al tr
							$tr->innerHtml[] = $th;
						}
					}
					
					// Agregar fila al thead
					$thead->innerHtml[] = $tr;
				}
			}
			
			// Agregar thead a la tabla
			$table->innerHtml[] = $thead;
		}
		// ---- thead (fin) ----
		
		// ---- tfoot (inicio) ----
		if(isset($this->element['footer'])) {
			// Crear pie de la tabla
			$tfoot_attr = array();
			if(isset($this->element['footer']['tfoot']))
				$tfoot_attr = $this->element['footer']['tfoot'];
			$tfoot = new HtmlTag('tfoot', $tfoot_attr, false);
			
			// Crear filas
			foreach($this->element['footer'] as $kTr => $vTr) {
				if(is_numeric($kTr)) {
					// Crear fila
					$tr_attr = array();
					if(isset($vTr['tr']))
						$tr_attr = $vTr['tr'];
					$tr = $htmlCtrl->generateTr($tr_attr);
					
					// Crear columnas
					foreach($vTr as $kTd => $vTd) {
						if(is_numeric($kTd)) {
							// Crear columna
							$td_attr = array();
							if(isset($vTd['td']))
								$td_attr = $vTd['td'];
							$td = $htmlCtrl->generateTd($td_attr);
							
							// Crear controles
							foreach($vTd as $kCtrl => $vCtrl) {
								if(is_numeric($kCtrl)) {
									$td->innerHtml[] = $htmlCtrl->generateControl($vCtrl);
								}
							}
							
							// Agregar td al tr
							$tr->innerHtml[] = $td;
						}
					}
					
					// Agregar fila al tfoot
					$tfoot->innerHtml[] = $tr;
				}
			}
			
			// Agregar tfoot a la tabla
			$table->innerHtml[] = $tfoot;
		}
		// ---- tfoot (fin) ----
		
		// ---- tbody (inicio) ----
		if(isset($this->element['items'])) {
			// Crear tbody de la tabla
			$tbody_attr = array();
			if(isset($this->element['items']['tbody']))
				$tbody_attr = $this->element['items']['tbody'];
			$tbody = new HtmlTag('tbody', $tbody_attr, false);
			
			// Recorrer filas del DataSource
			foreach($this->dataSource as $row => $rowData) {
				// Actualizar el DataSource del generador de controles
				$htmlCtrl->fieldData = $rowData;
				
				// Determinar el item a utilizar de acuerdo a la condición
				foreach($this->element['items'] as $kItem => $vItem) {
					if(is_numeric($kItem) && (!isset($vItem['condition']) || eval('return ' . $vItem['condition'] . ';'))) {
						// Crear fila
						$tr_attr = array();
						if(isset($vItem['tr']))
							$tr_attr = $vItem['tr'];
						$tr = $htmlCtrl->generateTr($tr_attr);
						
						// Crear columnas
						foreach($vItem as $kTd => $vTd) {
							if(is_numeric($kTd)) {
								// Crear columna
								$td_attr = array();
								if(isset($vTd['td']))
									$td_attr = $vTd['td'];
								$td = $htmlCtrl->generateTd($td_attr);
								
								// Crear controles
								foreach($vTd as $kCtrl => $vCtrl) {
									if(is_numeric($kCtrl) && (!isset($vCtrl['condition']) || eval('return ' . $vCtrl['condition'] . ';'))) {
										$vCtrl = str_replace('{#}', $row, $vCtrl);
										$td->innerHtml[] = $htmlCtrl->generateControl($vCtrl);
									}
								}
								
								// Agregar td al tr
								$tr->innerHtml[] = $td;
							}
						}
						
						// Agregar fila al tbody
						$tbody->innerHtml[] = $tr;
						break;
					}
				}
			}
			
			// Agregar tbody a la tabla
			$table->innerHtml[] = $tbody;
		}
		// ---- tbody (fin) ----
		
		// Agregar tabla al gridview
		$gridview->innerHtml[] = $table;
		
		// Generar gridview
		echo $gridview->generateTag($nivel) . "\n";
	} // generateGridView()
	
	
	//***************************************************************************
	// Generar fieldsets
	/** Array de estructura de los fieldset.
		* Elementos de la estructura fieldset:
		* $element[<main_prop>]{[<fs_n>][<fs_prop>][<row>]{[<col>]}[<ctrl_n>|'td']}
		* <main_prop>: Puede tener alguno de estos valores:
		* 	'type': (string) tipo de elemento (siempre es 'fieldsets' para este caso)
		* 	'id': (string) ID del contenedor de todos los fieldsets
		* 	'class': (string) class css del contenedor de todos los fieldsets
		* 	'fs': (array) detalle de los fieldset
		* <fs_n>: Número de fieldset, inicia en 0.
		* <fs_prop>: Puede tener alguno de estos valores:
		*		'type': (string) Determina el tipo de fieldset, pueder ser 'div' o 'fieldset'.
		*		'attr': (array) Atributos del 'div' o 'fieldset'.
		*		'br': (default y) Valor booleano (y|n;yes|no;true|false;1|0) que determina si se utiliza un br como separador o no.
		*		'legend': (string) Texto del control legend, sólo para 'type' => 'fieldset
		*		'table': Valor booleano (y|n;yes|no;true|false;1|0) que determina si se maqueta con tabla o no.
		*		'fields': Array de controles distribuidos en fila, columna y control
		* <row>: Número de fila, inicia en 0.
		* <col>: Número de columna, inicia en 0, sólo para 'table' => 'y'.
		* <ctrl_n>: Número de control (puede haber varios controles en una columna)
		* 	'td': Array de atributos para el td
		* Un Control se representa con un array que puede tener las siguientes claves:
		* 	'control': Tipo de control (label, textbox, checkbox, radios, dropdown, etc).
		* 	'field': Nombre del campo en el dataSource.
		* Ejemplo 1:
		* $element['id'] = 'divFieldsets';
		* $element['class'] = 'divFieldsets';
		* $element['type'] = 'fieldsets';
		* $element['fs'][0]['type'] = 'fieldset';
		* $element['fs'][0]['legend'] = 'Datos del Registro:';
		* $element['fs'][0]['table'] = 'y';
		* $element['fs'][0]['fields'][0][0][] = array('control' => 'label', 'field' => 'group_id');
		* $element['fs'][0]['fields'][0][1][] = array('control' => 'textbox', 'field' => 'group_id');
		*
		* Ejemplo 2:
		* $element['id'] = 'divFieldsets';
		* $element['class'] = 'divFieldsets';
		* $element['type'] = 'fieldsets';
		* $element['fs'][0]['type'] = 'div';
		* $element['fs'][0]['table'] = 'n';
		* $element['fs'][0]['fields'][0][] = array('control' => 'label', 'field' => 'group_id');
		* $element['fs'][0]['fields'][0][] = array('control' => 'textbox', 'field' => 'group_id');
		*/
	public function generateFieldsets($nivel = 0) {
		// Constructor de controles
		if(is_numeric(key($this->dataSource)))
			$this->dataSource = $this->dataSource[0];
		$htmlCtrl = new HtmlCtrl();
		$htmlCtrl->fieldData = $this->dataSource;
		$htmlCtrl->fieldSpec = $this->fieldSpec;
		$htmlCtrl->optionList = $this->optionList;
		
		// Crear contenedor de fieldsets
		$divAttr = array();
		if(isset($this->element['id'])) $divAttr['id'] = $this->element['id'];
		if(isset($this->element['class'])) $divAttr['class'] = $this->element['class'];
		$fieldsets = new HtmlTag('div', $divAttr, false, array());
		
		// Pueden haber varios fieldsets
		if(!isset($this->element['fs']) || !is_array($this->element['fs']))
			throw new Exception("'fs' no está definido o no es un array.");
		for($i = 0; $i < count($this->element['fs']); $i++) {
			// Preparar atributos del fieldset o div
			$attr = array();
			if(isset($this->element['fs'][$i]['attr']) && is_array($this->element['fs'][$i]['attr'])) {
				$attr = $this->element['fs'][$i]['attr'];
			}
			
			// Crear fieldset o div
			if($this->element['fs'][$i]['type'] == 'fieldset') {
				$fieldset = new HtmlTag('fieldset', $attr, false);
				$fieldset->innerHtml[] = new HtmlTag('legend', array(), false, $this->element['fs'][$i]['legend']);
			}
			else { //$this->element['fs'][$i]['type'] == 'div'
				$fieldset = new HtmlTag('div', $attr, false);
			}
			
			// Agregar campos
			if(is_True($this->element['fs'][$i]['table'])) {
				// Con tablas, los controles se distribuyen en filas (row) y columnas (col), una columna puede tener varios controles
				$table = $htmlCtrl->generateTable();
				
				// Recorrer filas
				$rows = count($this->element['fs'][$i]['fields']);
				for($row = 0; $row < $rows; $row++) {
					// Crear fila
					$tr = $htmlCtrl->generateTr(/*array('class' => "row_$row")*/);
					
					// Recorrer columnas
					$cols = count($this->element['fs'][$i]['fields'][$row]);
					for($col = 0; $col < $cols; $col++) {
						// Crear td
						$td_attr = array();
						if(isset($this->element['fs'][$i]['fields'][$row][$col]['td']))
							$td_attr = $this->element['fs'][$i]['fields'][$row][$col]['td'];
						$td = $htmlCtrl->generateTd($td_attr);
						
						// Agregar control al td
						foreach($this->element['fs'][$i]['fields'][$row][$col] as $kCtrl => $vCtrl) {
							if(is_numeric($kCtrl)) {
								$td->innerHtml[] = $htmlCtrl->generateControl($vCtrl);
							}
						}
						
						// Agregar td a la fila
						$tr->innerHtml[] = $td;
					}
					
					// Agregar fila a la tabla
					$table->innerHtml[] = $tr;
				}
				
				// Agregar tabla al fieldset
				$fieldset->innerHtml[] = $table;
			}
			else {
				// Sin tabla, los controles se distribuyen en filas (row), las columnas (col) representan un control
				$rows = count($this->element['fs'][$i]['fields']);
				for($row = 0; $row < $rows; $row++) {
					$cols = count($this->element['fs'][$i]['fields'][$row]);
					for($col = 0; $col < $cols; $col++) {
						$fieldset->innerHtml[] = $htmlCtrl->generateControl($this->element['fs'][$i]['fields'][$row][$col]);
					}
					// Se utiliza un br como marca de nueva fila, si no es la última fila
					if($row+1 < $rows)
						$fieldset->innerHtml[] = $htmlCtrl->generateBr();
				}
			}
			
			// Agregar fieldset y un espacio br
			$fieldsets->innerHtml[] = $fieldset;
			isset($this->element['fs'][$i]['br']) or $this->element['fs'][$i]['br'] = 'y';
			if(is_True($this->element['fs'][$i]['br'])) {
				$fieldsets->innerHtml[] = $htmlCtrl->generateBr();
			}
		}
		
		// Eliminar último br
		if(is_True($this->element['fs'][--$i]['br'])) {
			array_pop($fieldsets->innerHtml);
		}
		
		// Generar fieldsets
		echo $fieldsets->generateTag($nivel) . "\n";
	} // generateFieldsets()
}
?>
