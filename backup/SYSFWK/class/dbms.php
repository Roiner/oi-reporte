<?php
// *****************************************************************************
// Manejador de base de datos utilizando PDO
// Autor: Elvin Calder�n
// Versi�n: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALG�N CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

// Define el manejador de Base de Datos
class dbms {
	private $_connstr; // Cadena de conexi�n
	private $_pdo; // Objeto PDO
	private $_user; // Usuario
	private $_password; // Password
	
	private $_query; // �ltima consulta ejecutada
	private $_rowCount; // N�meros de registros afectados por al �ltima consulta
	
	private $_error; // �ltimo error
	
	//****************************************************************************
	// Constructor
	public function __construct($connstr, $user = null, $pass = null) {
		$this->_connstr = $connstr; // Guardar cadena de conexi�n
		$this->_user = $user; // Guardar usuario
		$this->_password = $pass; // Guardar password
		
		$this->connect(); // Conectar a la DB
	} // __construct()
	
	
	//****************************************************************************
	// Funciones Inpectoras
	public function getQuery() { return $this->_query; }
	public function getRowCount() { return $this->_rowCount; }
	public function getError() { return $this->_error; }
	
	
	//****************************************************************************
	// Conectar a la DB
	// Retorna true si tiene �xito o false en caso de error
	public function connect() {
		$this->_error = null; // Limipar error
		
		try {
			if(is_null($this->_user)) {
				// El usuario est� incluido en la cadena de conexi�n
				$this->_pdo = new pdo($this->_connstr); // Conectar
			}
			else {
				$this->_pdo = new pdo($this->_connstr, $this->_user, $this->_password); // Conectar
			}
			$this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Fijar excepcion como modo de error
			$result = true;
		}
		catch(PDOException $e) {
			$this->_error = $e->getMessage() . $e->getTraceAsString(); // Guardar error
			$result = false;
		}
		
		return $result;
	} // connect()
	
	
	//****************************************************************************
	// Desconectar de la DB
	public function disConnect() {
		$this->_pdo = null;
	} // disConnect()
	
	
	//****************************************************************************
	// Entrecomilla una cadena para usarla en una consulta
	public function quote($value) {
		if(is_int($value))
			$tp = PDO::PARAM_INT;
		elseif(is_bool($value))
			return ($value ? 'TRUE' : 'FALSE');
		elseif(is_null($value))
			return 'NULL';
		elseif(is_string($value))
			$tp = PDO::PARAM_STR;
		else
			$tp = PDO::PARAM_STR;
		return $this->_pdo->quote($value, $tp);
	} // quote()
	
	
	//****************************************************************************
	// Verificar si hay una transacci�n activa
	public function inTransaction() {
		return $this->_pdo->inTransaction();
	} // inTransaction()
	
	
	//****************************************************************************
	// Iniciar una transacci�n
	public function beginTransaction() {
		$this->_error = null; // Limipar error
		$result = $this->_pdo->beginTransaction();
		if(!$result) {
			$e = $this->_pdo->errorInfo();
			$this->_error = $e[2];
		}
		return $result;
	} // beginTransaction()
	
	
	//****************************************************************************
	// Ejecutar transacci�n
	public function commit() {
		$this->_error = null; // Limipar error
		$result = $this->_pdo->commit();
		if(!$result) {
			$e = $this->_pdo->errorInfo();
			$this->_error = $e[2];
		}
		return $result;
	} // commit()
	
	
	//****************************************************************************
	// Cancelar transacci�n
	public function rollBack() {
		$this->_error = null; // Limipar error
		$result = $this->_pdo->rollBack();
		if(!$result) {
			$e = $this->_pdo->errorInfo();
			$this->_error = $e[2];
		}
		return $result;
	} // rollBack()
	
	
	//****************************************************************************
	// Ejecutar una sentencia SQL que no es query
	// Retorna el n�mero de filas afectadas o false en caso de error
	public function executeNoQuery($query) {
		$this->_error = null; // Limipar error
		$this->_query = $query; // Guardar query
		
		try {
			$result = $this->_pdo->exec($this->_query); // Ejecutar query
			$this->_rowCount = $result;
		}
		catch(PDOException $e) {
			$this->_error = $e->getMessage(); // Guardar error
			$this->_rowCount = 0;
			$result = false;
		}
		
		return $result;
	} // executeNoQuery()
	
	
	//****************************************************************************
	// Ejecutar una sentencia SQL y retorna el valor de la primera columna de la primera fila
	// Retorna el n�mero de filas afectadas o false en caso de error
	public function executeScalar($query, $params = array()) {	
		$result = $this->executeQuery($query, PDO::FETCH_NUM, $params); // Ejecutar query
		if($result !== false && count($result) > 0)
			return $result[0][0];
		
		return false;
	} // executeScalar()
	
	
	//****************************************************************************
	// Ejecutar una sentencia SQL
	// $query: SQL statement seg�n PDO::prepare
	// $resultType: PDO::FETCH_NUM (default) o PDO::FETCH_ASSOC
	// $params: Array de par�metros (asociativo o numerico iniciando en 1 seg�n parameter markers en $query)
	// $paramsType: Array de constantes PDO::PARAM_... indicando el tipo deseado de los par�metros en $params
	// Retorna un array con el resultado o false en caso de error
	public function executeQuery($query, $resultType = PDO::FETCH_NUM, $params = array(), $paramsType = null) {
		$this->_error = null; // Limipar error
		$this->_query = $query; // Guardar query
		$strparams = ''; // Cadena de parametros
		
		try {
			// Preparar consulta
			$sth = $this->_pdo->prepare($query);
			
			// Agregar parametros
			foreach($params as $key => $value) {
				$pkey = (is_string($key) ? ":$key" : $key); // Determinar si la clave es asociativa o numerica
				$strparams .= "$pkey => $value, ";
				if($paramsType) {
					$sth->bindValue($pkey, $value, $paramsType[$key]); // Utilizar array de tipo deseado
				}
				else { // Determinar el tipo de par�metro de acuerdo al tipo de $value
					if(is_int($value))
						$tp = PDO::PARAM_INT;
					elseif(is_bool($value))
						$tp = PDO::PARAM_BOOL;
					elseif(is_null($value))
						$tp = PDO::PARAM_NULL;
					elseif(is_string($value))
						$tp = PDO::PARAM_STR;
					else
						$tp = PDO::PARAM_STR;
					
					// Vincular parametro
					if(!$sth->bindValue($pkey, $value, $tp)) {
						$e = $sth->errorInfo();
						$this->_error = $e[2];
					}
				}
			}
			$strparams = rtrim($strparams, ', ');

			$this->_query = $sth->queryString . (empty($strparams) ? '' : "; PARAMETERS($strparams)");
			$result = $sth->execute(); // Ejecutar query
			if(!$result) {
				$e = $sth->errorInfo();
				$this->_error = $e[2];
				$this->_rowCount = 0;
			}
			else {
				$this->_rowCount = $sth->rowCount();
				// Crear array de resultado
				$result = $sth->fetchAll($resultType);
			}
		}
		catch(PDOException $e) {
			$this->_error = $e->getMessage(); // Guardar error
			$result = false;
		}
		
		return $result;
	} // executeQuery()
	
	
	//****************************************************************************
	// Ejecutar una sentencia SQL varias veces con diferentes par�metros
	// $query: SQL statement seg�n PDO::prepare
	// $resultType: PDO::FETCH_NUM (default) o PDO::FETCH_ASSOC
	// $params: Lista de Array de par�metros (asociativo o numerico iniciando en 1 seg�n parameter markers en $query)
	// $paramsType: Lista de Array de constantes PDO::PARAM_... indicando el tipo deseado de los par�metros en $params
	// Retorna un array con el resultado o false en caso de error
	public function executeMultiQuery($query, $resultType = PDO::FETCH_NUM, $params = array(), $paramsType = array()) {
		$this->_error = null; // Limipar error
		$this->_query = ''; // Guardar query
		$this->_rowCount = 0;
		
		try {
			// Preparar consulta
			$sth = $this->_pdo->prepare($query);
			
			// Agregar par�metros y ejecutar
			for($i = 0; $i < count($params); $i++) {
				$strparams = ''; // Cadena de parametros
				// Agregar parametros
				foreach($params[$i] as $key => $value) {
					$pkey = (is_string($key) ? ":$key" : $key); // Determinar si la clave es asociativa o numerica
					$strparams .= "$pkey => $value, ";
					if(isset($paramsType[$i])) {
						$sth->bindValue($pkey, $value, $paramsType[$i][$key]); // Utilizar array de tipo deseado
					}
					else { // Determinar el tipo de par�metro de acuerdo al tipo de $value
						if(is_int($value))
							$tp = PDO::PARAM_INT;
						elseif(is_bool($value))
							$tp = PDO::PARAM_BOOL;
						elseif(is_null($value))
							$tp = PDO::PARAM_NULL;
						elseif(is_string($value))
							$tp = PDO::PARAM_STR;
						else
							$tp = PDO::PARAM_STR;
						
						// Vincular parametro
						if(!$sth->bindValue($pkey, $value, $tp)) {
							$e = $sth->errorInfo();
							$this->_error = $e[2];
						}
					}
				}
				$strparams = rtrim($strparams, ', ');
				
				$this->_query .= $sth->queryString . (empty($strparams) ? '; ' : "; PARAMETERS($strparams);\n");
				$result_aux = $sth->execute(); // Ejecutar query
				if(!$result_aux) {
					$e = $sth->errorInfo();
					$this->_error = $e[2];
					$this->_rowCount = 0;
				}
				else {
					$this->_rowCount += $sth->rowCount();
					// Crear array de resultado
					$result_temp = $sth->fetchAll($resultType);
					$result[$i] = $result_temp[0];
				}
			} // for
		}
		catch(PDOException $e) {
			$this->_error = $e->getMessage(); // Guardar error
			$result = false;
		}
		
		return $result;
	} // executeMultiQuery()
} // dbms
?>