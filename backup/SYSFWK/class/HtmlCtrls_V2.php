<?php
// *****************************************************************************
// Define las clases HtmlTag y HtmlCtrl utilizadas para construir
//	controles HTML
// Autor: Elvin Calderón
// Versión: 2.0 (*** EN CONSTRUCCIÓN ***)
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************


//***************************************************************************
// Clase HtmlTag
// Representa un tag html
//***************************************************************************
class HtmlTag {
	public $tag; // Nombre del tag html
	public $attributes; // Array de atributos
	public $isVoid; // Indica si el tag es vacío o no
	public $innerHtml; // Contenido interno del tag, si no es vacío


	//***************************************************************************
	// Constructor
	function __construct($tag, $attrs = array(), $isVoid = true, $html = null) {
		$this->tag = $tag;
		$this->attributes = $attrs;
		$this->isVoid = $isVoid;
		$this->innerHtml = $html;
	} // __construct()


	//***************************************************************************
	// Genera una cadena html del tag
	public function generateTag($nivel = 0) {
		// Si es un texto
		if($this->tag == 'text') return $this->innerHtml;

		// Preparar tabulador
		$tab = ($nivel > 0 ? str_pad('', $nivel, "\t") : '');

		// Generar resultado
		$result = "<" . $this->tag;
		foreach($this->attributes as $attr => $val) {
			$result .= ' ' . $attr . '="' . htmlspecialchars($val) . '"';
		}
		if($this->isVoid) {
			$result .= ' />';
		}
		else {
			$result .= '>';
			if(is_object($this->innerHtml) && method_exists($this->innerHtml, 'generateTag')) {
				$result .= "\n$tab\t" . $this->innerHtml->generateTag($nivel + 1) . "\n$tab";
			}
			elseif(is_array($this->innerHtml)) {
				foreach($this->innerHtml as $tag) {
					if(is_object($tag) && method_exists($tag, 'generateTag'))
						$result .= "\n$tab\t" . $tag->generateTag($nivel + 1); // . "\n$tab";
					elseif(is_string($tag) || is_numeric($tag))
						$result .= $this->innerHtml;
				}
				$result .= "\n$tab";
			}
			elseif(is_string($this->innerHtml) || is_numeric($this->innerHtml)) {
				$result .= $this->innerHtml;
			}
			$result .= "</" . $this->tag . '>';
		}
		return $result;
	} // generateTag()
} // class HtmlTag


/*******************************************************************************
	* Clase HtmlCtrl
	* Representa un control html
	******************************************************************************
	* Definición del array control:
	* $control = [
	* 	'control' => (requerido) '< ID del control>',
	* 	'field' => (opcional) '<ID del campo en el $dataSource>',
	* 	'content' => (opcional) '<contenido>' || <$control> || [0..n]<$control>,
	* 	'<atributo_i>' => (opcional) '<valor del atributo>' || [
	* 		'field' => (requerido) '<ID del campo en el $dataSource>',
	* 		'bind' => (opcional) 'field_name' || 'field_value' || '<ID del atributo en el $dataSource>'
	* 	], .. '<atributo_n>'
	* ]
	*
	*/
class HtmlCtrl {
	// Contantes
	const AT_STR = 'STR'; // Atributo de tipo string
	const AT_BLN = 'BLN'; // Atributo de tipo bool (Ej: hidden="hidden")
	const AT_PRE = 'PRE'; // Atributo de tipo prefijo (especial para data-*)
	// const ST_OPLST = 'OPLST'; // Optionlist del source

/** Definición del array dataSource:
	* $dataSource = [
	* 	'<ID del campo i>' => [
	* 		'field_name' => (requerido) '<Nombre del campo>',
	* 		'field_value' => (requerido) <Valor del campo>,
	* 		'optionlist' => (opcional) [0..n]['value' => <Valor de la opción>, 'text' => '<texto de la opción>'],
	* 		'<atributo_i>' => (opcional) <Valor del atributo_i>, .. '<atributo_n>'
	* 	], .. '<ID del campo n>'
	* ]
	*/
	public $dataSource;

	private $globalAttrs; // Array de atributos globales (pueden ser usado en cualquier control)
	private $globalEvents; // Eventos globales (pueden ser usado en casi cualquier control)
	
/** Definición del array ctrlDef:
	* $ctrlDef = [
	* 	'<ID del control i>' => [
	* 		'tag' => (requerido) '<Nombre del tag html que representa al control>',
	* 		'is_void' => (requerido) true || false,
	* 		'attrs' => (opcional) [
	* 			'<atributo_i>' => (requerido) '<valor del atributo>' || [
	* 				'type' => (requerido) AT_STR || AT_BLN || AT_PRE,
	* 				'bind' => (opcional) 'field_name' || 'field_value' || '<ID del atributo en el $dataSource>'
	* 			], .. '<atributo_n>'
	* 		],
	* 		'content' => (opcional) '<contenido>' || [
	* 		]
	* 	], .. '<ID del control n>'
	* ]
	*/
	private $ctrlDef;


	//***************************************************************************
	// Constructor
	function __construct() {
		$this->dataSource = array();

		// Inicilizar propiedades de controles
		$this->iniHtmlControl();
	} // __construct()


	//***************************************************************************
	// Crear un objeto HtmlTag correpondiente a un br
	// $attr: Array de atributos del br
	public function generateBr($attr = array()) {
		$attr['control'] = 'br';

		return $this->generateControl($attr);
	} // generateBr()


	//***************************************************************************
	// Crear un objeto HtmlTag correpondiente a una tabla
	// $attr: Array de atributos de la tabla
	public function generateTable($attr = array()) {
		$attr['control'] = 'table';

		return $this->generateControl($attr);
	} // generateTable()


	//***************************************************************************
	// Crear un objeto HtmlTag correpondiente a un tr
	// $attr: Array de atributos del tr
	public function generateTr($attr = array()) {
		$attr['control'] = 'tr';

		return $this->generateControl($attr);
	} // generateTr()


	//***************************************************************************
	// Crear un objeto HtmlTag correpondiente a un td
	// $attr: Array de atributos del td
	public function generateTd($attr = array()) {
		$attr['control'] = 'td';

		return $this->generateControl($attr);
	} // generateTd()


	//***************************************************************************
	// Crear un objeto HtmlTag correpondiente a un th
	// $attr: Array de atributos del th
	public function generateTh($attr = array()) {
		$attr['control'] = 'th';

		return $this->generateControl($attr);
	} // generateTh()


	//***************************************************************************
	// Crear un objeto HtmlTag correpondiente a un control
	// $control: Array con las propiedas del control a generar
	//	$control['control'] = Nombre (identificador) del control a generar
	//	$control['field<_*>'] = Nombre del campo asociado al control (pueden ser varios)
	public function generateControl($control) {
		// Obtener propiedades del control
		$ctrlName = $control['control'];
		if(!isset($this->ctrlDef[$ctrlName])) {
			throw new Exception("El control $ctrlName no está defindo.");
		}
		$tag = $this->ctrlDef[$ctrlName]['tag'];
		$isVoid = $this->ctrlDef[$ctrlName]['is_void'];

		// Obtener atributos globales
		$attrs = array();
		$this->setAttrs($attrs, $control, $this->globalAttrs);
		// echo '<pre>attrs: '; var_dump($attrs); echo '</pre>'; // DEBUG

		// Obtener atributos especificos
		if(isset($this->ctrlDef[$ctrlName]['attrs']))
			$this->setAttrs($attrs, $control, $this->ctrlDef[$ctrlName]['attrs']);
		// echo '<pre>attrs: '; var_dump($attrs); echo '</pre>'; // DEBUG

		// Obtener contenido si el control no es vacío
		$html = null;
		if(!$isVoid) {
			if(isset($this->ctrlDef[$ctrlName]['content'])) {
				// Contenido del control (texto simple)
				$html = $this->getContentVal($control,
					$this->ctrlDef[$ctrlName]['content']['source_type'],
					$this->ctrlDef[$ctrlName]['content']['source'], $this->ctrlDef[$ctrlName]['content']['bind']
				);
			}
			elseif(isset($this->ctrlDef[$ctrlName]['oplst_tmp'])) {
				// Crear controles hijos de acuerdo a 'oplst_tmp'
				$field = $control['field'];
				if(isset($this->fieldSpec[$field]['optionlist'])) {
					if(!is_array($this->optionList[$this->fieldSpec[$field]['optionlist']])) {
						echo '* El array "' . $this->fieldSpec[$field]['optionlist'] . '" no esta definido en "optionlist" *';
					}
					foreach($this->optionList[$this->fieldSpec[$field]['optionlist']] as $option) {
						foreach($this->ctrlDef[$ctrlName]['oplst_tmp'] as $child => $childAttr) {
							$childCtrl = array();
							$childCtrl['control'] = $child;
							foreach($childAttr as $childAttrName => $org) {
								$childCtrl[$childAttrName] = eval($org);
							}
							if(isset($this->fieldData[$field]) && $option['value'] == $this->fieldData[$field]) {
								$childCtrl['selected'] = 'y';
								$childCtrl['checked'] = 'y';
							}
							$html[] = $this->generateControl($childCtrl);
						}
					}
				}
			}
		}

		// Crear control
		$ctrl = new HtmlTag($tag, $attrs, $isVoid, $html);
		// echo '<pre>ctrl: '; var_dump($ctrl); echo '</pre>'; // DEBUG

		// Retornar control
		return $ctrl;
	} // generateControl()


	//---------------------------------------------------------------------------
	// Funciones privadas
	//---------------------------------------------------------------------------

	//***************************************************************************
	// Inicializar la definición de los controles
	private function iniHtmlControl() {
		// Definir atributos globales
		$this->globalAttrs = array(
			'accesskey' => array('type' => self::AT_STR),
			'class' => array('type' => self::AT_STR),
			'contenteditable' => array('type' => self::AT_STR),
			'contextmenu' => array('type' => self::AT_STR),
			'data-' => array('type' => self::AT_PRE),
			'dir' => array('type' => self::AT_STR),
			'draggable' => array('type' => self::AT_STR),
			'dropzone' => array('type' => self::AT_STR),
			'hidden' => array('type' => self::AT_BLN),
			'id' => array('type' => self::AT_STR),
			'lang' => array('type' => self::AT_STR),
			'spellcheck' => array('type' => self::AT_STR),
			'style' => array('type' => self::AT_STR),
			'tabindex' => array('type' => self::AT_STR),
			'title' => array('type' => self::AT_STR),
			'translate' => array('type' => self::AT_STR)
		);
		
		// Definir atributos globales
		$this->globalEvents = array(
			'onblur' => array('type' => self::AT_STR),
			'onchange' => array('type' => self::AT_STR),
			'oncontextmenu' => array('type' => self::AT_STR),
			'onfocus' => array('type' => self::AT_STR),
			'oninput' => array('type' => self::AT_STR),
			'oninvalid' => array('type' => self::AT_STR),
			'onreset' => array('type' => self::AT_STR),
			'onsearch' => array('type' => self::AT_STR),
			'onselect' => array('type' => self::AT_STR),
			'onsubmit' => array('type' => self::AT_STR),
			'onkeydown' => array('type' => self::AT_STR),
			'onkeypress' => array('type' => self::AT_STR),
			'onkeyup' => array('type' => self::AT_STR),
			'onclick' => array('type' => self::AT_STR),
			'ondblclick' => array('type' => self::AT_STR),
			'ondrag' => array('type' => self::AT_STR),
			'ondragend' => array('type' => self::AT_STR),
			'ondragenter' => array('type' => self::AT_STR),
			'ondragleave' => array('type' => self::AT_STR),
			'ondragover' => array('type' => self::AT_STR),
			'ondragstart' => array('type' => self::AT_STR),
			'ondrop' => array('type' => self::AT_STR),
			'onmousedown' => array('type' => self::AT_STR),
			'onmousemove' => array('type' => self::AT_STR),
			'onmouseout' => array('type' => self::AT_STR),
			'onmouseover' => array('type' => self::AT_STR),
			'onmouseup' => array('type' => self::AT_STR),
			'onscroll' => array('type' => self::AT_STR),
			'onwheel' => array('type' => self::AT_STR),
			'oncopy' => array('type' => self::AT_STR),
			'oncut' => array('type' => self::AT_STR),
			'onpaste' => array('type' => self::AT_STR)
		);

		// Array de definición de contoles
		$this->ctrlDef = array();
		// literal
		$this->ctrlDef['literal']['tag'] = 'text';
		$this->ctrlDef['literal']['is_void'] = false;
		$this->ctrlDef['literal']['content'] = array('source_type' => self::ST_DATA, 'source' => 'field', 'bind' => 'content');

		// label
		$this->ctrlDef['label']['tag'] = 'label';
		$this->ctrlDef['label']['is_void'] = false;
		$this->ctrlDef['label']['attrs'] = array(
			'for' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_NAME, 'source' => 'field', 'bind' => 'for'),
			'form' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'form')
		);
		$this->ctrlDef['label']['content'] = array('source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'label');

		// button
		$this->ctrlDef['button']['tag'] = 'button';
		$this->ctrlDef['button']['is_void'] = false;
		$this->ctrlDef['button']['attrs'] = array(
			'type' => array('attr_type' => self::AT_CTE, 'value' => 'button'),
			'autofocus' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'autofocus'),
			'disabled' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'disabled'),
			'name' => array('attr_type' => self::AT_STR),
			'value' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_DATA, 'source' => 'field')
		);
		$this->ctrlDef['button']['content'] = array('source_type' => self::ST_DATA, 'source' => 'field_content', 'bind' => 'content');

		// hidden
		$this->ctrlDef['hidden']['tag'] = 'input';
		$this->ctrlDef['hidden']['is_void'] = true;
		$this->ctrlDef['hidden']['attrs'] = array(
			'type' => array('attr_type' => self::AT_CTE, 'value' => 'hidden'),
			'id' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_NAME, 'source' => 'field'),
			'name' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_NAME, 'source' => 'field'),
			'value' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_DATA, 'source' => 'field')
		);

		// textbox
		$this->ctrlDef['textbox']['tag'] = 'input';
		$this->ctrlDef['textbox']['is_void'] = true;
		$this->ctrlDef['textbox']['attrs'] = array(
			'type' => array('attr_type' => self::AT_CTE, 'value' => 'text'),
			'id' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_NAME, 'source' => 'field'),
			'title' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'title'),
			'autocomplete' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'autocomplete'),
			'autofocus' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'autofocus'),
			'disabled' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'disabled'),
			'form' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'form'),
			'list' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'list'),
			'maxlength' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'size'),
			'name' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_NAME, 'source' => 'field'),
			'pattern' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'pattern'),
			'placeholder' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'placeholder'),
			'readonly' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'readonly'),
			'required' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'required'),
			'value' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_DATA, 'source' => 'field')
		);

		// uploadfile
		$this->ctrlDef['uploadfile']['tag'] = 'input';
		$this->ctrlDef['uploadfile']['is_void'] = true;
		$this->ctrlDef['uploadfile']['attrs'] = array(
			'type' => array('attr_type' => self::AT_CTE, 'value' => 'file'),
			'id' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_NAME, 'source' => 'field'),
			'title' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'title'),
			'accept' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'accept'),
			'disabled' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'disabled'),
			'form' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'form'),
			'maxlength' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'size'),
			'name' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_NAME, 'source' => 'field'),
			'pattern' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'pattern'),
			'placeholder' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'placeholder'),
			'readonly' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'readonly'),
			'required' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'required'),
			'value' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_DATA, 'source' => 'field')
		);

		// textarea
		$this->ctrlDef['textarea']['tag'] = 'textarea';
		$this->ctrlDef['textarea']['is_void'] = false;
		$this->ctrlDef['textarea']['attrs'] = array(
			'id' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_NAME, 'source' => 'field'),
			'title' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'title'),
			'autofocus' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'autofocus'),
			'cols' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'cols'),
			'disabled' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'disabled'),
			'form' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'form'),
			'maxlength' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'size'),
			'name' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_NAME, 'source' => 'field'),
			'placeholder' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'placeholder'),
			'readonly' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'readonly'),
			'required' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'required'),
			'rows' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'rows'),
			'wrap' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'wrap')
		);
		$this->ctrlDef['textarea']['content'] = array('source_type' => self::ST_DATA, 'source' => 'field', 'bind' => 'value');

		// checkbox
		$this->ctrlDef['checkbox']['tag'] = 'input';
		$this->ctrlDef['checkbox']['is_void'] = true;
		$this->ctrlDef['checkbox']['attrs'] = array(
			'type' => array('attr_type' => self::AT_CTE, 'value' => 'checkbox'),
			'id' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_NAME, 'source' => 'field'),
			'title' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'title'),
			'autofocus' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'autofocus'),
			'checked' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_DATA, 'source' => 'field', 'bind' => 'checked'),
			'disabled' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'disabled'),
			'form' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'form'),
			'name' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_NAME, 'source' => 'field'),
			'required' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'required'),
			'value' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_DATA, 'source' => 'field')
		);

		// radio
		$this->ctrlDef['radio']['tag'] = 'input';
		$this->ctrlDef['radio']['is_void'] = true;
		$this->ctrlDef['radio']['attrs'] = array(
			'type' => array('attr_type' => self::AT_CTE, 'value' => 'radio'),
			'id' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_NAME, 'source' => 'field'),
			'title' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'title'),
			'autofocus' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'autofocus'),
			'checked' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_DATA, 'source' => 'field', 'bind' => 'checked'),
			'disabled' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'disabled'),
			'form' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'form'),
			'name' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_NAME, 'source' => 'field'),
			'required' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'required'),
			'value' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_DATA, 'source' => 'field')
		);

		// option
		$this->ctrlDef['option']['tag'] = 'option';
		$this->ctrlDef['option']['is_void'] = false;
		$this->ctrlDef['option']['attrs'] = array(
			'disabled' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'disabled'),
			'label' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'label'),
			'selected' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_DATA, 'source' => 'field', 'bind' => 'selected'),
			'value' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_DATA, 'source' => 'field')
		);
		$this->ctrlDef['option']['content'] = array('source_type' => self::ST_DATA, 'source' => 'field', 'bind' => 'text');

		// dropdown
		$this->ctrlDef['dropdown']['tag'] = 'select';
		$this->ctrlDef['dropdown']['is_void'] = false;
		$this->ctrlDef['dropdown']['attrs'] = array(
			'id' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_NAME, 'source' => 'field'),
			'title' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'title'),
			'autofocus' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'autofocus'),
			'disabled' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'disabled'),
			'form' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'form'),
			'multiple' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'multiple'),
			'name' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_NAME, 'source' => 'field'),
			'required' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'required'),
			'size' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'size')
		);
		$this->ctrlDef['dropdown']['oplst_tmp'] = array(
			'option' => array('value' => 'return $option["value"];', 'text' => 'return $option["text"];')
		);

		// radios
		$this->ctrlDef['radios']['tag'] = 'div';
		$this->ctrlDef['radios']['is_void'] = false;
		$this->ctrlDef['radios']['oplst_tmp'] = array(
			'radio' => array('id' => 'return $field . "_" . $option["value"];', 'name' => 'return $field;', 'value' => 'return $option["value"];'),
			'label' => array('for' => 'return $field . "_" . $option["value"];', 'label' => 'return $option["text"];')
		);

		// radiosv
		$this->ctrlDef['radiosv'] = $this->ctrlDef['radios'];
		$this->ctrlDef['radiosv']['oplst_tmp']['br'] = array();

		// radiosh
		$this->ctrlDef['radiosh'] = $this->ctrlDef['radios'];
		$this->ctrlDef['radiosh']['oplst_tmp']['span'] = array();

		// br
		$this->ctrlDef['br']['tag'] = 'br';
		$this->ctrlDef['br']['is_void'] = true;

		// space
		$this->ctrlDef['space']['tag'] = 'span';
		$this->ctrlDef['space']['is_void'] = false;

		// img
		$this->ctrlDef['img']['tag'] = 'img';
		$this->ctrlDef['img']['is_void'] = true;
		$this->ctrlDef['img']['attrs'] = array(
			'alt' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'label'),
			'height' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'height'),
			'ismap' => array('attr_type' => self::AT_BLN, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'ismap'),
			'src' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_DATA, 'source' => 'field'),
			'usemap' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'usemap'),
			'width' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'width')
		);

		// table
		$this->ctrlDef['table']['tag'] = 'table';
		$this->ctrlDef['table']['is_void'] = false;

		// tr
		$this->ctrlDef['tr']['tag'] = 'tr';
		$this->ctrlDef['tr']['is_void'] = false;

		// td
		$this->ctrlDef['td']['tag'] = 'td';
		$this->ctrlDef['td']['is_void'] = false;
		$this->ctrlDef['td']['attrs'] = array(
			'colspan' => array('attr_type' => self::AT_STR),
			'headers' => array('attr_type' => self::AT_STR),
			'rowspan' => array('attr_type' => self::AT_STR)
		);

		// th
		$this->ctrlDef['th']['tag'] = 'th';
		$this->ctrlDef['th']['is_void'] = false;
		$this->ctrlDef['th']['attrs'] = array(
			'abbr' => array('attr_type' => self::AT_STR),
			'colspan' => array('attr_type' => self::AT_STR),
			'headers' => array('attr_type' => self::AT_STR),
			'rowspan' => array('attr_type' => self::AT_STR),
			'scope' => array('attr_type' => self::AT_STR)
		);

		// link
		$this->ctrlDef['link']['tag'] = 'a';
		$this->ctrlDef['link']['is_void'] = false;
		$this->ctrlDef['link']['attrs'] = array(
			'download' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'download'),
			'href' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_DATA, 'source' => 'field'),
			'hreflang' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'hreflang'),
			'media' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'media'),
			'rel' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'rel'),
			'target' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'target'),
			'type' => array('attr_type' => self::AT_STR, 'source_type' => self::ST_SPEC, 'source' => 'field', 'bind' => 'media_type')
		);
		$this->ctrlDef['link']['content'] = array('source_type' => self::ST_DATA, 'source' => 'field_content', 'bind' => 'content');
	} // iniHtmlControl()


	//***************************************************************************
	// Cargar los atributos de una lista
	// $destino: Array destino de los atributos
	// $origen: Array de propiedades del control
	// $attrLst: Array predefinido con la lista de atributos
	private function setAttrs(&$destino, &$origen, &$attrLst) {
		foreach($attrLst as $attr => $pAttr) {
			if(!isset($destino[$attr])) {
				if($pAttr['attr_type'] == self::AT_PRE) {
					foreach($origen as $oKey => $oVal) {
						if(preg_match('/^' . $attr . '/', $oKey)) {
							$destino[$oKey] = $oVal;
						}
					}
				}
				elseif($pAttr['attr_type'] == self::AT_CTE) {
					$destino[$attr] = $pAttr['value'];
				}
				else {
					$valor = $this->getAttrVal($origen, $attr, $pAttr);
					if(!is_null($valor))
						$destino[$attr] = $valor;
				}
			}
		}
	} // setAttrs()


	//***************************************************************************
	// Obtener valor del atributo
	private function getAttrVal(&$control, $attr, &$pAttr) {
		$attrVal = null;
		// Si está en control
		if(isset($control[$attr])) {
			if($pAttr['attr_type'] == self::AT_BLN)
				$attrVal = $attr;
			elseif($pAttr['attr_type'] == self::AT_STR)
				$attrVal = $control[$attr];
		}
		// No está en control, buscar en datasource
		elseif(isset($pAttr['source']) && isset($control[$pAttr['source']])) {
			// Obtener field
			$field = $control[$pAttr['source']];
			switch($pAttr['source_type']) {
				case self::ST_NAME:
					$attrVal = $field;
					break;
				case self::ST_DATA:
					if(isset($this->fieldData[$field]) && !is_null($this->fieldData[$field])) {
						if($pAttr['attr_type'] == self::AT_BLN && is_True($this->fieldData[$field]))
							$attrVal = $attr;
						elseif($pAttr['attr_type'] == self::AT_STR)
							$attrVal = $this->fieldData[$field];
					}
					break;
				case self::ST_SPEC: // Se asume que $bind es el campo en fieldSpec
					if(isset($this->fieldSpec[$field][$pAttr['bind']]))
						if($pAttr['attr_type'] == self::AT_BLN)
							$attrVal = $attr;
						elseif($pAttr['attr_type'] == self::AT_STR)
							$attrVal = $this->fieldSpec[$field][$pAttr['bind']];
					break;
			}
		}
		return $attrVal;
	} // getAttrVal()


	//***************************************************************************
	// Obtener valor del contenido
	private function getContentVal(&$control, $sourceType, $source, $bind) {
		$contentVal = null;
		// Si está en control
		if(isset($control[$bind])) {
				$contentVal = $control[$bind];
		}
		// No está en control, buscar en datasource
		elseif(isset($control[$source])) {
			// Obtener field
			$field = $control[$source];
			switch($sourceType) {
				case self::ST_DATA:
					if(isset($this->fieldData[$field]) && !is_null($this->fieldData[$field]))
						$contentVal = $this->fieldData[$field];
					break;
				case self::ST_SPEC: // Se asume que $bind es el campo en fieldSpec
					if(isset($this->fieldSpec[$field][$bind]))
							$contentVal = $this->fieldSpec[$field][$bind];
					break;
			}
		}
		return $contentVal;
	} // getContentVal()
} // class HtmlCtrl
?>