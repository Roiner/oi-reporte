<?php
// Representa la tabla Role
class Role extends DB_Table {

	// **************************************************************************
	// Constructor
	function __construct() {
		// Guardar el nombre del directorio del script actual
		$this->_dirName = dirname(__file__);
		
		// Orden por defecto de las consultas
		$this->defaultOrderBy = array('id_sistema' => 'ASC', 'id_role' => 'ASC');
		
		/** Ejecutar el constructor padre. Los parámetros del constructor son:
			* $connstr: Cadena de conexión, pueder un string o un array de la forma array(<cadena de conexión>, <usuario>, <password>)
			* $table: Nombre de la tabla en DB
			* $instdbms: (Opcional) Nombre que identifica un instancia en DB, por defecto es null
			* $dbschema: (Opcional) Nombre del esquema en DB, por defecto es null
			* $dbname: (Opcional) Nombre de la DB, por defecto es null
			* LLamada: parent::__construct($connstr, $table, $instdbms = null, $dbschema = null, $dbname = null, $dirNameDict = null);
			* Ejemplo: parent::__construct(array(CONN_STR, CONN_USR, CONN_PWD), 'prueba', null, DB_SCHEMA);
			*/
		parent::__construct(CONN_STR, 'role', DB_INST, SCH_SYSCTRL);
	} // __construct()
	
	
	//***************************************************************************
	// Obtener los roles, filtrados por sistema, como lista para un <select>
	public function getRoleList($params) {
		$where = ((isset($params['id_sistema']) && $params['id_sistema'] != '') ? array('id_sistema' => $params['id_sistema']) : array());
		// Obtener subprocesos
		$this->sqlSelect = "id_role AS value, (id_role || ' - ' || role_name) AS text";
		$this->sqlOrderBy = array('text' => 'ASC');
		$result = $this->getRecords($where);
		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . '</pre>'; // DEBUG
		if($result !== false) {
			//Agregar elemento extra si existe
			if(isset($params['extra'])) {
				array_unshift($result, array('value' => $params['extra']['value'], 'text' => $params['extra']['text']));
			}
			//Agregar elemento default si existe
			if($this->getNumRows() == 0 && isset($params['default'])) {
				array_unshift($result, array('value' => $params['default']['value'], 'text' => $params['default']['text']));
			}
		}
		return $result;
	} // getRoleList()
	
	
	//***************************************************************************
	// Realizar proceso personalizado antes de insertar resigtro en DB
	protected function _cm_pre_insertRecord($rowData) {
		// echo '<pre>rowData: '; var_dump($rowData); echo '</pre>'; // DEBUG
		
		// El campo id_role debe ser siempre en uppercase
		if(isset($rowData["id_role"]))
			$rowData["id_role"] = strtoupper($rowData["id_role"]);
		
		return $rowData;
	} // _cm_pre_insertRecord()
	
	
	//***************************************************************************
	// Realizar proceso personalizado antes de actualizar resigtro en DB
	protected function _cm_pre_updateRecord($updateData) {
		// echo '<pre>updateData: '; var_dump($updateData); echo '</pre>'; // DEBUG
		
		// El campo id_role debe ser siempre en uppercase
		if(isset($updateData["id_role"]))
			$updateData["id_role"] = strtoupper($updateData["id_role"]);
		
		return $updateData;
	} // _cm_pre_updateRecord()
	
	
	/* /***************************************************************************
	// Para implementar un cambio particular en las especificaciones de una tabla
	protected function _cm_changeConfig($params) {
		// echo '<pre>$params: '; var_dump($params); echo '</pre>'; // DEBUG
		$foreignDataList = $this->_foreignDataList;
		$fieldSpec = $this->_fieldSpec;
		
		// Configuración de acuerdo al task
		switch($params['pattern_type']) {
			case 'list1':
				// Agregar opcion todos
				foreach($foreignDataList as $list => $options) {
					array_unshift($foreignDataList[$list], array('value' => '', 'text' => '(Todos)'));
				}
				break;
			case 'add1':
				// Agregar opcion seleccione
				foreach($foreignDataList as $list => $options) {
					array_unshift($foreignDataList[$list], array('value' => '', 'text' => '(Seleccione)'));
				}
				break;
			case 'mod1':
				// Marcar clave primaria como campos deshabilitados
				foreach($this->_primaryKey as $key) {
					if(isset($fieldSpec[$key]))
						$fieldSpec[$key]['disabled'] = 'y';
				}
				break;
			case 'del1': case 'enq1':
				// Marcar todos los campos como deshabilitados
				foreach($fieldSpec as $field => $spec) {
					$fieldSpec[$field]['disabled'] = 'y';
				}
				break;
		}
		
		$this->_fieldSpec = $fieldSpec;
		$this->_foreignDataList = $foreignDataList;
	} // _cm_changeConfig()
	*/
}
?>
