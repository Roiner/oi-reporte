<?php
// Representa la tabla personal_role
class PersonalRole extends DB_Table {

	// **************************************************************************
	// Constructor
	function __construct() {
		// Guardar el nombre del directorio del script actual
		$this->_dirName = dirname(__file__);
		
		// Orden por defecto de las consultas
		$this->defaultOrderBy = array('id_personal' => 'ASC', 'id_sistema' => 'ASC', 'is_primary' => 'DESC', 'id_role' => 'ASC');
		
		/** Ejecutar el constructor padre. Los parámetros del constructor son:
			* $connstr: Cadena de conexión, pueder un string o un array de la forma array(<cadena de conexión>, <usuario>, <password>)
			* $table: Nombre de la tabla en DB
			* $instdbms: (Opcional) Nombre que identifica un instancia en DB, por defecto es null
			* $dbschema: (Opcional) Nombre del esquema en DB, por defecto es null
			* $dbname: (Opcional) Nombre de la DB, por defecto es null
			* LLamada: parent::__construct($connstr, $table, $instdbms = null, $dbschema = null, $dbname = null, $dirNameDict = null);
			* Ejemplo: parent::__construct(array(CONN_STR, CONN_USR, CONN_PWD), 'prueba', null, DB_SCHEMA);
			*/
		parent::__construct(CONN_STR, 'personal_role', DB_INST, SCH_SYSCTRL);
	} // __construct()
	
	
	/***************************************************************************
	// Realizar proceso personalizado antes de insertar resigtro en DB
	protected function _cm_pre_insertRecord($rowData) {
		// echo '<pre>rowData: '; var_dump($rowData); echo '</pre>'; // DEBUG
		
		// El campo id_role debe ser siempre en uppercase
		if(isset($rowData["id_role"]))
			$rowData["id_role"] = strtoupper($rowData["id_role"]);
		
		return $rowData;
	} // _cm_pre_insertRecord()*/
	
	
	/***************************************************************************
	// Realizar proceso personalizado antes de actualizar resigtro en DB
	protected function _cm_pre_updateRecord($updateData) {
		// echo '<pre>updateData: '; var_dump($updateData); echo '</pre>'; // DEBUG
		
		// El campo id_role debe ser siempre en uppercase
		if(isset($updateData["id_role"]))
			$updateData["id_role"] = strtoupper($updateData["id_role"]);
		
		return $updateData;
	} // _cm_pre_updateRecord()*/
}
?>
