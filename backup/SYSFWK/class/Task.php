<?php
// Representa la tabla Task
class Task extends DB_Table {

	// **************************************************************************
	// Constructor
	function __construct() {
		// Guardar el nombre del directorio del script actual
		$this->_dirName = dirname(__file__);
		
		// Orden por defecto de las consultas
		$this->defaultOrderBy = array('id_sistema' => 'ASC', 'id_task' => 'ASC');
		
		/** Ejecutar el constructor padre. Los parámetros del constructor son:
			* $connstr: Cadena de conexión, pueder un string o un array de la forma array(<cadena de conexión>, <usuario>, <password>)
			* $table: Nombre de la tabla en DB
			* $instdbms: (Opcional) Nombre que identifica un instancia en DB, por defecto es null
			* $dbschema: (Opcional) Nombre del esquema en DB, por defecto es null
			* $dbname: (Opcional) Nombre de la DB, por defecto es null
			* LLamada: parent::__construct($connstr, $table, $instdbms = null, $dbschema = null, $dbname = null, $dirNameDict = null);
			* Ejemplo: parent::__construct(array(CONN_STR, CONN_USR, CONN_PWD), 'prueba', null, DB_SCHEMA);
			*/
		parent::__construct(CONN_STR, 'task', DB_INST, SCH_SYSCTRL);
	} // __construct()
	
	
	//***************************************************************************
	// Obtener los task, filtrados por sistema, como lista para un <select>
	public function getTaskList($params) {
		$where = ((isset($params['id_sistema']) && $params['id_sistema'] != '') ? array('id_sistema' => $params['id_sistema']) : array());
		// Obtener subprocesos
		$this->sqlSelect = "id_task AS value, (id_task || ' - ' || task_title) AS text";
		$this->sqlOrderBy = array('text' => 'ASC');
		$result = $this->getRecords($where);
		if($result !== false) {
			//Agregar elemento extra si existe
			if(isset($params['extra'])) {
				array_unshift($result, array('value' => $params['extra']['value'], 'text' => $params['extra']['text']));
			}
			//Agregar elemento default si existe
			if($this->getNumRows() == 0 && isset($params['default'])) {
				array_unshift($result, array('value' => $params['default']['value'], 'text' => $params['default']['text']));
			}
		}
		return $result;
	} // getTaskList()
	
	
	//***************************************************************************
	// Realizar proceso personalizado despues de getExtraData
	protected function _cm_post_getExtraData($params) {
		// echo '<pre>$params: '; var_dump($params); echo '</pre>'; // DEBUG
		$foreignDataList = $this->_foreignDataList;
		
		// Agregar lista de opciones de tipo y metodo
		$foreignDataList['task_type'] = array(
			array('value' => 'MENU', 'text' => 'Menú'),
			array('value' => 'PROC', 'text' => 'Proceso')
		);
		$foreignDataList['method'] = array(
			array('value' => 'GET', 'text' => 'GET'),
			array('value' => 'POST', 'text' => 'POST')
		);
		
		$this->_foreignDataList = $foreignDataList;
		return $params;
	} // _cm_post_getExtraData()
	
	
	/* /***************************************************************************
	// Para implementar un cambio particular en las especificaciones de una tabla
	protected function _cm_changeConfig($params) {
		// echo '<pre>$params: '; var_dump($params); echo '</pre>'; // DEBUG
		$foreignDataList = $this->_foreignDataList;
		$fieldSpec = $this->_fieldSpec;
		
		// Configuración de acuerdo al task
		switch($params['pattern_type']) {
			case 'list1':
				// Agregar opcion todos
				foreach($foreignDataList as $list => $options) {
					array_unshift($foreignDataList[$list], array('value' => '', 'text' => '(Todos)'));
				}
				break;
			case 'add1':
				// Agregar opcion seleccione
				foreach($foreignDataList as $list => $options) {
					array_unshift($foreignDataList[$list], array('value' => '', 'text' => '(Seleccione)'));
				}
				break;
			case 'mod1':
				// Marcar clave primaria como campos deshabilitados
				foreach($this->_primaryKey as $key) {
					if(isset($fieldSpec[$key]))
						$fieldSpec[$key]['disabled'] = 'y';
				}
				break;
			case 'del1': case 'enq1':
				// Marcar todos los campos como deshabilitados
				foreach($fieldSpec as $field => $spec) {
					$fieldSpec[$field]['disabled'] = 'y';
				}
				break;
		}
		
		$this->_fieldSpec = $fieldSpec;
		$this->_foreignDataList = $foreignDataList;
	} // _cm_changeConfig()
	*/
}
?>
