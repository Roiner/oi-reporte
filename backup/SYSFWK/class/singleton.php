<?php
// *****************************************************************************
// Implementa el patron Singleton para crear una sola instancia de una clase
// Autor: Elvin Calder�n
// Versi�n: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALG�N CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

// Implementa el patron Singleton
final class singleton {
	// Array de instancias
	private static $_instances = array();

	//***************************************************************************
	// Crear instancia
	public static function getInstance($class, $key = null, $args = array()) {
		// Si key es null se utiliza la clase
		if(is_null($key))
			$key = $class;
		// Verificar si la instancia existe
		if(!array_key_exists($key, self::$_instances)) {
			// Crear nueva instancia
			if(empty($args)) {
				self::$_instances[$key] = new $class($args);
			}
			else {
				$ref = new ReflectionClass($class);
				self::$_instances[$key] = $ref->newInstanceArgs($args);
			}
		}
		$instance =& self::$_instances[$key];
		return $instance;
	}

	//***************************************************************************
	// Evitar crear instancias de la clase
	private function __construct() { }
	
	//***************************************************************************
	// Evitar que la instancia sea clonada
	public function __clone() {
		throw new Exception('Clone is not allowed.');
	}
} // singleton
?>