<?php
// Permite asignar role a usuarios de manera simple (un sólo role por usuario) al sistema
// identificador por SYS_ID
class AssignRoleSpl extends DB_Table {

	// **************************************************************************
	// Constructor
	function __construct() {
		// Guardar el nombre del directorio del script actual
		$this->_dirName = dirname(__file__);
		
		// Orden por defecto de las consultas
		$this->defaultOrderBy = array('id_personal' => 'ASC', 'id_sistema' => 'ASC', 'is_primary' => 'DESC', 'id_role' => 'ASC');
		
		/** Ejecutar el constructor padre. Los parámetros del constructor son:
			* $connstr: Cadena de conexión, pueder un string o un array de la forma array(<cadena de conexión>, <usuario>, <password>)
			* $table: Nombre de la tabla en DB
			* $instdbms: (Opcional) Nombre que identifica un instancia en DB, por defecto es null
			* $dbschema: (Opcional) Nombre del esquema en DB, por defecto es null
			* $dbname: (Opcional) Nombre de la DB, por defecto es null
			* LLamada: parent::__construct($connstr, $table, $instdbms = null, $dbschema = null, $dbname = null, $dirNameDict = null);
			* Ejemplo: parent::__construct(array(CONN_STR, CONN_USR, CONN_PWD), 'prueba', null, DB_SCHEMA);
			*/
		parent::__construct(CONN_STR, 'personal_role', DB_INST, SCH_SYSCTRL);
	} // __construct()
	
	
	//***************************************************************************
	// Realizar proceso personalizado antes de getExtraData
	protected function _cm_pre_getExtraData($params) {
		// Agregar filtro para listar solo los roles del sistema actual
		for($i = 0; $i < count($this->_parentRelations); $i++) {
			if($this->_parentRelations[$i]['parent'] == 'role') {
				$this->_parentRelations[$i]['parent_fields'] = array('role_name', 'is_sys_admin', 'is_sys_dev');
				$this->_parentRelations[$i]['filter'] = "id_sistema = " . SYS_ID . " AND is_sys_dev = false";
				break;
			}
		}
		return $params;
	} // _cm_pre_getExtraData()
	
	
	/* /***************************************************************************
	// Realizar proceso personalizado despues de getExtraData
	protected function _cm_post_getExtraData($params) {
		echo '<pre>params: '; var_dump($params); echo '</pre>'; // DEBUG
		echo $this->getErrorsString() . ' .:. ' . $this->getQuery();
		return $params;
	} // _cm_post_getExtraData()*/
	
	
	//***************************************************************************
	// Realizar proceso personalizado antes de obtener los registro de la DB
	protected function _cm_pre_getData($filter) {
		// echo '<pre>filter: '; var_dump($filter); echo '</pre>'; // DEBUG
		// Agregar el ID del sistema actual al filtro y excluir desarrollador
		if(is_array($filter)) {
			if(isset($filter['WHERE'])) {
				$filter['WHERE'] .= " AND t.id_sistema = " . SYS_ID . " AND is_sys_dev = false";
			}
			else
				$filter = array_merge($filter, array('id_sistema' => SYS_ID, 'is_sys_dev' => false));
		}
		// echo '<pre>filter: '; var_dump($filter); echo '</pre>'; // DEBUG
		
		return $filter;
	} // _cm_pre_getData()
	
	
	//***************************************************************************
	// Realizar proceso personalizado antes de insertar resigtro en DB
	protected function _cm_pre_insertRecord($rowData) {
		// echo '<pre>rowData: '; var_dump($rowData); echo '</pre>'; // DEBUG
		// Agregar el ID del sistema actual y marcar como role primario
		$rowData['id_sistema'] = SYS_ID;
		$rowData['is_primary'] = true;
		// echo '<pre>rowData: '; var_dump($rowData); echo '</pre>'; // DEBUG
		
		return $rowData;
	} // _cm_pre_insertRecord()
	
	
	//***************************************************************************
	// Realizar validaciones personalizadas de insert
	protected function _cm_validateInsert($insertData) {
		// echo '<pre>insertData: '; var_dump($insertData); echo '</pre>'; // DEBUG
		// Verificar si el personal ya tiene un role asignado en el sistema actual
		$fieldData = $this->_getData("t.id_personal = {$insertData['id_personal']} AND t.id_sistema = {$insertData['id_sistema']}");
		// echo '<pre>fieldData: '; var_dump($fieldData); echo $this->getErrorsString() . '</pre>'; // DEBUG
		if($fieldData === false) {
			return false;
		}
		elseif(!empty($fieldData)) {
			$this->_errors = array("El personal {$insertData['id_personal']} ya tiene un role asignado.");
			return false;
		}
		return $insertData;
	} // _cm_validateInsert()
	
	
	//***************************************************************************
	// Realizar proceso personalizado antes de eliminar resigtro en DB
	protected function _cm_pre_deleteRecord($deleteData) {
		// echo '<pre>deleteData: '; var_dump($deleteData); echo '</pre>'; // DEBUG
		// Agregar el ID del sistema actual
		$deleteData['id_sistema'] = SYS_ID;
		// echo '<pre>deleteData: '; var_dump($deleteData); echo '</pre>'; // DEBUG
		
		return $deleteData;
	} // _cm_pre_deleteRecord()
}
?>
