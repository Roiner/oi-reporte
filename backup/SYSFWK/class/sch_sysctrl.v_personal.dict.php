<?php
// Diccionario de Datos de la tabla sch_sysctrl.v_personal

/** Claves para las especificaciones de los campos de las tablas:
	* autoincrement: Si el campo es de tipo autoincremental, seral o identity
	* autoinsert: Valor para agregar al campo automaticamente al insertar si no está presente
	* autoupdate: Valor para agregar al campo automaticamente al actualizar si no está presente
	* false: Valor FALSE para la DB. Ejemplos: 'f', 0
	* label: Etiqueta que tendrá el campo en las páginas. Ejemplo: 'Nombre'
	* maxvalue: Valor máximo que puede tomar el campo (para validación). Ejemplo: 500
	* minvalue: Valor mínimo que puede tomar el campo (para validación). Ejemplo: 100
	* optionlist: Nombre del array en _ForeignDataList que contiene las opciones posibles para el campo.
	* queryoutput: Expresión que se utilizará como salida en los querys. Ejemplo: "to_char(t.fecha, 'DD/MM/YYYY HH24:MI')"
	* required: Si el campo es requerido (para validación). Ejemplo: 'y'
	* size: Cantidad de caracteres del campo (para validación). Ejemplo: 10
	* title: Descripción del campo para el atributo title en la páginas. Ejemplo: 'Nombre del Usuario'
	* true: Valor TRUE para la DB. Ejemplos: 'y', 1
	* type: Tipo de dato en la DB (para validación). Ejemplo: 'integer'
	* userformat: Formato del campo para el usuario.
	* dbformat: Formato del campo para la DB.
	*		- Para datos tipo timestamp o date, userformat y dbformat deben ser un string soportado por DateTime::createFromFormat
	*		- Para datos tipo double o float, userformat y dbformat deben ser un array asosiativo con los campo:
	*			dec_sep (separador decimal), group_sep (separdor de miles) and decs (cantidad de decimales)
	*/

// Especificaciones de los campos
$fieldspec['id_personal'] = array(
	'label' => 'ID Personal:',
	'type' => 'integer'
);

$fieldspec['id_planta'] = array(
	'label' => 'ID Planta:',
	'type' => 'integer'
);

$fieldspec['planta'] = array(
	'label' => 'Planta:',
	'type' => 'string'
);

$fieldspec['ficha'] = array(
	'label' => 'Ficha:',
	'type' => 'integer'
);

$fieldspec['ci'] = array(
	'label' => 'Cédula:',
	'type' => 'string'
);

$fieldspec['nombre'] = array(
	'label' => 'Nombre:',
	'type' => 'string'
);

$fieldspec['extension'] = array(
	'label' => 'Extension:',
	'type' => 'string'
);

$fieldspec['nombre_extension'] = array(
	'label' => 'Nombre Extension:',
	'type' => 'string'
);

$fieldspec['clave'] = array(
	'label' => 'Clave:',
	'type' => 'string'
);

$fieldspec['telefono'] = array(
	'label' => 'Telefono:',
	'type' => 'string'
);

$fieldspec['id_cargo'] = array(
	'label' => 'ID Cargo:',
	'type' => 'integer'
);

$fieldspec['cargo'] = array(
	'label' => 'Cargo:',
	'type' => 'string'
);

$fieldspec['id_unidad'] = array(
	'label' => 'ID Unidad:',
	'type' => 'integer'
);

$fieldspec['unidad'] = array(
	'label' => 'Unidad:',
	'type' => 'string'
);

$fieldspec['id_gerencia'] = array(
	'label' => 'ID Gerencia:',
	'type' => 'integer'
);

$fieldspec['gerencia'] = array(
	'label' => 'Gerencia:',
	'type' => 'string'
);

$fieldspec['centro_costo'] = array(
	'label' => 'Centro Costo:',
	'type' => 'string'
);

$fieldspec['fecha_ingreso'] = array(
	'label' => 'Fecha Ingreso:',
	'type' => 'timestamp',
	'queryoutput' => "to_char(t.fecha_ingreso, 'DD/MM/YYYY')"
);

$fieldspec['email'] = array(
	'label' => 'Email:',
	'type' => 'string'
);

$fieldspec['id_grupo'] = array(
	'label' => 'ID Grupo:',
	'type' => 'integer'
);

$fieldspec['grupo'] = array(
	'label' => 'Grupo:',
	'type' => 'string'
);

$fieldspec['usuario'] = array(
	'label' => 'Usuario:',
	'type' => 'string'
);

$fieldspec['sup_inmediato'] = array(
	'label' => 'Sup Inmediato:',
	'type' => 'integer'
);

$fieldspec['nombre_sup_inmediato'] = array(
	'label' => 'Nombre Sup Inmediato:',
	'type' => 'string'
);

$fieldspec['sexo'] = array(
	'label' => 'Sexo:',
	'type' => 'integer'
);

$fieldspec['activo'] = array(
	'label' => 'Activo:',
	'type' => 'boolean'
);

$fieldspec['grupo_personal'] = array(
	'label' => 'Grupo Personal:',
	'type' => 'integer'
);

$fieldspec['area_personal'] = array(
	'label' => 'Area Personal:',
	'type' => 'integer'
);


// Clave primaria
$this->_primaryKey = array('id_personal');

// Claves Unicas
$this->_uniqueKeys[] = array('ficha', 'ci', 'usuario');


/** Claves para las tablas hijas:
	* schema: Nombre del schema de la tabla hija
	* child: Nombre de la tabla hija
	* fields: Array (campo_local => campo_hijo) de los campos que forman la unión con la tabla hija
	*/

// Relaciones con tablas hijas
$this->_childRelations = array(); // Array vacío en caso de no tener tablas hijas


/** Claves para las tablas padres:
	* schema: Nombre del schema de la tabla padre (opcional)
	* parent: Nombre de la tabla padre
	* alias: Alias de la tabla padre (opcional)
	* join: Cadena que forma el join (opcional). Si no se especifica se asume 'LEFT JOIN' 
	* parent_fields: Array (campo => alias) ó string de los campos de la tabla padre a incluir en las consultas
	* fields: Array (campo_local => campo_padre) ó string que forman la unión con la tabla padre
	* filter: Array ó string con el where para filtrar los registro que van al optionlist
	*/

// Relaciones con tablas padres
$this->_parentRelations = array(); 
/*$this->_parentRelations[] = array(
	'schema' => SCH_SYSCTRL,
	'parent' => 'personal_rol',
	'parent_fields' => array('id_rol' => 'id_rol'),
	'fields' => array('id_personal AND id_sistema = ' . SYS_ID => 'id_personal')
);*/
?>
