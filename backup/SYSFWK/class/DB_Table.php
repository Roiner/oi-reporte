<?php
// *****************************************************************************
// Representa una tabla en Base de Datos y constituye la clase base para las
//	entidades del sistema
// Autor: Elvin Calderón
// Versión: 2.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

// Representa una tabla en DB
class DB_Table {
	// Variables para contruir el Query
	public $sqlSelect;
	public $sqlFrom;
	public $sqlGroupBy;
	public $sqlHaving;
	public $sqlOrderBy;
	public $defaultOrderBy; // Orden por defecto de la tabla
	
	protected $_connStr; // Cadena de conexión
	protected $_instDbms; // Nombre de la instancia que representa la conexión
	
	protected $_dbms; // Manejador de Base de Datos
	protected $_dbmsName; // Nombre del manejador de Base de Datos
	protected $_dbName; // Nombre de la Base de Datos
	protected $_dbSchema; // Nombre del Esquema
	protected $_tableName; // Nombre de la tabla
	
	protected $_dirName; // nombre del directorio del script actual
	protected $_dirNameDict; // nombre directorio donde están los script '*.dict.php' (optional)
		
	/* Array de datos (valores) de los campos asociados a los controles.
			Vector simple donde cada clave representa un campo y cada valor el dato del campo. */
	protected $_fieldData;
	
/** Claves para las especificaciones de los campos de las tablas:
	* autoincrement: Si el campo es de tipo autoincremental, seral o identity
	* autoinsert: Valor para agregar al campo automaticamente al insertar si no está presente
	* autoupdate: Valor para agregar al campo automaticamente al actualizar si no está presente
	* false: Valor FALSE para la DB. Ejemplos: 'f', 0
	* label: Etiqueta que tendrá el campo en las páginas. Ejemplo: 'Nombre'
	* maxvalue: Valor máximo que puede tomar el campo (para validación). Ejemplo: 500
	* minvalue: Valor mínimo que puede tomar el campo (para validación). Ejemplo: 100
	* optionlist: Nombre del array en _ForeignDataList que contiene las opciones posibles para el campo.
	* opall: Valor y texto (array(value => '', text => '(Todos)')) de la opción Todos para la función _cm_changeConfig.
	* opsel: Valor y texto (array(value => '', text => '(Seleccione)')) de la opción Seleccione para la función _cm_changeConfig.
	* queryoutput: Expresión que se utilizará como salida en los querys. Ejemplo: "to_char(t.fecha, 'DD/MM/YYYY HH24:MI')"
	* required: Si el campo es requerido (para validación). Ejemplo: 'y'
	* size: Cantidad de caracteres del campo (para validación). Ejemplo: 10
	* title: Descripción del campo para el atributo title en la páginas. Ejemplo: 'Nombre del Usuario'
	* true: Valor TRUE para la DB. Ejemplos: 'y', 1
	* type: Tipo de dato en la DB (para validación). Ejemplo: 'integer'
	* nodb: Indicar que el campo no es de la base de datos. Ejemplo: 'y'
	* userformat: Formato del campo para el usuario.
	* dbformat: Formato del campo para la DB.
	*		- Para datos tipo timestamp o date, userformat y dbformat deben ser un string soportado por DateTime::createFromFormat
	*		- Para datos tipo double o float, userformat y dbformat deben ser un array asosiativo con los campo:
	*			dec_sep (separador decimal), group_sep (separdor de miles) and decs (cantidad de decimales)
	*/
	protected $_fieldSpec; // Array de especificación de los campos
	
	// Array de con las listas de datos foráneos según las tablas padres
	protected $_foreignDataList;
	
	protected $_primaryKey; // Clave primaria
	protected $_uniqueKeys; // Claves Unicas
	
/** Claves para las tablas hijas:
	* schema: Nombre del schema de la tabla hija
	* child: Nombre de la tabla hija
	* fields: Array (campo_local => campo_hijo) de los campos que forman la unión con la tabla hija
	*/
	protected $_childRelations; // Relaciones con tablas hijas
	
/** Claves para las tablas padres:
	* schema: Nombre del schema de la tabla padre (opcional)
	* parent: Nombre de la tabla padre
	* alias: Alias de la tabla padre (opcional)
	* join: Cadena que forma el join (opcional). Si no se especifica se asume 'LEFT JOIN' 
	* parent_fields: Array (campo {para expresiones usar parentesis} => alias), (campo) ó string de los campos de la tabla padre a incluir en las consultas (sólo si no se usa optionlist)
	* fields: Array (campo_local => campo_padre) ó string que forman la unión con la tabla padre
	* filter: Array ó string con el where para filtrar los registro que van al optionlist
	*/
	protected $_parentRelations; // Relaciones con tablas padres
	
	protected $_errors; // Array de errores

	
	//***************************************************************************
	// Constructor
	public function __construct($connstr, $table, $instdbms = null, $dbschema = null, $dbname = null, $dirNameDict = null) {
		// Guardar el nombre del directorio del script actual
		// (Debe implementarse en el constructor de la clase heredada)
		//$this->_dirName   = dirname(__file__);
				
		$this->_errors = array();
		$this->_foreignDataList = array();
		
		// Obtener nombre del manejador de base de datos
		$dbmsName = explode(':', (is_array($connstr) ? $connstr[0] : $connstr), 2);
		if(count($dbmsName) < 2 && empty($dbmsName[0]))
			throw new Exception('Falta el nombre del manejador DBMS en la cadena de conexion');
		else
			$this->_dbmsName = $dbmsName[0];
		
		$this->_dbms = null; // Manejador de Base de Datos
		$this->_instDbms = $instdbms; // Nombre de la instancia
		
		if(is_null($dbname)) {
			// Obtener el nombre de la DB (buscar 'dbname=xxxxx' en la cadena)
			if(preg_match('/(?<=dbname=)\w+/i', (is_array($connstr) ? $connstr[0] : $connstr), $regs)) {
				$this->_dbName = $regs[0];
			}
		}
		else {
			$this->_dbName = $dbname; // Nombre de la Base de Datos
		}
		
		$this->_dbSchema = $dbschema; // Nombre del Esquema
		$this->_connStr = $connstr; // Cadena de conexión
		$this->_tableName = $table; // Nombre de la tabla
		
		// Crear instancia del dbms
		require_once('dbms.' . $this->_dbmsName . '.php');
		$this->_dbms = new $this->_dbmsName($this->_connStr, $this->_dbSchema, $this->_instDbms, $this->_dbName);

		// Cargar especificaciones de los campos por defecto
		$this->_fieldSpec = $this->getFieldSpecDefault();
		$this->_dbms->fieldSpec = $this->_fieldSpec;
		
		// Establecer consulta por defecto de la tabla
		$this->setSelectDefault();
	} // __construct()
	
	
	//---------------------------------------------------------------------------
	// Funciones publicas
	//---------------------------------------------------------------------------
	
	//***************************************************************************
	// Limpiar errores anteriores
	public function clearErrors() { $this->_errors = array(); }
	
	
	//***************************************************************************
	// Funciones inspectoras
	public function getDbName() { return $this->_dbName; }
	public function getDbSchema() { return $this->_dbSchema; }
	public function getTableName() { return $this->_tableName; }
	public function getFullTableName() { return (is_null($this->_dbSchema) ? $this->_tableName : $this->_dbSchema . '.' . $this->_tableName); }
	public function getFieldData() { return $this->_fieldData; }
	public function getFieldSpec() { return $this->_fieldSpec; }
	public function getPrimaryKey() { return $this->_primaryKey; }
	public function getForeignDataList() { return $this->_foreignDataList; }
	public function getErrors() { return $this->_errors; }
	public function getErrorsString() { return implode('; ', $this->_errors); }
	public function getQuery() { return $this->_dbms->getQuery(); }
	public function getNumRows() { return $this->_dbms->getNumRows(); }
	public function getPageNo() { return $this->_dbms->getPageNo(); }
	public function getRowsPerPage() { return $this->_dbms->getRowsPerPage(); }
	public function getLastPage() { return $this->_dbms->getLastPage(); }
	
	//***************************************************************************
	// Eliminar archivo
	// $params: (array) $params['path'] Debe contener la ruta del archivo a eliminar
	public function deleteFile($params) {
		// Resultado por defecto
		$result = array('error' => true, 'code' => 1, 'info' => '$params["path"] no está definido', 'path' => '', 'realpath' => '');
		// Si está definido el path
		if(!empty($params['path'])) {
			$result['path'] = $params['path'];
			$filePath = realpath($params['path']);
			if($filePath === false) {
				$result['code'] = 2;
				$result['info'] = "Falló la ejecución de realpath('{$result['path']}')";
			}
			else {
				$result['realpath'] = $filePath;
				if(file_exists($filePath)) {
					if(unlink($filePath)) {
						$result['error'] = false;
						$result['code'] = 0;
						$result['info'] = "Archivo eliminado correctamente";
					}
					else {
						$result['code'] = 4;
						$result['info'] = "Falló la ejecución de unlink('$filePath')";
					}
				}
				else {
					$result['code'] = 3;
					$result['info'] = "No existe el archivo $filePath";
				}
			}
		}
		return $result;
	} // deleteFile()
	
	
	//***************************************************************************
	// Obtener copia de todas las variables de construcción del query en un array
	public function getAllQueryVars() {
		$queryVars['select'] = $this->sqlSelect;
		$queryVars['from'] = $this->sqlFrom;
		$queryVars['groupby'] = $this->sqlGroupBy;
		$queryVars['having'] = $this->sqlHaving;
		$queryVars['orderby'] = $this->sqlOrderBy;
		return $queryVars;
	} // getAllQueryVars()
	
	
	//***************************************************************************
	// Establecer todas las variables de construcción del query con un array
	public function setAllQueryVars($queryVars) {
		$this->sqlSelect = $queryVars['select'];
		$this->sqlFrom = $queryVars['from'];
		$this->sqlGroupBy = $queryVars['groupby'];
		$this->sqlHaving = $queryVars['having'];
		$this->sqlOrderBy = $queryVars['orderby'];
	} // getAllQueryVars()
	
	
	//***************************************************************************
	// Establecer la cantidad de registros por página
	public function setRowsPerPage($rowsPerPage) {
		$this->_dbms->setRowsPerPage($rowsPerPage);
	} // setRowsPerPage()
	
	
	//***************************************************************************
	// Establecer el número de página
	public function setPageNo($page = 1) {
		$this->_dbms->setPageNo($page);
	} // setPageNo()
	
	
	//***************************************************************************
	// Define las especificaciones de los campos por defecto
	public function getFieldSpecDefault() {
		$fieldspec = array(); // Especificaciones de los campos
		$this->_primaryKey = array(); // Clave primaria
		$this->_uniqueKeys = array(); // Claves Unicas
		$this->_childRelations = array(); // Relaciones con tablas hijas
		$this->_parentRelations = array(); // Relaciones con tablas padres
		
		// Cargar diccionario de datos
		$tablename = $this->getFullTableName();
		if(!empty($this->_dirNameDict)) {
			require($this->_dirNameDict . '/' . $tablename . '.dict.php');
		}
		else {
			require($this->_dirName . '/' . $tablename . '.dict.php');
		}
		
		return $fieldspec;
	} // getFieldSpecDefault()
	
	
	//***************************************************************************
	// Inicializar la variables usadas para construir la instrucción SELECT
	public function sqlSelectInit() {
		$this->sqlSelect = null;
		$this->sqlFrom = null;
		$this->sqlGroupBy = null;
		$this->sqlHaving = null;
		$this->sqlOrderBy = null;
	} // sqlSelectInit()
	
	
	//***************************************************************************
	// Establecer la instrucción SELECT por defecto utilizando las relaciones
	//	con las tablas padres definidas en $this->_parentRelations
	public function setSelectDefault() {
		$this->sqlSelectInit(); // Incializar variables SELECT
		
		// Preparar from con el nombre de la tabla con 't' como alias
		$from = $this->getFullTableName() . ' t';
		
		// Cargar campos de la tabla (se considera 't' como alias de la tabla)
		$select = '';
		foreach($this->_fieldSpec as $field => $spec) {
			if(!isset($spec['nodb']))
				$select .= (isset($spec['queryoutput']) ? $spec['queryoutput'] . " AS $field, " : "t.$field, ");
		}
		$select = rtrim($select, ', ');
		
		// Cargar campos de las tablas padres
		foreach($this->_parentRelations as $i => $parent) {
			$join = (isset($parent['join']) ? $parent['join'] : 'LEFT JOIN');
			$alias = (isset($parent['alias']) ? $parent['alias'] : "p$i");
			$from .= "\n $join " . $this->_getParentFullName($parent) . " $alias";
			if(is_string($parent['parent_fields'])) {
				$select .= ', ' . $parent['parent_fields'];
			}
			elseif(is_array($parent['parent_fields'])) {
				foreach($parent['parent_fields'] as $pfield => $afield) {
					/*if(is_string($pfield))
						$select .= ", $alias.$pfield AS $afield";
					else
						$select .= ", $alias.$afield";*/
					if(!is_string($pfield))
						$pfield = $afield;
					if(strpos($pfield, '(') === false)
						$pfield = "$alias.$pfield";
					else
						$pfield = str_replace('{P}', "$alias.", $pfield);
					$select .= ", $pfield AS $afield";
				}
			}
			$conditions = '';
			if(is_string($parent['fields'])) {
				$conditions = $parent['fields'];
			}
			elseif(is_array($parent['fields'])) {
				foreach($parent['fields'] as $lfield => $pfield) {
					$conditions .= "$alias.$pfield = t.$lfield AND ";
				}
			}
			$from .= ' ON (' . rtrim($conditions, ' AND ') . ')';
		}
		
		// Establecer variables
		$this->sqlSelect = $select;
		$this->sqlFrom = $from;
		$this->sqlOrderBy = $this->defaultOrderBy;
	} // setSelectDefault()
	
	
	//***************************************************************************
	// Obtener valores iniciales
	public function getInicialData($fieldData) {
		$this->_fieldData = $fieldData;
		return $fieldData;
	} // getInicialData()
	
	
	//***************************************************************************
	// Obtener valores del registro
	public function getExtraData($params = array()) {
		// Ejecutar acciones pre-getExtraData
		$params = $this->_cm_pre_getExtraData($params);
		
		// Cargar la lista de opciones según las relaciones con las tablas padres
		$this->_loadForeignDataList();
		
		// Ejecutar acciones post-getExtraData
		$params = $this->_cm_post_getExtraData($params);
		
		// Cambiar la configuración actual de acuerdo al modo (opcional)
		$this->_cm_changeConfig($params);
	} // getExtraData()
	
	
	//***************************************************************************
	// Iniciar una transacción
	public function beginTransaction() {
		$this->clearErrors(); // Limpiar errores anteriores
		
		// Iniciar transacción
		$result = $this->_dbms->beginTransaction($this->_dbSchema);
		if(!$result) {
			$this->_errors = $this->_dbms->getErrors();
			return false;
		}
		return $result;
	} // beginTransaction()
	
	
	//***************************************************************************
	// Ejecutar transacción
	public function commit() {
		$result = $this->_dbms->commit();
		if(!$result) {
			$this->_errors = $this->_dbms->getErrors();
			return false;
		}
		return $result;
	} // commit()
	
	
	//***************************************************************************
	// Cancelar transacción
	public function rollBack() {
		$result = $this->_dbms->rollBack();
		if(!$result) {
			$this->_errors = $this->_dbms->getErrors();
			return false;
		}
		return $result;
	} // rollBack()
	
	
	//***************************************************************************
	// Obtiene la cantidad de registros que satisfacen el criterio de selección en $where
	public function getCount($where = '', $params = array()) {
		// Ejecutar getCount
		$result = $this->_dbms->getCount($this->_dbSchema, $this->getFullTableName(), $where, $params);
		if($result === false) {
			$this->_errors = $this->_dbms->getErrors();
			return false;
		}
		// Resultado
		return $result;
	} // getCount()
	
	
	//***************************************************************************
	// Obtiene el máximo de un campo que satisfacen el criterio de selección en $where
	public function getMaxValue($field, $where = '', $params = array()) {
		// Ejecutar getMaxValue
		$result = $this->_dbms->getMaxValue($this->_dbSchema, $this->getFullTableName(), $field, $where, $params);
		if($result === false) {
			$this->_errors = $this->_dbms->getErrors();
			return false;
		}
		// Resultado
		return $result;
	} // getMaxValue()
	
	
	//***************************************************************************
	// Obtiene el mínimo de un campo que satisfacen el criterio de selección en $where
	public function getMinValue($field, $where = '', $params = array()) {
		// Ejecutar getMaxValue
		$result = $this->_dbms->getMinValue($this->_dbSchema, $this->getFullTableName(), $field, $where, $params);
		if($result === false) {
			$this->_errors = $this->_dbms->getErrors();
			return false;
		}
		// Resultado
		return $result;
	} // getMinValue()
	
	
	//***************************************************************************
	// Obtener registros
	// $filter: Array (campo->valor) ó Array ('WHERE'->'where', 'PARAMS'->array()) ó Cadena que contituye el where
	//	También puede contener todo el _POST por lo que debe limpiarse en _cm_pre_getData
	public function getRecords($filter = array()) {
		$this->clearErrors(); // Limpiar errores anteriores
		
		// Eliminar formato
		if(is_array($filter)) {
			if(isset($filter['PARAMS'])) {
				$filterData = $filter['PARAMS'];
				$filter['PARAMS'] = $this->unFormatData($filterData);
			}
			else
				$filter = $this->unFormatData($filter);
		}
		
		// Configuracion preGetData, puede modificar variables SELECT, FROM, GROUPBY, HAVING, ORDER
		$where = $this->_cm_pre_getData($filter);
		if($where === false) return false;
		
		// Ejecutar query
		$fieldData = $this->_getData($where);
		if($fieldData === false) return false;
		
		// Procesamiento post
		$fieldData = $this->_cm_post_getData($fieldData, $filter);
		if($fieldData === false) return false;
		
		// Formatear resultado
		$fieldData = $this->formatData($fieldData);
		
		// Guardar resultado
		$this->_fieldData = $fieldData;
		
		return $fieldData;
	} // getRecords()
	
	
	//***************************************************************************
	// Prepara un array de datos para ser utilizado por la DB
	public function unFormatData($fieldData) {
		// Si $fieldData es un registro
		if(is_string(key($fieldData))) {
			$this->_unFormatData($fieldData);
		}
		else {
			// $fieldData es una tabla de registros
			foreach($fieldData as $key => $value) {
				$this->_unFormatData($value);
				$fieldData[$key] = $value;
			}
		}
		return $fieldData;
	} // unFormatData()
	
	
	//***************************************************************************
	// Prepara un array de datos para ser utilizado mostrado al usuario
	public function formatData($fieldData) {
		// Si $fieldData es un registro
		if(is_string(key($fieldData))) {
			$this->_formatData($fieldData);
		}
		else {
			// $fieldData es una tabla de registros
			foreach($fieldData as $key => $value) {
				$this->_formatData($value);
				$fieldData[$key] = $value;
			}
		}
		return $fieldData;
	} // formatData()
	
	
	//***************************************************************************
	// Eliminar del cualquier campo no definido en las especificaciones
	public function removeExtraData($fieldData) {
		// Si $fieldData es un registro
		if(is_string(key($fieldData))) {
			$this->_removeExtraData($fieldData);
		}
		else {
			// $fieldData es una tabla de registros
			foreach($fieldData as $key => $value) {
				$this->_removeExtraData($value);
				$fieldData[$key] = $value;
			}
		}
		return $fieldData;
	} // removeExtraData()
	
	
	//***************************************************************************
	// Insertar un registro
	// Retorna el fieldData insertado ó false en caso de error
	public function insertRecord($fieldData) {
		$this->clearErrors(); // Limpiar errores anteriores
		
		// unFormatData
		$fieldData = $this->unFormatData($fieldData);
		
		// Copiar datos
		$insertData = $fieldData;
		
		// Ejecutar pre-insert personalizado
		$insertData = $this->_cm_pre_insertRecord($insertData);
		if($insertData === false) return $insertData;
		
		// // Ejecutar validación primaria (se valida en javascript)
		// $insertData = $this->validateInsert($insertData);
		
		// Ejecutar validación común personalizada
		$insertData = $this->_cm_commonValidation($insertData);
		if($insertData === false) return $insertData;
		
		// Ejecutar validación de insert personalizada
		$insertData = $this->_cm_validateInsert($insertData);
		if($insertData === false) return $insertData;
		
		// Insertar registro
		$inserted = $this->_dbms_insertRecord($insertData);
		if($inserted === false) return $inserted;
		
		// Unir el array resultante
		$fieldData = array_merge($fieldData, $inserted);
		
		// Ejecutar post-insert personalizado
		$fieldData = $this->_cm_post_insertRecord($fieldData);
		if($fieldData === false) return $fieldData;
		
		// Formatear resultado
		$fieldData = $this->formatData($fieldData);
		
		// Guardar resultado
		$this->_fieldData = $fieldData;
		
		// Resultado
		return $fieldData;
	} // insertRecord()
	
	
	//***************************************************************************
	// Insertar varios registros
	// Retorna el fieldData insertado ó false en caso de error
	public function insertMultiRecord($fieldData) {
		$this->clearErrors(); // Limpiar errores anteriores
		
		// unFormatData
		$fieldData = $this->unFormatData($fieldData);
		
		// Copiar datos
		$insertData = $fieldData;
		
		// Ejecutar pre-insert personalizado
		$insertData = $this->_cm_pre_insertMultiRecord($insertData);
		if($insertData === false) return $insertData;
		
		// Insertar registros
		$inserted = $this->_dbms_insertMultiRecord($insertData);
		if($inserted === false) return $inserted;
		
		// Unir el array resultante
		for($i = 0; $i < count($inserted); $i++) {
			$fieldData[$i] = array_merge($fieldData[$i], $inserted[$i]);
		}
		
		// Ejecutar post-insert personalizado
		$fieldData = $this->_cm_post_insertMultiRecord($fieldData);
		if($fieldData === false) return $fieldData;
		
		// Formatear resultado
		$fieldData = $this->formatData($fieldData);
		
		// Guardar resultado
		$this->_fieldData = $fieldData;
		
		// Resultado
		return $fieldData;
	} // insertMultiRecord()
	
	
	//***************************************************************************
	// Modificar un registro
	// Retorna el fieldData insertado ó false en caso de error
	public function updateRecord($fieldData, $fieldDataOld = array()) {
		$this->clearErrors(); // Limpiar errores anteriores
		
		// unFormatData
		$fieldData = $this->unFormatData($fieldData);
		if(!empty($fieldDataOld))
			$fieldDataOld = $this->unFormatData($fieldDataOld);
		
		// Copiar datos
		$updateData = $fieldData;
		
		// Ejecutar pre-update personalizado
		$updateData = $this->_cm_pre_updateRecord($updateData);
		if($updateData === false) return $updateData;
		
		// // Ejecutar validación primaria (se valida en javascript)
		// $updateData = $this->validateUpdate($updateData);
		
		// Ejecutar validación común personalizada
		$updateData = $this->_cm_commonValidation($updateData);
		if($updateData === false) return $updateData;
		
		// Ejecutar validación de updateData personalizada
		$updateData = $this->_cm_validateUpdate($updateData);
		if($updateData === false) return $updateData;
		
		// Actualizar registro
		$updated = $this->_dbms_updateRecord($updateData, $fieldDataOld);
		if($updated === false) return $updated;
		
		// Unir el array resultante
		$fieldData = array_merge($fieldData, $updated);
		
		// Ejecutar post-update personalizado
		$fieldData = $this->_cm_post_updateRecord($fieldData);
		if($fieldData === false) return $fieldData;
		
		// Formatear resultado
		$fieldData = $this->formatData($fieldData);
		
		// Guardar resultado
		$this->_fieldData = $fieldData;
		
		// Resultado
		return $fieldData;
	} // updateRecord()
	
	
	//***************************************************************************
	// Modificar varios registros
	// Retorna el fieldData insertado ó false en caso de error
	public function updateMultiRecord($fieldData) {
		$this->clearErrors(); // Limpiar errores anteriores
		
		// unFormatData
		$fieldData = $this->unFormatData($fieldData);
		
		// Copiar datos
		$updateData = $fieldData;
		
		// Ejecutar pre-update personalizado
		$updateData = $this->_cm_pre_updateMultiRecord($updateData);
		if($updateData === false) return $updateData;
		
		// Actualizar registros
		$updated = $this->_dbms_updateMultiRecord($updateData);
		if($updated === false) return $updated;
		
		// Unir el array resultante
		for($i = 0; $i < count($updated); $i++) {
			$fieldData[$i] = array_merge($fieldData[$i], $updated[$i]);
		}
		
		// Ejecutar post-update personalizado
		$fieldData = $this->_cm_post_updateMultiRecord($fieldData);
		if($fieldData === false) return $fieldData;
		
		// Formatear resultado
		$fieldData = $this->formatData($fieldData);
		
		// Guardar resultado
		$this->_fieldData = $fieldData;
		
		// Resultado
		return $fieldData;
	} // updateMultiRecord()
	
	
	//***************************************************************************
	// Modificar registros basado en un where libre
	// $fieldData: Array (campo->valor) de datos a actualizar
	// $where: Cadena WHERE SQL
	// Retorna el fieldData insertado ó false en caso de error
	public function updateData($fieldData, $where) {
		$this->clearErrors(); // Limpiar errores anteriores
		
		// unFormatData
		$fieldData = $this->unFormatData($fieldData);
		
		// Copiar datos
		$updateData = $fieldData;
		
		// Ejecutar pre-update personalizado
		$updateData = $this->_cm_pre_updateData($updateData, $where);
		if($updateData === false) return $updateData;
		
		// Actualizar registros
		$updated = $this->_dbms_updateData($updateData, $where);
		if($updated === false) return $updated;
		
		// Unir el array resultante
		$fieldData = array_merge($fieldData, $updated);
		
		// Ejecutar post-update personalizado
		$fieldData = $this->_cm_post_updateData($fieldData);
		if($fieldData === false) return $fieldData;
		
		// Formatear resultado
		$fieldData = $this->formatData($fieldData);
		
		// Guardar resultado
		$this->_fieldData = $fieldData;
		
		// Resultado
		return $fieldData;
	} // updateData()
	
	
	//***************************************************************************
	// Eliminar un registro
	// $fieldData: Array (campo->valor) de datos a utilizar como clave en where
	// Retorna el fieldData eliminado ó false en caso de error
	public function deleteRecord($fieldData) {
		$this->clearErrors(); // Limpiar errores anteriores
		
		// unFormatData
		$fieldData = $this->unFormatData($fieldData);
		
		// Copiar datos
		$deleteData = $fieldData;
		
		// Ejecutar pre-delete personalizado
		$deleteData = $this->_cm_pre_deleteRecord($deleteData);
		if($deleteData === false) return $deleteData;
		
		// Verificar que el regritro se pueda eliminar
		if(!$this->_validateDelete($deleteData)) {
			return false;
		}
		
		// Eliminar registro
		$deleted = $this->_dbms_deleteRecord($deleteData);
		if($deleted === false) return $deleted;
		
		// Unir el array resultante
		$fieldData = array_merge($fieldData, $deleted);
		
		// Ejecutar post-delete personalizado
		$fieldData = $this->_cm_post_deleteRecord($fieldData);
		if($fieldData === false) return $fieldData;
		
		// Formatear resultado
		$fieldData = $this->formatData($fieldData);
		
		// Guardar resultado
		$this->_fieldData = $fieldData;
		
		// Resultado
		return $fieldData;
	} // deleteRecord()
	
	
	//***************************************************************************
	// Eliminar varios registros
	public function deleteMultiRecord($fieldData) {
		$this->clearErrors(); // Limpiar errores anteriores
		
		// unFormatData
		$fieldData = $this->unFormatData($fieldData);
		
		// Copiar datos
		$deleteData = $fieldData;
		
		// Ejecutar pre-delete personalizado
		$deleteData = $this->_cm_pre_deleteMultiRecord($deleteData);
		if($deleteData === false) return $deleteData;
		
		// Eliminar registros
		$deleted = $this->_dbms_deleteMultiRecord($deleteData);
		if($deleted === false) return $deleted;
		
		// Unir el array resultante
		for($i = 0; $i < count($deleted); $i++) {
			$fieldData[$i] = array_merge($fieldData[$i], $deleted[$i]);
		}
		
		// Ejecutar post-delete personalizado
		$fieldData = $this->_cm_post_deleteMultiRecord($fieldData);
		if($fieldData === false) return $fieldData;
		
		// Formatear resultado
		$fieldData = $this->formatData($fieldData);
		
		// Guardar resultado
		$this->_fieldData = $fieldData;
		
		// Resultado
		return $fieldData;
	} // deleteMultiRecord()
	
	
	//***************************************************************************
	// Eliminar registros basados en un where libre
	public function deleteData($where) {
		$this->clearErrors(); // Limpiar errores anteriores
		
		// Ejecutar pre-delete personalizado
		$where = $this->_cm_pre_deleteData($where);
		if($where === false) return $where;
		
		// Eliminar registro
		$deleted = $this->_dbms_deleteData($where);
		if($deleted === false) return $deleted;
		
		// Ejecutar post-update personalizado
		$fieldData = $this->_cm_post_deleteData($deleted);
		if($fieldData === false) return $fieldData;
		
		// Formatear resultado
		$fieldData = $this->formatData($fieldData);
		
		// Guardar resultado
		$this->_fieldData = $fieldData;
		
		// Resultado
		return $fieldData;
	} // deleteData()
	
	
	//---------------------------------------------------------------------------
	// Funciones protegidas
	//---------------------------------------------------------------------------
	
	//***************************************************************************
	// Obtener registros
	// $where: Array (campo->valor) ó Array ('WHERE'->'where', 'PARAMS'->array()) ó Cadena que contituye el where
	protected function _getData($where = '') {
		// Preparar variables SQL
		$this->_dbms->sqlSelect = $this->sqlSelect;
		$this->_dbms->sqlFrom = $this->sqlFrom;
		$this->_dbms->sqlGroupBy = $this->sqlGroupBy;
		$this->_dbms->sqlHaving = $this->sqlHaving;
		$sort_str = '';
		if(empty($this->sqlOrderBy)) {
			$sort_str = '';
		}
		elseif(is_string($this->sqlOrderBy)) {
			$sort_str = $this->sqlOrderBy;
		}
		elseif(is_array($this->sqlOrderBy)) {
			foreach($this->sqlOrderBy as $k => $v) {
				$sort_str .= "$k $v, ";
			}
			$sort_str = rtrim($sort_str, ', ');
		}
		$this->_dbms->sqlOrderBy = $sort_str;
		
		// Preparar WHERE
		$whereStr = '';
		$params = array();
		if(is_string($where)) {
			$whereStr = $where;
		}
		elseif(is_array($where)) {
			if(isset($where['WHERE']) && isset($where['PARAMS']) && is_string($where['WHERE']) && is_array($where['PARAMS'])) {
				$whereStr = $where['WHERE'];
				$params = $where['PARAMS'];
			}
			elseif(!empty($where)) {
				$whereStr = $this->_buildWhere(array_keys($where), '=', 'AND');
				$params = $where;
			}
		}
		
		// Ejecutar query
		$fieldData = $this->_dbms->getData($this->_dbSchema, $this->getFullTableName(), $whereStr, $params);
		if($fieldData === false) {
			$this->_errors[] = $this->_dbms->getErrorsString();
		}
		$this->_fieldData = $fieldData;
		
		return $fieldData;
	} // _getData()
	
	
	//***************************************************************************
	// Obtener el nombre completo de una tabla hija en _childRelations
	// $child: Una tabla hija de _childRelations
	protected function _getChildFullName($child) {
		$tablename = (isset($child['schema']) ? $child['schema'] . '.' . $child['child'] : (
			is_null($this->_dbSchema) ? $child['child'] : $this->_dbSchema . '.' . $child['child']
		));
		return $tablename;
	} // _getChildFullName()
	
	
	//***************************************************************************
	// Obtener el nombre completo de una tabla padre en _parentRelations
	// $parent: Un padre de _parentRelations
	protected function _getParentFullName($parent) {
		$tablename = (isset($parent['schema']) ? $parent['schema'] . '.' . $parent['parent'] : (
			is_null($this->_dbSchema) ? $parent['parent'] : $this->_dbSchema . '.' . $parent['parent']
		));
		return $tablename;
	} // _getParentFullName()
	
	
	//***************************************************************************
	// Contruye una cadena para utilizar en la instrucción WHERE
	// $fields: Array de campos ó campo->valor
	// $oper: Operador de comparación
	// $join: Operador lógico entre comparaciones
	protected function _buildWhere($fields, $oper, $join) {
		$whereSql = '';
		foreach($fields as $k => $v) {
			if(is_string($k)) {
				$alias = $this->_getFieldTableAlias($k);
				$where = "$k $oper " . $this->_dbms->quote($v) . " $join ";
			}
			else {
				$alias = $this->_getFieldTableAlias($v);
				$where = "$v $oper :$v $join ";
			}
			if(!empty($alias))
				$alias .= '.';
			$whereSql .= $alias . $where;
		}
		return rtrim($whereSql, " $join ");
	} // _buildWhere()
	
	
	//***************************************************************************
	// Obtiene el alias de la tabla correspondiente al campo
	// $field: Nombre del campo
	protected function _getFieldTableAlias($field) {
		$alias = '';
		// Para los campos de la tabla se considera 't' como alias
		if(array_key_exists($field, $this->_fieldSpec)) {
			$alias = 't';
		}
		else {
			// Buscar en las relaciones con tablas padre
			$found = false;
			foreach($this->_parentRelations as $i => $parent) {
				if(is_string($parent['parent_fields']) && strpos($parent['parent_fields'], $field) !== false)
					$found = true;
				elseif(is_array($parent['parent_fields'])) {
					foreach($parent['parent_fields'] as $pfield => $afield) {
						if(is_string($pfield) && $pfield == $field)
							$found = true;
						elseif($afield == $field)
							$found = true;
					}
				}
				if($found) {
					$alias = (isset($parent['alias']) ? $parent['alias'] : "p$i");
					break;
				}
			}
		}
		return $alias;
	} // _getFieldTableAlias()
	
	
	//***************************************************************************
	// Cargar la lista de datos foráneos según las relaciones con las tablas padres
	protected function _loadForeignDataList() {
		// Guardar variables query original
		$queryVarsOrg = $this->getAllQueryVars();
		$rowsPerPage = $this->getRowsPerPage();
		$this->setRowsPerPage(0);
		
		// Obtener los datos de cada relación
		foreach($this->_parentRelations as $parent)	{
			// Verificar si está definido el optionlist en _fieldSpec
			if(is_array($parent['fields'])) {
				$foreignkey = array_keys($parent['fields']);
				if(isset($this->_fieldSpec[$foreignkey[0]]['optionlist'])) {
					// Obtener tabla padre
					$tablename = $this->_getParentFullName($parent) . ' t';
					
					// Obtener campos text y valor
					$text = array_keys($parent['parent_fields']);
					if(!is_string($text[0]))
						$text[0] = $parent['parent_fields'][0];
					$value = array_values($parent['fields']);
					// $fields = "{$value[0]} as value, {$text[0]} as text";
					$fields = "{$value[0]} as value, " . str_replace('{P}', 't.', $text[0]) . " as text";
					
					// Obtener filtro
					$filter = (isset($parent['filter']) ? $parent['filter'] : null);
					
					// Obtener orden
					$order = (isset($parent['order']) ? $parent['order'] : array('text' => 'ASC'));
					
					// Obtener datos
					$this->sqlSelect = $fields;
					$this->sqlFrom = $tablename;
					$this->sqlOrderBy = $order;
					$result = $this->_getData($filter);
					
					// Agregar a la lista
					$this->_foreignDataList[$this->_fieldSpec[$foreignkey[0]]['optionlist']] = $result;
				}
			}
		}
		
		// Restaurar variables query original
		$this->setAllQueryVars($queryVarsOrg);
		$this->setRowsPerPage($rowsPerPage);
	} // _loadForeignDataList()
	
	
	//***************************************************************************
	// Prepara un array de datos para ser utilizado por la DB
	protected function _unFormatData(&$fieldData) {
		$s = array("\\'", '\\"', '\\\\'); $r = array("'", '"', '\\');
		// Revisar al especificacion de los campos
		foreach($this->_fieldSpec as $field => $spec) {
			if(isset($spec['userformat']) && isset($spec['dbformat'])) {
				// Si el campo existe y es una cadena
				if(isset($fieldData[$field]) && is_string($fieldData[$field]) && !empty($fieldData[$field])) {
					// Cambiar formato de acuerdo al tipo de dato
					switch($spec['type']) {
						case 'timestamp': case 'date':
							// echo '<pre>$spec'; var_dump($spec); echo '</pre>'; // DEBUG
							$date = change_date_format($fieldData[$field], $spec['userformat'], $spec['dbformat']);
							$fieldData[$field] = ($date === false ? $fieldData[$field] : $date);
							break;
						case 'double': case 'float': case 'numeric':
							if(is_array($spec['userformat']) && is_array($spec['dbformat'])) {
								// Quitar separador de grupo
								$fieldData[$field] = str_replace($spec['userformat']['group_sep'], '', $fieldData[$field]);
								// Cambiar separador decimal
								$fieldData[$field] = str_replace($spec['userformat']['dec_sep'], $spec['dbformat']['dec_sep'], $fieldData[$field]);
							}
							break;
					}
				}
			}
			// Eliminar espacios en blanco y otros caracteres
			if(isset($fieldData[$field]) && is_string($fieldData[$field])) {
				// Cambiar formato de acuerdo al tipo de dato
				switch($spec['type']) {
					case 'string':
						$fieldData[$field] = str_replace($s, $r, trim($fieldData[$field]));
						break;
				}
				// Convertir cadenas vacías en null
				if(strlen($fieldData[$field]) == 0) {
					$fieldData[$field] = null;
				}
			}
		}
	} // _unFormatData()
	
	
	//***************************************************************************
	// Prepara un array de datos para ser utilizado mostra al usuario
	protected function _formatData(&$fieldData) {
		// Revisar al especificacion de los campos
		foreach($this->_fieldSpec as $field => $spec) {
			// if(isset($spec['userformat']) && isset($spec['dbformat']) && !isset($spec['queryoutput'])) {
			if(isset($spec['userformat']) && isset($spec['dbformat'])) {
				// Si el campo existe
				if(isset($fieldData[$field])) {
					// Cambiar formato de acuerdo al tipo de dato
					switch($spec['type']) {
						case 'timestamp': case 'date':
							$date = change_date_format($fieldData[$field], $spec['dbformat'], $spec['userformat']);
							$fieldData[$field] = ($date === false ? $fieldData[$field] : $date);
							break;
						case 'double': case 'float': case 'numeric':
							if(is_array($spec['userformat']) && is_array($spec['dbformat'])) {
								$fieldData[$field] = number_format($fieldData[$field], $spec['userformat']['decs'],
									$spec['userformat']['dec_sep'], $spec['userformat']['group_sep']);
							}
							break;
					}
				}
			}
		}
	} // _formatData()
	
	
	//***************************************************************************
	// Eliminar del cualquier campo no definido en las especificaciones
	protected function _removeExtraData(&$fieldData) {
		// Remover cualquier campo que no sea de la DB
		foreach($fieldData as $field => $fieldvalue) {
			if(!array_key_exists($field, $this->_fieldSpec) || isset($this->_fieldSpec[$field]['nodb'])) {
				unset($fieldData[$field]);
			}
		}
	} // removeExtraData()
	
	
	//***************************************************************************
	// Ejecutar el insert de un registro
	// Retorna el fieldData insertado ó false en caso de error
	protected function _dbms_insertRecord($fieldData) {
		// Remover datos que no son de la DB
		$fieldData = $this->removeExtraData($fieldData);
		// Insertar registro
		$fieldData = $this->_dbms->insertRecord($this->_dbSchema, $this->getFullTableName(), $fieldData);
		if($fieldData === false) $this->_errors = $this->_dbms->getErrors();
		// Resultado
		return $fieldData;
	} // _dbms_insertRecord()
	
	
	//***************************************************************************
	// Insertar varios registros
	// Retorna el fieldData insertado ó false en caso de error
	protected function _dbms_insertMultiRecord($fieldData) {
		// Remover datos que no son de la DB
		$fieldData = $this->removeExtraData($fieldData);
		// Insertar registros
		$fieldData = $this->_dbms->insertMultiRecord($this->_dbSchema, $this->getFullTableName(), $fieldData);
		if($fieldData === false) $this->_errors = $this->_dbms->getErrors();
		// Resultado
		return $fieldData;
	} // _dbms_insertMultiRecord()
	
	
	//***************************************************************************
	// Ejecutar el update de un registro basado en la clave primaria
	// Retorna el fieldData actulizado ó false en caso de error
	protected function _dbms_updateRecord($fieldData, $fieldDataOld = array()) {
		// Remover datos que no son de la DB
		$fieldData = $this->removeExtraData($fieldData);
		
		$updateData = $fieldData; // Copiar array de datos
		
		// Determinar clave primaria
		$primaryKey = array();
		foreach($this->_primaryKey as $fieldvalue) {
			// Si el campo no está en fieldDataOld se toma de fieldData
			if(isset($fieldDataOld[$fieldvalue]))
				$primaryKey[$fieldvalue] = $fieldDataOld[$fieldvalue];
			elseif(isset($fieldData[$fieldvalue])) {
				$primaryKey[$fieldvalue] = $fieldData[$fieldvalue];
				unset($updateData[$fieldvalue]); // Quitar el pk del array a actualizar
			}
			else {
				$this->_errors[] = "Falta valor de clave primaria del campo '$fieldvalue'";
				return false;
			}
		}
		
		// Eliminar campos que no cambian
		foreach($updateData as $field => $fieldvalue) {
			if(isset($fieldDataOld[$field]) && $updateData[$field] === $fieldDataOld[$field]) {
				//echo '<pre>'; echo $updateData[$field] . ' === ' . $fieldDataOld[$field] . ' : '; var_dump($updateData[$field] === $fieldDataOld[$field]); echo '</pre>';
				unset($updateData[$field]);
			}
		}
		
		// Si no hay cambios no actualiza
		if(count($updateData) == 0) {
			$this->_errors[] = "No hay campos que actualizar";
			return $fieldData;
		}
		
		// Actualizar registro
		$fieldData = $this->_dbms->updateRecord($this->_dbSchema, $this->getFullTableName(), $updateData, $primaryKey);
		if($fieldData === false) $this->_errors = $this->_dbms->getErrors();
		
		// Resultado
		return $fieldData;
	} // _dbms_updateRecord()
	
	
	//***************************************************************************
	// Ejecutar el update de varios registro basado en la clave primaria
	// Retorna el fieldData actulizado ó false en caso de error
	protected function _dbms_updateMultiRecord($fieldData) {
		// Remover datos que no son de la DB
		$fieldData = $this->removeExtraData($fieldData);
		
		$updateData = $fieldData; // Copiar array de datos
		
		// Determinar clave primaria
		$primaryKey = array();
		foreach($this->_primaryKey as $fieldvalue) {
			for($i = 0; $i < count($fieldData); $i++) {
				if(isset($fieldData[$i][$fieldvalue])) {
					$primaryKey[$i][$fieldvalue] = $fieldData[$i][$fieldvalue];
					unset($updateData[$i][$fieldvalue]); // Eliminar clave primaria
				}
				else {
					$this->_errors[] = "Falta valor de clave primaria del campo '$fieldvalue' ($i)";
					return false;
				}
			}
		}
		
		// Actualizar registro
		$fieldData = $this->_dbms->updateMultiRecord($this->_dbSchema, $this->getFullTableName(), $updateData, $primaryKey);
		if($fieldData === false) $this->_errors = $this->_dbms->getErrors();
		
		// Resultado
		return $fieldData;
	} // _dbms_updateMultiRecord()
	
	
	//***************************************************************************
	// Ejecutar el update de registros basado en un where libre
	// Retorna el fieldData actulizado ó false en caso de error
	protected function _dbms_updateData($fieldData, $where) {
		// Remover datos que no son de la DB
		$fieldData = $this->removeExtraData($fieldData);
		
		// Actualizar registro
		$fieldData = $this->_dbms->updateData($this->_dbSchema, $this->getFullTableName(), $fieldData, $where);
		if($fieldData === false) $this->_errors = $this->_dbms->getErrors();
		
		// Resultado
		return $fieldData;
	} // _dbms_updateData()
	
	
	//***************************************************************************
	// Ejecutar el delete de un registro basado en la clave primaria
	// Retorna el fieldData eliminado ó false en caso de error
	protected function _dbms_deleteRecord($fieldData) {
		// Determinar clave primaria
		$primaryKey = array();
		foreach($this->_primaryKey as $fieldvalue) {
			if(isset($fieldData[$fieldvalue]))
				$primaryKey[$fieldvalue] = $fieldData[$fieldvalue];
			else {
				$this->_errors[] = "Falta valor de clave primaria del campo '$fieldvalue'";
				return false;
			}
		}
		
		// Eliminar registro
		$fieldData = $this->_dbms->deleteRecord($this->_dbSchema, $this->getFullTableName(), $primaryKey);
		if($fieldData === false) $this->_errors = $this->_dbms->getErrors();
		
		// Resultado
		return $fieldData;
	} // _dbms_deleteRecord()
	
	
	//***************************************************************************
	// Ejecutar el delete de varios registro basado en la clave primaria
	// Retorna el fieldData eliminado ó false en caso de error
	protected function _dbms_deleteMultiRecord($fieldData) {
		// Determinar clave primaria
		$primaryKey = array();
		foreach($this->_primaryKey as $fieldvalue) {
			for($i = 0; $i < count($fieldData); $i++) {
				if(isset($fieldData[$i][$fieldvalue]))
					$primaryKey[$i][$fieldvalue] = $fieldData[$i][$fieldvalue];
				else {
					$this->_errors[] = "Falta valor de clave primaria del campo '$fieldvalue' ($i)";
					return false;
				}
			}
		}
		
		// Eliminar registro
		$fieldData = $this->_dbms->deleteMultiRecord($this->_dbSchema, $this->getFullTableName(), $primaryKey);
		if($fieldData === false) $this->_errors = $this->_dbms->getErrors();
		
		// Resultado
		return $fieldData;
	} // _dbms_deleteMultiRecord()
	
	
	//***************************************************************************
	// Ejecutar el delete de registros basado en un where libre
	// Retorna el fieldData eliminado ó false en caso de error
	protected function _dbms_deleteData($where) {
		// Actualizar registro
		$fieldData = $this->_dbms->deleteData($this->_dbSchema, $this->getFullTableName(), $where);
		if($fieldData === false) $this->_errors = $this->_dbms->getErrors();
		// Resultado
		return $fieldData;
	} // _dbms_deleteData()
	
	
	//***************************************************************************
	// Verificar que un registro pueda ser eliminado
	protected function _validateDelete($fieldData) {
		// Retornar true si no hay relación con tablas hijas
		if(empty($this->_childRelations))
			return true;
			
		// Guardar variables query original
		$queryVarsOrg = $this->getAllQueryVars();
		$rowsPerPage = $this->getRowsPerPage();
		$this->setRowsPerPage(0);
		
		// Obtener los datos de cada relación con tablas hijas
		$result = true;
		foreach($this->_childRelations as $child)	{
			// Obtener clave
			$foreignkey = array_keys($child['fields']);
			
			// Obtener tabla hija
			$tablename = $this->_getChildFullName($child);
			
			// Obtener campos
			$fields = implode(', ', array_values($child['fields']));
			
			// Obtener filtro
			$filter = array();
			foreach($foreignkey as $key) {
				$filter[$child['fields'][$key]] = $fieldData[$key];
			}
			
			// Obtener datos
			$this->sqlSelectInit();
			$this->sqlSelect = $fields;
			$this->sqlFrom = $tablename . ' t';
			$childData = $this->_getData($filter);
			if($childData === false) {
				$result = false; // Error al consultar tabla hija
				break;
			}
			elseif(!empty($childData)) {
				$this->_errors[] = "No se puede eliminar el registro porque tiene información relacionada en la tabla $tablename";
				$result = false;
				break;
			}
		}
		
		// Restaurar variables query original
		$this->setAllQueryVars($queryVarsOrg);
		$this->setRowsPerPage($rowsPerPage);
		
		return $result;
	} // _validateDelete()
	
	
	//***************************************************************************
	// Ejecutar un query libre con la funcion executeQuery del dbms
	protected function _executeQuery($query, $params = array()) {
		// Ejecutar consulta
		$result = $this->_dbms->executeQuery($this->_dbSchema, $query, $params);
		if($result === false) $this->_errors = $this->_dbms->getErrors();
		// Resultado
		return $result;
	} // _executeQuery()
	
	
	//***************************************************************************
	// Ejecutar un query multiple libre con la funcion executeMultiQuery del dbms
	protected function _executeMultiQuery($query, $params = array()) {
		// Ejecutar consulta
		$result = $this->_dbms->executeMultiQuery($this->_dbSchema, $query, $params);
		if($result === false) $this->_errors = $this->_dbms->getErrors();
		// Resultado
		return $result;
	} // _executeMultiQuery()
	
	
	//---------------------------------------------------------------------------
	// Funciones de configuración personalizables '_cm_'
	//---------------------------------------------------------------------------
	
	
	//***************************************************************************
	// Realizar proceso personalizado antes de getExtraData
	protected function _cm_pre_getExtraData($params) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $params;
	} // _cm_pre_getExtraData()
	
	
	//***************************************************************************
	// Realizar proceso personalizado despues de getExtraData
	protected function _cm_post_getExtraData($params) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $params;
	} // _cm_post_getExtraData()
	
	
	//***************************************************************************
	// Para implementar un cambio particular en las especificaciones de una tabla
	protected function _cm_changeConfig($params) {
		$foreignDataList = $this->_foreignDataList;
		$fieldSpec = $this->_fieldSpec;
		
		// Configuración de acuerdo al task
		switch($params['pattern_type']) {
			case 'list1':
				// Agregar opcion todos si está definida
				foreach($foreignDataList as $list => $options) {
					foreach($fieldSpec as $field => $spec) {
						if(isset($fieldSpec[$field]['optionlist']) && $fieldSpec[$field]['optionlist'] == $list) {
							if(isset($fieldSpec[$field]['opall']['value']) && isset($fieldSpec[$field]['opall']['text'])) {
								array_unshift($foreignDataList[$list], $fieldSpec[$field]['opall']);
								break;
							}
						}
					}
				}
				break;
			case 'add1':
				// Agregar opcion seleccione
				foreach($foreignDataList as $list => $options) {
					foreach($fieldSpec as $field => $spec) {
						if(isset($fieldSpec[$field]['optionlist']) && $fieldSpec[$field]['optionlist'] == $list) {
							if(isset($fieldSpec[$field]['opsel']['value']) && isset($fieldSpec[$field]['opsel']['text'])) {
								array_unshift($foreignDataList[$list], $fieldSpec[$field]['opsel']);
								break;
							}
						}
					}
				}
				break;
			case 'mod1':
				// Agregar opcion seleccione
				foreach($foreignDataList as $list => $options) {
					foreach($fieldSpec as $field => $spec) {
						if(isset($fieldSpec[$field]['optionlist']) && $fieldSpec[$field]['optionlist'] == $list) {
							if(isset($fieldSpec[$field]['opsel']['value']) && isset($fieldSpec[$field]['opsel']['text'])) {
								array_unshift($foreignDataList[$list], $fieldSpec[$field]['opsel']);
								break;
							}
						}
					}
				}
				// Marcar clave primaria como campos deshabilitados
				foreach($this->_primaryKey as $key) {
					if(isset($fieldSpec[$key]))
						$fieldSpec[$key]['disabled'] = 'y';
				}
				break;
			case 'del1': case 'enq1':
				// Agregar opcion seleccione
				foreach($foreignDataList as $list => $options) {
					foreach($fieldSpec as $field => $spec) {
						if(isset($fieldSpec[$field]['optionlist']) && $fieldSpec[$field]['optionlist'] == $list) {
							if(isset($fieldSpec[$field]['opsel']['value']) && isset($fieldSpec[$field]['opsel']['text'])) {
								array_unshift($foreignDataList[$list], $fieldSpec[$field]['opsel']);
								break;
							}
						}
					}
				}
				// Marcar todos los campos como deshabilitados
				foreach($fieldSpec as $field => $spec) {
					$fieldSpec[$field]['disabled'] = 'y';
				}
				break;
		}
		
		$this->_fieldSpec = $fieldSpec;
		$this->_foreignDataList = $foreignDataList;
	} // _cm_changeConfig()
	
		
	//***************************************************************************
	// Realizar proceso personalizado antes de obtener los registro de la DB
	protected function _cm_pre_getData($filter) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $filter;
	} // _cm_pre_getData()
	
	
	//***************************************************************************
	// Realizar proceso personalizado a los registros obtenidos de la DB
	protected function _cm_post_getData($fieldData, $filter) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $fieldData;
	} // _cm_post_getData()
	
	
	//***************************************************************************
	// Realizar validaciones personalizadas comunes a insert y update
	protected function _cm_commonValidation($insertData) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $insertData;
	} // _cm_commonValidation()
	
	
	//***************************************************************************
	// Realizar validaciones personalizadas de insert
	protected function _cm_validateInsert($insertData) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $insertData;
	} // _cm_validateInsert()
	
	
	//***************************************************************************
	// Realizar validaciones personalizadas de update
	protected function _cm_validateUpdate($updateData) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $updateData;
	} // _cm_validateUpdate()
	
	
	//***************************************************************************
	// Realizar proceso personalizado antes de insertar resigtro en DB
	protected function _cm_pre_insertRecord($rowData) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $rowData;
	} // _cm_pre_insertRecord()
	
	
	//***************************************************************************
	// Realizar proceso personalizado despues de insert
	protected function _cm_post_insertRecord($fieldData) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $fieldData;
	} // _cm_post_insertRecord()
	
	
	//***************************************************************************
	// Realizar proceso personalizado antes de insertar multi resigtro en DB
	protected function _cm_pre_insertMultiRecord($fieldData) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $fieldData;
	} // _cm_pre_insertMultiRecord()
	
	
	//***************************************************************************
	// Realizar proceso personalizado despues de insert multi
	protected function _cm_post_insertMultiRecord($fieldData) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $fieldData;
	} // _cm_post_insertMultiRecord()
	
	
	//***************************************************************************
	// Realizar proceso personalizado antes de actualizar resigtro en DB
	protected function _cm_pre_updateRecord($updateData) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $updateData;
	} // _cm_pre_updateRecord()
	
	
	//***************************************************************************
	// Realizar proceso personalizado despues de actualizar
	protected function _cm_post_updateRecord($fieldData) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $fieldData;
	} // _cm_post_updateRecord()
	
	
	//***************************************************************************
	// Realizar proceso personalizado antes de update multi
	protected function _cm_pre_updateMultiRecord($updateData) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $updateData;
	} // _cm_pre_updateMultiRecord()
	
	
	//***************************************************************************
	// Realizar proceso personalizado despues de update multi
	protected function _cm_post_updateMultiRecord($fieldData) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $fieldData;
	} // _cm_post_updateMultiRecord()
	
	
	//***************************************************************************
	// Realizar proceso personalizado antes de update data
	protected function _cm_pre_updateData($updateData, $where) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $updateData;
	} // _cm_pre_updateData()
	
	
	//***************************************************************************
	// Realizar proceso personalizado despues de update data
	protected function _cm_post_updateData($fieldData) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $fieldData;
	} // _cm_post_updateData()
	
	
	//***************************************************************************
	// Realizar proceso personalizado antes de eliminar resigtro en DB
	protected function _cm_pre_deleteRecord($deleteData) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $deleteData;
	} // _cm_pre_deleteRecord()
	
	
	//***************************************************************************
	// Realizar proceso personalizado despues de eliminar resigtro en DB
	protected function _cm_post_deleteRecord($fieldData) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $fieldData;
	} // _cm_post_deleteRecord()
	
	
	//***************************************************************************
	// Realizar proceso personalizado antes de deleteMultiRecord
	protected function _cm_pre_deleteMultiRecord($deleteData) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $deleteData;
	} // _cm_pre_deleteMultiRecord()
	
	
	//***************************************************************************
	// Realizar proceso personalizado despues de deleteMultiRecord
	protected function _cm_post_deleteMultiRecord($fieldData) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $fieldData;
	} // _cm_post_deleteMultiRecord()
	
	
	//***************************************************************************
	// Realizar proceso personalizado antes de deleteData
	protected function _cm_pre_deleteData($where) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $where;
	} // _cm_pre_deleteData()
	
	
	//***************************************************************************
	// Realizar proceso personalizado despues de deleteData
	protected function _cm_post_deleteData($deleted) {
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $deleted;
	} // _cm_post_deleteData()
}
?>
