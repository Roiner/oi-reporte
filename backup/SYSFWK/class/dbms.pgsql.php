<?php
// *****************************************************************************
// Manejador de Base de Datos para PostgreSQL
// Requiere las clases singleton y dbms
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

require_once('singleton.php');
require_once('dbms.php');

// DB engine for PostgreSQL
class pgsql {
	// Variables para contruir el Query
	public $sqlSelect;
	public $sqlFrom;
	public $sqlGroupBy;
	public $sqlHaving;
	public $sqlOrderBy;

	public $fieldSpec; // Array de especificación de los campos
	
	protected $_connStr; // Cadena de conexión
	protected $_instDbms; // Nombre de la instancia que representa la conexión
	
	protected $_dbms; // Manejador de Base de Datos
	protected $_dbName; // Nombre de la Base de Datos
	protected $_dbSchema; // Nombre del Esquema
	
	protected $_query; // Última consulta ejecutada
	protected $_numRows; // Número de filas afectadas por la última consulta
	
	protected $_pageNo; // Número de página solicitado
	protected $_rowsPerPage; // Filas por páginas
	protected $_lastPage; // Último número de página disponible en el query actual
	
	protected $_errors; // Array de errores
	
	
	//***************************************************************************
	// Constructor
	public function __construct($connstr, $dbschema, $instdbms = null, $dbname = null) {
		$this->_errors = array();
		$this->_rowsPerPage = 0;
		$this->_pageNo = 0;
		
		$this->_dbms = null; // Manejador de Base de Datos
		$this->_instDbms = $instdbms; // Nombre de la instancia
		
		$this->_dbName = $dbname; // Nombre de la Base de Datos
		$this->_dbSchema = $dbschema; // Nombre del Esquema
		$this->_connStr = $connstr; // Cadena de conexión
	} // __construct()
	
	
	//---------------------------------------------------------------------------
	// Funciones publicas
	//---------------------------------------------------------------------------
	
	//***************************************************************************
	// Funciones inspectoras
	public function getDbName() { return $this->_dbName; }
	public function getDbSchema() { return $this->_dbSchema; }
	public function getErrors() { return $this->_errors; }
	public function getErrorsString() { return implode('; ', $this->_errors); }
	public function getQuery() { return $this->_query; }
	public function getNumRows() { return $this->_numRows; }
	public function getPageNo() { return $this->_pageNo; }
	public function getRowsPerPage() { return $this->_rowsPerPage; }
	public function getLastPage() { return $this->_lastPage; }
	
	
	//***************************************************************************
	// Establecer la cantidad de registros por página
	public function setRowsPerPage($rowsPerPage) {
		if($rowsPerPage >= 0) {
			$this->_rowsPerPage = (int)$rowsPerPage;
		}
	} // setRowsPerPage()
	
	
	//***************************************************************************
	// Establecer el número de página
	public function setPageNo($page = 1) {
		$this->_pageNo = (int)$page;
	} // setPageNo()
	
	
	//***************************************************************************
	// Conectar
	//	Retorna true si OK, false en case de error
	public function connect($schema = null) {
		$this->_errors = array(); // Limipar errores anteriores
		
		// Verificar si ya hay conexión
		if(!$this->_dbms) {
			// Conectar
			$params = (is_array($this->_connStr) ? $this->_connStr : array($this->_connStr));
			$this->_dbms = singleton::getInstance('dbms', $this->_instDbms, $params);
			
			if(!$this->_dbms || !is_null($this->_dbms->getError())) {
				$this->_errors[] = $this->_dbms->getError();
				$this->_dbms = null;
				return false; // No se puede conectar
			}
			
			// Establecer la ruta de busqueda con el esquema
			if($schema) {
				$this->setSchema($schema);
			}
		}
		
		return true;
	} // connect()
	
	
	//***************************************************************************
	// Establecer la ruta de busqueda con el esquema
	//	Retorna true si OK, false en case de error
	public function setSchema($schema) {
		if(!empty($schema) && ($schema != $this->_dbSchema)) {
			if(strtolower($schema) != 'public') {
				$search_path = '"' . $schema . '", public';
			}
			$query = $this->_query;
			$this->_query = 'SET search_path TO ' . $search_path;
			if($this->_dbms->executeNoQuery($this->_query) !== false) {
				$this->_query = $query;
				$this->_dbSchema = $schema;
				return true;
			}
		}
		$this->_errors[] = $this->_dbms->getError();
		return false;
	} // setSchema()
	
	
	//***************************************************************************
	// Iniciar una transacción
	//	Retorna true si OK, false en case de error
	public function beginTransaction($schema) {
		$this->_errors = array(); // Limpiar errores anteriores
		
		if(!$this->connect($schema)) return false; // Conectar con BD
		
		// Iniciar transacción
		$result = $this->_dbms->beginTransaction();
		if(!$result) {
			$this->_errors = $this->_dbms->getError();
			return false;
		}
		return $result;
	} // beginTransaction()
	
	
	//***************************************************************************
	// Ejecutar transacción
	//	Retorna true si OK, false en case de error
	public function commit() {
		$result = $this->_dbms->commit();
		if(!$result) {
			$this->_errors = $this->_dbms->getError();
			return false;
		}
		return $result;
	} // commit()
	
	
	//***************************************************************************
	// Cancelar transacción
	//	Retorna true si OK, false en case de error
	public function rollBack() {
		$result = $this->_dbms->rollBack();
		if(!$result) {
			$this->_errors = $this->_dbms->getError();
			return false;
		}
		return $result;
	} // rollBack()
	
	
	//***************************************************************************
	// Obtiene la cantidad de registros que satisfacen el criterio de selección en $where
	//	Retorna la cantidad de registros, false en case de error
	public function getCount($schema, $tablename, $where = '', $params = array()) {
		$this->_errors = array(); // Limpiar errores anteriores
		
		// $where comienza con 'SELECT' y se usa como un query completo
		if(preg_match('/^(select )/i', $where)) {
			$sql = $where;
		}
		else {
			// No comienza con 'SELECT' por lo que debe ser un 'where'
			if(empty($where))
				$sql = "SELECT count(*) FROM $tablename";
			else
				$sql = "SELECT count(*) FROM $tablename WHERE $where";
		}
		
		// Conectar con BD
		if(!$this->Connect($schema)) {
			return false;
		}
		
		$this->_query = $sql;
		
		// Si hay 'GROUP BY' retornar el número de filas (ignorar GROUP BY si está en un subselect)
		if(preg_match("/group by /i", $this->_query) == true AND !preg_match("/\(SELECT .+group by.+\)/i", $this->_query)) {
			$result = $this->_dbms->executeQuery($this->_query, PDO::FETCH_NUM, $params);
			$count = $this->_dbms->getRowCount();
		}
		else {
			$result = $this->_dbms->executeScalar($this->_query, $params);
			$count = $result;
		} // if

		// Si hay error
		if($result === false) {
			$this->_numRows = 0;
			$this->_errors[] = $this->_dbms->getError();
			return false;
		}
		
		// Resultado
		$this->_numRows = $this->_dbms->getRowCount();
		$this->_query = $this->_dbms->getQuery();
		return $count;
	} // getCount()
		
	
	//***************************************************************************
	// Obtiene el máximo de un campo que satisfacen el criterio de selección en $where
	//	Retorna el máximo, false en case de error
	public function getMaxValue($schema, $tablename, $field, $where = '', $params = array()) {
		// Ejecutar función max
		$result = $this->getFunctionValue($schema, $tablename, 'max', $field, $where, $params);
		
		// Resultado
		if($result !== false)
			$result = (is_null($result) ? 0 : $result);
		return $result;
	} // getMaxValue()
	
	
	//***************************************************************************
	// Obtiene el mínimo de un campo que satisfacen el criterio de selección en $where
	//	Retorna mínimo, false en case de error
	public function getMinValue($schema, $tablename, $field, $where = '', $params = array()) {
		// Ejecutar función min
		$result = $this->getFunctionValue($schema, $tablename, 'min', $field, $where, $params);
		
		// Resultado
		if($result !== false)
			$result = (is_null($result) ? 0 : $result);
		return $result;
	} // getMinValue()
	
	
	//***************************************************************************
	// Obtiene el resultado de una funcion aplicada a un campo que satisfacen el criterio de selección en $where
	//	Retorna resultado de la función, false en case de error
	public function getFunctionValue($schema, $tablename, $function, $field, $where = '', $params = array()) {
		$this->_errors = array(); // Limpiar errores anteriores
		
		// $where comienza con 'SELECT' y se usa como un query completo
		if(preg_match('/^(select )/i', $where)) {
			$sql = $where;
		}
		else {
			// No comienza con 'SELECT' por lo que debe ser un 'where'
			if(empty($where))
				$sql = "SELECT $function($field) FROM $tablename";
			else
				$sql = "SELECT $function($field) FROM $tablename WHERE $where";
		}
		
		// Conectar con BD
		if(!$this->connect($schema)) {
			return false;
		}

		// Ejecutar consulta
		$this->_query = $sql;
		$result = $this->_dbms->executeScalar($this->_query, $params);
		if($result === false) {
			$this->_numRows = 0;
			$this->_errors[] = $this->_dbms->getError();
			return false;
		}
		
		// Resultado
		$this->_numRows = $this->_dbms->getRowCount();
		$this->_query = $this->_dbms->getQuery();
		return $result;
	} // getFunctionValue()
	
	
	// ****************************************************************************
	// Obtener registros
	// $schema: String con el nombre del schema
	// $tablename: String con el nombre de la tabla
	// $where: String con la instrucción WHERE SQL
	// $params: Array (identificador => valor) de parametros del WHERE
	// Retorna array de registro, false en case de error
	public function getData($schema, $tablename, $where, $params = array()) {
		$this->_errors = array(); // Limpiar errores anteriores

		// Conectar con BD
		if(!$this->connect($schema)) {
			return false;
		}
		
		$this->_numRows = 0;
		$result = array(); // Array de resultado
		
		// Inicializar paginado
		$pageNo = (int)$this->_pageNo;
		$rowsPerPage = (int)$this->_rowsPerPage;
		$this->_lastPage = 0;

		// Usar los parametros del SELECT o todos los campos por defecto
		if(empty($this->sqlSelect)) {
			$select_str = '*';
		}
		else {
			$select_str = $this->sqlSelect;
		}

		// Usar parametro from o la tabla actual
		if(empty($this->sqlFrom)) {
			$from_str = $tablename;
		}
		else {
			$from_str = $this->sqlFrom;
		}

		// Agregar el parametro GROUP BY
		if(!empty($this->sqlGroupBy)) {
			$group_str = "\nGROUP BY " . $this->sqlGroupBy;
		}
		else {
			$group_str = NULL;
		}
		
		// Preparar order by
		if(!empty($this->sqlOrderBy)) {
			$sort_str = "\nORDER BY " . $this->sqlOrderBy;
		}
		else {
			$sort_str = '';
		}

		// Agregar el parametro HAVING
		if(!empty($this->sqlHaving)) {
			$having_str = "\nHAVING " . $this->sqlHaving;
		}
		else {
			$having_str = null;
		}
		
		// Agregar el where
		$sql_count = '';
		$where = trim($where);
		if(empty($where)) {
			$where_str = '';
		}
		elseif(preg_match('/^(select )/i', $where)) {
			// Si comienza con SELECT se utiliza la consulta tal cual
			$sql = $where;
			$sql_count = 'SELECT count(*) ' . stristr($where, 'FROM');
		}
		else {
			$where_str = "\nWHERE " . $where;
			$sql_count = "SELECT count(*) FROM $from_str $where_str $group_str $having_str";
		}
		
		// Preparar paginado
		if($rowsPerPage > 0) {
			// Obtener el número de filas que satifacen el query
			$this->_numRows = $this->getCount($schema, $tablename, $sql_count, $params);
			if($this->_numRows === false) {
				$this->_numRows = 0;
				$this->_errors[] = $this->_dbms->getError();
				return false;
			}

			if($this->_numRows <= 0) {
				$this->_pageNo = 0;
				return $result; // Retornar array vacío si no hay resultados
			}

			// Calcular el número total de paginas para este query
			$this->_lastPage = ceil($this->_numRows / $rowsPerPage);
		}
		else {
			$this->_lastPage = 1;
		}
		
		// Aseguar que el número de página esté dentro del rango
		if($pageNo < 1) {
			$pageNo = 1;
		}
		elseif($pageNo > $this->_lastPage) {
			$pageNo = $this->_lastPage;
		}
		$this->_pageNo = $pageNo;
		
		// Establecer el LIMIT y OFFSET para obtener el número de página especificado
		if($rowsPerPage > 0)
			$limit_str = 'LIMIT ' . $rowsPerPage . ' OFFSET ' . ($pageNo - 1) * $rowsPerPage;
		else
			$limit_str = '';
			
		// Ejecutar consulta
		if(isset($sql))
			$this->_query = $sql;
		else
			$this->_query = "SELECT $select_str \nFROM $from_str $where_str $group_str $having_str $sort_str $limit_str";
		$result = $this->_dbms->executeQuery($this->_query, PDO::FETCH_ASSOC, $params);
		if($result === false) {
			$this->_numRows = 0;
			$this->_errors[] = $this->_dbms->getError();
			return false;
		}

		// Actualizar la cantidad de registros si el resultado no es paginado
		if($rowsPerPage == 0) {
			$this->_numRows = $this->_dbms->getRowCount();
		}

		// Resultado
		$this->_query = $this->_dbms->getQuery();
		return $result;
	} // getData()
	
	
	//***************************************************************************
	// Ejecutar el insert de un registro
	// Se espera que los del fieldData estén de acuerdo con tabla tablename
	// Retorna el fieldData insertado ó false en caso de error
	public function insertRecord($schema, $tablename, $fieldData) {
		// Preparar campos de la instrucción returning
		$returning = null;
		foreach($this->fieldSpec as $field => $fieldvalue) {
			// Agregar campos autoincrement
			if(isset($fieldvalue['autoincrement'])) {
				$returning[] = $field;
			}
			// Agregar campos autoinsert
			if(isset($fieldvalue['autoinsert']) && !isset($fieldData[$field])) {
				$fieldData[$field] = $fieldvalue['autoinsert'];
				$returning[] = $field;
			}
		}
		
		// Preparar instrucción
		$fields = implode(', ', array_keys($fieldData));
		$marks = implode(', :', array_keys($fieldData));
		$sql = "INSERT INTO $tablename ($fields) VALUES (:$marks)";
		if(!is_null($returning))
			$sql .= ' RETURNING ' . implode(', ', $returning);
		
		// Eliminar marcas de campos autoincrement
		foreach($fieldData as $field => $fieldvalue) {
			if(isset($this->fieldSpec[$field]['autoincrement'])) {
				$sql = str_replace(array(":$field,", ":$field)"), array('DEFAULT,', 'DEFAULT)'), $sql);
				unset($fieldData[$field]);
			}
		}
		
		// Ejecutar insert
		$this->_query = $sql;
		$result = $this->_dbmsExecuteQuery($schema, $fieldData);
		if($result === false) {
			return false; // Error
		}
		else {			
			// Resultado
			if(!is_null($returning))
				return array_merge($fieldData, $result[0]);
			else
				return $fieldData;
		}
	} // insertRecord()
	
	
	//***************************************************************************
	// Ejecutar el insert de varios registros
	// Se espera que los del fieldData estén de acuerdo con tabla tablename
	// Retorna el fieldData insertado ó false en caso de error
	public function insertMultiRecord($schema, $tablename, $fieldData) {
		// Preparar campos de la instrucción returning
		$returning = null;
		foreach($this->fieldSpec as $field => $fieldvalue) {
			// Agregar campos autoincrement
			if(isset($fieldvalue['autoincrement'])) {
				$returning[] = $field;
			}
			// Agregar campos autoinsert
			if(isset($fieldvalue['autoinsert'])) {
				$returning[] = $field;
				for($i = 0; $i < count($fieldData); $i++) {
					if(!isset($fieldData[$i][$field])) {
						$fieldData[$i][$field] = $fieldvalue['autoinsert'];
					}
				}
			}
		}
	
		// Preparar instrucción
		$fields = implode(', ', array_keys($fieldData[0]));
		$marks = implode(', :', array_keys($fieldData[0]));
		$sql = "INSERT INTO $tablename ($fields) VALUES (:$marks)";
		if(!is_null($returning))
			$sql .= ' RETURNING ' . implode(', ', $returning);
		
		// Eliminar marcas de campos autoincrement
		foreach($fieldData[0] as $field => $fieldvalue) {
			if(isset($this->fieldSpec[$field]['autoincrement'])) {
				$sql = str_replace(":$field", 'DEFAULT', $sql);
				for($i = 0; $i < count($fieldData); $i++)
					unset($fieldData[$i][$field]);
			}
		}
		
		// Ejecutar insert
		$this->_query = $sql;
		$result = $this->_dbmsExecuteMultiQuery($schema, $fieldData);
		if($result === false) {
			return false; // Error
		}
		else {
			// Resultado
			if(!is_null($returning)) {
				for($i = 0; $i < count($result); $i++) {
					$fieldData[$i] = array_merge($fieldData[$i], $result[$i]);
				}
			}
			return $fieldData;
		}
	} // insertMultiRecord()
	
	
	//***************************************************************************
	// Ejecutar el update de un registro basado en la clave primaria
	// $fieldData: Array (campo->valor) de datos a actualizar
	// $primaryKey: Array (campo->valor) de la clave primaria de la tabla
	// Retorna el fieldData actulizado ó false en caso de error
	public function updateRecord($schema, $tablename, $fieldData, $primaryKey) {
		$fieldDataSql = $fieldData; // Copiar array de datos
		
		// Preprar los arrarys para el manejo de la clave primaria
		$pkey = array(); $pkmarks = array();
		foreach($primaryKey as $field => $fieldvalue) {
			$pkey[$field . '_pk'] = $fieldvalue;
			$pkmarks[] = $field . ' = :' . $field . '_pk';
		}
		
		// Preparar campos de la instrucción returning
		$returning = null;
		foreach($this->fieldSpec as $field => $fieldvalue) {
			// Agregar campos autoupdate
			if(isset($fieldvalue['autoupdate']) && !isset($fieldDataSql[$field])) {
				$fieldDataSql[$field] = $fieldvalue['autoupdate'];
				$returning[] = $field;
			}
		}
		
		// Preparar instrucción
		$where = implode(' AND ', $pkmarks);
		$fields = implode(', ', array_keys($fieldDataSql));
		$marks = implode(', :', array_keys($fieldDataSql));
		$sql = "UPDATE $tablename SET ($fields) = (:$marks) WHERE $where";
		if(!is_null($returning))
			$sql .= ' RETURNING ' . implode(', ', $returning);
		
		// Agregar clave primaria al fieldDataSql
		$fieldDataSql = array_merge($fieldDataSql, $pkey);
		//echo '<pre>'; var_dump($fieldDataSql); echo '</pre>';
		
		// Ejecutar update
		$this->_query = $sql;
		$result = $this->_dbmsExecuteQuery($schema, $fieldDataSql);
		if($result === false) {
			return false; // Error
		}
		else {
			// Resultado
			$fieldData = array_merge($primaryKey, $fieldData);
			if(!is_null($returning) && $this->_numRows > 0)
				return array_merge($fieldData, $result[0]);
			else
				return $fieldData;
		}
	} // updateRecord()
	
	
	//***************************************************************************
	// Ejecutar el update de varios registros basado en la clave primaria
	// $fieldData: Array multiple ([i][campo]->valor) de datos a actualizar
	// $primaryKey: Array multiple ([i][campo]->valor) de la clave primaria de la tabla
	// Retorna el fieldData actulizado ó false en caso de error
	public function updateMultiRecord($schema, $tablename, $fieldData, $primaryKey) {
		$fieldDataSql = $fieldData; // Copiar array de datos
		
		// Preprar los arrarys para el manejo de la clave primaria
		$pkey = array(); $pkmarks = array();
		foreach($primaryKey[0] as $field => $fieldvalue) {
			for($i = 0; $i < count($fieldData); $i++) {
				$pkey[$i][$field . '_pk'] = $primaryKey[$i][$field];
			}
			$pkmarks[] = $field . ' = :' . $field . '_pk';
		}
		
		// Preparar campos de la instrucción returning
		$returning = null;
		foreach($this->fieldSpec as $field => $fieldvalue) {
			for($i = 0; $i < count($fieldDataSql); $i++) {
				// Agregar campos autoupdate
				if(isset($fieldvalue['autoupdate']) && !isset($fieldDataSql[$i][$field])) {
					$fieldDataSql[$i][$field] = $fieldvalue['autoupdate'];
					if($i == 0)
						$returning[] = $field;
				}
			}
		}
		
		// Preparar instrucción
		$where = implode(' AND ', $pkmarks);
		$fields = implode(', ', array_keys($fieldDataSql[0]));
		$marks = implode(', :', array_keys($fieldDataSql[0]));
		$sql = "UPDATE $tablename SET ($fields) = (:$marks) WHERE $where";
		if(!is_null($returning))
			$sql .= ' RETURNING ' . implode(', ', $returning);
		
		// Agregar clave primaria al fieldDataSql
		for($i = 0; $i < count($fieldDataSql); $i++) {
			$fieldDataSql[$i] = array_merge($fieldDataSql[$i], $pkey[$i]);
		}
		
		// Ejecutar update
		$this->_query = $sql;
		$result = $this->_dbmsExecuteMultiQuery($schema, $fieldDataSql);
		if($result === false) {
			return false; // Error
		}
		else {
			// Resultado
			for($i = 0; $i < count($fieldData); $i++) {
				$fieldData[$i] = array_merge($primaryKey[$i], $fieldData[$i]);
			}
			if(!is_null($returning) && $this->_numRows > 0) {
				for($i = 0; $i < count($result); $i++) {
					if(is_array($result[$i]))
						$fieldData[$i] = array_merge($fieldData[$i], $result[$i]);
				}
			}
			return $fieldData;
		}
	} // updateMultiRecord()
	
	
	//***************************************************************************
	// Ejecutar el update de registros basado en un where libre
	// $fieldData: Array (campo->valor) de datos a actualizar
	// $where: Cadena WHERE SQL
	// Retorna el fieldData actulizado ó false en caso de error
	public function updateData($schema, $tablename, $fieldData, $where) {
		// Preparar campos de la instrucción returning
		$returning = null;
		foreach($this->fieldSpec as $field => $fieldvalue) {
			// Agregar campos autoupdate
			if(isset($fieldvalue['autoupdate']) && !isset($fieldData[$field])) {
				$fieldData[$field] = $fieldvalue['autoupdate'];
				$returning[] = $field;
			}
		}
	
		// Preparar instrucción
		$fields = implode(', ', array_keys($fieldData));
		$marks = implode(', :', array_keys($fieldData));
		$sql = "UPDATE $tablename SET ($fields) = (:$marks) WHERE $where";
		if(!is_null($returning))
			$sql .= ' RETURNING ' . implode(', ', $returning);
		//echo '<pre>'; var_dump($fieldData); echo '</pre>';
		
		// Ejecutar update
		$this->_query = $sql;
		$result = $this->_dbmsExecuteQuery($schema, $fieldData);
		if($result === false) {
			return false; // Error
		}
		else {
			// Resultado
			if(!is_null($returning) && $this->_numRows > 0)
				return array_merge($fieldData, $result[0]);
			else
				return $fieldData;
		}
	} // updateData()
	
	
	//***************************************************************************
	// Ejecutar el delete de registros basado en campos (campo1 = valor1 AND campoN = valorN)
	// $fieldData: Array (campo->valor) de datos a utilizar como clave en where
	// Retorna el fieldData eliminado ó false en caso de error
	public function deleteRecord($schema, $tablename, $fieldData) {
		// Preprar las marcas para el manejo de campos clave
		$marks = array();
		foreach($fieldData as $field => $fieldvalue) {
			$marks[] = $field . ' = :' . $field;
		}
		
		// Preparar instrucción
		$where = implode(' AND ', $marks);
		$sql = "DELETE FROM $tablename WHERE $where RETURNING *";
		//echo '<pre>'; var_dump($fieldData); echo '</pre>';
		
		// Ejecutar delete
		$this->_query = $sql;
		$result = $this->_dbmsExecuteQuery($schema, $fieldData);
		if($result === false) {
			return false; // Error
		}
		else {
			// Resultado
			if($this->_numRows > 0)
				return array_merge($fieldData, $result[0]);
			else
				return $fieldData;
		}
	} // deleteRecord()
	
	
	//***************************************************************************
	// Ejecutar el delete de varios registros basado en campos (campo1 = valor1 AND campoN = valorN)
	// $fieldData: Array multiple ([i][campo]->valor) de datos a utilizar como clave en where
	// Retorna el fieldData eliminado ó false en caso de error
	public function deleteMultiRecord($schema, $tablename, $fieldData) {
		// Preprar las marcas para el manejo de campos clave
		$marks = array();
		foreach($fieldData[0] as $field => $fieldvalue) {
			$marks[] = $field . ' = :' . $field;
		}
		
		// Preparar instrucción
		$where = implode(' AND ', $marks);
		$sql = "DELETE FROM $tablename WHERE $where";
		//echo '<pre>'; var_dump($fieldData); echo '</pre>';
		
		// Ejecutar delete
		$this->_query = $sql;
		$result = $this->_dbmsExecuteMultiQuery($schema, $fieldData);
		if($result === false) {
			return false; // Error
		}
		else {
			// Resultado
			return $fieldData;
		}
	} // deleteMultiRecord()
	
	
	//***************************************************************************
	// Ejecutar el delete de registros basado en un where libre
	// $where: Cadena WHERE SQL
	// Retorna el fieldData eliminado ó false en caso de error
	public function deleteData($schema, $tablename, $where) {
		// Preparar instrucción
		$sql = "DELETE FROM $tablename WHERE $where RETURNING *";
		//echo '<pre>'; var_dump($sql); echo '</pre>'; DEBUG

		// Ejecutar delete
		$this->_query = $sql;
		$result = $this->_dbmsExecuteQuery($schema);
		// Resultado
		return $result;
	} // _dbms_DeleteData()
	
	
	//****************************************************************************
	// Entrecomilla una cadena para usarla en una consulta
	public function quote($value) {
		return $this->_dbms->quote($value);
	} // quote()
	
	
	//***************************************************************************
	// Ejecutar un query cargado en $query con la funcion executeQuery del dbms
	// $params: Array de parámetros (asociativo o numerico iniciando en 1 según parameter markers en $query)
	public function executeQuery($schema, $query, $params = array()) {
		$this->_query = $query;
		return $this->_dbmsExecuteQuery($schema, $params);
	} // _dbmsExecuteQuery()
	
	
	//***************************************************************************
	// Ejecutar un query multiple cargado en $query con la funcion executeMultiQuery del dbms
	// $params: Array de parámetros (asociativo o numerico iniciando en 1 según parameter markers en $query)
	// Esta funcion es utilizada por las operaciones insert, update y delete
	public function executeMultiQuery($schema, $query, $params = array()) {
		$this->_query = $query;
		return $this->_dbmsExecuteMultiQuery($schema, $params);
	} // _dbmsExecuteMultiQuery()
	
	
	//---------------------------------------------------------------------------
	// Funciones protegidas
	//---------------------------------------------------------------------------
	
	//***************************************************************************
	// Ejecutar un query cargado en $this->_query con la funcion executeQuery del dbms
	// $params: Array de parámetros (asociativo o numerico iniciando en 1 según parameter markers en $query)
	// Esta funcion es utilizada por las operaciones insert, update y delete
	protected function _dbmsExecuteQuery($schema, $params = array()) {
		$this->_errors = array(); // Limpiar errores anteriores
		$this->_numRows = 0;
		
		// Conectar con BD
		if(!$this->connect($schema)) {
			return false;
		}
		
		// Ejecutar query
		$result = $this->_dbms->executeQuery($this->_query, PDO::FETCH_ASSOC, $params);
		if($result === false) {
			$this->_numRows = 0;
			$this->_query = $this->_dbms->getQuery();
			$this->_errors[] = $this->_dbms->getError();
		}
		else {
			$this->_numRows = $this->_dbms->getRowCount();
			$this->_query = $this->_dbms->getQuery();
		}
		return $result;
	} // _dbmsExecuteQuery()
	
	
	//***************************************************************************
	// Ejecutar un query multiple cargado en $this->_query con la funcion executeMultiQuery del dbms
	// $params: Array de parámetros (asociativo o numerico iniciando en 1 según parameter markers en $query)
	// Esta funcion es utilizada por las operaciones insert, update y delete
	protected function _dbmsExecuteMultiQuery($schema, $params = array()) {
		$this->_errors = array(); // Limpiar errores anteriores
		$this->_numRows = 0;
		
		// Conectar con BD
		if(!$this->connect($schema)) {
			return false;
		}
		
		// Ejecutar query
		$result = $this->_dbms->executeMultiQuery($this->_query, PDO::FETCH_ASSOC, $params);
		if($result === false) {
			$this->_numRows = 0;
			$this->_query = $this->_dbms->getQuery();
			$this->_errors[] = $this->_dbms->getError();
		}
		else {
			$this->_numRows = $this->_dbms->getRowCount();
			$this->_query = $this->_dbms->getQuery();
		}
		return $result;
	} // _dbmsExecuteMultiQuery()
	
	
	//---------------------------------------------------------------------------
	// Funciones DDL (Data Definition Language) FALTA
	//---------------------------------------------------------------------------
}
?>
