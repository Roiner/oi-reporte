<?php
// *****************************************************************************
// Clase para el maneja de la pila de páginas del sistema
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

class PageStack {
	//***************************************************************************
	// Agregar página a la pila
	// $script: Arrary asociativo que contenga 'task_id', 'container' y 'params'
	public static function update($script) {
		// Obtener la pila
		$pageStack = self::getPageStack();
		
		// Limpiar la pila
		self::cleanPageStack($pageStack, $script['task_id']);
		
		// Agregar el script al final de la pila y guardar en session
		$pageStack[] = $script;
		$_SESSION[PAGE_STACK] = serialize($pageStack);
	} // update()
	
	
	//***************************************************************************
	// Obtener el script anterior
	public static function previous($script) {
		// Obtener la pila
		$pageStack = self::getPageStack();
		
		// Limpiar la pila
		self::cleanPageStack($pageStack, $script['task_id']);
		
		// Actualizar pila en session 
		$_SESSION[PAGE_STACK] = serialize($pageStack);
		
		// Obtener script anterior
		if(count($pageStack) > 0) {
			$previous = array_pop($pageStack);
			$previous['url'] = ($previous['task_id'] == 'index.php' ? $GLOBALS['url_root'] : $GLOBALS['url_task'])
				. $previous['task_id'];
		}
		else {
			$previous = array(
				'url' => $GLOBALS['url_root'] . 'index.php',
				'task_id' => 'index.php',
				'container' => 'window',
				'params' => array()
			);
		}
		return $previous;
	} // previous()
	
	
	//***************************************************************************
	// Obtener pila
	private static function getPageStack() {
		return (isset($_SESSION[PAGE_STACK]) ? unserialize($_SESSION[PAGE_STACK]) : array());
	} // getPageStack()
	
	
	//***************************************************************************
	// Limpiar la pila
	private static function cleanPageStack(&$pageStack, $taskId) {
		// Si el script está en la pila, removerlo junto con todos los sucesores
		for($i = 0; $i < count($pageStack); $i++) {
			if($pageStack[$i]['task_id'] == $taskId) {
				do {
					$last = array_pop($pageStack);
				} while($last['task_id'] != $taskId);
				break;
			}
		}
	} // cleanPageStack()
	
} // PageStack
?>