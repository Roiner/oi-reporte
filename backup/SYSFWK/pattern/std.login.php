<?php
// *****************************************************************************
// Página login del sistema
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

// Definir los enlaces básicos
require($GLOBALS['path_fwk_view'] . 'basicHeadref.php');

// Definir clase de autenticacion de usuario
if(defined('AUTHENTICATION_CLASS'))
	$jsAuthenticateClass = '"entity": "' . AUTHENTICATION_CLASS . '"';
else
	$jsAuthenticateClass = '"class": "User"';

// Id del panel de mensajes
$messageId = 'frmLoginMsg';

// Definir url del index
isset($urlIndex) or $urlIndex = 'index.php';

// Obtener parametros
if(isset($_SESSION[INDEX_PARAMS])) {
	$urlIndex .= "?params={$_SESSION[INDEX_PARAMS]}";
	unset($_SESSION[INDEX_PARAMS]);
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>.:<?php echo strtoupper(SYS_SIGLAS); ?>:.</title>
	<?php $headRef = $cssHeadRef; require($GLOBALS['path_fwk_view'] . 'headref.php'); ?>
	<?php $headRef = $jsHeadRef; require($GLOBALS['path_fwk_view'] . 'headref.php'); ?>
	
<script>
$(document).ready(function(){
	// Configuración
	sysfwk.setConfig({
		urlAjaxRequestJson: '<?php echo $GLOBALS['url_task'] . 'ajax_request_json.php'; ?>'
	});
	
	// Iniciar session
	function InicioSesion() {
		var login = $("#txtUsuario").val();
		var pass = $("#txtPassword").val();

		if(login != "" && pass != "") {
			sysfwk.msgPanel({
				id: '<?php echo $messageId; ?>', title: 'Autenticando...',
				info: '<img alt="Loading..." title="Autenticando..." src="' + '<?php echo $GLOBALS['url_fwk_css'] . 'img/ajax-loader-circle.gif'; ?>' + '" />',
				state: sysfwk.OK
			});
			
			$.post("<?php echo $GLOBALS['url_fwk_lib']; ?>sessionldap.php", {"user": login, "pass": pass}, function(data) {
				var result = $.parseJSON(data);

				if(result.ficha != "0") {
					sysfwk.ajaxRequestJson({<?php echo $jsAuthenticateClass; ?>, "method": "authenticate", "params": result}, function(data) {
						if(data.access_granted) {
							sysfwk.msgPanel({
								id: '<?php echo $messageId; ?>', title: 'Ingresando al sistema...',
								info: '<img alt="Loading..." title="Ingresando al sistema..." src="' + '<?php echo $GLOBALS['url_fwk_css'] . 'img/ajax-loader-circle.gif'; ?>' + '" />',
								state: sysfwk.OK
							});
							window.location.href = '<?php echo $urlIndex; ?>';
						}
						else {
							sysfwk.msgPanel({id: '<?php echo $messageId; ?>', title: 'Error:', info: data.info, state: sysfwk.ERROR});
						}
					});
				}
				else {
					sysfwk.msgPanel({id: '<?php echo $messageId; ?>', title: 'Error:', info: result.info, state: sysfwk.ERROR});
				}
			});
		}
		else {
			sysfwk.msgPanel({id: '<?php echo $messageId; ?>', title: 'Error:', info: 'No pueden quedar campos vacios', state: sysfwk.ERROR});
		}
	}

	// Evento de tecla enter
	$('#fsLogon input').each(function() {
		$(this).keypress(function(event) {
			if(event.which == 13)
				InicioSesion();
		});
	});

	// Configurar el dialogo
	$('#divFrm').removeClass('ui-helper-hidden');
	$('#divFrm').dialog({
		autoOpen: true,
		//height: 260,
		width: 350,
		modal: true,
		draggable: false,
		resizable: false,
		close: function(event, ui){ $(this).dialog('destroy'); window.location.href = '<?php echo URL_ON_EXIT; ?>'; },
		buttons: {
			Aceptar: function(){ InicioSesion(); },
			Cancelar: function(){ $(this).dialog('close'); }
		}
	});

<?php
	// Si hay parametros
	// if(isset($params) && !empty($params)) {
		// $task = $params['task'];
		// $container = (isset($params['container']) ? $params['container'] : 'mainContent');
		// $data = (isset($params['data']) ? ',' . $params['data'] : '');
		// echo "sysfwk.loadPage('task/$task.php', '$container'$data);\n";
	// }
?>
});
</script>
</head>
<body>
<header>
	<div class="appTitle"><?php echo SYS_TITLE; ?></div>
</header>

<nav></nav>

<div class="backgroundContent">
	<div id="mainContent">
		<div class="imgLogin">
			<div class="appTitleLogin"><?php echo SYS_TITLE; ?></div>
		</div>
		<div id="divFrm" title="Ingreso al Sistema" class="ui-helper-hidden">
			<form id="frmLogin" name="frmLogin" method="post" action="login.php" class="formEntry">
				<?php
				// Panel de mensajes
				include($GLOBALS['path_fwk_view'] . 'std.message.php');
				?>
				<fieldset id="fsLogon">
					<legend>Acceso:</legend>
					<label id="lblUsuario" for="txtUsuario">Usuario:</label>
					<input id="txtUsuario" name="txtUsuario" type="text" maxlength="30" /><br /><br />
					<label id="lblPassword" for="txtPassword">Contraseña:</label>
					<input id="txtPassword" name="txtPassword" type="password" maxlength="30" />
				</fieldset>
			</form>
		</div>
	</div>
</div>

<br /><br />
<footer><?php echo SYS_COPYRIGHT; ?></footer>
</body>
</html>
