<?php
// *****************************************************************************
// Página principal del sistema
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

// Actualizar page_stack
// echo "SCRIPT_NAME: {$_SERVER['SCRIPT_NAME']}  SCRIPT_FILENAME: {$_SERVER['SCRIPT_FILENAME']}"
	// . "  get_script_name(): " . get_script_name(); // DEBUG
isset($structure['page_stack']) or $structure['page_stack'] = true; // Carga el task en la pila
if($structure['page_stack']) {
	PageStack::update(array('task_id' => get_script_name(), 'container' => 'window', 'params' => $_GET));
}
$_SESSION[CURRENT_TASK] = array(
	'id' => get_script_name(),
	'pattern' => array('id' => 'index', 'type' => 'index'),
);

// Incluir clase de menu principal
require_once($GLOBALS['path_fwk_class'] . 'StdViewMainMenu.php');

// Crear menu principal
if(isset($mainMenuItems))
	$mainMenu = new StdViewMainMenu('fly', $mainMenuItems);

// Definir los enlaces básicos
require($GLOBALS['path_fwk_view'] . 'basicHeadref.php');

// Obtener parametros
if(isset($_GET['params'])) {
	$params = params_decode($_GET['params']);
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>.:<?php echo strtoupper(SYS_SIGLAS); ?>:.</title>
	<?php $headRef = $cssHeadRef; require($GLOBALS['path_fwk_view'] . 'headref.php'); ?>
	<?php $headRef = $jsHeadRef; require($GLOBALS['path_fwk_view'] . 'headref.php'); ?>
	
<script>
$(document).ready(function(){
	// Configuración
	sysfwk.setConfig({
		urlLoadingImg: '<?php echo $GLOBALS['url_fwk_css'] . 'img/ajax-loader-bar.gif'; ?>',
		dlgLoading: 'divLoadingDlg',
		msgLoading: 'Cargando, por favor espere...',
		urlAjaxRequestJson: '<?php echo $GLOBALS['url_task'] . 'ajax_request_json.php'; ?>',
		urlLogOutPage: '<?php echo $GLOBALS['url_root'] . 'logout.php'; ?>'
	});
	
<?php
	// Si hay parametros
	if(isset($params) && !empty($params)) {
		$task = $params['task'];
		$container = (isset($params['container']) ? $params['container'] : 'mainContent');
		$data = (isset($params['data']) ? ',' . $params['data'] : '');
		echo "sysfwk.loadPage('task/$task.php', '$container'$data);\n";
	}
?>
});
</script>
</head>
<body>
<header>
	<div id="divAppActualUser" class="userName">
		Bienvenido: <span><?php echo $_SESSION[USER_NAME]; ?></span> <span><?php echo $_SESSION[USER_ROLE_NAME]; ?></span>
	</div>
	<div class="appTitle"><?php echo SYS_TITLE; ?></div>
</header>

<!-- Menú Principal -->
<?php if(isset($mainMenu)) $mainMenu->Generate(); ?>

<div class="backgroundContent">
	<div id="mainContent">
		<div class="img">
			<div class="appTitle"><?php echo SYS_TITLE; ?></div>
		</div>
		<?php //echo '<pre>PAGE_STACK: '; var_dump(unserialize($_SESSION[PAGE_STACK])); echo '</pre>'; ?>
	</div>
</div>

<div id="divDialog"></div>
<div id="divDialog1"></div>
<div id="divDialog2"></div>
<div id="divLoadingDlg"></div>
<br /><br />
<footer><?php echo SYS_COPYRIGHT; ?></footer>
</body>
</html>
