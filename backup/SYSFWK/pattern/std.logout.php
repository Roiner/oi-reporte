<?php
// Destruir todas las variables de sesi�n
$_SESSION = array();

// Borrar la cookie de sesion si existe
if(ini_get("session.use_cookies")) {
	$params = session_get_cookie_params();
	setcookie(session_name(), '', time() - 42000,
		$params["path"], $params["domain"],
		$params["secure"], $params["httponly"]
	);
}

// Destruir la sesi�n
session_destroy();

// Ir a p�gina de login
header("Location: {$GLOBALS['url_root']}login.php");
?>