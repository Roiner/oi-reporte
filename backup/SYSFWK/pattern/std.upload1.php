<?php
// *****************************************************************************
// Patron para procesar archivos subidos al servidor en el array $_FILES
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

/** Sumario:
	* Patrón para procesar las peticiones ajax de carga de archivos al servidor.
	* Retorna un JSON con los siguientes datos:
	* - error: (boolean) true en caso de error, false si todo bien.
	* - code: (integer) Código de error, 0 si no hay error.
	* - info: (string) Mensaje descriptivo del resultado de la operación.
	* - userfilename: (string) Nombre original del archivo en la máquina cliente.
	* - serverfilename: (string) Nombre del archivo en el servidor.
	*
	* Variables utilizadas:
	* $structure['userfile']: (string) Clave del array $_FILES donde se carga el archivo (requerida).
	* $structure['target']: (string) Ruta destino (completa o relativa, sin el nombre) del archivo (requerida).
	* $structure['postname']: (string) Clave del array $_POST que contiene el archivo (requerida).
	* $structure['allow_ext']: (array) Lista de extensiones permitidas (opcional).
	* $structure['denied_ext']: (array) Lista de extensiones denegadas (opcional).
	* $structure['max_size']: (integer) Tamaño máximo permitido en Bytes del archivo (opcional).
	* $structure['min_size']: (integer) Tamaño mínimo permitido en Bytes del archivo (opcional).
	*/

// Variables requeridas
isset($structure['userfile']) or die('Error: $structure["userfile"] debe estar definido');
isset($structure['target']) or die('Error: $structure["target"] debe estar definido.');
isset($structure['postname']) or die('Error: $structure["postname"] debe estar definido.');

// Variables predefinidas
$structure['task_id'] = get_script_name();
$structure['pattern_id'] = 'std.upload1';
$structure['pattern_type'] = 'upload1';
$_SESSION[CURRENT_TASK] = array(
	'id' => $structure['task_id'],
	'pattern' => array('id' => $structure['pattern_id'], 'type' => $structure['pattern_type']),
);

// Verificar roles permitidos
include($GLOBALS['path_fwk_script'] . 'check_permission.php');

// Mensajes de error de carga de archivos
function upload_codeToMessage($code) {
	switch($code) {
		case UPLOAD_ERR_INI_SIZE:
			$message = "El archivo subido excede la directiva upload_max_filesize (" . ini_get('upload_max_filesize') . ") en php.ini";
			break;
		case UPLOAD_ERR_FORM_SIZE:
			$message = "El archivo subido excede la directiva MAX_FILE_SIZE que fue especificada en el formulario HTML";
			break;
		case UPLOAD_ERR_PARTIAL:
			$message = "El archivo subido fue sólo parcialmente cargado";
			break;
		case UPLOAD_ERR_NO_FILE:
			$message = "Ningún archivo fue subido";
			break;
		case UPLOAD_ERR_NO_TMP_DIR:
			$message = "Falta la carpeta temporal";
			break;
		case UPLOAD_ERR_CANT_WRITE:
			$message = "No se pudo escribir el archivo en el disco";
			break;
		case UPLOAD_ERR_EXTENSION:
			$message = "Una extensión de PHP detuvo la carga de archivos";
			break;
		default:
			$message = "Unknown upload error";
			break;
	}
	return $message;
} 

// Resultado
$result = array('error' => true, 'code' => 100, 'info' => '$_FILES["' . $structure['userfile'] . '"] no está definido');

// Si hay archivo cargado
if(isset($_FILES[$structure['userfile']])) {
	$result['userfilename'] = $_FILES[$structure['userfile']]['name'];
	if($_FILES[$structure['userfile']]['error'] == UPLOAD_ERR_OK) {
		$fileOk = true;
		// Obtener la extension
		$ext = pathinfo($_FILES[$structure['userfile']]['name'], PATHINFO_EXTENSION);
		
		// Validar si la extensión es permitida
		if(!empty($structure['allow_ext'])) {
			if(!in_array($ext, $structure['allow_ext'])) {
				$result['error'] = true;
				$result['code'] = 101;
				$result['info'] = "Los archivos de tipo $ext no están permitidos";
				$fileOk = false;
			}
		}
		elseif(!empty($structure['denied_ext'])) {
			// Validar si la extensión es denegada
			if(in_array($ext, $structure['denied_ext'])) {
				$result['error'] = true;
				$result['code'] = 102;
				$result['info'] = "Los archivos de tipo $ext no están permitidos";
				$fileOk = false;
			}
		}
		
		// Validar tamaño mínimo
		if(!empty($structure['min_size'])) {
			if($_FILES[$structure['userfile']]['size'] < $structure['min_size']) {
				$result['error'] = true;
				$result['code'] = 103;
				$result['info'] = "El archivo no puede ser menor de {$structure['min_size']} Bytes";
				$fileOk = false;
			}
		}
		
		// Validar tamaño máximo
		if(!empty($structure['max_size'])) {
			if($_FILES[$structure['userfile']]['size'] > $structure['max_size']) {
				$result['error'] = true;
				$result['code'] = 104;
				$result['info'] = "El archivo no puede ser mayor de {$structure['max_size']} Bytes";
				$fileOk = false;
			}
		}
		
		if($fileOk) {
			// Preparar nombre del archivo
			if(!empty($_POST[$structure['postname']])) {
				$fileName = "{$_POST[$structure['postname']]}.$ext";
			}
			else {
				// Si el nombre no se suministra en el $_POST se toma el nombre original del archivo
				$fileName = basename($_FILES[$structure['userfile']]['name']);
			}
			$result['serverfilename'] = $fileName;
			
			// Mover el archivo a la ruta destino
			if(move_uploaded_file($_FILES[$structure['userfile']]['tmp_name'], $structure['target'] . $fileName)) {
				$result['error'] = false;
				$result['code'] = 0;
				$result['info'] = "El archivo {$result['userfilename']} ha sido subido correctamente con el nombre {$result['serverfilename']}.";
			}
			else {
				$result['error'] = true;
				$result['code'] = 105;
				$result['info'] = 'El archivo NO se ha podido subir.';
			}
		}
	}
	else {
		$result['error'] = true;
		$result['code'] = $_FILES[$structure['userfile']]['error'];
		$result['info'] = upload_codeToMessage($_FILES[$structure['userfile']]['error']);
	}
}

// Resultado
echo json_encode($result, JSON_FORCE_OBJECT);
?>