<?php
// *****************************************************************************
// Patron para listar/buscar registros de una entidad en la usando la vista mc.list1
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

// Variables requeridas
isset($structure['entity']) or die('Error: $structure["entity"] debe estar definido');
isset($structure['task_list']) or die('Error: $structure["task_list"] debe estar definido.');

// Variables predefinidas
$structure['task_id'] = get_script_name();
$structure['pattern_id'] = 'mc_list1';
$structure['pattern_type'] = 'list1';
$_SESSION[CURRENT_TASK] = array(
	'id' => $structure['task_id'],
	'pattern' => array('id' => $structure['pattern_id'], 'type' => $structure['pattern_type']),
);

// Verificar roles permitidos
include($GLOBALS['path_fwk_script'] . 'check_permission.php');

// Variables del patrón
isset($structure['container']) or $structure['container'] = 'mainContent';
isset($structure['auto_load_list']) or $structure['auto_load_list'] = true; // Cargar automaticamente la lista
isset($structure['auto_load_search']) or $structure['auto_load_search'] = true; // Cargar automaticamente la busqueda
isset($structure['page_stack']) or $structure['page_stack'] = true; // Carga el task en la pila

// Utilizar el estilo de lista sin selección si no está definido
if(!isset($structure['list_style']))
	$structure['list_style'] = LST_NOS;

// Funciones javascript de los botones utilizadas por el patrón
$jsActionButtons = array(
	BTN_ADD => array('javascript' => "alert('FALTA IMPLEMENTAR');"),
	BTN_MOD_SEL => array('javascript' => "alert('FALTA IMPLEMENTAR');"),
	BTN_DEL_SEL => array('javascript' => "alert('FALTA IMPLEMENTAR');"),
	BTN_BACK => array('javascript' => 'goBack();')
);

// Cargar botones basicos
include($GLOBALS['path_fwk_view'] . 'basicButtons.php');

// Variables de javascript
$javascript['global'] = (isset($javascript['global']) ? $javascript['global'] : '');
$javascript['def'] = (isset($javascript['def']) ? $javascript['def'] : '');
$javascript['exec'] = (isset($javascript['exec']) ? $javascript['exec'] : '');

// Variables search
if(isset($structure['search']['form'])) {
	if(!isset($structure['search']['container_id']))
		$structure['search']['container_id'] = 'div_search_' . strtolower($structure['entity']) . '_list1';
	if(!isset($structure['search']['form_id']))
		$structure['search']['form_id'] = 'frm_search_' . strtolower($structure['entity']) . '_list1';
	if(!isset($structure['search'][BTN_SEARCH]))
		$structure['search'][BTN_SEARCH] = array_merge($basicButtons[BTN_SEARCH], array('javascript' => 'search();'));
	
	// Crear objeto entidad
	require_once($GLOBALS['path_class'] . "{$structure['entity']}.class.php");
	$objEntity = new $structure['entity']();
	
	// Obtener variables de busqueda anteriores
	if(empty($searchData))
		$searchData = array();
	if(isset($_SESSION[$structure['task_list']]))
		$searchData = unserialize($_SESSION[$structure['task_list']]);
	unset($searchData['_page_num']); unset($searchData['_page_rows']); unset($searchData['_order_by']);
	$active = (empty($searchData) ?  'false' : 0);

	// Obtener datos extras
	$objEntity->getExtraData(
		array('task_id' => $structure['task_id'], 'pattern_id' => $structure['pattern_id'], 'pattern_type' => $structure['pattern_type'])
	);
	
	// Javascript de busqueda
	$javascript['def'] .= <<<EOS
// Buscar
function search(btn) {
	// Obtener datos a enviar
	var data = sysfwk.getControlData('{$structure['search']['form_id']}');
	
	// Cargar resultado
	loadList(data);
}

$('#{$structure['search']['container_id']}').accordion({collapsible: true, active: $active});
EOS;

	// Javascript de cargar búsqueda
	if($structure['auto_load_search']) {
		$javascript['exec'] .= <<<EOS
		// Cargar lista
		search();
EOS;
	}
}
else {
	// Javascript cargar lista
	if($structure['auto_load_list']) {
		$javascript['exec'] .= <<<EOS
		// Cargar lista
		loadList({});
EOS;
	}
}

// Actualizar page_stack
if($structure['page_stack']) {
	PageStack::update(array('task_id' => $structure['task_id'], 'container' => $structure['container'], 'params' => $_GET));
}

// Preparar botones
// -> Boton agregar
if(empty($structure['task_add'])) {
	unset($actionButtons[BTN_ADD]);
}
else {
	if(!isset($structure['task_add_container'])) $structure['task_add_container'] = 'mainContent';
	$actionButtons[BTN_ADD] = array_merge($actionButtons[BTN_ADD],
		array('javascript' => "sysfwk.loadPage('" . $GLOBALS['url_task'] . $structure['task_add']
			. "', '{$structure['task_add_container']}');")
	);
}

// -> Boton Editar selección
if(empty($structure['task_mod'])) {
	unset($actionButtons[BTN_MOD_SEL]);
}
else {
	$actionButtons[BTN_MOD_SEL] = array_merge($actionButtons[BTN_MOD_SEL],
		array('javascript' => "editSel();")
	);
}

// -> Boton Eliminar selección
if(empty($structure['task_del'])) {
	unset($actionButtons[BTN_DEL_SEL]);
}
else {
	$actionButtons[BTN_DEL_SEL] = array_merge($actionButtons[BTN_DEL_SEL],
		array('javascript' => "delSel();")
	);
}
$structure['buttons'] = array_values($actionButtons);

// Cargar la vista
include($GLOBALS['path_fwk_view'] . 'mc.list1.php');
?>
<script>
// Definiciones globales
<?php echo $javascript['global']; ?>

// Script de inicio
$(document).ready(function() {
	// Cargar lista
	function loadList(data) {
		sysfwk.loadPage('<?php echo $GLOBALS['url_task'] . $structure['task_list']; ?>', 'listContainer', data, sysfwk.POST);
	}
	
	// Editar varios registros
	function editSel() {
		alert('FALTA IMPLEMENTAR');
	}
	
	// Editar varios registros
	function delSel() {
		alert('FALTA IMPLEMENTAR');
	}
	
	// Ir al script anterior
	function goBack() {
		sysfwk.scriptPrevious({'task_id': '<?php echo $structure['task_id']; ?>'});
	}
	
	// Definiciones
	<?php echo $javascript['def']; ?>

	// Ejecuciones
	<?php echo $javascript['exec']; ?>
});
</script>