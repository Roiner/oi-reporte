<?php
// *****************************************************************************
// Patron que implementa un dialogo para autocompletar utilizando la vista dlg.acmp
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

// Si no está definido el container tomarlo del request si exsite
if(!isset($structure['container']) && isset($_REQUEST['container']))
	$structure['container'] = $_REQUEST['container'];

// Si no está definido el js_func tomarlo del request si exsite
if(!isset($structure['js_func']) && isset($_REQUEST['js_func']))
	$structure['js_func'] = $_REQUEST['js_func'];

// Variables requeridas
isset($structure['entity']) or die('Error: $structure["entity"] debe estar definido');
isset($structure['method']) or die('Error: $structure["method"] debe estar definido');
isset($structure['container']) or die('Error: $structure["container"] debe estar definido');
isset($structure['js_func']) or die('Error: $structure["js_func"] debe estar definido');

// Variables predefinidas
$structure['task_id'] = get_script_name();
$structure['pattern_id'] = 'dlg_acmp';
$structure['pattern_type'] = 'acmp';
$_SESSION[CURRENT_TASK] = array(
	'id' => $structure['task_id'],
	'pattern' => array('id' => $structure['pattern_id'], 'type' => $structure['pattern_type']),
);

// Verificar roles permitidos
include($GLOBALS['path_fwk_script'] . 'check_permission.php');

// Funciones javascript de los botones utilizadas por el patrón
$jsActionButtons = array(
	BTN_OK => array('javascript' => "if(acmp_result()) $(this).dialog('close');"),
	BTN_CANCEL => array('javascript' => "$(this).dialog('close');")
);

// Cargar botones basicos
include($GLOBALS['path_fwk_view'] . 'basicButtons.php');

// Variables de javascript
$javascript['global'] = (isset($javascript['global']) ? $javascript['global'] : '');
$javascript['def'] = (isset($javascript['def']) ? $javascript['def'] : '');
$javascript['exec'] = (isset($javascript['exec']) ? $javascript['exec'] : '');

// ID del panel de mensajes
isset($structure['msg_id']) or $structure['msg_id'] = 'msg_' . strtolower($structure['entity']) . '_acmp';

// Array de mensajes
if(isset($messages)) {
	$messages['id'] = $structure['msg_id'];
}
else
	$messages = array('id' => $structure['msg_id'], 'state' => SC_HIDDEN);

// Opciones por defecto del dialogo
isset($structure['dlg_options']['width']) or $structure['dlg_options']['width'] = 400;

// Valores por defecto de campos autocompletar
isset($structure['search']['label']) or $structure['search']['label'] = 'Buscar:';
isset($structure['search']['id']) or $structure['search']['id'] = 'search_' . strtolower($structure['entity']) . '_acmp';
isset($structure['search']['name']) or $structure['search']['name'] = 'search';
isset($structure['search']['maxlength']) or $structure['search']['maxlength'] = 30;
isset($structure['result']['label']) or $structure['result']['label'] = 'Resultado:';
isset($structure['result']['id']) or $structure['result']['id'] = 'result_' . strtolower($structure['entity']) . '_acmp';
isset($structure['result']['name']) or $structure['result']['name'] = $structure['result']['id'];
isset($structure['result']['size']) or $structure['result']['size'] = 10;

// Preparar botones
$structure['buttons'] = array_values($actionButtons);

// Capturar salida de la función beforeSearch si existe
$outbeforeSearch = '';
if(function_exists('beforeSearch')) {
	ob_start();
	beforeSearch();
	$outbeforeSearch = ob_get_clean();
}

// Javascript
$javascript['def'] .= <<<EOS
var acmp_timeout = null;
function acmp_search() {
	var param = {};
	param['{$structure['search']['name']}'] = $('#{$structure['search']['id']}').val();
	$outbeforeSearch
	sysfwk.loadSelect('{$structure['entity']}', '{$structure['method']}', param, '{$structure['result']['id']}');
	acmp_timeout = null;
}

function acmp_result() {
	var data = {
		value: $('#{$structure['result']['id']}').val(),
		text: $('#{$structure['result']['id']} option:selected').text()
	}
	if(data.value == null) {
		alert('Debe seleccionar un elemento');
		return false;
	}
	{$structure['js_func']}(data);
	return true;
}
EOS;

$javascript['exec'] .= <<<EOS
// Evento para buscar
$('#{$structure['search']['id']}').keyup(function(e) {
	if(acmp_timeout == null) {
		acmp_timeout = setTimeout(acmp_search, 300);
	}
});

// Busqueda inicial
if($('#{$structure['search']['id']}').val().length > 0)
	acmp_search();

// Permitir seleccionar con doubleclick
$('#{$structure['result']['id']}').dblclick(function() {
	acmp_result();
	$('#{$structure['container']}').dialog('close');
});

// Seleccionar el campo de busqueda
$('#{$structure['search']['id']}').focus();
EOS;

// Cargar la vista
include($GLOBALS['path_fwk_view'] . 'dlg.acmp.php');
?>