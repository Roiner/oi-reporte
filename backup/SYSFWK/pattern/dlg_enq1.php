<?php
// *****************************************************************************
// Patron que implementa el script enq1 (Ver un registro de una tabla) en la
// 	vista dlg.abm1
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

// Variables requeridas
isset($structure['entity']) or die('Error: $structure["entity"] debe estar definido');
isset($structure['form_action']) or die('Error: $structure["form_action"] debe estar definido.');

// Variables predefinidas
$structure['task_id'] = get_script_name();
$structure['pattern_id'] = 'dlg_enq1';
$structure['pattern_type'] = 'enq1';
$_SESSION[CURRENT_TASK] = array(
	'id' => $structure['task_id'],
	'pattern' => array('id' => $structure['pattern_id'], 'type' => $structure['pattern_type']),
);

// Verificar roles permitidos
include($GLOBALS['path_fwk_script'] . 'check_permission.php');

// Contenedor del contenido
isset($structure['container']) or $structure['container'] = 'divDialog';

// Funciones javascript de los botones utilizadas por el patrón
$jsActionButtons = array(
	BTN_COPY => array('javascript' => 'frmCopy(event.currentTarget);'),
	BTN_OK => array('javascript' => "$(this).dialog('close');")
);

// Cargar botones basicos
include($GLOBALS['path_fwk_view'] . 'basicButtons.php');

// Variables de javascript
$javascript['global'] = (isset($javascript['global']) ? $javascript['global'] : '');
$javascript['def'] = (isset($javascript['def']) ? $javascript['def'] : '');
$javascript['exec'] = (isset($javascript['exec']) ? $javascript['exec'] : '');

// ID del panel de mensajes
isset($structure['msg_id']) or $structure['msg_id'] = 'msg_' . strtolower($structure['entity']) . '_enq1';

// ID del panel de mensajes de validación
isset($structure['msg_val_id']) or $structure['msg_val_id'] = 'msg_val_' . strtolower($structure['entity']) . '_enq1';

// ID del form
isset($structure['form_id']) or $structure['form_id'] = 'frm_' . strtolower($structure['entity']) . '_enq1';

// Ejecutar scrpit controlador
include($GLOBALS['path_fwk_script'] . 'std.enq1.php');

// Preparar botones
$structure['buttons'] = array_values($actionButtons);

// Cargar la vista
include($GLOBALS['path_fwk_view'] . 'dlg.abm1.php');
?>