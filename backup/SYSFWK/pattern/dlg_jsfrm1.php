<?php
// *****************************************************************************
// Patron que implementa un dialogo para mostrar un form y ejecutar una función
//	js pasandole como parámetros un objeto JS con los datos del form.
// Utiliza la vista dlg.jsfrm1
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

// Si no está definido el container tomarlo del request si exsite
if(!isset($structure['container']) && isset($_REQUEST['container']))
	$structure['container'] = $_REQUEST['container'];

// Si no está definido el js_func tomarlo del request si exsite
if(!isset($structure['js_func']) && isset($_REQUEST['js_func']))
	$structure['js_func'] = $_REQUEST['js_func'];

// Variables requeridas
isset($structure['entity']) or die('Error: $structure["entity"] debe estar definido');
// isset($structure['method']) or die('Error: $structure["method"] debe estar definido');

// Variables predefinidas
$structure['task_id'] = get_script_name();
$structure['pattern_id'] = 'dlg_jsfrm1';
$structure['pattern_type'] = 'jsfrm1';
$_SESSION[CURRENT_TASK] = array(
	'id' => $structure['task_id'],
	'pattern' => array('id' => $structure['pattern_id'], 'type' => $structure['pattern_type']),
);

// Verificar roles permitidos
include($GLOBALS['path_fwk_script'] . 'check_permission.php');

// Funciones javascript de los botones utilizadas por el patrón
$jsActionButtons = array(
	BTN_COPY => array('javascript' => 'frmCopy(event.currentTarget);'),
	BTN_PASTE => array('javascript' => 'frmPaste();'),
	BTN_OK => array('javascript' => "if(jsfrm1_ok(event.currentTarget)) $(this).dialog('close');"),
	BTN_CANCEL => array('javascript' => "$(this).dialog('close');")
);

// Cargar botones basicos
include($GLOBALS['path_fwk_view'] . 'basicButtons.php');

// Variables de javascript
$javascript['global'] = (isset($javascript['global']) ? $javascript['global'] : '');
$javascript['def'] = (isset($javascript['def']) ? $javascript['def'] : '');
$javascript['exec'] = (isset($javascript['exec']) ? $javascript['exec'] : '');

// ID del panel de mensajes
isset($structure['msg_id']) or $structure['msg_id'] = 'msg_' . strtolower($structure['entity']) . '_jsfrm1';

// ID del panel de mensajes de validación
isset($structure['msg_val_id']) or $structure['msg_val_id'] = 'msg_val_' . strtolower($structure['entity']) . '_jsfrm1';

// ID del form
isset($structure['form_id']) or $structure['form_id'] = 'frm_' . strtolower($structure['entity']) . '_jsfrm1';

// Array de mensajes
if(isset($messages)) {
	$messages['id'] = $structure['msg_id'];
}
else {
	$messages = array('id' => $structure['msg_id'], 'state' => SC_HIDDEN);
}

// -----------------------------------------------------------------------------
// Crear objeto entidad
require_once($GLOBALS['path_class'] . "{$structure['entity']}.class.php");
$objEntity = new $structure['entity']();

// Obtener datos iniciales
$fieldData = $_REQUEST;
$fieldData = $objEntity->getInicialData(array_merge($fieldData, array(
	'task_id' => $structure['task_id'], 'pattern_id' => $structure['pattern_id'], 'pattern_type' => $structure['pattern_type']
)));
if($fieldData === false) {
	$messages['state'] = SC_ERROR;
	$messages['title'] = 'Error:';
	$messages['info'] = "Al obtener datos iniciales";
	if($_SESSION[USER_ROLE][0]['is_sys_dev']) {
		$messages['info'] .= " en '{$structure['entity']}->$metodo()'";
		$messages['extra'] = '<pre>' . $objEntity->getErrorsString() . "\n" . $objEntity->getQuery() . '</pre>';
	}
}

// Configuración de botones de acción
if(isset($actionButtons[BTN_PASTE]) && empty($_SESSION[CLIPBOARD]))
	$actionButtons[BTN_PASTE] = array_merge($actionButtons[BTN_PASTE], array('disabled' => 'y'));

// Obtener datos extras
$objEntity->getExtraData(array_merge($fieldData, array(
	'task_id' => $structure['task_id'], 'pattern_id' => $structure['pattern_id'], 'pattern_type' => $structure['pattern_type']
)));
// -----------------------------------------------------------------------------

// Preparar botones
$structure['buttons'] = array_values($actionButtons);

// Capturar salida de la función beforeOk si existe
$outBeforeOk = '';
if(function_exists('beforeOk')) {
	ob_start();
	beforeOk();
	$outBeforeOk = ob_get_clean();
}

// Javascript
$javascript['def'] .= <<<EOS

function jsfrm1_ok(btn) {
	// Validar form
	if(validator.form()) {
		$(btn).button('disable'); // Deshabilitar boton
		// Obtener datos a enviar
		var data = sysfwk.getControlData('{$structure['form_id']}');
		// alert(JSON.stringify(data));
		$outBeforeOk
		{$structure['js_func']}(data);
		return true;
	}
	else {
		validator.focusInvalid();
		return false;
	}
}
EOS;

$javascript['exec'] .= <<<EOS
EOS;

// Cargar la vista
include($GLOBALS['path_fwk_view'] . 'dlg.jsfrm1.php');
?>