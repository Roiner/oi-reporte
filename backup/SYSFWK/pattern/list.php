<?php
// *****************************************************************************
// Patron para listar registros de una entidad
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

// Variables requeridas
isset($structure['entity']) or die('Error: $structure["entity"] debe estar definido');
isset($structure['container']) or die('Error: $structure["container"] debe estar definido');
isset($structure['gridview']) or die('Error: $structure["gridview"] debe estar definido');

// Variables predefinidas
$structure['task_id'] = get_script_name();
$structure['pattern_id'] = 'list';
$structure['pattern_type'] = 'list';
$_SESSION[CURRENT_TASK] = array(
	'id' => $structure['task_id'],
	'pattern' => array('id' => $structure['pattern_id'], 'type' => $structure['pattern_type']),
);

// Verificar roles permitidos
include($GLOBALS['path_fwk_script'] . 'check_permission.php');

// Variables opcionales
if(!isset($structure['list_style'])) $structure['list_style'] = LST_NOS;
if(!isset($structure['gridview']['class'])) $structure['gridview']['class'] = 'gridview';
if(!isset($structure['page_rows'])) $structure['page_rows'] = 0;
if(!isset($structure['page_rows_list'])) $structure['page_rows_list'] = array(10, 25, 50, 75, 100);
if(!isset($structure['paginator'])) $structure['paginator'] = PAGINATOR_TOP + PAGINATOR_BOTTOM;
if(!isset($structure['order_asc'])) $structure['order_asc'] = 'ASC';
if(!isset($structure['order_desc'])) $structure['order_desc'] = 'DESC';
$where = array();

// Variables de javascript
$javascript['global'] = (isset($javascript['global']) ? $javascript['global'] : '');
$javascript['def'] = (isset($javascript['def']) ? $javascript['def'] : '');
$javascript['exec'] = (isset($javascript['exec']) ? $javascript['exec'] : '');

// Agregar selector o botones de comado según el estilo de la lista
if($structure['list_style'] == LST_SPL) {
	// Agregar columna de cmd al header
	if(isset($structure['gridview']['header'])) {
		$headerRows = count($structure['gridview']['header']);
		$headerCols = count($structure['gridview']['header'][0]);
		if($headerRows > 1)
			$structure['gridview']['header'][0][$headerCols]['th'] = array('rowspan' => $headerRows);
		$structure['gridview']['header'][0][$headerCols][] = array('control' => 'literal', 'content' => '&nbsp;');
	}
	
	// Agregar columna de cmd al footer
	if(isset($structure['gridview']['footer'])) {
		$footerRows = count($structure['gridview']['footer']);
		$footerCols = count($structure['gridview']['footer'][0]);
		if($footerRows > 1)
			$structure['gridview']['footer'][0][$footerCols]['td'] = array('rowspan' => $footerRows);
		$structure['gridview']['footer'][0][$footerCols][] = array('control' => 'literal', 'content' => '&nbsp;');
	}
	
	// Preparar botones de comando
	$colCmd = array();
	$colCmd['td'] = array('class' => 'cmd');
	if(isset($structure['task_enq'])) {
		if(!isset($structure['task_enq_container'])) $structure['task_enq_container'] = 'mainContent';
		$colCtrl = array('control' => 'button', 'value' => BTN_ENQ, 'content' => 'Ver detalles del registro');
		if(isset($structure['task_enq_condition'])) $colCtrl['condition'] = $structure['task_enq_condition'];
		$colCmd[] = $colCtrl;
		$javascript['def'] .= "\n$('#" . $structure['container'] .
			" button[value=" . BTN_ENQ . "]').button({text: false, icons: {primary: 'ui-icon-zoomin'}});\n";
		$javascript['exec'] .= "$('#" . $structure['container'] . " button[value=" . BTN_ENQ .
			"]').click(function(){ btnCmd(this, '{$structure['task_enq']}', '{$structure['task_enq_container']}'); });\n";
	}
	if(isset($structure['task_mod'])) {
		if(!isset($structure['task_mod_container'])) $structure['task_mod_container'] = 'mainContent';
		$colCtrl = array('control' => 'button', 'value' => BTN_MOD, 'content' => 'Editar registro');
		if(isset($structure['task_mod_condition'])) $colCtrl['condition'] = $structure['task_mod_condition'];
		$colCmd[] = $colCtrl;
		$javascript['def'] .= "$('#" . $structure['container'] .
			" button[value=" . BTN_MOD . "]').button({text: false, icons: {primary: 'ui-icon-pencil'}});\n";
		$javascript['exec'] .= "$('#" . $structure['container'] . " button[value=" . BTN_MOD .
			"]').click(function(){ btnCmd(this, '{$structure['task_mod']}', '{$structure['task_mod_container']}'); });\n";
	}
	if(isset($structure['task_del'])) {
		if(!isset($structure['task_del_container'])) $structure['task_del_container'] = 'mainContent';
		$colCtrl = array('control' => 'button', 'value' => BTN_DEL, 'content' => 'Eliminar registro');
		if(isset($structure['task_del_condition'])) $colCtrl['condition'] = $structure['task_del_condition'];
		$colCmd[] = $colCtrl;
		$javascript['def'] .= "$('#" . $structure['container'] .
			" button[value=" . BTN_DEL . "]').button({text: false, icons: {primary: 'ui-icon-trash'}});\n";
		$javascript['exec'] .= "$('#" . $structure['container'] . " button[value=" . BTN_DEL .
			"]').click(function(){ btnCmd(this, '{$structure['task_del']}', '{$structure['task_del_container']}'); });\n";
	}
	
	// Agregar columna de cmd al item
	foreach($structure['gridview']['items'] as $itemNum => $item) {
		$itemCols = count($item);
		$structure['gridview']['items'][$itemNum][$itemCols] = $colCmd;
	}
} // if($structure['list_style'] == LST_SPL)
/*elseif($structure['list_style'] == LST_MLT) {
	// Agregar columna de selección al header
	if(isset($structure['gridview']['header'])) {
		$headerRows = count($structure['gridview']['header']);
		$headerCols = count($structure['gridview']['header'][0]);
		if($headerRows > 1)
			$structure['gridview']['header'][0][$headerCols]['th'] = array('rowspan' => $headerRows);
		$structure['gridview']['header'][0][$headerCols][] = array('control' => 'literal', 'content' => '&nbsp;');
	}
}*/

// ID del panel de mensajes
isset($structure['msg_id']) or $structure['msg_id'] = 'msg_' . strtolower($structure['entity']) . '_list';

// Obtener variables del POST
$scriptVarsPost = $_POST;

// Obtener variables del script guardados anteriormente
$scriptVarsAnt = (isset($_SESSION[$structure['task_id']]) ? unserialize($_SESSION[$structure['task_id']]) : array());

// Determinar variables actuales
$scriptVars = array_merge($scriptVarsAnt, $scriptVarsPost);
foreach($scriptVars as $key => $value) {
	if(empty($value) && !is_numeric($value))
		unset($scriptVars[$key]);
}
unset($key, $value);
if(!isset($scriptVars['_page_num'])) $scriptVars['_page_num'] = 1;
if(!isset($scriptVars['_page_rows'])) $scriptVars['_page_rows'] = $structure['page_rows'];

// Guardar variables en sessión
$_SESSION[$structure['task_id']] = serialize($scriptVars);

// Preparar variables de filtro de consulta
$where = $scriptVars;
unset($where['_page_num'], $where['_page_rows'], $where['_order_by']);

// Crear objeto entidad
require_once($GLOBALS['path_class'] . "{$structure['entity']}.class.php");
$objEntity = new $structure['entity']();

// Array de mensajes
$messages = array('id' => $structure['msg_id'], 'state' => SC_HIDDEN);

// Array de datos
$fieldData = array();

// Obtener datos extras
$objEntity->getExtraData(
	array('task_id' => $structure['task_id'], 'pattern_id' => $structure['pattern_id'], 'pattern_type' => $structure['pattern_type'])
);

// echo '<pre>fieldData: '; var_dump($objEntity->getForeignDataList()); echo '</pre>'; // DEBUG

// Preparar paginado y orden
$objEntity->setPageNo($scriptVars['_page_num']);
$objEntity->setRowsPerPage($scriptVars['_page_rows']);
if(!empty($scriptVars['_order_by']))
	$objEntity->sqlOrderBy = json_decode($scriptVars['_order_by'], true);

// Preparar where si hay configuración especial
// echo '<pre>where: '; var_dump($where); echo '</pre>'; // DEBUG
if(!empty($structure['where'])) {
	$whereFixed = array('WHERE' => ':t', 'PARAMS' => array('t' => true));
	foreach($structure['where'] as $filter) {
		if(array_keys_exists($filter['fields'], $where)) {
			$whereFixed['WHERE'] .= " AND {$filter['sql']}";
			foreach($filter['params'] as $param => $field) {
				$whereFixed['PARAMS'][$param] = $where[$field];
			}
		}
	}
	unset($filter, $param, $field);
	$where = $whereFixed;
}

// Realizar consulta
// echo '<pre>sqlOrderBy: '; var_dump($objEntity->sqlOrderBy); echo '</pre>'; // DEBUG
// echo '<pre>where: '; var_dump($where); echo '</pre>'; // DEBUG
$fieldData = $objEntity->getRecords($where);
// echo '<pre>fieldData: '; var_dump($fieldData); echo $objEntity->getQuery() . '</pre>'; // DEBUG
if($fieldData === false) {
	// Preparar mensajes
	$messages['state'] = SC_ERROR;
	$messages['title'] = 'Error:';
	$messages['info'] = "Al agregar consultar en '{$structure['entity']}->getRecords()'";
	$messages['extra'] = '<pre>' . $objEntity->getErrorsString() . "\n" . $objEntity->getQuery() . '</pre>';
	$structure['paginator'] = PAGINATOR_NONE;
}
elseif(empty($fieldData)) {
	$messages['state'] = SC_OK;
	$messages['title'] = 'Información:';
	$messages['info'] = 'No se encontraron registros';
	$messages['extra'] = '';
	$structure['paginator'] = PAGINATOR_NONE;
}
else {
	$pageDisplay = "Página {$scriptVars['_page_num']} de {$objEntity->getLastPage()} ({$objEntity->getNumRows()} registros)";
}

// Agregar clase para la creacion de controles
require_once($GLOBALS['path_fwk_class'] . 'StdView.php');

// Generador de controles del forms
$fieldSpec = $objEntity->getFieldSpec();
$foreignDataList = $objEntity->getForeignDataList();
$gridview = new StdView($structure['gridview'], $fieldData, $fieldSpec, $foreignDataList);

// Panel de mensajes
$messageId = $structure['msg_id'];
include($GLOBALS['path_fwk_view'] . 'std.message.php');
unset($messageId);

echo '<br />';
// Paginador superior
if($structure['paginator'] & PAGINATOR_TOP) {
	$paginator['id'] = 'top_paginator_' . strtolower($structure['entity']);
	$paginator['page_display'] = $pageDisplay;
	$paginator['page_rows'] = $scriptVars['_page_rows'];
	$paginator['page_rows_list'] = $structure['page_rows_list'];
	include($GLOBALS['path_fwk_view'] . 'std.paginator.php');
}

$gridview->generate(1);

// Paginador inferior
if($structure['paginator'] & PAGINATOR_BOTTOM) {
	$paginator['id'] = 'bottom_paginator_' . strtolower($structure['entity']);
	$paginator['page_display'] = $pageDisplay;
	$paginator['page_rows'] = $scriptVars['_page_rows'];
	$paginator['page_rows_list'] = $structure['page_rows_list'];
	$paginator['load_js'] = true;
	include($GLOBALS['path_fwk_view'] . 'std.paginator.php');
}

// Capturar salida de la función beforeSendCmd si existe
$outBeforeSendCmd = '';
if(function_exists('beforeSendCmd')) {
	ob_start();
	beforeSendCmd();
	$outBeforeSendCmd = ob_get_clean();
}
?>
<script>
// Definiciones globales
<?php echo $javascript['global']; ?>
	
// Script de inicio
$(document).ready(function() {
	// Comando de botones en fila
	function btnCmd(btn, task, container) {
		var $tr = $(btn).closest('tr');
		var data = sysfwk.getControlData($tr);
		<?php echo $outBeforeSendCmd; ?>
		sysfwk.loadPage('<?php echo $GLOBALS['url_task']; ?>' + task, container, data);
	}
	
	// Cargar lista
	function loadList(data) {
		sysfwk.loadPage('<?php echo $GLOBALS['url_task'] . $structure['task_id']; ?>',
			'<?php echo $structure['container']; ?>', data, sysfwk.POST);
	}
	
	// Cambiar pagina
	function changePage(cmd) {
		var pageNum = <?php echo $scriptVars['_page_num']; ?>;
		var pageRows = <?php echo $scriptVars['_page_rows']; ?>;
		var pageLast = <?php echo $objEntity->getLastPage(); ?>;
		var change = false;
		switch(cmd) {
			case 'F':
				if(pageNum > 1) {
					pageNum = 1; change = true;
				}
				break;
			case 'P':
				if(pageNum > 1) {
					pageNum--; change = true;
				}
				break;
			case 'N':
				if(pageNum < pageLast) {
					pageNum++; change = true;
				}
				break;
			case 'L':
				if(pageNum < pageLast) {
					pageNum = pageLast; change = true;
				}
				break;
			default:
				pageNum = 1; pageRows = cmd; change = true;
				break;
		}
		
		// Actualizar lista
		if(change) {
			var data = {};
			data['_page_num'] = pageNum;
			data['_page_rows'] = pageRows;
			loadList(data);
		}
	}
	
	// Definiciones
	<?php echo $javascript['def']; ?>
	
	<?php if(is_True($structure['gridview']['sorted'])) { ?>
	// Convertir los encabezados en columnas ordenables
	$('#<?php echo $structure['container']; ?> th[data-sort_field]').each(function() {
		var orderStr = '<?php if(isset($scriptVars['_order_by'])) echo $scriptVars['_order_by']; ?>';
		var field = $(this).attr('data-sort_field');
		var cont = $(this).html();
		var icon = 'triangle-2-n-s';
		
		// Verificar orden actual
		if(orderStr.length > 0) {
			var order = $.parseJSON(orderStr);
			if(order[field] != undefined) {
				icon = (order[field] == '<?php echo $structure['order_asc']; ?>' ? 'triangle-1-n' : 'triangle-1-s');
			}
		}
		
		// Crear control
		$(this).html('<span class="sorted">' + cont + '</span><span class="ui-icon ui-icon-' + icon + ' sorted"></span>');
		$(this).click(function() {
			var order = ($(this).find('.ui-icon-triangle-1-n').length > 0 ? '<?php echo $structure['order_desc']; ?>' : '<?php echo $structure['order_asc']; ?>');
			var data = {};
			data['_page_num'] = 1;
			data['_order_by'] = '{"' + $(this).attr('data-sort_field') + '":"' + order + '"}';
			loadList(data);
		});
	});
	<?php } ?>

	// Ejecuciones
	<?php if(isset($messages)) echo 'sysfwk.msgPanel(' . json_encode($messages, JSON_FORCE_OBJECT) . ');'; ?>
	
	<?php echo $javascript['exec']; ?>
});
</script>