<?php
// *****************************************************************************
// Vista que define la estructura básica del formulario principal ABM
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

/** Sumario:
	* Genera el HTML necesario para el formulario principal y guarda en $javascript['def']
	* las funciones comunes del los botones y el validador.
	*
	* Variables utilizadas:
	* $objEntity: Objeto entidad heredado de DB_Table (requerida).
	* $structure['container']: (string) ID del contenedor (requerida).
	* $structure['task_id']: (string) ID del task actual (requerida).
	* $structure['form_action']: (string) Nombre del archivo Task que procesará los datos del form (requerida).
	* $structure['msg_id']: (string) ID del panel de mensajes (requerida).
	* $structure['msg_val_id']: (string) ID del panel de mensajes de validaciones (requerida).
	* $structure['form_id']: (string) ID del form (requerida).
	* $structure['form']: (array) que define el contenido del formulario (requerida).
	* $structure['form_class']: (string) Clase CSS del form (opcional).
	*/

// Variables requeridas
isset($objEntity) or die('Error: $objEntity debe estar definido.');
isset($structure['container']) or die('Error: $structure["container"] debe estar definido.');
isset($structure['task_id']) or die('Error: $structure["task_id"] debe estar definido.');
isset($structure['form_action']) or die('Error: $structure["form_action"] debe estar definido.');
isset($structure['msg_id']) or die('Error: $structure["msg_id"] debe estar definido.');
isset($structure['msg_val_id']) or die('Error: $structure["msg_val_id"] debe estar definido.');
isset($structure['form_id']) or die('Error: $structure["form_id"] debe estar definido.');
isset($structure['form']) or die('Error: $structure["form"] debe estar definido.');

// Clase CSS general del form
$structure['form_class'] = (isset($structure['form_class']) ? $structure['form_class'] . ' formEntry' : 'formEntry');

// Agregar clase para la creacion de controles
require_once($GLOBALS['path_fwk_class'] . 'StdView.php');
if(!isset($fieldData) || $fieldData === false)
	$fieldData = array();

// Generador de controles del forms
// echo '<pre>'; var_dump($objEntity->getFieldSpec()); echo '</pre>'; // DEBUG
$fieldSpec = $objEntity->getFieldSpec();
$foreignDataList = $objEntity->getForeignDataList();
$frmCtrls = new StdView($structure['form'], $fieldData, $fieldSpec, $foreignDataList);
?>
<form id="<?php echo $structure['form_id']; ?>" method="post" class="<?php echo $structure['form_class']; ?>">
<!-- Mensajes de validación -->
<div id="<?php echo $structure['msg_val_id']; ?>" class="ui-state-error ui-helper-hidden msgVal"><span></span><ul></ul></div>

<!-- Cargar controles del formulario -->
<?php $frmCtrls->generate(1); ?>

</form>
<?php
// Definir url completa del task que procesará los datos del form
$urlFormAction = $GLOBALS['url_task'] . $structure['form_action'];

// Capturar salida de la función beforeSubmit si existe
$outBeforeSubmit = '';
if(function_exists('beforeSubmit')) {
	ob_start();
	beforeSubmit();
	$outBeforeSubmit = ob_get_clean();
}

// Funciones generales del formulario
$jsPasteButton = '';
if(isset($actionButtons[BTN_PASTE]))
	$jsPasteButton = <<<EOS
$(btn).parent().find("button:contains('{$actionButtons[BTN_PASTE]['label']}')").button('enable');
EOS;

$javascript['def'] .= <<<EOS

	// Copiar datos
	function frmCopy(btn) {
		// Copiar datos
		sysfwk.copyToClipBoard('{$structure['form_id']}');
		// Mensaje
		sysfwk.msgPanel({id: '{$structure['msg_id']}', title: 'Copiar:',
			info: 'Los datos han sido copiado al portapapeles', state: sysfwk.OK});
		$jsPasteButton
	}
	
	// Pegar datos
	function frmPaste() {
		if(confirm('Confirme que desea pegar los datos del portapapeles.\\nAlgunos datos actuales del formulario serán modificados.')) {
			// Copiar datos
			sysfwk.pageFromClipBoard('{$structure['form_id']}');
			// Mensaje
			sysfwk.msgPanel({id: '{$structure['msg_id']}', title: 'Pegar:',
				info: 'Los datos han sido pegados', state: sysfwk.OK});
		}
	}
	
	// Ejecutar Submit para guardar
	function frmSubmitSave(btn) {
		var submit = false;
		// Validar form
		if(validator.form()) {
			submit = true;
		}
		else {
			validator.focusInvalid();
		}
		if(submit) {
			frmSubmit(btn);
		}
	}
	
	// Ejecutar Submit para Eliminar
	function frmSubmitDelete(btn) {
		var submit = false;
		// Confirmar eliminar
		submit = confirm("Esta seguro que desea eliminar este registro?\\nUna vez eliminado, la información no podrá ser recuperada.");
		if(submit) {
			frmSubmit(btn);
		}
	}
	
	// Ejecutar Submit
	function frmSubmit(btn) {
		$(btn).button('disable'); // Deshabilitar boton
		// Obtener datos a enviar
		var data = sysfwk.getControlData('{$structure['form_id']}');
		// alert(JSON.stringify(data));
		$outBeforeSubmit
		
		// Cargar resultado
		sysfwk.loadPage('$urlFormAction', '{$structure['container']}', data, sysfwk.POST);
	}
	
	// Ir al script anterior
	function goBack() {
		sysfwk.scriptPrevious({'task_id': '{$structure['task_id']}'});
	}
	
	// Validation
	var validator = $('#{$structure['form_id']}').validate({
		highlight: function(element, errorClass) {
			$(element).addClass('ui-state-error');
		},
		unhighlight: function(element, errorClass) {
			$(element).removeClass('ui-state-error');
		},
		errorContainer: '#{$structure['msg_val_id']}',
		errorLabelContainer: '#{$structure['msg_val_id']} ul',
		wrapper: 'li',
		invalidHandler: function(event, validator) {
			if(validator.numberOfInvalids()) {
				$('#{$structure['msg_val_id']} span').html('Hay errores de validación:');
			}
		}
	});
EOS;
unset($outBeforeSubmit, $urlFormAction);
?>