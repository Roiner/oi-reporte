<?php
// *****************************************************************************
// Vista que define la estructura básica del contenido de un dialogo para
// autocompletar
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

/** Sumario:
	* Genera la estrutura de autocompletar (un campo de busqueda y un select de respuesta)
	* en un dialogo jqueryui con los siguiente componentes:
	* 	1) Panel de mensajes (vista std.message.php)
	* 	2) Controles para la busqueda y select de respuesta
	* 	3) Vista del dialogo (vista dlg.php)
	* Modifica la variable $structure['dlg_options']['close'] para que limpie la pila
	* despues de cerrar el dialogo.
	*
	* Variables utilizadas:
	* $structure['msg_id']: (string) ID del panel de mensajes (requerida).
	* $structure['task_id']: (string) ID del task actual (requerida).
	* $structure['search']['label']: (string) Contenido del label del campo de busqueda (requerida).
	* $structure['search']['id']: (string) ID del campo de busqueda (requerida).
	* $structure['search']['name']: (string) name del campo de busqueda (requerida).
	* $structure['search']['maxlength']: (integer) Cantidad máxima de caracteres del campo de busqueda (requerida).
	* $structure['search']['value']: (string) valor inicial del campo de busqueda (opcional).
	* $structure['search']['legend']: (string) Contenido de la leyenda del filedset de busqueda (opcional).
	* $structure['result']['id']: (string) ID del campo de resultado (requerida).
	* $structure['result']['name']: (string) name del campo de resultado (requerida).
	* $structure['result']['size']: (integer) size del campo de resultado (requerida).
	* $structure['result']['label']: (string) Contenido del label del campo de resultado (opcional).
	*/

// Variables requeridas
isset($structure['msg_id']) or die('Error: $structure["msg_id"] debe estar definido.');
isset($structure['task_id']) or die('Error: $structure["task_id"] debe estar definido.');
isset($structure['search']['label']) or die('Error: $structure["search"]["label"] debe estar definido.');
isset($structure['search']['id']) or die('Error: $structure["search"]["id"] debe estar definido.');
isset($structure['search']['name']) or die('Error: $structure["search"]["name"] debe estar definido.');
isset($structure['search']['maxlength']) or die('Error: $structure["search"]["maxlength"] debe estar definido.');
isset($structure['result']['id']) or die('Error: $structure["result"]["id"] debe estar definido.');
isset($structure['result']['name']) or die('Error: $structure["result"]["name"] debe estar definido.');
isset($structure['result']['size']) or die('Error: $structure["result"]["size"] debe estar definido.');

// Panel de mensajes
$messageId = $structure['msg_id'];
include('std.message.php');
unset($messageId);

// Controles
?>
<br />
<!-- Bloque de busqueda -->
<fieldset class="formEntry">
	<?php
	if(isset($structure['search']['legend']))
		echo "<legend>{$structure['search']['legend']}</legend>\n";
	echo '<label for="' . $structure['search']['id'] . '">' . $structure['search']['label'] . "</label>\n";
	echo '<input type="text" id="' . $structure['search']['id'] . '" name="' . $structure['search']['name'] .
		'" value="' . $structure['search']['value'] . '" maxlength="' . $structure['search']['maxlength'] . '" />';
	?>
</fieldset>

<!-- Bloque de resultado -->
<br />
<div class="result_acmp formEntry">
	<?php
	if(isset($structure['result']['label']))
		echo '<label for="' . $structure['result']['id'] . '">' . $structure['result']['label'] . '</label><br />';
	echo '<select id="' . $structure['result']['id'] . '" name="' . $structure['result']['name'] .
		'" size="' . $structure['result']['size'] . '"></select>';
	?>
</div>
<?php

// Agregar el js de mensaje si hay
if(isset($messages)) {
	$javascript['exec'] .= "\nsysfwk.msgPanel(" . json_encode($messages, JSON_FORCE_OBJECT) . ");";
}

// Modificar la función close del dlg para que limpie la pila despues de cerrar el dialago
$structure['dlg_options']['close'] =
	"function(event, ui){ $(this).dialog('destroy').empty(); sysfwk.scriptPreviousClean({'task_id': '{$structure['task_id']}'}); }";

// Generar dialogo
include('dlg.php');
?>