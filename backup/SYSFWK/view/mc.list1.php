<?php
// *****************************************************************************
// Vista que define la estructura básica del contenido del div 'mainContent'
// 	para busqueda y listado de registros de una entidad
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

/** Estructura de la vista mc_list1
	* $structure['location']: string para mostrar en la barra de ubicación.
	* $structure['title']: string para mostrar como título del task
	* $structure['search']: (opcional) array que define el contenido del formulario de busqueda
	* $structure['buttons']: (opcional) array que define los botones de acción
	* $structure['task_list']: Nombre del task que gestiona la lista de registros
	*/

// Variables requeridas
// isset($structure['task_list']) or die('Error: $structure["task_list"] debe estar definido.');
// isset($structure['msg_id']) or die('Error: $structure["msg_id"] debe estar definido.');
// isset($structure['msg_val_id']) or die('Error: $structure["msg_val_id"] debe estar definido.');
// isset($structure['form']) or die('Error: $structure["form"] debe estar definido.');

// Barra de ubicación
include('std.locationbar.php');

// Titulo (nombre del task)
include('std.tasktitle.php');

// Construir formulario de busqueda si es requerido
if(isset($structure['search'])) {
	// Variables requeridas de search
	isset($objEntity) or die('Error: $objEntity debe estar definido.');
	isset($structure['search']['container_id']) or die('Error: $structure["search"]["container_id"] debe estar definido.');
	isset($structure['search']['form_id']) or die('Error: $structure["search"]["form_id"] debe estar definido.');
	isset($structure['search']['form']) or die('Error: $structure["search"]["form"] debe estar definido.');
	isset($structure['search'][BTN_SEARCH]) or die('Error: $structure["search"][BTN_SEARCH] debe estar definido.');
	
	// Agregar clase para la creacion de controles
	require_once($GLOBALS['path_fwk_class'] . 'StdView.php');
	if(!isset($searchData) || $searchData === false)
		$searchData = array();

	// Generador de controles del forms
	$fieldSpec = $objEntity->getFieldSpec();
	$foreignDataList = $objEntity->getForeignDataList();
	$frmSearch = new StdView($structure['search']['form'], $searchData, $fieldSpec, $foreignDataList);
?>
<br />
<div id="<?php echo $structure['search']['container_id']; ?>">
<h3>Buscar</h3>
<form id="<?php echo $structure['search']['form_id']; ?>" method="post" class="formEntry" novalidate="novalidate">
<!-- Cargar controles del formulario -->
<?php
$frmSearch->generate(1);

// Crear boton buscar y reset
$buttons = array(BTN_SEARCH => $structure['search'][BTN_SEARCH]);
$javascript['def'] .= <<<EOS
$('#{$structure['search']['form_id']} div.buttons').append('<button id="{$structure['search']['form_id']}_reset" type="reset" value="Restaurar" title="Restaurar Campos">Restaurar</button>');
$('#{$structure['search']['form_id']}_reset').button({ icons: { primary: 'ui-icon-refresh' }});\n
EOS;
include('std.buttons.php');
?>
</form>
</div>
<br />
<?php
} // if(isset($structure['search']))

// Cargar panel de botones
if(isset($structure['buttons'])) {
	$buttons = &$structure['buttons'];
	include('std.buttons.php');
}
?>
<!-- Resultado de la consulta -->
<br />
<div id="listContainer" class="list"></div>
<br /><br /><br /><br />