<?php
// *****************************************************************************
// Define los botones básicos del sistema
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

// Botone basicos
$basicButtons = array();
$basicButtons[BTN_COPY] = array('id' => 'btn_' . BTN_COPY, 'label' => 'Copiar', 'title' => 'Copiar datos', 'icon_p' => 'copy',
	'javascript' => (isset($jsActionButtons[BTN_COPY]['javascript']) ? $jsActionButtons[BTN_COPY]['javascript'] : "alert('FALTA IMPLEMENTAR');"));
$basicButtons[BTN_PASTE] = array('id' => 'btn_' . BTN_PASTE, 'label' => 'Pegar', 'title' => 'Pegar datos', 'icon_p' => 'clipboard',
	'javascript' => (isset($jsActionButtons[BTN_PASTE]['javascript']) ? $jsActionButtons[BTN_PASTE]['javascript'] : "alert('FALTA IMPLEMENTAR');"));
$basicButtons[BTN_CANCEL] = array('id' => 'btn_' . BTN_CANCEL, 'label' => 'Cancelar', 'title' => 'Cancelar operación', 'icon_p' => 'cancel',
	'javascript' => (isset($jsActionButtons[BTN_CANCEL]['javascript']) ? $jsActionButtons[BTN_CANCEL]['javascript'] : "alert('FALTA IMPLEMENTAR');"));
$basicButtons[BTN_OK] = array('id' => 'btn_' . BTN_OK, 'label' => 'Aceptar', 'title' => 'Aceptar operación', 'icon_p' => 'check',
	'javascript' => (isset($jsActionButtons[BTN_OK]['javascript']) ? $jsActionButtons[BTN_OK]['javascript'] : "alert('FALTA IMPLEMENTAR');"));
$basicButtons[BTN_SAVE] = array('id' => 'btn_' . BTN_SAVE, 'label' => 'Guardar', 'title' => 'Guardar datos', 'icon_p' => 'disk',
	'javascript' => (isset($jsActionButtons[BTN_SAVE]['javascript']) ? $jsActionButtons[BTN_SAVE]['javascript'] : "alert('FALTA IMPLEMENTAR');"));
$basicButtons[BTN_DEL] = array('id' => 'btn_' . BTN_DEL, 'label' => 'Eliminar', 'title' => 'Eliminar datos', 'icon_p' => 'trash',
	'javascript' => (isset($jsActionButtons[BTN_DEL]['javascript']) ? $jsActionButtons[BTN_DEL]['javascript'] : "alert('FALTA IMPLEMENTAR');"));
$basicButtons[BTN_SEARCH] = array('id' => 'btn_' . BTN_SEARCH, 'label' => 'Buscar', 'title' => 'Buscar registros', 'icon_p' => 'search',
	'javascript' => (isset($jsActionButtons[BTN_SEARCH]['javascript']) ? $jsActionButtons[BTN_SEARCH]['javascript'] : "alert('FALTA IMPLEMENTAR');"));
$basicButtons[BTN_CLOSE] = array('id' => 'btn_' . BTN_CLOSE, 'label' => 'Cerrar', 'title' => 'Cerrar', 'icon_p' => 'close',
	'javascript' => (isset($jsActionButtons[BTN_CLOSE]['javascript']) ? $jsActionButtons[BTN_CLOSE]['javascript'] : "alert('FALTA IMPLEMENTAR');"));
$basicButtons[BTN_ADD] = array('id' => 'btn_' . BTN_ADD, 'label' => 'Nuevo', 'title' => 'Agregar nuevo registro', 'icon_p' => 'document',
	'javascript' => (isset($jsActionButtons[BTN_ADD]['javascript']) ? $jsActionButtons[BTN_ADD]['javascript'] : "alert('FALTA IMPLEMENTAR');"));
$basicButtons[BTN_MOD_SEL] = array('id' => 'btn_' . BTN_MOD_SEL, 'label' => 'Editar', 'title' => 'Editar registros seleccionados', 'icon_p' => 'pencil',
	'javascript' => (isset($jsActionButtons[BTN_MOD_SEL]['javascript']) ? $jsActionButtons[BTN_MOD_SEL]['javascript'] : "alert('FALTA IMPLEMENTAR');"));
$basicButtons[BTN_DEL_SEL] = array('id' => 'btn_' . BTN_DEL_SEL, 'label' => 'Eliminar', 'title' => 'Eliminar registro seleccionados', 'icon_p' => 'trash',
	'javascript' => (isset($jsActionButtons[BTN_DEL_SEL]['javascript']) ? $jsActionButtons[BTN_DEL_SEL]['javascript'] : "alert('FALTA IMPLEMENTAR');"));
$basicButtons[BTN_BACK] = array('id' => 'btn_' . BTN_BACK, 'label' => 'Regresar', 'title' => 'Regresar a la pantalla anterior', 'icon_p' => 'arrowreturnthick-1-w',
	'javascript' => (isset($jsActionButtons[BTN_BACK]['javascript']) ? $jsActionButtons[BTN_BACK]['javascript'] : "alert('FALTA IMPLEMENTAR');"));
	
// Preparar botones utilizados en el patrón
if(isset($structure['buttons'])) {
	foreach($structure['buttons'] as $k => $v) {
		if(is_string($k)) {
			if(is_string($v))
				$actionButtons[$k] = $basicButtons[$v];
			elseif(is_array($v))
				$actionButtons[$k] = $v;
		}
	}
}
else {
	foreach($jsActionButtons as $k => $v) {
		$actionButtons[$k] = $basicButtons[$k];
	}
}
unset($k, $v);
?>