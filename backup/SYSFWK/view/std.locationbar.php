<?php
// *****************************************************************************
// Script para incluir la barra de ubicación
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

if(isset($structure['location']))
	echo '<div class="locationBar ui-widget ui-widget-header ui-corner-all">' . $structure['location'] . '</div>';
?>