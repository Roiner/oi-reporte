<?php
// *****************************************************************************
// Vista que define la estructura básica de un dialogo
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

/** Sumario:
	* Genera el js necesario para convertir un contenedor en un dialgo jqueryui en
	* el siguiente orden:
	* 	1) Genera el js contenido en la variable $javascript['global'] y $javascript['def'].
	* 	2) Genere el js propio del dialgo.
	* 	3) Genera el js contenido en la variable $javascript['exec'].
	*
	* Variables utilizadas:
	* $structure['container']: (string) ID del contenedor del dialogo (requerida).
	* $structure['dlg_options']: (array) Opciones del dialogo (opcional).
	* $structure['title']: (string) Titulo del dialogo (opcional).
	* $structure['buttons']: (array) Botones del dialogo según std.buttons.php (opcional).
	* $javascript['global']: Definiciones (variables, funciones) de javascript globales (antes del document ready) (opcional).
	* $javascript['def']: Definiciones (variables, funciones) de javascript (opcional).
	* $javascript['exec']: javascript de ejecución (opcional).
	*/

// Variables requeridas
isset($structure['container']) or die('Error: $structure["container"] debe estar definido');

// Opciones por defecto del dialogo
$dlgDefaultOptions = array(
	'modal' => 'true',
	'close' => "function(event, ui){ $(this).dialog('destroy').empty(); }"
);

// Preparar opciones del dialogo
isset($structure['dlg_options']) or $structure['dlg_options'] = array();
if(isset($structure['title'])) $structure['dlg_options']['title'] = "'{$structure['title']}'";
$dlgOptions = array_merge($dlgDefaultOptions, $structure['dlg_options']);

// Preparar de botones del dialogo
if(isset($structure['buttons'])) {
	$jsButton = "";
	foreach($structure['buttons'] as $button) {
		$jsButton .= "\n{\n";
		$jsButton .= "text: '{$button['label']}',\n";
		if(isset($button['icon_p']) || isset($button['icon_s'])) {
			// Iconos en botones de JQueryUI V < 1.12.0
			if(V_JQUERY_UI < V_JQUERY_UI_1_12_0) {
				$jsButton .= "icons: {\n";
				if(isset($button['icon_p'])) $jsButton .= "primary: 'ui-icon-{$button['icon_p']}',\n";
				if(isset($button['icon_s'])) $jsButton .= "secondary: 'ui-icon-{$button['icon_s']}'\n";
				$jsButton .= "},\n";
			}
			else {
				// JQueryUI V 1.12.0 Deprecated: icons option; replaced with icon and iconPosition (#14744)
				if(isset($button['icon_p'])) $jsButton .= "icon: 'ui-icon-{$button['icon_p']}',\n";
			}
		}
		if(isset($button['disabled']) && is_True($button['disabled'])) {
			$jsButton .= "disabled: true,\n";
		}
		if(isset($button['javascript'])) $jsButton .= "click: function(event) { {$button['javascript']} }\n";
		$jsButton .= "},";
	}
	unset($button);
}
?>
<script>
// Definiciones globales
<?php echo $javascript['global']; ?>

// Script de inicio
$(document).ready(function() {
	// Definiciones
	<?php echo $javascript['def']; ?>
	
	// Contruir el dialogo
	$('#<?php echo $structure['container']; ?>').dialog({
<?php
foreach($dlgOptions as $key => $value)
	echo "\t\t$key: $value,\n";
echo "buttons: [$jsButton\n]\n";
?>
	});

	// Ejecuciones
	<?php echo $javascript['exec']; ?>
});
</script>