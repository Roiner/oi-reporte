<?php
// *****************************************************************************
// Vista que define la estructura básica de un formulario
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

/** Sumario:
	* Genera el HTML necesario para un formulario y guarda en $javascript['def']
	* las funciones comunes de los botones y el validador.
	*
	* Variables utilizadas: Todas las variables que utiliza esta vista deben estar
	* contenidas en el array $form con la siguiente estructura:
	* $form['attr']: (array, opcional) Atributos del form, ej. array('id' => 'my_form', 'class' => 'my_class').
	* $form['msg_val_id']: (string, opcional) ID del panel de mensajes de validaciones.
	* $form['ctrls']['structure']: (array, requerido) Define el contenido del formulario.
	* $form['ctrls']['data']: (array, opcional) fieldData de los controles del formulario.
	* $form['ctrls']['spec']: (array, requerido) Especificaciones de los datos de los controles del formulario.
	* $form['ctrls']['datalist']: (array, requerido) ForeignDataList de los datos de los controles del formulario.
	* $form['submit']: (array, opcional) Datos necesarios para el proceso de submit del formulario:
	* $form['submit']['form_action']: (string, requerido) URL que procesará los datos del formulario.
	* $form['submit']['container']: (string, requerido) ID del contenedor para el resultado del submit.
	* $form['submit']['msg_del']: (string, opcional) Mensaje al eliminar.
	* $form['copy_paste']: (array, opcional) Datos para las funciones de copy & paste:
	* $form['copy_paste']['paste_label']: (string, requerido) Label de botón paste.
	* $form['msg_id']: (string, opcional) ID del panel de mensajes.
	* $form['goback']: (string, opcional) ID del task actual.
	*/

// Variables requeridas
isset($form['ctrls']['structure']) or die('Error: $form["ctrls"]["structure"] debe estar definido.');
isset($form['ctrls']['spec']) or die('Error: $form["ctrls"]["spec"] debe estar definido.');
isset($form['ctrls']['datalist']) or die('Error: $form["ctrls"]["datalist"] debe estar definido.');

// Varibles opcionales
isset($form['attr']) or $form['attr'] = array();
if(isset($form['submit'])) {
	isset($form['submit']['form_action']) or die('Error: $form["submit"]["form_action"] debe estar definido.');
	isset($form['submit']['container']) or die('Error: $form["submit"]["container"] debe estar definido.');
	isset($form['msg_val_id']) or die('Error: $form["msg_val_id"] debe estar definido.');
	isset($form['attr']['id']) or die('Error: $form["attr"]["id"] debe estar definido.');
	isset($form['submit']['msg_del']) or
		$form['submit']['msg_del'] = "Esta seguro que desea eliminar este registro?\\nUna vez eliminado, la información no podrá ser recuperada.";
}
if(isset($form['copy_paste'])) {
	isset($form['copy_paste']['paste_label']) or die('Error: $form["copy_paste"]["paste_label"] debe estar definido.');
	isset($form['msg_id']) or die('Error: $form["msg_id"] debe estar definido.');
}
if(isset($form['msg_val_id'])) {
	isset($form['attr']['id']) or die('Error: $form["attr"]["id"] debe estar definido.');
}

// Clase CSS general del form
// $form['attr']['class'] = (isset($form['attr']['class']) ? $form['attr']['class'] . ' formEntry' : 'formEntry');

// Agregar clase para la creacion de controles
require_once($GLOBALS['path_fwk_class'] . 'StdView.php');
if(!isset($form['ctrls']['data']) || $form['ctrls']['data'] === false)
	$form['ctrls']['data'] = array();

// Generador de controles del forms
// echo '<pre>'; var_dump($objEntity->getFieldSpec()); echo '</pre>'; // DEBUG
// $fieldSpec = $objEntity->getFieldSpec();
// $foreignDataList = $objEntity->getForeignDataList();
$frmCtrls = new StdView($form['ctrls']['structure'], $form['ctrls']['data'], $form['ctrls']['spec'], $form['ctrls']['datalist']);
?>
<form <?php foreach($form['attr'] as $key => $value) { echo "$key=\"$value\" "; } ?>>
<?php if(isset($form['msg_val_id'])) { ?>
<!-- Mensajes de validación -->
<div id="<?php echo $form['msg_val_id']; ?>" class="ui-state-error ui-helper-hidden msgVal"><span></span><ul></ul></div>
<?php } ?>

<!-- Cargar controles del formulario -->
<?php $frmCtrls->generate(1); ?>

</form>
<?php
// Funciones necesarias para los procesos de copy & paste
if(isset($form['copy_paste'])) {
	$jsPasteButton = '';
	if(isset($form['copy_paste']['paste_label']))
		$jsPasteButton = <<<EOS
$(btn).parent().find("button:contains('{$form['copy_paste']['paste_label']}')").button('enable');
EOS;

	$javascript['def'] .= <<<EOS

	// Copiar datos
	function frmCopy(btn) {
		// Copiar datos
		sysfwk.copyToClipBoard('{$form['attr']['id']}');
		// Mensaje
		sysfwk.msgPanel({id: '{$form['msg_id']}', title: 'Copiar:',
			info: 'Los datos han sido copiado al portapapeles', state: sysfwk.OK});
		$jsPasteButton
	}
	
	// Pegar datos
	function frmPaste() {
		if(confirm('Confirme que desea pegar los datos del portapapeles.\\nAlgunos datos actuales del formulario serán modificados.')) {
			// Copiar datos
			sysfwk.pageFromClipBoard('{$form['attr']['id']}');
			// Mensaje
			sysfwk.msgPanel({id: '{$form['msg_id']}', title: 'Pegar:',
				info: 'Los datos han sido pegados', state: sysfwk.OK});
		}
	}
EOS;
	unset($jsPasteButton);
}


// Función necesaria para ir al task anterior
if(isset($form['goback'])) {
	$javascript['def'] .= <<<EOS

	// Ir al script anterior
	function goBack() {
		sysfwk.scriptPrevious({'task_id': '{$form['goback']}'});
	}
EOS;
}

// Funciones necesarias para el proceso de Submit del formulario
if(isset($form['submit'])) {
	// Capturar salida de la función beforeSubmit si existe
	$outBeforeSubmit = '';
	if(function_exists('beforeSubmit')) {
		ob_start();
		beforeSubmit();
		$outBeforeSubmit = ob_get_clean();
	}

	$javascript['def'] .= <<<EOS

	// Ejecutar Submit para guardar
	function frmSubmitSave(btn) {
		var submit = false;
		// Validar form
		if(validator.form()) {
			submit = true;
		}
		else {
			validator.focusInvalid();
		}
		if(submit) {
			frmSubmit(btn);
		}
	}
	
	// Ejecutar Submit para Eliminar
	function frmSubmitDelete(btn) {
		var submit = false;
		// Confirmar eliminar
		submit = confirm("{$form['submit']['msg_del']}");
		if(submit) {
			frmSubmit(btn);
		}
	}
	
	// Ejecutar Submit
	function frmSubmit(btn) {
		$(btn).button('disable'); // Deshabilitar boton
		// Obtener datos a enviar
		var data = sysfwk.getControlData('{$form['attr']['id']}');
		// alert(JSON.stringify(data));
		$outBeforeSubmit
		
		// Cargar resultado
		sysfwk.loadPage('{$form['submit']['form_action']}', '{$form['submit']['container']}', data, sysfwk.POST);
	}
EOS;
	unset($outBeforeSubmit);
}

if(isset($form['msg_val_id'])) {
	$javascript['def'] .= <<<EOS
	
	// Validation
	var validator = $('#{$form['attr']['id']}').validate({
		highlight: function(element, errorClass) {
			$(element).addClass('ui-state-error');
		},
		unhighlight: function(element, errorClass) {
			$(element).removeClass('ui-state-error');
		},
		errorContainer: '#{$form['msg_val_id']}',
		errorLabelContainer: '#{$form['msg_val_id']} ul',
		wrapper: 'li',
		invalidHandler: function(event, validator) {
			if(validator.numberOfInvalids()) {
				$('#{$form['msg_val_id']} span').html('Hay errores de validación:');
			}
		}
	});
EOS;
}
?>