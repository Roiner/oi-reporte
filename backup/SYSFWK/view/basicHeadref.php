<?php
// *****************************************************************************
// Define los enlaces básicos de css y js
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

// Deben estar definidas las constantes de versión de jquery y plugins asociados
(defined('V_JQUERY') && defined('V_JQUERY_UI') && defined('V_TIMEPICKER') && defined('V_VALIDATION'))
	or die('Error: V_JQUERY, V_JQUERY_UI, V_TIMEPICKER y V_VALIDATION deben estár definidas');

// Tema por defecto de jquery ui
defined('JQUERY_UI_THEME') or define('JQUERY_UI_THEME', 'redmond');

// Enlaces basicos
if(!isset($headRefLinks)) $headRefLinks = array();
$headRefLinks['css_jquery_ui']['type'] = 'css';
$headRefLinks['css_jquery_ui']['links'] = array(
	$GLOBALS['url_fwk_js'] . 'jquery-ui/jquery-ui-' . V_JQUERY_UI . '/themes/' . JQUERY_UI_THEME . '/jquery-ui.min.css'
);

$headRefLinks['css_timepicker']['type'] = 'css';
$headRefLinks['css_timepicker']['links'] = array($GLOBALS['url_fwk_js'] . 'jquery-ui/timepicker-' . V_TIMEPICKER . '/timepicker.min.css');

$headRefLinks['css_sysfwk']['type'] = 'css';
$headRefLinks['css_sysfwk']['links'] = array($GLOBALS['url_fwk_css'] . 'main.css');

$headRefLinks['js_jquery']['type'] = 'js';
$headRefLinks['js_jquery']['links'] = array($GLOBALS['url_fwk_js'] . 'jquery/jquery-' . V_JQUERY . '.min.js');

$headRefLinks['js_jquery_ui']['type'] = 'js';
$headRefLinks['js_jquery_ui']['links'] = array(
	$GLOBALS['url_fwk_js'] . 'jquery-ui/jquery-ui-' . V_JQUERY_UI . '/jquery-ui.min.js',
	$GLOBALS['url_fwk_js'] . 'jquery-ui/jquery-ui-' . V_JQUERY_UI . '/datepicker-es.js'
);

$headRefLinks['js_timepicker']['type'] = 'js';
$headRefLinks['js_timepicker']['links'] = array(
	$GLOBALS['url_fwk_js'] . 'jquery-ui/timepicker-' . V_TIMEPICKER . '/timepicker.min.js',
	$GLOBALS['url_fwk_js'] . 'jquery-ui/timepicker-' . V_TIMEPICKER . '/timepicker-es.js',
	$GLOBALS['url_fwk_js'] . 'jquery-ui/timepicker-' . V_TIMEPICKER . '/sliderAccess.js'
);

$headRefLinks['js_validation']['type'] = 'js';
$headRefLinks['js_validation']['links'] = array(
	$GLOBALS['url_fwk_js'] . 'jquery/validation-' . V_VALIDATION . '/jquery.validate.min.js',
	$GLOBALS['url_fwk_js'] . 'jquery/validation-' . V_VALIDATION . '/additional-methods.min.js',
	$GLOBALS['url_fwk_js'] . 'jquery/validation-' . V_VALIDATION . '/date-methods.js',
	$GLOBALS['url_fwk_js'] . 'jquery/validation-' . V_VALIDATION . '/localization/messages_es.min.js'
);

$headRefLinks['js_sysfwk']['type'] = 'js';
$headRefLinks['js_sysfwk']['links'] = array($GLOBALS['url_fwk_js'] . 'sysfwk.js');

// agregar css básicos
if(!isset($cssHeadRef)) $cssHeadRef = array();
$cssHeadRef = array_merge(array('css_jquery_ui', 'css_timepicker', 'css_sysfwk'), $cssHeadRef);

// agregar js básicos
if(!isset($jsHeadRef)) $jsHeadRef = array();
$jsHeadRef = array_merge(array('js_jquery', 'js_jquery_ui', 'js_timepicker', 'js_validation', 'js_sysfwk'), $jsHeadRef);
?>