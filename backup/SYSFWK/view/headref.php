<?php
// *****************************************************************************
// Script para incluir los enlaces a archivos css y js
// Espera que el array $headRef contenga los nombres de los enlaces en $headRefLinks
// 	a imprimir
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

/** Estructura de $headRefLinks
	* $headRefLinks[<link_name>]['type']: String con el tipo de enlace ('css' ó 'js')
	* $headRefLinks[<link_name>]['links']: Array con los urls de los enlaces
	* <link_name>: Nombre que identifica al enlace
	*/

// Requiere que el array $headRefLinks este definido
isset($headRefLinks) or die('Error: $headRefLinks debe estar defino');

// Imprimir enlaces
if(isset($headRef) && is_array($headRef)) {
	foreach($headRef as $ref) {
		if($headRefLinks[$ref]['type'] == 'css')
			$sLink = '<link rel="stylesheet" type="text/css" href="{REF}" />' . "\n";
		elseif($headRefLinks[$ref]['type'] == 'js')
			$sLink = '<script type="text/javascript" src="{REF}"></script>' . "\n";
		else
			die("Error: $ref no está definido como enlace");
		foreach($headRefLinks[$ref]['links'] as $link) {
			echo str_replace('{REF}', $link, $sLink);
		}
	}
}
?>