<?php
// *****************************************************************************
// Vista que define la estructura básica del contenido de un dialogo
// 	para formularios de tipo jsfrm1
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

/** Sumario:
	* Genera la estrutura de formularios tipo jsfrm1 en un dialogo jqueryui con los
	* siguiente componentes:
	* 	1) Panel de mensajes (vista std.message.php)
	* 	2) Formulario principal (vista std.form0.php)
	* 	3) Vista del dialogo (vista dlg.php)
	*
	* Variables utilizadas:
	* $structure['form_id']: (string, requerida) ID del formulario.
	* $structure['msg_id']: (string, requerida) ID del panel de mensajes.
	*/

// Variables requeridas
isset($structure['form_id']) or die('Error: $structure["form_id"] debe estar definido.');
isset($structure['msg_id']) or die('Error: $structure["msg_id"] debe estar definido.');

// Variables opcionales

// Panel de mensajes
$messageId = $structure['msg_id'];
include('std.message.php');
unset($messageId);

// Generador de controles del forms
$fieldSpec = $objEntity->getFieldSpec();
$foreignDataList = $objEntity->getForeignDataList();

// Formulario
$form['attr'] = array(
	'id' => $structure['form_id'],
	'class' => (isset($structure['form_class']) ? $structure['form_class'] . ' formEntry' : 'formEntry')
);
$form['msg_val_id'] = $structure['msg_val_id'];
$form['ctrls']['structure'] = &$structure['form'];
$form['ctrls']['data'] = &$fieldData;
$form['ctrls']['spec'] = $objEntity->getFieldSpec();
$form['ctrls']['datalist'] = $objEntity->getForeignDataList();
if(isset($actionButtons[BTN_PASTE])) {
	$form['copy_paste']['paste_label'] = $actionButtons[BTN_PASTE]['label'];
}
$form['msg_id'] = $structure['msg_id'];
echo '<br />';
include('std.form0.php');
echo '<br />';

// Agregar el js de mensaje si hay
if(isset($messages)) {
	$javascript['exec'] .= "\nsysfwk.msgPanel(" . json_encode($messages, JSON_FORCE_OBJECT) . ");";
}

// Generar dialogo
include('dlg.php');
?>