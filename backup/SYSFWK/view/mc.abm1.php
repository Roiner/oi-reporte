<?php
// *****************************************************************************
// Vista que define la estructura básica del contenido del div 'mainContent'
// 	para formularios de tipo abm1
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

/** Sumario:
	* Genera la estrutura de formularios tipo abm1 para el contenedor 'mainContent' con los
	* siguiente componentes:
	* 	1) Barra de ubicación (vista std.locationbar.php)
	* 	2) Titulo del task (vista std.tasktitle.php)
	* 	3) Panel de mensajes (vista std.message.php)
	* 	4) Formulario principal (vista std.form1.php)
	* 	5) Panel de botones (vista std.buttons.php)
	* Al final genera el js contenido en las variables $javascript['global'],
	* $javascript['def'] y $javascript['exec']; y el js del panel de mensajes.
	*
	* Variables utilizadas:
	* $structure['msg_id']: (string) ID del panel de mensajes (requerida).
	* $structure['location']: (string) Contenido de la barra de ubicación (opcional).
	* $structure['title']: (string) Contenido del título del task (opcional).
	* $structure['buttons']: (array) Estructura del panel de botones (opcional).
	* $structure['form_class']: (string) Clase CSS del form (opcional, utiliza 'ui-widget-content' si no se define).
	* $javascript['global']: (string) Definiciones (variables, funciones) de javascript globales (antes del document ready) (opcional).
	* $javascript['def']: (string) Codigo de deficiones js (opcional).
	* $javascript['exec']: (string) Codigo de ejecuciones js (opcional).
	*/

// Variables requeridas
isset($structure['msg_id']) or die('Error: $structure["msg_id"] debe estar defino.');

// Barra de ubicación
include('std.locationbar.php');

// Titulo (nombre del task)
include('std.tasktitle.php');

// Panel de mensajes
$messageId = $structure['msg_id'];
include('std.message.php');
unset($messageId);

// Formulario
isset($structure['form_class']) or $structure['form_class'] = 'ui-widget-content';
echo '<br />';
include('std.form1.php');
echo '<br />';

// Cargar panel de botones
if(isset($structure['buttons'])) {
	$buttons = &$structure['buttons'];
	include('std.buttons.php');
}
?>
<script>
// Definiciones globales
<?php echo $javascript['global']; ?>

// Script de inicio
$(document).ready(function() {
	// Definiciones
	<?php echo $javascript['def']; ?>

	// Ejecuciones
	<?php echo $javascript['exec']; ?>
	
	<?php if(isset($messages)) echo 'sysfwk.msgPanel(' . json_encode($messages, JSON_FORCE_OBJECT) . ');'; ?>
});
</script>