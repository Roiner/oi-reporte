<?php
// *****************************************************************************
// Vista que define la estructura básica del contenido del div 'mainContent'
// 	para formularios de tipo abm1
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

/** Estructura de la vista mc_abm1
	* $structure['location']: string para mostrar en la barra de ubicación.
	* $structure['title']: string para mostrar como título del task
	* $structure['msg_id']: string ID del panel de mensajes
	* $structure['form_id']: string ID del form
	* $structure['form']: array que define el contenido del formulario
	*/

// Variables requeridas
isset($objEntity) or die('Error: $objEntity debe estar defino.');
isset($structure['msg_id']) or die('Error: $structure["msg_id"] debe estar defino.');
isset($structure['msg_val_id']) or die('Error: $structure["msg_val_id"] debe estar defino.');
isset($structure['form_id']) or die('Error: $structure["form_id"] debe estar defino.');
isset($structure['form']) or die('Error: $structure["form"] debe estar defino.');

// Barra de ubicación
include('std.locationbar.php');

// Titulo (nombre del task)
include('std.tasktitle.php');

// Agregar clase para la creacion de controles
require_once($GLOBALS['path_fwk_class'] . 'StdView.php');
if(!isset($fieldData) || $fieldData === false)
	$fieldData = array();

// Generador de controles del forms
// echo '<pre>'; var_dump($objEntity->getFieldSpec()); echo '</pre>'; // DEBUG
$fieldSpec = $objEntity->getFieldSpec();
$foreignDataList = $objEntity->getForeignDataList();
$frmCtrls = new StdView($structure['form'], $fieldData, $fieldSpec, $foreignDataList);

// Panel de mensajes
$messageId = $structure['msg_id'];
include('std.message.php');
unset($messageId);
?>
<br />
<form id="<?php echo $structure['form_id']; ?>" method="post" class="ui-widget-content formEntry">
<!-- Mensajes de validación -->
<div id="<?php echo $structure['msg_val_id']; ?>" class="ui-state-error ui-helper-hidden msgVal"><span></span><ul></ul></div>

<!-- Cargar controles del formulario -->
<?php $frmCtrls->generate(1); ?>

</form>
<br />
<?php
// Cargar panel de botones
if(isset($structure['buttons'])) {
	$buttons = &$structure['buttons'];
	include('std.buttons.php');
}
?>
<script>
// Script de inicio
$(document).ready(function() {
	// Copiar datos
	function frmCopy() {
		// Copiar datos
		sysfwk.copyToClipBoard('<?php echo $structure['form_id']; ?>');
		// Mensaje
		sysfwk.msgPanel({id: '<?php echo $structure['msg_id']; ?>', title: 'Copiar:',
			info: 'Los datos han sido copiado al portapapeles', state: sysfwk.OK});
		$('#<?php echo $actionButtons[BTN_PASTE]['id']; ?>').button('enable');
	}
	
	// Pegar datos
	function frmPaste() {
		if(confirm('Confirme que desea pegar los datos del portapapeles.\nAlgunos datos actuales del formulario serán modificados.')) {
			// Copiar datos
			sysfwk.pageFromClipBoard('<?php echo $structure['form_id']; ?>');
			// Mensaje
			sysfwk.msgPanel({id: '<?php echo $structure['msg_id']; ?>', title: 'Pegar:',
				info: 'Los datos han sido pegados', state: sysfwk.OK});
		}
	}
	
	// Ejecutar Submit para guardar
	function frmSubmitSave(btn) {
		var submit = false;
		
		// Validar form
		if(validator.form()) {
			submit = true;
		}
		else {
			validator.focusInvalid();
		}
		
		if(submit) {
			frmSubmit(btn);
		}
	}
	
	// Ejecutar Submit para Eliminar
	function frmSubmitDelete(btn) {
		var submit = false;
		
		// Confirmar eliminar
		submit = confirm("Esta seguro que desea eliminar este registro?\nUna vez eliminado, la información no podrá ser recuperada.");
		
		if(submit) {
			frmSubmit(btn);
		}
	}
	
	// Ejecutar Submit
	function frmSubmit(btn) {
		$(btn).button('disable'); // Deshabilitar boton
		// Obtener datos a enviar
		var data = sysfwk.getControlData('<?php echo $structure['form_id']; ?>');
		// alert(JSON.stringify(data));
		<?php if(function_exists('beforeSubmit')) beforeSubmit(); ?>
		
		// Cargar resultado
		sysfwk.loadPage('<?php echo $GLOBALS['url_task'] . $structure['form_action']; ?>', 'mainContent', data, sysfwk.POST);
	}
	
	// Ir al script anterior
	function goBack() {
		sysfwk.scriptPrevious({'task_id': '<?php echo $structure['task_id']; ?>'});
	}
	
	// Validation
	var validator = $('#<?php echo $structure['form_id']; ?>').validate({
		highlight: function(element, errorClass) {
			$(element).addClass('ui-state-error');
		},
		unhighlight: function(element, errorClass) {
			$(element).removeClass('ui-state-error');
		},
		errorContainer: '#<?php echo $structure['msg_val_id']; ?>',
		errorLabelContainer: '#<?php echo $structure['msg_val_id']; ?> ul',
		wrapper: 'li',
		invalidHandler: function(event, validator) {
			if(validator.numberOfInvalids()) {
				$('#<?php echo $structure['msg_val_id']; ?> span').html('Hay errores de validación:');
			}
		}
	});
	// Definiciones
	<?php echo $javascript['def']; ?>

	// Ejecuciones
	<?php echo $javascript['exec']; ?>
	
	<?php if(isset($messages)) echo 'sysfwk.msgPanel(' . json_encode($messages, JSON_FORCE_OBJECT) . ');'; ?>
});
</script>