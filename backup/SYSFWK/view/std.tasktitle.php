<?php
// *****************************************************************************
// Script para incluir el título del task
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

if(isset($structure['title']))
	echo '<div class="taskTitle">' . $structure['title'] . '</div>';
?>