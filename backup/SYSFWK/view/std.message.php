<?php
// *****************************************************************************
// Script para incluir el panel de mensajes
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

// Variables requeridas
isset($messageId) or die('Error: $messageId debe estar defino.');
?>
<div id="<?php echo $messageId; ?>" class="message ui-helper-hidden">
	<div id="<?php echo $messageId; ?>_inner" class="ui-corner-all" style="padding: 0 .7em; text-align:left">
		<p><span id="<?php echo $messageId; ?>_icon" class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
		<strong id="<?php echo $messageId; ?>_title"></strong> <span id="<?php echo $messageId; ?>_info"></span></p>
		<div id="<?php echo $messageId; ?>_extra" style="overflow: auto;"></div>
	</div>
</div>