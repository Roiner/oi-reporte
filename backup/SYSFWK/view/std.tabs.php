<?php
// *****************************************************************************
// Vista que define la estructura básica de tabs
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

/** Sumario:
	* Genera la estrutura div para utilizador como tabs de jqueryui.
	* Al final genera el js contenido en las variables $javascript['def'],
	* $javascript['exec'] y $javascript['tabs_options'].
	*
	* Estructura de tabs $structure['tabs'][<n>][<prop>]
	* <n>: Número de tab (empieza en 0)
	* <prop>: Puede tener alguno de estos valores:
	* 	'id': (string, requerido) ID del tab.
	* 	'title': (string, requerido) Titulo del tab.
	* 	'content': (string, opcional) Contenido del panel del tab.
	*
	* Variables utilizadas:
	* $structure['tabs_id']: (string) ID del contenedor del tabs (requerida).
	* $structure['tabs']: (array) Estructura del tabs (requerida).
	* $structure['tabs_class']: (string) Clase CSS del div contenedor del tabs (opcional).
	* $javascript['def']: (string) Codigo de deficiones js (opcional).
	* $javascript['exec']: (string) Codigo de ejecuciones js (opcional).
	* $javascript['tabs_options']: (string) Objeto js con las opciones del tabs (opcional).
	*/

// Variables requeridas
isset($structure['tabs_id']) or die('Error: $structure["tabs_id"] debe estar defino.');
isset($structure['tabs']) or die('Error: $structure["tabs"] debe estar defino.');

// Sin opciones por defecto
isset($javascript['tabs_options']) or $javascript['tabs_options'] = '';
$tabsClass = '';
if(isset($structure['tabs_class']))
	$tabsClass = 'class="' . $structure['tabs_class'] . '"';

// Preparar tabs y panel
$tabs = ''; $panels = '';
foreach($structure['tabs'] as $tab) {
	$tabs .= "<li><a href='#{$tab['id']}'>{$tab['title']}</a></li>\n";
	$panels .= "<div id='{$tab['id']}'>" . (isset($tab['content']) ? $tab['content'] : '') . "</div>\n";
}
?>
<div id="<?php echo $structure['tabs_id']; ?>" <?php echo $tabsClass; ?> >
	<ul>
		<?php echo $tabs; ?>
	</ul>
	<?php echo $panels; ?>
</div>
<br /><br /><br /><br />
<script>
// Script de inicio
$(document).ready(function() {
	// Definiciones
	<?php echo $javascript['def']; ?>
	
	// Crear tabs
	$('#<?php echo $structure['tabs_id']; ?>').tabs(<?php echo $javascript['tabs_options']; ?>);

	// Ejecuciones
	<?php echo $javascript['exec']; ?>
});
</script>