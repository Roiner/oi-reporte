<?php
// *****************************************************************************
// Script para incluir el panel de botones
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

/** Estructura del panel de botones $buttons[<n>][<prop>]
	* <n>: Número de boton (empieza en 0)
	* <prop>: Puede tener alguno de estos valores:
	* 	'id': (string, requerido) ID del botón.
	* 	'label': (string, opcional) Texto del botón.
	* 	'name': (string, opcional) Nombre asociado al valor a ser enviado al servidor.
	* 	'value': (string, opcional) Valor a ser enviado al servidor (deber estar name).
	* 	'title': (string, opcional) Descripción del botón.
	* 	'icon_p': (string, opcional) Nombre del icono primario de jqueryui sin 'ui-icon-'.
	* 	'icon_s': (string, opcional) Nombre del icono secundario de jqueryui sin 'ui-icon-'.
	* 	'disabled': (string, opcional) Si el botón está deshabilitado.
	* 	'javascript': (string, opcional) Javascript a ejecutar por el botón.
	*/

// Verificar si hay botones definidos
if(!isset($buttons) || empty($buttons))
	return;

// Crear panel de botones
echo '<div class="buttons">';

// Javascript de botones
$jsButtonsDef = '';
$jsButtonsEvent = '';

// Crear botones
foreach($buttons as $button) {
	// Preparar botón
	$sButton = "\n" . '<button type="button" id="' . $button['id'] . '"';
	if(isset($button['name'])) $sButton .= ' name="' . $button['name'] . '"';
	if(isset($button['value'])) $sButton .= ' value="' . $button['value'] . '"';
	if(isset($button['title'])) $sButton .= ' title="' . $button['title'] . '"';
	$sButton .= '>' . (isset($button['label']) ? $button['label'] : '');
	$sButton .= '</button>';
	
	// Crear botón
	echo $sButton;
	
	// Crear javascript de botones
	$jsButton = array();
	if(isset($button['disabled'])) $jsButton['disabled'] = true;
	if(V_JQUERY_UI < V_JQUERY_UI_1_12_0) {
		if(!isset($button['label'])) $jsButton['text'] = false;
		if(isset($button['icon_p']) || isset($button['icon_s'])) {
			if(isset($button['icon_p'])) $jsButton['icons']['primary'] = 'ui-icon-' . $button['icon_p'];
			if(isset($button['icon_s'])) $jsButton['icons']['secondary'] = 'ui-icon-' . $button['icon_s'];
		}
	}
	else {
		if(!isset($button['label'])) $jsButton['showLabel'] = false;
		if(isset($button['icon_p'])) {
			$jsButton['icon'] = 'ui-icon-' . $button['icon_p'];
		}
		elseif(isset($button['icon_s'])) {
			$jsButton['icon'] = 'ui-icon-' . $button['icon_s'];
			$jsButton['iconPosition'] = 'end';
		}
	}
	$jsButton = json_encode($jsButton, JSON_FORCE_OBJECT);
	$jsButtonsDef .= "$('#{$button['id']}').button($jsButton);\n";
	if(isset($button['javascript']))
		$jsButtonsEvent .= "$('#{$button['id']}').click(function() { {$button['javascript']} });\n";
}

// Cerrar panel de botones
echo "\n</div>\n";

// Agregar javascript de botones al javascript general de inicio
$javascript['def'] = $jsButtonsDef . $javascript['def'];
$javascript['exec'] = $jsButtonsEvent . $javascript['exec'];

// Eliminar variables temporales del script
unset($button, $sButton, $jsButton, $jsButtonsDef, $jsButtonsEvent);
?>