<?php
// *****************************************************************************
// Vista que define la estructura básica del contenido de un dialogo
// 	para formularios de tipo abm1
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

/** Sumario:
	* Genera la estrutura de formularios tipo abm1 en un dialogo jqueryui con los
	* siguiente componentes:
	* 	1) Panel de mensajes (vista std.message.php)
	* 	2) Formulario principal (vista std.form1.php)
	* 	3) Vista del dialogo (vista dlg.php)
	* Modifica la variable $structure['dlg_options']['close'] para que limpie la pila
	* despues de cerrar el dialogo.
	*
	* Variables utilizadas:
	* $structure['msg_id']: (string) ID del panel de mensajes (requerida).
	* $structure['task_id']: (string) ID del task actual (requerida).
	*/

// Variables requeridas
isset($structure['msg_id']) or die('Error: $structure["msg_id"] debe estar definido.');

// Panel de mensajes
$messageId = $structure['msg_id'];
include('std.message.php');
unset($messageId);

// Formulario
echo '<br />';
include('std.form1.php');
echo '<br />';

// Agregar el js de mensaje si hay
if(isset($messages)) {
	$javascript['exec'] .= "\nsysfwk.msgPanel(" . json_encode($messages, JSON_FORCE_OBJECT) . ");";
}

// Modificar la función close del dlg para que limpie la pila despues de cerrar el dialago
$structure['dlg_options']['close'] =
	"function(event, ui){ $(this).dialog('destroy').empty(); sysfwk.scriptPreviousClean({'task_id': '{$structure['task_id']}'}); }";

// Generar dialogo
include('dlg.php');
?>