<?php
// *****************************************************************************
// Script para incluir el panel de paginación
// Autor: Elvin Calderón
// Versión: 1.0
// Nota: NO MODIFICAR ESTE ARCHIVO, SI NECESITA ALGÚN CAMBIO NOTIFICAR AL AUTOR
// *****************************************************************************

/** Estructura del panel de paginación $paginator[<prop>]
	* <prop>: Puede tener alguno de estos valores:
	* 	'id': (string, requerido) ID del paginador.
	* 	'page_display': (string, requerido) Texto que indica la página actual.
	* 	'page_rows': (integer, requerido) Cantidad de filas por páginas.
	* 	'page_rows_list': (array, requerido) Listado de selección de filas por páginas.
	* 	'load_js': (boolean, opcional) Indicar si cargar javascript del paginador.
	*/

// Variables requeridas
isset($paginator['id']) or die('Error: $paginator["id"] debe estar defino.');
isset($paginator['page_display']) or die('Error: $paginator["page_display"] debe estar defino.');
isset($paginator['page_rows']) or die('Error: $paginator["page_rows"] debe estar defino.');
!empty($paginator['page_rows_list']) or die('Error: $paginator["page_rows_list"] debe estar defino.');

// Script del paginador
if(isset($paginator['load_js'])) {
	$jsPagerDef = <<<'EOS'
	// Botones del paginador
	$('.btnFirst').button({ icons: {primary:'ui-icon-seek-first'}, text: false });
	$('.btnPrev').button({ icons: {primary:'ui-icon-seek-prev'}, text: false });
	$('.btnNext').button({ icons: {primary:'ui-icon-seek-next'}, text: false });
	$('.btnLast').button({ icons: {primary:'ui-icon-seek-end'}, text: false });
EOS;

$jsPagerExec = <<<'EOS'
	// Eventos de los botones del paginador
	$('.btnFirst').click(function() { changePage('F'); });
	$('.btnPrev').click(function() { changePage('P'); });
	$('.btnNext').click(function() { changePage('N'); });
	$('.btnLast').click(function() { changePage('L'); });
	$('.pageRows').change(function() { changePage($(this).val()); });
EOS;

	$javascript['def'] = $jsPagerDef . $javascript['def'];
	$javascript['exec'] = $jsPagerExec . $javascript['exec'];
}
?>
<div id="<?php echo $paginator['id']; ?>" class="paginator">
	<button class="btnFirst">Inicio</button>
	<button class="btnPrev">Anterior</button>
	<span class="pageDisplay"><?php echo $paginator['page_display']; ?></span>
	<button class="btnNext">Siguiente</button>
	<button class="btnLast">Fin</button>
	<select class="pageRows" title="Filas por Página">
<?php
	foreach($paginator['page_rows_list'] as $rows) {
		echo '<option value="' . $rows . '"' . ($rows == $paginator['page_rows'] ? 'selected' : '') . '>' . $rows . '</option>';
	}
	unset($rows);
?>
	</select>
</div>