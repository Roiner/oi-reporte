<?php
// Representa la tabla sch_sisgrel.v_ejecutor_area 
// Nombre de la entidad: EjecutorArea
class EjecutorArea extends DB_Table {

	// **************************************************************************
	// Constructor
	function __construct() {
		// Guardar el nombre del directorio del script actual
		$this->_dirName = dirname(__file__);
		
		// Orden por defecto de las consultas
		$this->defaultOrderBy = array('id_ejecutor' => 'ASC');
		
		/** Ejecutar el constructor padre. Los parámetros del constructor son:
			* $connstr: Cadena de conexión, pueder un string o un array de la forma array(<cadena de conexión>, <usuario>, <password>)
			* $table: Nombre de la tabla en DB
			* $instdbms: (Opcional) Nombre que identifica un instancia en DB, por defecto es null
			* $dbschema: (Opcional) Nombre del esquema en DB, por defecto es null
			* $dbname: (Opcional) Nombre de la DB, por defecto es null
			* LLamada: parent::__construct($connstr, $table, $instdbms = null, $dbschema = null, $dbname = null, $dirNameDict = null);
			* Ejemplo: parent::__construct(array(CONN_STR, CONN_USR, CONN_PWD), 'prueba', null, DB_SCHEMA);
			*/
		parent::__construct(CONN_STR, 'v_ejecutor_area', DB_INST, DB_SCHEMA);
	} // __construct()


	//***************************************************************************
	// Obtener areas
	public function getArea($params) {
		$params['gerencia']='G2';
		$where = array('WHERE' => "id_gerencia = :gerencia", 'PARAMS' => array('gerencia' => $params['gerencia']));
		// Obtener subprocesos
		$this->sqlSelect = "distinct id_area_operativa AS value, desc_area AS text";
		$this->sqlOrderBy = array('text' => 'ASC');
		$result = $this->getRecords($where);
		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . '</pre>'; // DEBUG
		if($result !== false) {
			//Agregar elemento extra si existe
			if(isset($params['extra'])) {
				array_unshift($result, array('value' => $params['extra']['value'], 'text' => $params['extra']['text']));
			}
			//Agregar elemento default si existe
			if($this->getNumRows() == 0 && isset($params['default'])) {
				array_unshift($result, array('value' => $params['default']['value'], 'text' => $params['default']['text']));
			}
		}
		return $result;
	} // getArea()


	//***************************************************************************
	// Obtener ejecutores
	public function getEjecutor($params) {
		$where = ((isset($params['id_area_operativa']) && $params['id_area_operativa'] != '') ? array('id_area_operativa' => $params['id_area_operativa']) : array());
		// Obtener subprocesos
		$this->sqlSelect = "id_ejecutor AS value, desc_ejecutor AS text";
		$this->sqlOrderBy = array('text' => 'ASC');
		$result = $this->getRecords($where);
		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . '</pre>'; // DEBUG
		if($result !== false) {
			//Agregar elemento extra si existe
			if(isset($params['extra'])) {
				array_unshift($result, array('value' => $params['extra']['value'], 'text' => $params['extra']['text']));
			}
			//Agregar elemento default si existe
			if($this->getNumRows() == 0 && isset($params['default'])) {
				array_unshift($result, array('value' => $params['default']['value'], 'text' => $params['default']['text']));
			}
		}
		return $result;
	} // getEjecutor()


	//***************************************************************************
	// Obtener areas
	public function getAllArea($params) {
		$where = array();
		// Obtener subprocesos
		$this->sqlSelect = "distinct id_area_operativa AS value, desc_area AS text";
		$this->sqlOrderBy = array('text' => 'ASC');
		$result = $this->getRecords($where);
		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . '</pre>'; // DEBUG
		if($result !== false) {
			//Agregar elemento extra si existe
			if(isset($params['extra'])) {
				array_unshift($result, array('value' => $params['extra']['value'], 'text' => $params['extra']['text']));
			}
			//Agregar elemento default si existe
			if($this->getNumRows() == 0 && isset($params['default'])) {
				array_unshift($result, array('value' => $params['default']['value'], 'text' => $params['default']['text']));
			}
		}
		return $result;
	} // getAllArea()
}
?>
