<?php
// Representa la tabla sch_sisgrel.a_tipos
// Nombre de la entidad: AsistenciaTipo
class AsistenciaTipo extends DB_Table {

	// **************************************************************************
	// Constructor
	function __construct() {
		// Guardar el nombre del directorio del script actual
		$this->_dirName = dirname(__file__);
		
		// Orden por defecto de las consultas
		$this->defaultOrderBy = array('id_a_tipo' => 'ASC');
		
		/** Ejecutar el constructor padre. Los parámetros del constructor son:
			* $connstr: Cadena de conexión, pueder un string o un array de la forma array(<cadena de conexión>, <usuario>, <password>)
			* $table: Nombre de la tabla en DB
			* $instdbms: (Opcional) Nombre que identifica un instancia en DB, por defecto es null
			* $dbschema: (Opcional) Nombre del esquema en DB, por defecto es null
			* $dbname: (Opcional) Nombre de la DB, por defecto es null
			* LLamada: parent::__construct($connstr, $table, $instdbms = null, $dbschema = null, $dbname = null, $dirNameDict = null);
			* Ejemplo: parent::__construct(array(CONN_STR, CONN_USR, CONN_PWD), 'prueba', null, DB_SCHEMA);
			*/
		parent::__construct(CONN_STR, 'a_tipos', DB_INST, DB_SCHEMA);
	} // __construct()

	//***************************************************************************
	// Obtener tipos de asistencia
	public function getTipos($params) {
		$where = array();
		// Obtener subprocesos
		$this->sqlSelect = "id_a_tipo AS value, nombre AS text";
		$this->sqlOrderBy = array('text' => 'ASC');
		$result = $this->getRecords($where);
		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . '</pre>'; // DEBUG
		if($result !== false) {
			//Agregar elemento extra si existe
			if(isset($params['extra'])) {
				array_unshift($result, array('value' => $params['extra']['value'], 'text' => $params['extra']['text']));
			}
			//Agregar elemento default si existe
			if($this->getNumRows() == 0 && isset($params['default'])) {
				array_unshift($result, array('value' => $params['default']['value'], 'text' => $params['default']['text']));
			}
		}
		return $result;
	} // getTipo()

	public function getNombreTipo($params) {
		//echo '<pre>params: '; var_dump($params); echo '</pre>'; //DEBUG
		// Obtener datos del usuario
		$result = $this->getRecords($params);
		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . $this->getErrorsString() . '</pre>'; //DEBUG
		if(!empty($result)) {
			$result = $result[0]; // Solo el primer registro
		}

		return $result;
	}

}
?>
