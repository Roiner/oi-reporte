<?php
// Representa la tabla sch_sisgrel.materiales
// Nombre de la entidad: Material
class Material extends DB_Table {

	// **************************************************************************
	// Constructor
	function __construct() {
		// Guardar el nombre del directorio del script actual
		$this->_dirName = dirname(__file__);
		
		// Orden por defecto de las consultas
		$this->defaultOrderBy = array('nombre' => 'ASC');
		
		/** Ejecutar el constructor padre. Los parámetros del constructor son:
			* $connstr: Cadena de conexión, pueder un string o un array de la forma array(<cadena de conexión>, <usuario>, <password>)
			* $table: Nombre de la tabla en DB
			* $instdbms: (Opcional) Nombre que identifica un instancia en DB, por defecto es null
			* $dbschema: (Opcional) Nombre del esquema en DB, por defecto es null
			* $dbname: (Opcional) Nombre de la DB, por defecto es null
			* LLamada: parent::__construct($connstr, $table, $instdbms = null, $dbschema = null, $dbname = null, $dirNameDict = null);
			* Ejemplo: parent::__construct(array(CONN_STR, CONN_USR, CONN_PWD), 'prueba', null, DB_SCHEMA);
			*/
		parent::__construct(CONN_STR, 'materiales', DB_INST, DB_SCHEMA);
	} // __construct()
}
?>
