<?php
// Representa la tabla sch_sisgrel.personales
// Nombre de la entidad: Personal
class Personal extends DB_Table {

	// **************************************************************************
	// Constructor
	function __construct() {
		// Guardar el nombre del directorio del script actual
		$this->_dirName = dirname(__file__);
		
		// Orden por defecto de las consultas
		$this->defaultOrderBy = array('id_supervisor' => 'ASC');
		
		/** Ejecutar el constructor padre. Los parámetros del constructor son:
			* $connstr: Cadena de conexión, pueder un string o un array de la forma array(<cadena de conexión>, <usuario>, <password>)
			* $table: Nombre de la tabla en DB
			* $instdbms: (Opcional) Nombre que identifica un instancia en DB, por defecto es null
			* $dbschema: (Opcional) Nombre del esquema en DB, por defecto es null
			* $dbname: (Opcional) Nombre de la DB, por defecto es null
			* LLamada: parent::__construct($connstr, $table, $instdbms = null, $dbschema = null, $dbname = null, $dirNameDict = null);
			* Ejemplo: parent::__construct(array(CONN_STR, CONN_USR, CONN_PWD), 'prueba', null, DB_SCHEMA);
			*/
		parent::__construct(CONN_STR, 'personales', DB_INST, DB_SCHEMA);
	} // __construct()

	protected function _cm_pre_getData($filter) {
		// echo "<pre>";
		// var_dump($_SESSION);
		// echo "</pre>";
		if ($_SESSION[CURRENT_TASK]['id']=='tsk_personalesAsignados(list).php') {
			$this->sqlSelect ='distinct t.id_supervisor, p1.nombre as supervisor';
		}
		//------------------------------------------
		// Implementar en clases heredadas (opcional)
		//------------------------------------------
		return $filter;
	} // _cm_pre_getData()
	
	public function insertRecord($fieldData) {
		// var_dump($fieldData);
		$dataDetallePersonal = array();
		foreach($fieldData['id_personal'] as $key => $val) {
			$dataDetallePersonal[] = array(
				'id_personal' => $val,
				'id_supervisor' => $fieldData['id_supervisor2'][$key]
			);
		}

		// $objDetallePersonal = new Personal();
		$dataDetallePersonal = $this->insertMultiRecord($dataDetallePersonal);
		var_dump($this->getQuery());echo "     /   "; var_dump($this->getErrorsString());
		if($dataDetallePersonal === false) {
			// $this->_errors = $objDetallePersonal->getErrors();
			return false;
		}

		return $fieldData;
	}
}
?>
