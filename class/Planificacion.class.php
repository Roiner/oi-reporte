<?php
// Representa la tabla sch_sisgrel.planificaciones
// Nombre de la entidad: Planificacion
class Planificacion extends DB_Table {

	// **************************************************************************
	// Constructor
	function __construct() {
		// Guardar el nombre del directorio del script actual
		$this->_dirName = dirname(__file__);
		
		// Orden por defecto de las consultas
		$this->defaultOrderBy = array('id_planificacion' => 'ASC');
		
		/** Ejecutar el constructor padre. Los parámetros del constructor son:
			* $connstr: Cadena de conexión, pueder un string o un array de la forma array(<cadena de conexión>, <usuario>, <password>)
			* $table: Nombre de la tabla en DB
			* $instdbms: (Opcional) Nombre que identifica un instancia en DB, por defecto es null
			* $dbschema: (Opcional) Nombre del esquema en DB, por defecto es null
			* $dbname: (Opcional) Nombre de la DB, por defecto es null
			* LLamada: parent::__construct($connstr, $table, $instdbms = null, $dbschema = null, $dbname = null, $dirNameDict = null);
			* Ejemplo: parent::__construct(array(CONN_STR, CONN_USR, CONN_PWD), 'prueba', null, DB_SCHEMA);
			*/
		parent::__construct(CONN_STR, 'planificaciones', DB_INST, DB_SCHEMA);
	} // __construct()


	//***************************************************************************
	// Realizar proceso personalizado despues de insert
	protected function _cm_post_insertRecord($fieldData) {
		//echo '<pre>fieldData '; var_dump($fieldData); echo '</pre>';

		// preparar datos de detalle
		$dataDetalle = array();
		foreach($fieldData['id_st_ot'] as $key => $val) {
			$dataDetalle[] = array(
				'id_planificacion' => $fieldData['id_planificacion'],
				'id_st_ot' => $val,
				'hh_p' => 1, //$fieldData['id_planificacion'],
				'fecha_inicio' => $fieldData['fecha_ini'][$key],
				'estado' => 'SIN_COMENZAR',
				'observaciones' => $fieldData['observaciones'][$key],
			);
		}

		require_once($GLOBALS['path_class'] . 'DetallePlanificacion.class.php');
		$objDetalle = new DetallePlanificacion();
		$dataDetalle = $objDetalle->insertMultiRecord($dataDetalle);
		if($dataDetalle === false) {
			$this->_errors = $objDetalle->getErrors();
			return false;
		}

		return $fieldData;
	} // _cm_post_insertRecord()
}
?>
