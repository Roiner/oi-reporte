<?php
// Representa la tabla sch_sisgrel.v_st_ot
// Nombre de la entidad: OT
class OT extends DB_Table {

	// **************************************************************************
	// Constructor
	function __construct() {
		// Guardar el nombre del directorio del script actual
		$this->_dirName = dirname(__file__);
		
		// Orden por defecto de las consultas
		$this->defaultOrderBy = array('id_st_ot' => 'ASC');
		
		/** Ejecutar el constructor padre. Los parámetros del constructor son:
			* $connstr: Cadena de conexión, pueder un string o un array de la forma array(<cadena de conexión>, <usuario>, <password>)
			* $table: Nombre de la tabla en DB
			* $instdbms: (Opcional) Nombre que identifica un instancia en DB, por defecto es null
			* $dbschema: (Opcional) Nombre del esquema en DB, por defecto es null
			* $dbname: (Opcional) Nombre de la DB, por defecto es null
			* LLamada: parent::__construct($connstr, $table, $instdbms = null, $dbschema = null, $dbname = null, $dirNameDict = null);
			* Ejemplo: parent::__construct(array(CONN_STR, CONN_USR, CONN_PWD), 'prueba', null, DB_SCHEMA);
			*/
		parent::__construct(CONN_STR, 'v_st_ot', DB_INST, DB_SCHEMA);
	} // __construct()

		
	//***************************************************************************
	// Obtener las ordenes de trabajo que pertenecen a un ejecutor
	public function getOt($params) {
		// Prepara where
		//echo '<pre>params: '; var_dump($params); echo '</pre>'; //DEBUG
		// var_dump($params['cadenaOt']);
		$params['cadenaOt']=str_replace('"',"'",$params['cadenaOt']);
		// echo "la cadena es :"; var_dump($params['cadenaOt']);
		// $cadena= '["' . implode('","',$params['arregloOT']) . '"]';
		$where = array('WHERE' => "(id_ejecutor = :ejecutor AND id_st_ot NOT IN (SELECT id_st_ot FROM sch_sisgrel.d_planificaciones d WHERE d.estado='TERMINADO') AND id_st_ot NOT IN (".$params['cadenaOt']."))", 'PARAMS' => array('ejecutor' => $params['ejecutor']));
		if(isset($params['search']) && $params['search'] != '') {
			$where['WHERE'] .= " AND (id_st_ot::text ilike ('%' || :id_st_ot || '%') OR trab_solicitado ilike ('%' || :trab_solicitado || '%'))";
			$where['PARAMS']['id_st_ot'] = $params['search'];
			$where['PARAMS']['trab_solicitado'] = $params['search'];
		}
		// Guardar variables query originales
		$orgQueryVars = $this->getAllQueryVars();
		
		// Obtener ordenes de trabajo
		$this->sqlSelect = "t.id_st_ot as value, (id_st_ot || ' - ' || trab_solicitado) as text";
		$this->sqlOrderBy = array('value' => 'ASC');
		$result = $this->getRecords($where);
		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . '</pre>'; //DEBUG
		if($result !== false) {
			// Agregar elemento extra si existe
			if(isset($params['extra'])) {
				array_unshift($result, array('value' => $params['extra']['value'], 'text' => $params['extra']['text']));
			}
			// Agregar elemento default si existe
			if($this->getNumRows() == 0 && isset($params['default'])) {
				array_unshift($result, array('value' => $params['default']['value'], 'text' => $params['default']['text']));
			}
		}
		
		// Restaurar variables query originales
		$this->setAllQueryVars($orgQueryVars);

		return $result;
	} // getOt()


	//***************************************************************************
	// Obtener informacion de una OT
	// $params: array de filtro
	public function getOtInfo($params) {
		//echo '<pre>params: '; var_dump($params); echo '</pre>'; //DEBUG
		// Obtener datos del usuario
		$result = $this->getRecords($params);
		//echo '<pre>result: '; var_dump($result); echo $this->getQuery() . $this->getErrorsString() . '</pre>'; //DEBUG
		if(!empty($result)) {
			$result = $result[0]; // Solo el primer registro
		}

		return $result;
	} // getOtInfo()
	

	
	//***************************************************************************
	// Obtener las ordenes de trabajo que pertenecen a una planificacion
	public function getOtPlanificacion($params) {
		// Prepara where
		//echo '<pre>params: '; var_dump($params); echo '</pre>'; //DEBUG
		$where = array('WHERE' => 'id_st_ot IN (SELECT id_st_ot FROM sch_sisgrel.d_planificaciones d
				WHERE d.estado!="TERMINADO"
				) AND id_st_ot NOT IN (SELECT id_st_ot FROM sch_sisgrel.a_ots a
				WHERE a.estado!="TERMINADO"
				)
				AND id_ejecutor = :ejecutor', 'PARAMS' => array('ejecutor' => $params['ejecutor']));
		if(isset($params['search']) && $params['search'] != '') {
			$where['WHERE'] .= " AND (id_st_ot::text ilike ('%' || :id_st_ot || '%') OR desc_actividad ilike ('%' || :desc_actividad || '%'))";  
			$where['PARAMS']['id_st_ot'] = $params['search'];
			$where['PARAMS']['desc_actividad'] = $params['search'];
		}
		
		// Guardar variables query originales
		$orgQueryVars = $this->getAllQueryVars();
		
		// Obtener ordenes de trabajo
		$this->sqlSelect = "t.id_st_ot as value, (id_st_ot || ' - ' || desc_actividad) as text";
		$this->sqlOrderBy = array('value' => 'ASC');
		$result = $this->getRecords($where);
		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . '</pre>'; //DEBUG
		if($result !== false) {
			// Agregar elemento extra si existe
			if(isset($params['extra'])) {
				array_unshift($result, array('value' => $params['extra']['value'], 'text' => $params['extra']['text']));
			}
			// Agregar elemento default si existe
			if($this->getNumRows() == 0 && isset($params['default'])) {
				array_unshift($result, array('value' => $params['default']['value'], 'text' => $params['default']['text']));
			}
		}
		
		// Restaurar variables query originales
		$this->setAllQueryVars($orgQueryVars);

		return $result;
	} // getOtPlanificacion()

}
?>
