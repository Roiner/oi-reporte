<?php
// Representa la tabla sch_sisgrel.v_recurso_rutina
// Nombre de la entidad: RecursoRutina
class RecursoRutina extends DB_Table {

	// **************************************************************************
	// Constructor
	function __construct() {
		// Guardar el nombre del directorio del script actual
		$this->_dirName = dirname(__file__);
		
		// Orden por defecto de las consultas
		$this->defaultOrderBy = array('fecha_ejecucion' => 'ASC');
		
		/** Ejecutar el constructor padre. Los parámetros del constructor son:
			* $connstr: Cadena de conexión, pueder un string o un array de la forma array(<cadena de conexión>, <usuario>, <password>)
			* $table: Nombre de la tabla en DB
			* $instdbms: (Opcional) Nombre que identifica un instancia en DB, por defecto es null
			* $dbschema: (Opcional) Nombre del esquema en DB, por defecto es null
			* $dbname: (Opcional) Nombre de la DB, por defecto es null
			* LLamada: parent::__construct($connstr, $table, $instdbms = null, $dbschema = null, $dbname = null, $dirNameDict = null);
			* Ejemplo: parent::__construct(array(CONN_STR, CONN_USR, CONN_PWD), 'prueba', null, DB_SCHEMA);
			*/
		parent::__construct(CONN_STR, 'v_recurso_rutina', DB_INST, DB_SCHEMA);
	} // __construct()
	

	public function getHorasRecurso($params) {	
		$where = array('WHERE' => "t.id_ejecutor = :id_ejecutor AND t.fecha_ejecucion >= :fecha_inicio AND t.fecha_ejecucion <= :fecha_fin", 'PARAMS' => array('id_ejecutor' => $params['id_ejecutor'],'fecha_inicio' => $params['fecha_inicio'], 'fecha_fin' => $params['fecha_fin']));
			// Guardar variables query originales
		$orgQueryVars = $this->getAllQueryVars();
		$this->sqlSelect = "sum(hh_ejecucion)";
		
		
		// Obtener ordenes de trabajo
		$this->sqlOrderBy = array();
		$result = $this->getRecords($where);

		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . '</pre>'; //DEBUG


		// echo '<pre>params: '; var_dump($params); echo '</pre>'; //DEBUG
		// Obtener datos del usuario
		// $result = $this->getRecords($params);
		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . $this->getErrorsString() . '</pre>'; //DEBUG
		// return false;
		return $result;
	}
}
?>
