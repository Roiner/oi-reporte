<?php
// Representa la tabla sch_sisgrel.d_planificaciones
// Nombre de la entidad: DetallePlanificacion
class DetallePlanificacion extends DB_Table {

	// **************************************************************************
	// Constructor
	function __construct() {
		// Guardar el nombre del directorio del script actual
		$this->_dirName = dirname(__file__);
		
		// Orden por defecto de las consultas
		$this->defaultOrderBy = array('fecha_inicio' => 'ASC');
		
		/** Ejecutar el constructor padre. Los parámetros del constructor son:
			* $connstr: Cadena de conexión, pueder un string o un array de la forma array(<cadena de conexión>, <usuario>, <password>)
			* $table: Nombre de la tabla en DB
			* $instdbms: (Opcional) Nombre que identifica un instancia en DB, por defecto es null
			* $dbschema: (Opcional) Nombre del esquema en DB, por defecto es null
			* $dbname: (Opcional) Nombre de la DB, por defecto es null
			* LLamada: parent::__construct($connstr, $table, $instdbms = null, $dbschema = null, $dbname = null, $dirNameDict = null);
			* Ejemplo: parent::__construct(array(CONN_STR, CONN_USR, CONN_PWD), 'prueba', null, DB_SCHEMA);
			*/
		parent::__construct(CONN_STR, 'd_planificaciones', DB_INST, DB_SCHEMA);
	} // __construct()

	public function getTotalHorasTrabajoSocialOt($params) {	
		$params['tag_activo']='SG-TRAB-SOCIAL-000';
		$params['id_prioridad']=1;
		$where = array('WHERE' => "p0.id_ejecutor = :id_ejecutor AND t.fecha_inicio >= :fecha_inicio AND t.fecha_inicio <= :fecha_fin AND p0.tag_activo = :tag_activo AND p0.id_prioridad!= :id_prioridad", 'PARAMS' => array('id_ejecutor' => $params['id_ejecutor'],'fecha_inicio' => $params['fecha_inicio'], 'fecha_fin' => $params['fecha_fin'],'tag_activo' => $params['tag_activo'], 'id_prioridad' => $params['id_prioridad']));
			// Guardar variables query originales
		$orgQueryVars = $this->getAllQueryVars();
		$this->sqlSelect = "sum(hh_p), count(*)";
		
		
		// Obtener ordenes de trabajo
		$this->sqlOrderBy = array();
		$result = $this->getRecords($where);

		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . '</pre>'; //DEBUG


		// echo '<pre>params: '; var_dump($params); echo '</pre>'; //DEBUG
		// Obtener datos del usuario
		// $result = $this->getRecords($params);
		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . $this->getErrorsString() . '</pre>'; //DEBUG
		// return false;
		return $result;
	}

	public function getTotalHorasOt($params) {	
		$params['tag_activo']='SG-TRAB-SOCIAL-000';
		$params['id_prioridad']=1;
		$where = array('WHERE' => "p0.id_ejecutor = :id_ejecutor AND t.fecha_inicio >= :fecha_inicio AND t.fecha_inicio <= :fecha_fin AND p0.tag_activo != :tag_activo AND p0.id_prioridad!= :id_prioridad", 'PARAMS' => array('id_ejecutor' => $params['id_ejecutor'],'fecha_inicio' => $params['fecha_inicio'], 'fecha_fin' => $params['fecha_fin'],'tag_activo' => $params['tag_activo'], 'id_prioridad' => $params['id_prioridad']));
			// Guardar variables query originales
		$orgQueryVars = $this->getAllQueryVars();
		$this->sqlSelect = "sum(hh_p), count(*)";
		
		
		// Obtener ordenes de trabajo
		$this->sqlOrderBy = array();
		$result = $this->getRecords($where);

		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . '</pre>'; //DEBUG


		// echo '<pre>params: '; var_dump($params); echo '</pre>'; //DEBUG
		// Obtener datos del usuario
		// $result = $this->getRecords($params);
		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . $this->getErrorsString() . '</pre>'; //DEBUG
		// return false;
		return $result;
	}
}
?>
