<?php
// Representa la tabla sch_sisgrel.t_actividades
// Nombre de la entidad: TrabajadorActividad
class TrabajadorActividad extends DB_Table {

	// **************************************************************************
	// Constructor
	function __construct() {
		// Guardar el nombre del directorio del script actual
		$this->_dirName = dirname(__file__);
		
		// Orden por defecto de las consultas
		$this->defaultOrderBy = array('id_t_actividad' => 'ASC');
		
		/** Ejecutar el constructor padre. Los parámetros del constructor son:
			* $connstr: Cadena de conexión, pueder un string o un array de la forma array(<cadena de conexión>, <usuario>, <password>)
			* $table: Nombre de la tabla en DB
			* $instdbms: (Opcional) Nombre que identifica un instancia en DB, por defecto es null
			* $dbschema: (Opcional) Nombre del esquema en DB, por defecto es null
			* $dbname: (Opcional) Nombre de la DB, por defecto es null
			* LLamada: parent::__construct($connstr, $table, $instdbms = null, $dbschema = null, $dbname = null, $dirNameDict = null);
			* Ejemplo: parent::__construct(array(CONN_STR, CONN_USR, CONN_PWD), 'prueba', null, DB_SCHEMA);
			*/
		parent::__construct(CONN_STR, 't_actividades', DB_INST, DB_SCHEMA);
	} // __construct()

	//***************************************************************************
	// Realizar proceso personalizado despues de insert
	protected function _cm_post_insertRecord($fieldData) {
		require_once($GLOBALS['path_class'] . 'TrabajadorDetalle.class.php');
		//echo '<pre>fieldData '; var_dump($fieldData); echo '</pre>';
		// foreach ($fieldData as $key => $value) {
		// 	$fieldData[$key]['id_t_actividad']=$fieldData['id_t_actividad'];
		// }
		
		
		foreach ($fieldData['recurso'] as $key => $value) {
			$fieldData['recurso'][$key]['id_t_actividad']=$fieldData['id_t_actividad'];
		}
	

		if ($fieldData['ot']) {
			$dataUpdateOt=array();
			$fieldData['ot']['id_t_actividad']=$fieldData['id_t_actividad'];
			$dataOt = $fieldData['ot'];
			$dataRecurso = $fieldData['recurso'];
			// var_dump($dataRecurso);
			require_once($GLOBALS['path_class'] . 'ActividadOT.class.php');
			$objActividadOT = new ActividadOT();
			$dataOt = $objActividadOT->insertRecord($dataOt);
			//var_dump($objActividadOT->getQuery());echo "     /   "; var_dump($objActividadOT->getErrorsString());
			if($dataOt === false) {
				$this->_errors = $objActividadOT->getErrors();
				return false;
			}
			if ($fieldData['id_planificacion']!=-1) {
				$dataUpdateOt['id_st_ot']=$fieldData['ot']['id_st_ot'];
				$dataUpdateOt['id_planificacion']= $fieldData['id_planificacion'];
				$dataUpdateOt['estado']='TERMINADO';

				require_once($GLOBALS['path_class'] . 'DetallePlanificacion.class.php');
				$objDetallePlanificacion = new DetallePlanificacion();
				$dataUpdateOt = $objDetallePlanificacion->updateRecord($dataUpdateOt);
				if($dataUpdateOt === false) {
					$this->_errors = $objDetallePlanificacion->getErrors();
					return false;
				}
			}
			$objRecurso = new TrabajadorDetalle();
			$dataRecurso = $objRecurso->insertMultiRecord($dataRecurso);
			if($dataRecurso === false) {
				$this->_errors = $objRecurso->getErrors();
				return false;
			}
		}else{
			if (empty($fieldData['emergencia']['id_t_actividad'])) {
				$fieldData['emergencia']['id_t_actividad']=$fieldData['id_t_actividad'];
			}
			$dataEmergencia = $fieldData['emergencia'];
			$dataRecurso = $fieldData['recurso'];
			require_once($GLOBALS['path_class'] . 'ActividadEmergencia.class.php');
			$objActividadEmergencia = new ActividadEmergencia();
			if (empty($dataEmergencia['id_a_emerg'])) {
				$dataEmergencia = $objActividadEmergencia->insertRecord($dataEmergencia);
			}else{
				if(!empty($dataRecurso)){
					$dataEmergencia2 = array('id_a_emerg' => $dataEmergencia['id_a_emerg'], 'estado' => 'TERMINADO');
					$dataEmergencia2 = $objActividadEmergencia->updateRecord($dataEmergencia2);
					$dataEmergencia = $objActividadEmergencia->insertRecord($dataEmergencia);
				}
			}
			if($dataEmergencia === false) {
				$this->_errors = $objActividadEmergencia->getErrors();
				return false;
			}
			if($dataEmergencia2 === false) {
				$this->_errors = $objActividadEmergencia->getErrors();
				return false;
			}

			if (!empty($dataRecurso)) {
				$objRecurso = new TrabajadorDetalle();
				$dataRecurso = $objRecurso->insertMultiRecord($dataRecurso);
				if($dataRecurso === false) {
					$this->_errors = $objRecurso->getErrors();
					return false;
				}
			}
		}
			// return false;
		 return $fieldData;
	} // _cm_post_insertRecord()
}
?>
