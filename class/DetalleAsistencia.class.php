<?php
// Representa la tabla sch_sisgrel.d_asistencias
// Nombre de la entidad: DetalleAsistencia
class DetalleAsistencia extends DB_Table {

	// **************************************************************************
	// Constructor
	function __construct() {
		// Guardar el nombre del directorio del script actual
		$this->_dirName = dirname(__file__);
		
		// Orden por defecto de las consultas
		$this->defaultOrderBy = array('id_personal' => 'ASC');
		
		/** Ejecutar el constructor padre. Los parámetros del constructor son:
			* $connstr: Cadena de conexión, pueder un string o un array de la forma array(<cadena de conexión>, <usuario>, <password>)
			* $table: Nombre de la tabla en DB
			* $instdbms: (Opcional) Nombre que identifica un instancia en DB, por defecto es null
			* $dbschema: (Opcional) Nombre del esquema en DB, por defecto es null
			* $dbname: (Opcional) Nombre de la DB, por defecto es null
			* LLamada: parent::__construct($connstr, $table, $instdbms = null, $dbschema = null, $dbname = null, $dirNameDict = null);
			* Ejemplo: parent::__construct(array(CONN_STR, CONN_USR, CONN_PWD), 'prueba', null, DB_SCHEMA);
			*/
		parent::__construct(CONN_STR, 'd_asistencias', DB_INST, DB_SCHEMA);
	} // __construct()


	public function getDetalleAsistencia($params) {		
		$where = array('WHERE' => "t.id_a_diaria = :id_a_diaria ", 'PARAMS' => array('id_a_diaria' => $params['id_a_diaria']));
			// Guardar variables query originales
		$orgQueryVars = $this->getAllQueryVars();
		$this->sqlSelect = "t.id_a_diaria,t.id_personal,t.hh_disponibles,t.id_a_tipo,p0.nombre,p0.desc_cargo,p2.nombre as tipoAsistencia";
		
		
		// Obtener ordenes de trabajo
		$this->sqlOrderBy = array('id_a_diaria' => 'ASC');
		$result = $this->getRecords($where);

		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . '</pre>'; //DEBUG


		// echo '<pre>params: '; var_dump($params); echo '</pre>'; //DEBUG
		// Obtener datos del usuario
		// $result = $this->getRecords($params);
		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . $this->getErrorsString() . '</pre>'; //DEBUG
		// return false;
		return $result;
	}

	// $where = array('WHERE' => "(id_ejecutor = :ejecutor AND id_st_ot NOT IN (SELECT id_st_ot FROM sch_sisgrel.d_planificaciones d WHERE d.estado='TERMINADO')) AND id_st_ot NOT IN (".$params['cadenaOt'].")", 'PARAMS' => array('ejecutor' => $params['ejecutor']));
	public function getAsistencia($params) {		
		$where = array('WHERE' => "t.id_personal = :id_personal AND t.id_a_diaria IN (".$params['cadenaActividad'].")", 'PARAMS' => array('id_personal' => $params['id_personal']));
			// Guardar variables query originales
		$orgQueryVars = $this->getAllQueryVars();
		$this->sqlSelect = "t.id_a_diaria,t.id_personal,t.hh_disponibles,t.id_a_tipo,p0.nombre,p0.desc_cargo,p2.nombre as tipoAsistencia";
		
		
		// Obtener ordenes de trabajo
		$this->sqlOrderBy = array('id_a_diaria' => 'ASC');
		$result = $this->getRecords($where);

		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . '</pre>'; //DEBUG


		// echo '<pre>params: '; var_dump($params); echo '</pre>'; //DEBUG
		// Obtener datos del usuario
		// $result = $this->getRecords($params);
		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . $this->getErrorsString() . '</pre>'; //DEBUG
		// return false;
		return $result;
	}

	public function getAsistenciaTipo($params) {		
		$where = array('WHERE' => "t.id_personal = :id_personal AND t.id_a_tipo = :id_a_tipo AND t.id_a_diaria IN (".$params['cadenaActividad'].")", 'PARAMS' => array('id_personal' => $params['id_personal'],'id_a_tipo' => $params['id_a_tipo']));
			// Guardar variables query originales
		$orgQueryVars = $this->getAllQueryVars();
		$this->sqlSelect = "count(*)";
		
		
		// Obtener ordenes de trabajo
		$this->sqlOrderBy = array();
		$result = $this->getRecords($where);

		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . '</pre>'; //DEBUG


		// echo '<pre>params: '; var_dump($params); echo '</pre>'; //DEBUG
		// Obtener datos del usuario
		// $result = $this->getRecords($params);
		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . $this->getErrorsString() . '</pre>'; //DEBUG
		// return false;
		return $result;
	}

}
?>
