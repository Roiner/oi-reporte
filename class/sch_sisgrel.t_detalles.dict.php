<?php
// Diccionario de Datos de la tabla sch_sisgrel.t_detalles

/** Claves para las especificaciones de los campos de las tablas:
	* autoincrement: Si el campo es de tipo autoincremental, seral o identity
	* autoinsert: Valor para agregar al campo automaticamente al insertar si no está presente
	* autoupdate: Valor para agregar al campo automaticamente al actualizar si no está presente
	* false: Valor FALSE para la DB. Ejemplos: 'f', 0
	* label: Etiqueta que tendrá el campo en las páginas. Ejemplo: 'Nombre'
	* maxvalue: Valor máximo que puede tomar el campo (para validación). Ejemplo: 500
	* minvalue: Valor mínimo que puede tomar el campo (para validación). Ejemplo: 100
	* optionlist: Nombre del array en _ForeignDataList que contiene las opciones posibles para el campo.
	* opall: Valor y texto (array(value => '', text => '(Todos)')) de la opción Todos para la función _cm_changeConfig.
	* opsel: Valor y texto (array(value => '', text => '(Seleccione)')) de la opción Seleccione para la función _cm_changeConfig.
	* queryoutput: Expresión que se utilizará como salida en los querys. Ejemplo: "to_char(t.fecha, 'DD/MM/YYYY HH24:MI')"
	* required: Si el campo es requerido (para validación). Ejemplo: 'y'
	* size: Cantidad de caracteres del campo (para validación). Ejemplo: 10
	* title: Descripción del campo para el atributo title en la páginas. Ejemplo: 'Nombre del Usuario'
	* true: Valor TRUE para la DB. Ejemplos: 'y', 1
	* type: Tipo de dato en la DB (para validación). Ejemplo: 'integer'
	* nodb: Indicar que el campo no es de la base de datos. Ejemplo: 'y'
	* userformat: Formato del campo para el usuario.
	* dbformat: Formato del campo para la DB.
	*		- Para datos tipo timestamp o date, userformat y dbformat deben ser un string soportado por DateTime::createFromFormat
	*		- Para datos tipo double o float, userformat y dbformat deben ser un array asosiativo con los campo:
	*			dec_sep (separador decimal), group_sep (separdor de miles) and decs (cantidad de decimales)
	*/

// Especificaciones de los campos
$fieldspec['id_t_actividad'] = array(
	'label' => 'ID Trabajador actividad:',
	'type' => 'integer',
	'required' => 'y',
);

$fieldspec['id_especialidad'] = array(
	'label' => 'ID especialidad:',
	'type' => 'string',
	'size' => 15,
	'required' => 'y'
);

$fieldspec['cantidad'] = array(
	'label' => 'Cantidad:',
	'type' => 'integer',
	'required' => 'y',
);

$fieldspec['hh'] = array(
	'label' => 'Horas Hombre:',
	'type' => 'integer',
	'required' => 'y',
);


// Clave primaria
$this->_primaryKey = array('id_t_actividad','id_especialidad');

// Claves Unicas
// $this->_uniqueKeys[] = array('campo2');


/** Claves para las tablas hijas:
	* schema: Nombre del schema de la tabla hija
	* child: Nombre de la tabla hija
	* fields: Array (campo_local => campo_hijo) de los campos que forman la unión con la tabla hija
	*/

// Relaciones con tablas hijas
$this->_childRelations = array(); // Array vacío en caso de no tener tablas hijas
// $this->_childRelations[] = array(
	// 'schema' => DB_SCHEMA,
	// 'child' => 'd_planificaciones',
	// 'fields' => array('id_planificacion' => 'id_planificacion')
// );


/** Claves para las tablas padres:
	* schema: Nombre del schema de la tabla padre (opcional)
	* parent: Nombre de la tabla padre
	* alias: Alias de la tabla padre (opcional)
	* join: Cadena que forma el join (opcional). Si no se especifica se asume 'LEFT JOIN' 
	* parent_fields: Array (campo => alias) ó string de los campos de la tabla padre a incluir en las consultas
	* fields: Array (campo_local => campo_padre) ó string que forman la unión con la tabla padre
	* filter: Array ó string con el where para filtrar los registro que van al optionlist
	*/

// Relaciones con tablas padres
// $this->_parentRelations = array(); // Array vacío en caso de no tener tablas padres

$this->_parentRelations[] = array(
	'schema' => DB_SCHEMA,
	'parent' => 't_actividades',
	'parent_fields' => array('id_t_actividad' => 'trabajador_actividad'),
	'fields' => array('id_t_actividad' => 'id_t_actividad')
);

$this->_parentRelations[] = array(
	'schema' => 'sch_sigma',
	'parent' => 'mst_especialidad',
	'parent_fields' => array('desc_especialidad' => 'especialidad'),
	'fields' => array('id_especialidad' => 'id_especialidad')
);

?>
