<?php
// Representa la tabla sch_sisgrel.a_diarias
// Nombre de la entidad: ActividadDiaria
class ActividadDiaria extends DB_Table {

	// **************************************************************************
	// Constructor
	function __construct() {
		// Guardar el nombre del directorio del script actual
		$this->_dirName = dirname(__file__);
		
		// Orden por defecto de las consultas
		$this->defaultOrderBy = array('id_a_diaria' => 'ASC');
		
		/** Ejecutar el constructor padre. Los parámetros del constructor son:
			* $connstr: Cadena de conexión, pueder un string o un array de la forma array(<cadena de conexión>, <usuario>, <password>)
			* $table: Nombre de la tabla en DB
			* $instdbms: (Opcional) Nombre que identifica un instancia en DB, por defecto es null
			* $dbschema: (Opcional) Nombre del esquema en DB, por defecto es null
			* $dbname: (Opcional) Nombre de la DB, por defecto es null
			* LLamada: parent::__construct($connstr, $table, $instdbms = null, $dbschema = null, $dbname = null, $dirNameDict = null);
			* Ejemplo: parent::__construct(array(CONN_STR, CONN_USR, CONN_PWD), 'prueba', null, DB_SCHEMA);
			*/
		parent::__construct(CONN_STR, 'a_diarias', DB_INST, DB_SCHEMA);
	} // __construct()

	//***************************************************************************
	// Obtener valores iniciales
	public function getInicialData($fieldData) {
		$fieldData['fecha'] = date(F_DMY);
		$this->_fieldData = $fieldData;
		return $fieldData;
	} // getInicialData()


	//***************************************************************************
	// Realizar proceso personalizado despues de insert
	protected function _cm_post_insertRecord($fieldData) {
		// echo '<pre>fieldData '; var_dump($fieldData); echo '</pre>';
		error_reporting(E_ERROR | E_PARSE);
		require_once($GLOBALS['path_class'] . 'ActividadOT.class.php');
		require_once($GLOBALS['path_class'] . 'TrabajadorActividad.class.php');
		require_once($GLOBALS['path_class'] . 'ActividadRutinaria.class.php');
		require_once($GLOBALS['path_class'] . 'DetalleAsistencia.class.php');

		$dataRutina = array();
		$dataRecursoOt = array();
		$dataRecursoEmergencia = array();
		$auxiliarRecurso = array();
		$dataAsistencia = array();

		foreach ($fieldData['id_a_tipo'] as $key => $val) {
			$hh=0;
			if ($val==SYS_ID_ASISTENCIA) {
				$hh=6;
			}
			$dataAsistencia[] = array(
				'id_a_diaria' => $fieldData['id_a_diaria'],
				'id_personal' => $fieldData['id_p'][$key],
				'hh_disponibles' => $hh,
				'id_a_tipo' => $val
			);
		}
		var_dump($dataAsistencia);
		$objAsistencia = new DetalleAsistencia();
		$dataAsistencia = $objAsistencia->insertMultiRecord($dataAsistencia);
		if($dataAsistencia === false) {
			$this->_errors = $objAsistencia->getErrors();
			return false;
		}

		foreach($fieldData['id_st_ot'] as $key => $val) {
			$dataOt = array();
			$dataOt['id_planificacion']= $fieldData['id_planificacion'][$key];
			$dataOt['desc_actividad'] = $fieldData['desc_actividad'][$key];
			$dataOt['ot'] = array(
				'id_a_diaria' => $fieldData['id_a_diaria'],
				'id_st_ot' => $val,
				'cantidad' => $fieldData['cantidad'][$key],
				'duracion' => $fieldData['duracion'][$key],
				'hh_total' => $fieldData['hh_total'][$key],
				'estado' => $fieldData['estado'][$key],
				'id_t_actividad' => 0,
			);
			
			foreach (json_decode($fieldData['recursos_t'][$key],true) as $llave => $arreglo) {				
					$cant = 0;
					if (!empty($arreglo ['cantidad'])) {
						$cant=$arreglo ['cantidad'];
					}
					$dataOt['recurso'][] = array(
						'id_t_actividad' => 0,
						'id_especialidad' => $arreglo['id_especialidad'],
						'cantidad' => $cant,
						'hh' => $fieldData['duracion'][$key],
						);
			}
			$dataOt['constante']=0;
			$objOt = new TrabajadorActividad();
			//var_dump($dataOt);
			$dataOt = $objOt->insertRecord($dataOt);
			// echo "ordenes de trabajo =" ; var_dump($dataOt);
			// var_dump($objOt->getQuery());echo "     /   "; var_dump($objOt->getErrorsString());
			if($dataOt === false) {
				$this->_errors = $objOt->getErrors();
				return false;
			}
		}


		foreach($fieldData['rutina'] as $key => $val) {
			$dataRutina[] = array(
				'cantidad' => $fieldData['cant_personal'][$key],
				'duracion' => $fieldData['duracion_r'][$key],
				'hh_total' => $fieldData['hh_total_r'][$key],
				'id_a_diaria' => $fieldData['id_a_diaria'],
				'id_area_operativa' => $fieldData['id_area_operativa_r'][$key],
				'cantidad_rutinas' => $fieldData['cant_rutinas'][$key]
			);
		}

		//var_dump($dataRutina);
		$objRutinas = new ActividadRutinaria();
		$dataRutina = $objRutinas->insertMultiRecord($dataRutina);
		//var_dump($objRutinas->getQuery());echo "separacion/   "; var_dump($objRutinas->getErrorsString());
		// echo "Rutinas: " ; var_dump($dataRutina);
		if($dataRutina === false) {
			$this->_errors = $objRutinas->getErrors();
			return false;
		}


		foreach($fieldData['emergencia'] as $key => $val) {
			$dataEmergencia = array();
			$dataEmergencia['emergencia'] = array(
				'cantidad' => ($fieldData['hh_total_e'][$key]/$fieldData['duracion_e'][$key]),
				'duracion' => $fieldData['duracion_e'][$key],
				'hh_total' => $fieldData['hh_total_e'][$key],
				'estado' => $fieldData['estado_e'][$key],
				'descripcion' => $fieldData['descripcion'][$key],
				'id_a_diaria' => $fieldData['id_a_diaria'],				
				'id_t_actividad' => 0,				
				'id_area_operativa' => $fieldData['id_area_operativa_e'][$key],
				'id_ejecutor' => $fieldData['id_ejecutor']

			);
			if (!empty($fieldData['id_a_emerg'][$key])) {
				$dataEmergencia['emergencia']['id_a_emerg'] = $fieldData['id_a_emerg'][$key];
				$dataEmergencia['emergencia']['id_t_actividad'] = $fieldData['id_t_actividad'][$key];
			}
			
			foreach (json_decode($fieldData['recursos_eme'],true) as $llave => $valor) {
				$t_actividad=0;
				$cant=0;	
				if (!empty($fieldData['id_a_emerg'][$key])) {
					$t_actividad = $fieldData['id_a_emerg'][$key];
				}
				if (!empty($fieldData['recursos_e'][$valor][$key])) {
					$cant=$fieldData['recursos_e'][$valor][$key];
				}
				$dataEmergencia['recurso'][] = array(
					'id_t_actividad' => 0,
					'id_especialidad' => $valor,
					'cantidad' => $cant,
					'hh' => $fieldData['duracion_e'][$key],
					);
			}
			$dataEmergencia['constante']=0;
			$objEmergencia = new TrabajadorActividad();
			// var_dump($dataEmergencia);
			// echo "<br>";
			$dataEmergencia = $objEmergencia->insertRecord($dataEmergencia);
			// echo "ordenes de trabajo =" ; var_dump($dataOt);
			// var_dump($objOt->getQuery());echo "     /   "; var_dump($objOt->getErrorsString());
			if($dataEmergencia === false) {
				$this->_errors = $objEmergencia->getErrors();
				return false;
			}
			
		}

		// return false;
		return $fieldData;
	} // _cm_post_insertRecord()

	public function getActividadDiaria($params) {
		//echo '<pre>params: '; var_dump($params); echo '</pre>'; //DEBUG
		// Obtener datos del usuario
		$this->sqlSelect = "t.id_a_diaria, p0.desc_ejecutor, p0.desc_area,t.fecha, p0.id_ejecutor, t.hh_disp, t.id_turno";
		$this->sqlOrderBy = array('p0.desc_area' => 'ASC');
		$result = $this->getRecords($params);
		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . $this->getErrorsString() . '</pre>'; //DEBUG
		if(!empty($result)) {
			$result = $result[0]; // Solo el primer registro
		}

		return $result;
	}

	public function getActDiaria($params) {	


		$where = array('WHERE' => "t.fecha >= :fecha_inicio AND t.fecha <= :fecha_fin ", 'PARAMS' => array('fecha_inicio' => $params['fecha_inicio'], 'fecha_fin' => $params['fecha_fin']));
			// Guardar variables query originales
		$orgQueryVars = $this->getAllQueryVars();
		$this->sqlSelect = "t.id_a_diaria, p0.desc_ejecutor, p0.desc_area,t.fecha, p0.id_ejecutor, t.hh_disp, t.id_turno";
		
		
		// Obtener ordenes de trabajo
		$this->sqlOrderBy = array('p0.desc_area' => 'ASC');
		$result = $this->getRecords($where);

		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . '</pre>'; //DEBUG


		// echo '<pre>params: '; var_dump($params); echo '</pre>'; //DEBUG
		// Obtener datos del usuario
		// $result = $this->getRecords($params);
		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . $this->getErrorsString() . '</pre>'; //DEBUG
		// return false;
		return $result;
	}

	public function getAreaActividadesDiarias($params){
		$where = array('WHERE' => "t.fecha >= :fecha_inicio AND t.fecha <= :fecha_fin ", 'PARAMS' => array('fecha_inicio' => $params['fecha_inicio'], 'fecha_fin' => $params['fecha_fin']));
			// Guardar variables query originales
		$orgQueryVars = $this->getAllQueryVars();
		$this->sqlSelect = "distinct p0.desc_area";
		
		
		// Obtener ordenes de trabajo
		$this->sqlOrderBy = array('p0.desc_area' => 'ASC');
		$result = $this->getRecords($where);

		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . '</pre>'; //DEBUG


		// echo '<pre>params: '; var_dump($params); echo '</pre>'; //DEBUG
		// Obtener datos del usuario
		// $result = $this->getRecords($params);
		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . $this->getErrorsString() . '</pre>'; //DEBUG
		// return false;
		return $result;
	}

	public function getEjecutoresActividadesDiarias($params){
		$where = array('WHERE' => "t.fecha >= :fecha_inicio AND t.fecha <= :fecha_fin AND id_area_operativa = :id_area_operativa", 'PARAMS' => array('fecha_inicio' => $params['fecha_inicio'], 'fecha_fin' => $params['fecha_fin'], 'id_area_operativa' => $params['id_area_operativa']));
			// Guardar variables query originales
		$orgQueryVars = $this->getAllQueryVars();
		$this->sqlSelect = "distinct t.id_ejecutor, p0.desc_ejecutor";
		
		
		// Obtener ordenes de trabajo
		$this->sqlOrderBy = array('t.id_ejecutor' => 'ASC');
		$result = $this->getRecords($where);

		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . '</pre>'; //DEBUG


		// echo '<pre>params: '; var_dump($params); echo '</pre>'; //DEBUG
		// Obtener datos del usuario
		// $result = $this->getRecords($params);
		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . $this->getErrorsString() . '</pre>'; //DEBUG
		// return false;
		return $result;
	}

	public function getActividadPorArea($params) {	


		$where = array('WHERE' => "t.fecha >= :fecha_inicio AND t.fecha <= :fecha_fin AND p0.desc_area = :desc_area", 'PARAMS' => array('fecha_inicio' => $params['fecha_inicio'], 'fecha_fin' => $params['fecha_fin'], 'desc_area' => $params['desc_area']));
			// Guardar variables query originales
		$orgQueryVars = $this->getAllQueryVars();
		$this->sqlSelect = "t.id_a_diaria, p0.desc_ejecutor, p0.desc_area,t.fecha, p0.id_ejecutor, t.hh_disp, t.id_turno";
		
		
		// Obtener ordenes de trabajo
		$this->sqlOrderBy = array('p0.desc_area' => 'ASC');
		$result = $this->getRecords($where);

		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . '</pre>'; //DEBUG


		// echo '<pre>params: '; var_dump($params); echo '</pre>'; //DEBUG
		// Obtener datos del usuario
		// $result = $this->getRecords($params);
		echo '<pre>result: '; var_dump($result); echo $this->getQuery() . $this->getErrorsString() . '</pre>'; //DEBUG
		// return false;
		return $result;
	}


	public function getActDiariaIDEjecutor($params) {	


		$where = array('WHERE' => "t.fecha >= :fecha_inicio AND t.fecha <= :fecha_fin AND p0.id_ejecutor = :id_ejecutor", 'PARAMS' => array('fecha_inicio' => $params['fecha_inicio'], 'fecha_fin' => $params['fecha_fin'], 'id_ejecutor' => $params['id_ejecutor']));
			// Guardar variables query originales
		$orgQueryVars = $this->getAllQueryVars();
		$this->sqlSelect = "t.id_a_diaria";
		
		
		// Obtener ordenes de trabajo
		$this->sqlOrderBy = array('t.id_a_diaria' => 'ASC');
		$result = $this->getRecords($where);

		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . '</pre>'; //DEBUG


		// echo '<pre>params: '; var_dump($params); echo '</pre>'; //DEBUG
		// Obtener datos del usuario
		// $result = $this->getRecords($params);
		// echo '<pre>result: '; var_dump($result); echo $this->getQuery() . $this->getErrorsString() . '</pre>'; //DEBUG
		// return false;
		return $result;
	}


}
?>
