<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'Planificacion';

// Estructura del contenido
$structure['container'] = 'listContainer';
$structure['page_rows'] = 10; // Filas por páginas

$structure['task_mod'] = 'tsk_planificaciones(mc_mod1).php';
$structure['task_mod_container'] = 'mainContent';

$structure['list_style'] = LST_SPL;

// -> Estructura del WHERE (opcional)
$structure['where'][0]['fields'] = array('id_area_operativa');
$structure['where'][0]['sql'] = "id_area_operativa = :id_area_operativa";
$structure['where'][0]['params'] = array('id_area_operativa' => 'id_area_operativa');


$structure['where'][1]['fields'] = array('id_ejecutor');
$structure['where'][1]['sql'] = "t.id_ejecutor = :id_ejecutor";
$structure['where'][1]['params'] = array('id_ejecutor' => 'id_ejecutor');

$structure['where'][2]['fields'] = array('fecha_inicio','fecha_fin');
$structure['where'][2]['sql'] = "t.fecha_inicio >= :fecha_inicio AND t.fecha_inicio <= :fecha_fin";
$structure['where'][2]['params'] = array('fecha_inicio' => 'fecha_inicio', 'fecha_fin' => 'fecha_fin');

// $structure['where'][1]['fields'] = array('<campo2>');
// $structure['where'][1]['sql'] = "(t.<campo2> ilike ('%' || :<campo2> || '%'))";
// $structure['where'][1]['params'] = array('<campo2>' => '<campo2>');

// $structure['where'][2]['fields'] = array('nombre');
// $structure['where'][2]['sql'] = "(t.nombre ilike ('%' || :nombre1 || '%') OR t.desc ilike ('%' || :nombre2 || '%'))";
// $structure['where'][2]['params'] = array('nombre1' => 'nombre', 'nombre2' => 'nombre');

// -> Estructura del contenido del gridview
$structure['gridview']['type'] = 'gridview';
$structure['gridview']['id'] = 'tsk_planificaciones_list';
$structure['gridview']['caption'] = 'Lista de Planificaciones';
$structure['gridview']['sorted'] = true;

// $structure['gridview']['header'][0][0]['th'] = array('sort_field' => 't.fecha_inicio');
// $structure['gridview']['header'][0][0][] = array('control' => 'literal', 'content' => 'Fecha Inicio');

// $structure['gridview']['header'][0][1]['th'] = array('sort_field' => 't.fecha_fin');
// $structure['gridview']['header'][0][1][] = array('control' => 'literal', 'content' => 'Fecha Final');

// $structure['gridview']['header'][0][2]['th'] = array('sort_field' => 'fecha_reunion');
// $structure['gridview']['header'][0][2][] = array('control' => 'literal', 'content' => 'Fecha Reunion');

// $structure['gridview']['header'][0][3]['th'] = array('sort_field' => 'fecha_fin');
// $structure['gridview']['header'][0][3][] = array('control' => 'literal', 'content' => 'Fecha Final');

// $structure['gridview']['header'][0][4]['th'] = array('sort_field' => 'ejecutor');
// $structure['gridview']['header'][0][4][] = array('control' => 'literal', 'content' => 'Ejecutor');

// $structure['gridview']['header'][0][5]['th'] = array('sort_field' => 'superintendencia');
// $structure['gridview']['header'][0][5][] = array('control' => 'literal', 'content' => 'Superintendencia');

// /*fin del header*/

// $structure['gridview']['footer'][0][0]['td'] = array('colspan' => 1);
// $structure['gridview']['footer'][0][0][] = array('control' => 'literal', 'content' => '&nbsp;');

// $structure['gridview']['items'][0][0][] = array('control' => 'literal', 'field' => 'nombre');
// $structure['gridview']['items'][0][0][] = array('control' => 'hidden', 'field' => 'id_planificacion', 'id' => 'id_planificacion[{#}]');

$structure['gridview']['header'][0][0]['th'] = array('sort_field' => 'fecha_inicio');
$structure['gridview']['header'][0][0][] = array('control' => 'literal', 'content' => 'Fecha de inicio');

$structure['gridview']['header'][0][1]['th'] = array('sort_field' => 'fecha_fin');
$structure['gridview']['header'][0][1][] = array('control' => 'literal', 'content' => 'Fecha final');

$structure['gridview']['header'][0][2]['th'] = array('sort_field' => 'fecha_reunion');
$structure['gridview']['header'][0][2][] = array('control' => 'literal', 'content' => 'Fecha de Reunion');

$structure['gridview']['header'][0][3]['th'] = array('sort_field' => 'ejecutor');
$structure['gridview']['header'][0][3][] = array('control' => 'literal', 'content' => 'Ejecutor');

$structure['gridview']['header'][0][4]['th'] = array('sort_field' => 'area');
$structure['gridview']['header'][0][4][] = array('control' => 'literal', 'content' => 'Superintendencia');

$structure['gridview']['footer'][0][0]['td'] = array('colspan' => 1);
$structure['gridview']['footer'][0][0][] = array('control' => 'literal', 'content' => '&nbsp;');

$structure['gridview']['items'][0][0][] = array('control' => 'literal', 'field' => 'fecha_inicio');
$structure['gridview']['items'][0][1][] = array('control' => 'literal', 'field' => 'fecha_fin');
$structure['gridview']['items'][0][2][] = array('control' => 'literal', 'field' => 'fecha_reunion');
$structure['gridview']['items'][0][3][] = array('control' => 'literal', 'field' => 'ejecutor');
$structure['gridview']['items'][0][4][] = array('control' => 'literal', 'field' => 'area');
$structure['gridview']['items'][0][0][] = array('control' => 'hidden', 'field' => 'id_planificacion', 'id' => 'id_material[{#}]');


// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'list.php');
?>