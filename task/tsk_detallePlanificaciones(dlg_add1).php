<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'DetallePlanificacion';

// Estructura del contenido
$structure['title'] = 'Nueva Orden de trabajo';
$structure['container'] = 'divDialog';
$structure['form_action'] = 'tsk_detallePlanificaciones(dlg_add1).php';

// -> Estructura del panel de botones (opcional)
/*$structure['buttons'] = array(
	BTN_SAVE => BTN_SAVE,
	BTN_CANCEL => BTN_CANCEL,
	'BTN_BUSCAR' => array('id' => 'btn_BUSCAR', 'label' => 'Buscar', 'title' => 'Botón Buscar', 'icon_p' => 'search',
		'javascript' => "sysfwk.loadPage('task/tsk_st_ot(dlg_acmp).php', 'divDialog1', { container: 'divDialog1', js_func: 'ActualizarOt', ejecutor: $('#id_ejecutor').val()});"),
	BTN_COPY => BTN_COPY,
	BTN_PASTE => BTN_PASTE,
	BTN_OK => BTN_OK,
);*/

// Cargar configuración del screen
include('../screen/scr_detallePlanificaciones(abm1).php');
$structure['form']['fs'][0]['fields'][0][1][] = array('control' => 'button', 'id' => 'btnSelOt', 'content' => '...',
	'title' => 'Seleccionar Ot');

if(!empty($_GET['id_planificacion'])){
	$structure['form']['fs'][0]['fields'][3][1][] = array('control' => 'hidden', 'field' => 'id_planificacion', 'value' => $_GET['id_planificacion'] );
}

$javascript['global'] = <<<'EOS'
// Actualizar datos OT
function ActualizarDatosOt(id_st_ot) {
	sysfwk.ajaxRequestJson({"entity": "OT", "method": "getOtInfo", "params": {id_st_ot: id_st_ot}}, function(data) {
		//console.log(data);
		$('#id_st_ot').val(data.id_st_ot);
		$('#tag_activo').val(data.tag_activo);
		$('#desc_activo').val(data.desc_activo);
		$('#desc_actividad').val(data.desc_actividad);
		$('#id_prioridad').val(data.id_prioridad);
		$('#f_inicio').val(data.f_inicio);
	});
}

//Actualizar datos Ot
function ActualizarOt(datos) {
	ActualizarDatosOt(datos.value);
}
EOS;

$javascript['exec'] = <<<'EOS'
$('#f_inicio').datepicker();
// Botones
$('#btnSelOt').button({ icons: {primary:'ui-icon-search'}, text: false });
$('#btnSelOt').click(function() {
	sysfwk.loadPage('task/tsk_st_ot(dlg_acmp).php', 'divDialog1', { container: 'divDialog1', js_func: 'ActualizarOt', ejecutor: $('#id_ejecutor').val()});
});

EOS;

// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'dlg_add1.php');
?>