<?php
header('Content-Type: text/html; charset=UTF-8');
require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'Planificacion';

// Estructura del contenido
$structure['location'] = 'SISGREL &raquo; PLANIFICACION &raquo; &raquo; Lista de Planificaciones';
$structure['title'] = 'Administrar Planificaciones';
$structure['task_list'] = 'tsk_personalesAsignados(list).php'; // Tarea para listar/buscar

$structure['task_add'] = 'tsk_personalesAsignados(mc_add1).php'; // Tarea para agregar
$structure['task_mod'] = 'tsk_personalesAsignados(mc_mod1).php';


// -> Estructura del contenido del form de búsqueda (opcional)
$structure['search']['form']['type'] = 'fieldsets';
$structure['search']['form']['fs'][0]['type'] = 'fieldset';
$structure['search']['form']['fs'][0]['legend'] = 'Filtro de la búsqueda';
$structure['search']['form']['fs'][0]['table'] = 'y';

/*$structure['search']['form']['fs'][0]['fields'][0][0][]= array('control' => 'literal', 'content' => '<h1></h1>');*/
$structure['search']['form']['fs'][0]['fields'][0][0]['td'] = array('class' => 'label');
$structure['search']['form']['fs'][0]['fields'][0][0][] = array('control' => 'label', 'label' => 'Supervisor:');
$structure['search']['form']['fs'][0]['fields'][0][1]['td'] = array('class' => 'field');
$structure['search']['form']['fs'][0]['fields'][0][1][]= array('control' => 'textbox', 'field' => 'id_supervisor');




// -> Estructura del panel de botones (opcional)
// $structure['buttons'] = array(
	// BTN_SAVE => BTN_SAVE,
	// BTN_CANCEL => BTN_CANCEL,
	// 'MI_BOTON' => array('id' => 'btn_'.MI_BOTON, 'label' => 'Mi Boton', 'title' => 'Botón personalizado', 'icon_p' => 'comment',
		// 'javascript' => "alert('Mi boton propio')"),
	// BTN_COPY => BTN_COPY,
	// BTN_PASTE => BTN_PASTE
// );

// Javascript
include('../lib_js/script_planificacion.php');

// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'mc_list1.php');
?>