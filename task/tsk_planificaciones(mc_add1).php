<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'Planificacion';

// Estructura del contenido
$structure['location'] = 'SISGREL &raquo; Planificacion &raquo; Agregar Planificación';
$structure['title'] = 'Agregar Planificación';
$structure['form_action'] = 'tsk_planificaciones(mc_add1).php';
$structure['task_mode'] = 'add1';

// Cargar configuración del screen
include('../screen/scr_planificaciones(abm1).php');


// -> Estructura del panel de botones (opcional)
 $structure['buttons'] = array(
	 BTN_SAVE => BTN_SAVE,
	 BTN_CANCEL => BTN_CANCEL,
	 'BTN_CREAR' => array('id' => 'btn_CREAR', 'label' => 'Cargar Planificacion', 'title' => 'Nueva Planificacion', 'icon_p' => 'document',
		 'javascript' => "sysfwk.loadPage('task/tsk_detallePlanificaciones(list).php', 'divDetallePlanificacion',{},sysfwk.POST);"),
	 BTN_OK => BTN_OK
 );


function beforeSubmit(){
	echo "var data2 = sysfwk.getControlData('tsk_detallePlanificaciones_list');";
	echo "$.extend(data, data2);";
	echo "console.log(data);";
}

// Javascript

$javascript['global'] = <<<'EOS'
// Actualizar personal
function ActualizarPersonal(datos) {
	$('#id_supervisor').val(datos.value);
}
EOS;

include('../lib_js/script_planificacion.php');

$javascript['exec'] .= <<<'EOS'
$('#btn_SAVE').hide();
$('#btn_OT').hide();

$('#id_area_operativa').change(function(){
	sysfwk.loadPage('task/tsk_detallePlanificaciones(list).php', 'divDetallePlanificacion');
	$('#btn_OT').show();
});

$('#id_ejecutor').change(function(){
	sysfwk.loadPage('task/tsk_detallePlanificaciones(list).php', 'divDetallePlanificacion');
	$('#btn_OT').show();
	ejecutor=$('#id_ejecutor').val()
});

$('#btn_CREAR').click(function() {
	$('#btn_OT').show();
});


// Botones
$('#btnSelPersonalABM').button({ icons: {primary:'ui-icon-search'}, text: false });
$('#btnSelPersonalABM').click(function() {
	sysfwk.loadPage('task/tsk_personal_sel(dlg_acmp).php', 'divDialog1', { container: 'divDialog1', js_func: 'ActualizarPersonal'});
});
EOS;



// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'mc_add1.php');
echo "<div id='divDetallePlanificacion'></div>";
?>
