<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<style type="text/css">
		h1, h2{
			text-align: center;
			color:#0072FF;
		}
		.tabla-encabezado{
			width: 90%;
		}
		h3 label{
			text-decoration: underline;
		}
		.titulo-tabla{
			text-align: center;
		}
		.tabla-bordes{
			width: 100%;
			border-collapse: collapse;
			
		}
		table tbody{
			text-align: center;

		}
		.tabla-bordes th, .borde-tr td, .borde-td{
			border:1px solid black;
		}

		.tabla-bordes thead{
			background-color: #0ACDDF;
		}

		.sin_borde{
			border:0px;
		}
	</style>
</head>

<?php
	header('Content-Type: text/html; charset=UTF-8');

	require_once('../main.php');
	//	 echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
	include($GLOBALS['path_fwk_script'] . 'check_session.php');
	require_once($GLOBALS['path_class'] . 'ActividadDiaria.class.php');
	require_once($GLOBALS['path_class'] . 'DetalleAsistencia.class.php');
	require_once($GLOBALS['path_class'] . 'EjecutorEspecialidad.class.php');
	require_once($GLOBALS['path_class'] . 'ActividadOT.class.php');
	require_once($GLOBALS['path_class'] . 'ActividadRutinaria.class.php');
	require_once($GLOBALS['path_class'] . 'ActividadEmergencia.class.php');
	require_once($GLOBALS['path_class'] . 'TrabajadorDetalle.class.php');


	if (!empty($_POST['fecha_inicio']) && !empty($_POST['fecha_fin'])) {
		$params['fecha_inicio']=$_POST['fecha_inicio'];
		$params['fecha_fin']=$_POST['fecha_fin'];
		$actividadDiaria= new ActividadDiaria();
		$arregloActDiaria=$actividadDiaria->getActDiaria($params);

?>
<body>
	<h1>GERENCIA DE SERVICIOS GENERALES</h1>
	<h2>Reporte Diario de Actividades</h2>

	<div>
		<table class="tabla-bordes">
			<thead>
				<tr><th colspan="10">Actividades No Rutinarias</th></tr>
				<tr>
					<th>Fecha</th>
					<th>Area</th>
					<th>Unidad</th>
					<th>OT</th>
					<th>Prioridad</th>
					<th>Codigo Equipo</th>
					<th>Actividad</th>
					<th>Recursos</th>
					<th>Duracion</th>
					<th>HH Trabajadas</th>
				</tr>
			</thead>
			<tbody>
<?php
		$actividadesOt=  new ActividadOT();
		$actividadesRutinarias=  new ActividadRutinaria();
		$actividadesEmergencias=  new ActividadEmergencia();
		$hh_ot=0;
		$recursos_ot=0;
		$duracion_ot=0;
		foreach ($arregloActDiaria as $key => $value) {
			$param['id_a_diaria']=$arregloActDiaria[$key]['id_a_diaria'];
			$params['id_a_diaria']=$arregloActDiaria[$key]['id_a_diaria'];

			$arregloActividadDiaria=$actividadDiaria->getActividadDiaria($param);
			$arregloActividadesOt=$actividadesOt->getActividadOt($params);
			foreach ($arregloActividadesOt as $key => $value) {
?>
				<tr class="borde-tr">
					<td><?php echo $arregloActividadDiaria["fecha"]; ?></td>
					<td><?php echo $arregloActividadDiaria["desc_area"]; ?></td>
					<td><?php echo $arregloActividadDiaria["desc_ejecutor"]; ?></td>
					<td><?php echo $value["id_st_ot"]; ?></td>
					<td><?php echo $value["id_prioridad"]; ?></td>
					<td><?php echo $value["tag_activo"]; ?></td>
					<td><?php echo $value["trab_solicitado"]; ?></td>
					<td><?php $recursos_ot+=$value["cantidad"]; echo $value["cantidad"]; ?></td>	
					<td><?php $duracion_ot+=$value["duracion"];echo $value["duracion"]; ?></td>
					<td><?php $hh_ot+=$value["hh_total"]; echo $value["hh_total"]; ?></td>
				</tr>
<?php	
			}
		}
?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="7" style="text-align: right; padding-right: 10px">Resultados:</td>
					<td class="borde-td"><?php echo $recursos_ot; ?></td>
					<td class="borde-td"><?php echo $duracion_ot; ?></td>
					<td class="borde-td"><?php echo $hh_ot; ?></td></td>
				</tr>
			</tfoot>
		</table>
		<br><br><br>
		<table class="tabla-bordes">
			<thead>
				<tr><th colspan="8">Actividades Rutinarias</th></tr>
				<tr>
					<th>Fecha</th>
					<th>Area Ejecutora</th>
					<th>Unidad</th>
					<th>Area Operativa</th>
					<th>Cantidad de Rutinas</th>
					<th>Cantidad de Personal</th>
					<th>Duracion</th>
					<th>HH Trabajadas</th>
				</tr>
			</thead>
			<tbody>
<?php
		$hh_rutinas=0;
		$recursos_rutinas=0;
		$duracion_rutinas=0;
		$cantidad_rutinas=0;
		foreach ($arregloActDiaria as $key => $value) {
			$param['id_a_diaria']=$arregloActDiaria[$key]['id_a_diaria'];
			$params['id_a_diaria']=$arregloActDiaria[$key]['id_a_diaria'];
			$arregloActividadDiaria=$actividadDiaria->getActividadDiaria($param);
			$arregloActividadesRutinarias=$actividadesRutinarias->getActividadRutinaria($params);
			foreach ($arregloActividadesRutinarias as $key => $value) {
?>
				<tr class="borde-tr">
					<td><?php echo $arregloActividadDiaria["fecha"]; ?></td>
					<td><?php echo $arregloActividadDiaria["desc_area"]; ?></td>
					<td><?php echo $arregloActividadDiaria["desc_ejecutor"]; ?></td>
					<td><?php echo $value["desc_area"]; ?></td>
					<td><?php $cantidad_rutinas+=$value["cantidad_rutinas"];echo $value["cantidad_rutinas"]; ?></td>
					<td><?php $recursos_rutinas+=$value["cantidad"]; echo $value["cantidad"]; ?></td>
					<td><?php $duracion_rutinas+=$value["duracion"]; echo $value["duracion"]; ?></td>
					<td><?php $hh_rutinas+=$value["hh_total"]; echo $value["hh_total"]; ?></td>
				</tr>
<?php
			}
		}
?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="4" style="text-align: right; padding-right: 10px">Resultados:</td>
					<td class="borde-td"><?php echo $cantidad_rutinas; ?></td>
					<td class="borde-td"><?php echo $recursos_rutinas; ?></td>
					<td class="borde-td"><?php echo $duracion_rutinas; ?></td>
					<td class="borde-td"><?php echo $hh_rutinas; ?></td></td>
				</tr>
			</tfoot>
			</table>
			<br><br><br>
			<table class="tabla-bordes">
			<thead>
				<tr><th colspan="9">Actividades No Planificadas</th></tr>
				<tr>
					<th>Fecha</th>
					<th>Area Ejecutora</th>
					<th>Unidad</th>
					<th >Area Operativa</th>
					<th >Estado</th>
					<th >Descripcion</th>
					<th >Cantidad Recurso</th>
					<th >Duracion</th>
					<th >HH Trabajadas</th>
				</tr>
			</thead>
			<tbody>
<?php
		$cantidad_emergencias=0;
		$recursos_emergencia=0;
		$duracion_emergencias=0;
		$hh_emergencias=0;
		foreach ($arregloActDiaria as $key => $value) {
			$param['id_a_diaria']=$arregloActDiaria[$key]['id_a_diaria'];
			$params['id_a_diaria']=$arregloActDiaria[$key]['id_a_diaria'];
			$arregloActividadDiaria=$actividadDiaria->getActividadDiaria($param);
			$arregloActividadesEmergencias=$actividadesEmergencias->getActividadEmergencia($params);
			foreach ($arregloActividadesEmergencias as $key => $value) {
?>
				<tr class="borde-tr">
					<td><?php echo $arregloActividadDiaria["fecha"]; ?></td>
					<td><?php echo $arregloActividadDiaria["desc_area"]; ?></td>
					<td><?php echo $arregloActividadDiaria["desc_ejecutor"]; ?></td>
					<td><?php echo $value["desc_area"]; ?></td>
					<td><?php echo $value["estado"]; ?></td>
					<td><?php echo $value["descripcion"]; ?></td>
					<td><?php $recursos_emergencia+=$value["cantidad"]; echo $value["cantidad"]; ?></td>
					<td><?php $duracion_emergencias+=$value["duracion"]; echo $value["duracion"]; ?></td>
					<td><?php $hh_emergencias+=$value["hh_total"]; echo $value["hh_total"]; ?></td>
				</tr>
<?php
			
			}
		}
?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="6" style="text-align: right; padding-right: 10px">Resultados:</td>
				<td class="borde-td"><?php echo $recursos_emergencia; ?></td>
				<td class="borde-td"><?php echo $duracion_emergencias; ?></td>
				<td class="borde-td"><?php echo $hh_emergencias; ?></td></td>
			</tr>
		</tfoot>
		</table>
		<br><br><br>
<?php
	}
?>


		