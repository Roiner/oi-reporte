<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<style type="text/css">
		h1, h2{
			text-align: center;
			color:#0072FF;
		}
		.tabla-encabezado{
			width: 90%;
		}
		h3 label{
			text-decoration: underline;
		}
		.titulo-tabla{
			text-align: center;
		}
		.tabla-bordes{
			width: 100%;
			border-collapse: collapse;
			
		}
		table tbody{
			text-align: center;

		}
		.tabla-bordes th, .borde-tr td, .borde-td{
			border:1px solid black;
		}

		.tabla-bordes thead{
			background-color: #0ACDDF;
		}

		.sin_borde{
			border:0px;
		}

		table thead{
			font-size: xx-small;
		}
	</style>
</head>
<body>
	
<?php
	header('Content-Type: text/html; charset=UTF-8');

	require_once('../main.php');
	//	 echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
	include($GLOBALS['path_fwk_script'] . 'check_session.php');
	require_once($GLOBALS['path_class'] . 'ActividadDiaria.class.php');
	require_once($GLOBALS['path_class'] . 'DetalleAsistencia.class.php');
	require_once($GLOBALS['path_class'] . 'EjecutorArea.class.php');
	require_once($GLOBALS['path_class'] . 'AsistenciaTipo.class.php');
	require_once($GLOBALS['path_class'] . 'ActividadOT.class.php');
	require_once($GLOBALS['path_class'] . 'ActividadRutinaria.class.php');
	require_once($GLOBALS['path_class'] . 'ActividadEmergencia.class.php');
	require_once($GLOBALS['path_class'] . 'TrabajadorDetalle.class.php');
	require_once($GLOBALS['path_class'] . 'DetallePlanificacion.class.php');
	require_once($GLOBALS['path_class'] . 'RecursoRutina.class.php');
	require_once($GLOBALS['path_class'] . 'RecursoOt.class.php');


	if (!empty($_POST['fecha_inicio']) && !empty($_POST['fecha_fin'])) {
		$indicadores=array();
		$params['fecha_inicio']=$_POST['fecha_inicio'];
		$params['fecha_fin']=$_POST['fecha_fin'];
		$params['id_ejecutor']=$_POST['id_ejecutor'];
		$actividadDiaria= new ActividadDiaria();
		$detalleAsistencia= new DetalleAsistencia();
		$asistenciaTipo = new AsistenciaTipo();
		$arregloActividadDiaria=$actividadDiaria->getActDiariaIDEjecutor($params);
		
		$params['id_a_diaria']=$arregloActividadDiaria[0]['id_a_diaria'];
		$trabajadores=$detalleAsistencia->getDetalleAsistencia($params);
		$arregloAsistenciaTipos=$asistenciaTipo->getTipos($params);
		// var_dump($arregloActividadDiaria);
		$aDiaria=array();
		foreach ($arregloActividadDiaria as $key => $value) {
			$aDiaria[]=$value['id_a_diaria'];
		}
		
		$cadena = "'" . implode("','",$aDiaria) . "'";
		// echo $cadena;
		$params['cadenaActividad']=$cadena;
		$personal=array();

		foreach ($trabajadores as $key => $value) {
			$params['id_personal']=$value['id_personal'];
			$prueba=$detalleAsistencia->getAsistencia($params);
			foreach ($arregloAsistenciaTipos as $llave => $valor) {
				$params['id_a_tipo']=$valor["value"];
				$arregloTipodeAsistencia=$detalleAsistencia->getAsistenciaTipo($params);
				$trabajadores[$key]['tipoDeAsistencia'][] = array('tipo' => $valor['text'], 'cantidad' => $arregloTipodeAsistencia[0]['count']);
				// echo $valor['text'];
				// var_dump($prueba2);
				// echo "<br>";echo "<br>";
			}
			// var_dump($prueba);
			// echo "<br>";echo "<br>";
		}
		foreach ($trabajadores as $key => $value) {
			var_dump($value['tipoDeAsistencia']);
		}

		// foreach ($arregloActividadDiaria as $key => $value) {
		// 	echo $value['id_a_diaria'];
		// }

	}
		
			
?>
<h1>GERENCIA DE SERVICIOS GENERALES</h1>
	<h2>Reporte de Indicadores del <?php echo $POST['fecha_inicio'];  ?> al <?php echo $POST['fecha_fin'];  ?></h2>

	<div>
		<table class="tabla-bordes">
			<thead>
				<tr>
					<th>Unidad</th>
					<th>OT Prog Trab Soc</th>
					<th>OT Ejec Trab Soc</th>
					<th>% Cump Trab Soc</th>
					<th>OT Prog</th>
					<th>OT Ej</th>
					<th>OT Pend</th>
					<th>% Cump OT</th>
					<th>OT Ad</th>
					<th>HH Ad</th>
					<th>OT Ej + Ad</th>
					<th>HH OT Trab Soc</th>
					<th>HH OT Prog Ej</th>
					<th>HH Rut Ej</th>
					<th>HHP1+ HH Prog Ej</th>
					<th>HH Rutinas+HHP1+HH Real+HH OT TRAB SOC</th>
					<th>Reactividad</th>
				</tr>
			</thead>
			<tbody>
</body>


		