<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = '<Entity>';

// Estructura del contenido
$structure['location'] = 'SISGREL &raquo; ADMINISTRAR &raquo; Tablas Maestras &raquo; <Entity> &raquo; Eliminar <Entity>';
$structure['title'] = 'Eliminar <Entity>';
$structure['form_action'] = 'tsk_<Entity>(mc_del1).php';

// Cargar configuración del screen
include('../screen/scr_<Entity>(abm1).php');

// -> Estructura del panel de botones (opcional)
// $structure['buttons'] = array(
	// BTN_SAVE => BTN_SAVE,
	// BTN_CANCEL => BTN_CANCEL,
	// 'MI_BOTON' => array('id' => 'btn_'.MI_BOTON, 'label' => 'Mi Boton', 'title' => 'Botón personalizado', 'icon_p' => 'comment',
		// 'javascript' => "alert('Mi boton propio')"),
	// BTN_COPY => BTN_COPY,
	// BTN_PASTE => BTN_PASTE
// );

// Javascript
// $javascript['exec'] = <<<'EOS'
	// $('#siglas').datepicker();
// EOS;

// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'mc_del1.php');
?>