<?php
header('Content-Type: text/html; charset=UTF-8');
require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'ActividadDiaria';

// Estructura del contenido
$structure['location'] = 'SISGREL &raquo; REPORTES &raquo; &raquo; Resumen de actividades';
$structure['title'] = 'Administrar Planificaciones';
$structure['task_list'] = 'tsk_resumenDiario(list).php'; // Tarea para listar/buscar


// -> Estructura del contenido del form de búsqueda (opcional)
$structure['search']['form']['type'] = 'fieldsets';
$structure['search']['form']['fs'][0]['type'] = 'fieldset';
$structure['search']['form']['fs'][0]['legend'] = 'Filtro de la búsqueda';
$structure['search']['form']['fs'][0]['table'] = 'y';

/*$structure['search']['form']['fs'][0]['fields'][0][0][]= array('control' => 'literal', 'content' => '<h1></h1>');*/

$structure['search']['form']['fs'][0]['fields'][0][0]['td'] = array('class' => 'label');
$structure['search']['form']['fs'][0]['fields'][0][0][] = array('control' => 'label', 'label' => 'Fecha Inicio:');

$structure['search']['form']['fs'][0]['fields'][0][1]['td'] = array('class' => 'field');
$structure['search']['form']['fs'][0]['fields'][0][1][]= array('control' => 'textbox', 'field' => 'fecha_inicio', 'class'=>'datepicker');

$structure['search']['form']['fs'][0]['fields'][0][2]['td'] = array('class' => 'label');
$structure['search']['form']['fs'][0]['fields'][0][2][] = array('control' => 'label', 'label' => 'Fecha final:');

$structure['search']['form']['fs'][0]['fields'][0][3]['td'] = array('class' => 'field');
$structure['search']['form']['fs'][0]['fields'][0][3][]= array('control' => 'textbox', 'field' => 'fecha_fin');



// -> Estructura del panel de botones (opcional)
// $structure['buttons'] = array(
	// BTN_SAVE => BTN_SAVE,
	// BTN_CANCEL => BTN_CANCEL,
	// 'MI_BOTON' => array('id' => 'btn_'.MI_BOTON, 'label' => 'Mi Boton', 'title' => 'Botón personalizado', 'icon_p' => 'comment',
		// 'javascript' => "alert('Mi boton propio')"),
	// BTN_COPY => BTN_COPY,
	// BTN_PASTE => BTN_PASTE
// );

// Javascript

$javascript['exec'] = <<<'EOS'
	$('#fecha_inicio').datepicker();
	$('#fecha_fin').datepicker();
EOS;

// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'mc_list1.php');
?>