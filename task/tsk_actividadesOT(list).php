<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'ActividadOT';

// Estructura del contenido
$structure['container'] = 'listContainer';
$structure['page_rows'] = 10; // Filas por páginas

// $structure['task_mod'] = 'tsk_materiales(dlg_mod1).php';
// $structure['task_mod_container'] = 'divDialog';
// $structure['task_del'] = 'tsk_materiales(dlg_del1).php';
// $structure['task_del_container'] = 'divDialog';

$structure['list_style'] = LST_SPL;

$_POST['id_st_ot'] = 0;
$structure['where'][0]['fields'] = array('id_st_ot');
$structure['where'][0]['sql'] = "t.id_st_ot IS NULL";
$structure['where'][0]['params'] = array();

// -> Estructura del WHERE (opcional)
// $structure['where'][0]['fields'] = array('nombre');
// $structure['where'][0]['sql'] = "t.nombre ilike ('%' || :nombre || '%')";
// $structure['where'][0]['params'] = array('nombre' => 'nombre');

// $structure['where'][1]['fields'] = array('<campo2>');
// $structure['where'][1]['sql'] = "(t.<campo2> ilike ('%' || :<campo2> || '%'))";
// $structure['where'][1]['params'] = array('<campo2>' => '<campo2>');

// $structure['where'][2]['fields'] = array('nombre');
// $structure['where'][2]['sql'] = "(t.nombre ilike ('%' || :nombre1 || '%') OR t.desc ilike ('%' || :nombre2 || '%'))";
// $structure['where'][2]['params'] = array('nombre1' => 'nombre', 'nombre2' => 'nombre');

// -> Estructura del contenido del gridview
$structure['gridview']['type'] = 'gridview';
$structure['gridview']['id'] = 'tsk_actividadesOT_list';
$structure['gridview']['caption'] = 'Lista de Actividades no Rutinarias';
$structure['gridview']['sorted'] = true;


$structure['gridview']['header'][0][0]['th'] = array('sort_field' => 'id_st_ot');
$structure['gridview']['header'][0][0][] = array('control' => 'literal', 'content' => 'OT');

$structure['gridview']['header'][0][1][] = array('control' => 'literal', 'content' => 'Codigo de Equipo');

$structure['gridview']['header'][0][2][] = array('control' => 'literal', 'content' => 'Prioridad');

$structure['gridview']['header'][0][3][] = array('control' => 'literal', 'content' => 'Actividad');

$structure['gridview']['header'][0][4][] = array('control' => 'literal', 'content' => 'Cantidad de Personal');

$structure['gridview']['header'][0][5][] = array('control' => 'literal', 'content' => 'Duracion');

$structure['gridview']['header'][0][6][] = array('control' => 'literal', 'content' => 'HH Trabajadas');


$structure['gridview']['items'][0][0][] = array('control' => 'literal', 'field' => 'id_st_ot');
$structure['gridview']['items'][0][0][] = array('control' => 'hidden', 'field' => 'id_st_ot', 'id' => 'id_st_ot[{#}]');


$structure['gridview']['footer'][0][0]['td'] = array('colspan' => 5);
$structure['gridview']['footer'][0][0][] = array('control' => 'button', 'content' => 'Agregar',
'id' => 'btnAddOt', 'name' => 'btnAddOt');

$structure['gridview']['footer'][0][1][] = array('control' => 'literal', 'content' => 'Horas Hombre Totales');
$structure['gridview']['footer'][0][2][] = array('control' => 'textbox', 'field' => 'hh_ot', 'disabled' => 'y', 'class' => 'hh');
$structure['gridview']['footer'][0][2][] = array('control' => 'hidden', 'field' => 'cantidad_ot', 'value' => 0,'disabled' => 'y');

$javascript['global'] = <<<'EOS'

function contarHorasOt()
{
	var resultado=0;
	$(".hh_t_ot").each(function(){
        resultado+=parseInt($(this).val());
    });
    $("#hh_ot").val(resultado);
    contarHhEj();
}

function contarOT()
{
	var nFila = $("#tsk_actividadesOT_list table tbody tr").length;
	$("#cantidad_ot").val(nFila);
}

// Eliminar Fila
function delRowDiagTrat(btn) {
	if(confirm('Confirme que desea eliminar la Orden de trabajo')) {
		$(btn).closest('tr').remove();
		contarHorasOt();
		contarOT();
	}
}

// Editar Fila
function modRowDiagTrat(n) {
	var recursos = $("#recursos_"+n).val();
	var id_st_ot = $("#id_st_ot_"+n).val();
	var hh_total = $("#hh_total_"+n).val();
	var cantidad = $("#cantidad_"+n).val();

	var data = { container: 'divDialog', js_func: 'addRowDiagTrat', id_ejecutor: $('#id_ejecutor').val(), recursos:recursos, id_st_ot: id_st_ot, hh_total: hh_total, tr:n, cantidad: cantidad};
	sysfwk.loadPage('task/tsk_actividadesOT(dlg_jsfrm1).php', 'divDialog', data, sysfwk.POST);
}

// Agregar Fila
function addRowDiagTrat(data) {
	var cantidad=[];
	var bandera=false;
	var recursos = JSON.parse(data.recursos);
	var recursos_t = JSON.parse(data.recursos_t);

	if(!data.tr){
		var nFila = $("#tsk_actividadesOT_list table tbody tr").length;
		var jerarquia=nFila+1;
		if(nFila > 0) {
			nFila = Number($("#tsk_actividadesOT_list table tbody tr").filter(':last').find('input[name=rownum]').val());
			nFila++;
		}

		var fila = '<tr class="'+nFila+'">';
		fila += '<td><input type="hidden" name="rownum" value="' + nFila + '"/><input id="id_st_ot_'+ nFila +'" type="hidden" name="id_st_ot[' + nFila + ']" class="contar_ot" value="' + data.id_st_ot + '"/>' + data.id_st_ot + '</td>';
		fila += '<td><input type="hidden" name="tag_activo[' + nFila + ']" value="' + data.tag_activo + '"/>' + data.tag_activo + '</td>';
		fila += '<td><input type="hidden" name="id_prioridad[' + nFila + ']" value="' + data.id_prioridad + '"/>' + data.id_prioridad + '</td>';
		fila += '<td><input type="hidden" name="desc_actividad[' + nFila + ']" value="' + data.desc_actividad + '"/>' + data.desc_actividad + '</td>';
		fila += '<td><input type="hidden" name="cantidad[' + nFila + ']" id="cantidad_'+ nFila +'" value="' + data.cantidad + '"/> <label>' + data.cantidad + ' </label></td>';
		fila += '<td><input type="hidden" id="hh_total_'+ nFila +'" name="duracion[' + nFila + ']" value="' + data.hh_total + '"/><label>' + data.hh_total + '</label></td>';
		fila += '<td><input type="hidden" name="hh_total[' + nFila + ']" class="hh_t_ot" value="' + data.hh_total*data.cantidad + '"/><label>' + data.hh_total*data.cantidad + '</label></td>';
		fila += '<td class="oculto"><input type="hidden" id="estado'+ nFila +'" name="estado[' + nFila + ']" value="' + data.estado + '" /></td>';
		fila += '<td class="oculto"><input type="hidden" name="id_planificacion[' + nFila + ']" value="' + data.id_planificacion + '"/></td>';
		fila += '<td class="oculto"><input type="hidden" id="recursos_'+ nFila +'" name="recursos[' + nFila + ']" /></td>';
		fila += '<td class="oculto"><input type="hidden" id="recursos_t_'+ nFila +'" name="recursos_t[' + nFila + ']" /></td>';
		$.each(recursos, function( index, value ) {
			fila += '<td class="oculto"><input type="hidden" id="'+nFila+'_' + index + '" name="recurso['+nFila+'][' + index + ']" value="' + value + '"/></td>';
		});
		fila += '<td><button type="button" id="'+nFila+'" class="editar" "value="Editar" title="Editar registro">Eliminar</button><button type="button" class="eliminar" onclick="delRowDiagTrat(this);" "value="Eliminar" title="Eliminar registro">Eliminar</button></td></tr>';

		fila = $(fila);
		fila.find('#recursos_'+nFila).val(data.recursos);
		fila.find('#recursos_t_'+nFila).val(data.recursos_t);
		fila.find('.oculto').hide();
		fila.find('.editar').button({ icons: { primary: 'ui-icon ui-icon-pencil' }, text: false }).click(function(){
			modRowDiagTrat($(this).attr("id"));
		});
		fila.find('.eliminar').button({ icons: { primary: 'ui-icon ui-icon-trash' }, text: false });
		$("#tsk_actividadesOT_list table tbody").append(fila);
	}else{
		$("."+data.tr).children("td").each(function (index2) 
		{
			switch (index2) 
			{
				case 4: {
					$(this).children("label").text(data.cantidad);
					$(this).children("input").val(data.cantidad);
					break;
				}
				case 5: {
					$(this).children("label").text(data.hh_total);
					$(this).children("input").val(data.hh_total);
					break;
				}
				case 6: {
					$(this).children("label").text(data.hh_total*data.cantidad);
					$(this).children("input").val(data.hh_total*data.cantidad);
					break;
				}
				case 7: {
					$(this).children("input").val(data.recursos);
					break;
				}
				
			}
			$.each(recursos, function( index, value ) {
				$("#"+data.tr+"_"+index).val(value);
			});
		});
	}
contarHorasOt();
contarOT();
}

EOS;

$javascript['exec'] = <<<EOS

$('#msg_actividadot_list').hide();
// Botones
$('#btnAddOt').button({ icons: {primary:'ui-icon-circle-plus'} });
//console.log($('#id_ejecutor').val());
$('#btnAddOt').click(function(){
	if($('#id_ejecutor').val()!=''){
		var data = { container: 'divDialog', js_func: 'addRowDiagTrat', id_ejecutor: $('#id_ejecutor').val() };
		sysfwk.loadPage('task/tsk_actividadesOT(dlg_jsfrm1).php', 'divDialog', data, sysfwk.POST);
	}
	else{
		alert('Debe escoger una unidad para poder agregar una orden de trabajo');
	}
});

EOS;


// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'list.php');
?>