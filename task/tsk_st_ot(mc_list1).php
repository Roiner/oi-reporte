<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';

// Definir la entidad
$structure['entity'] = 'OT';
$structure['method'] = 'getUserList'; // Método de la entidad que realizará la búsqueda
$structure['container'] = 'divDialog1';

// Estructura del contenido
$structure['title'] = 'Lista de ordenes de trabajo';

// Mensaje por defecto (opcional)
// $messages['title'] = 'Información:';
// $messages['info'] = 'Ingrese la ficha, cédula o nombre del personal';
// $messages['extra'] = 'En la lista de resultado apareceran las coincidencias ya sea por ficha, cédula o nombre del personal';
// $messages['state'] = SC_OK;

// Opciones del dialogo
$structure['dlg_options'] = array(
	'width' => 800
);

// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'dlg_acmp.php');
?>