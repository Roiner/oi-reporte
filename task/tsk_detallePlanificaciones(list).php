<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'DetallePlanificacion';

// Estructura del contenido
$structure['container'] = 'divDetallePlanificacion';
$structure['page_rows'] = 20; // Filas por páginas


$structure['task_del'] = 'tsk_detallePlanificaciones(dlg_del1).php';
$structure['task_del_container'] = 'divDialog';

$structure['list_style'] = LST_SPL;

// -> Estructura del WHERE (opcional)

// $structure['where'][1]['fields'] = array('<campo2>');
// $structure['where'][1]['sql'] = "(t.<campo2> ilike ('%' || :<campo2> || '%'))";
// $structure['where'][1]['params'] = array('<campo2>' => '<campo2>');

// $structure['where'][2]['fields'] = array('nombre');
// $structure['where'][2]['sql'] = "(t.nombre ilike ('%' || :nombre1 || '%') OR t.desc ilike ('%' || :nombre2 || '%'))";
// $structure['where'][2]['params'] = array('nombre1' => 'nombre', 'nombre2' => 'nombre');
$modo="";

if(empty($_POST['id_planificacion'])) {
	$modo="add1";
	$_POST['id_planificacion'] = 0;
	$structure['where'][0]['fields'] = array('id_planificacion');
	$structure['where'][0]['sql'] = "t.id_planificacion IS NULL";
	$structure['where'][0]['params'] = array();
} 
else {
	$modo="mod1";
	$structure['where'][0]['fields'] = array('id_planificacion');
	$structure['where'][0]['sql'] = "t.id_planificacion = :id_planificacion";
	$structure['where'][0]['params'] = array('id_planificacion' => 'id_planificacion');
}


// -> Estructura del contenido del gridview
$structure['gridview']['type'] = 'gridview';
$structure['gridview']['id'] = 'tsk_detallePlanificaciones_list';
$structure['gridview']['caption'] = 'Planificacion';
$structure['gridview']['sorted'] = true;


$structure['gridview']['header'][0][0][] = array('control' => 'label', 'field' => 'id_st_ot');

$structure['gridview']['header'][0][1][] = array('control' => 'literal', 'content' => 'CODIGO/EQUIPO');

$structure['gridview']['header'][0][2][] = array('control' => 'literal', 'content' => 'DESC/EQUIPO');

$structure['gridview']['header'][0][3][] = array('control' => 'literal', 'content' => 'DESCRIPCION DEL TRABAJO');

$structure['gridview']['header'][0][4][] = array('control' => 'literal', 'content' => 'PRI');

$structure['gridview']['header'][0][5][] = array('control' => 'literal', 'content' => 'JER');

$structure['gridview']['header'][0][6][] = array('control' => 'literal', 'content' => 'F.INICIO');

$structure['gridview']['header'][0][7][] = array('control' => 'literal', 'content' => 'OBSERVACION');

$structure['gridview']['header'][0][8][] = array('control' => 'literal', 'content' => '&nbsp;');

$structure['gridview']['items'][0][0][] = array('control' => 'literal', 'field' => 'id_st_ot');
$structure['gridview']['items'][0][0][] = array('control' => 'hidden', 'field' => 'id_st_ot', 'id' => 'id_st_ot[{#}]', 'class' => 'contar_ot');

$structure['gridview']['footer'][0][0]['td'] = array('colspan' => 9);
$structure['gridview']['footer'][0][0][] = array('control' => 'button', 'content' => 'Agregar',
'id' => 'btnAddOt', 'name' => 'btnAddOt');

$structure['gridview']['items'][0][1][] = array('control' => 'literal', 'field' => 'tag_activo');
$structure['gridview']['items'][0][1][] = array('control' => 'hidden', 'field' => 'tag_activo', 'id' => 'tag_activo[{#}]');

$structure['gridview']['items'][0][2][] = array('control' => 'literal', 'field' => 'desc_activo');
$structure['gridview']['items'][0][2][] = array('control' => 'hidden', 'field' => 'desc_activo', 'id' => 'desc_activo[{#}]');

$structure['gridview']['items'][0][3][] = array('control' => 'literal', 'field' => 'desc_actividad');
$structure['gridview']['items'][0][3][] = array('control' => 'hidden', 'field' => 'desc_actividad', 'id' => 'desc_actividad[{#}]');

$structure['gridview']['items'][0][4][] = array('control' => 'literal', 'field' => 'id_prioridad');
$structure['gridview']['items'][0][4][] = array('control' => 'hidden', 'field' => 'id_prioridad', 'id' => 'id_prioridad[{#}]');

$structure['gridview']['items'][0][5][] = array('control' => 'literal', 'field' => 'id_prioridad');
$structure['gridview']['items'][0][5][] = array('control' => 'hidden', 'field' => 'id_prioridad', 'id' => 'id_prioridad[{#}]');

$structure['gridview']['items'][0][6][] = array('control' => 'literal', 'field' => 'fecha_inicio');
$structure['gridview']['items'][0][6][] = array('control' => 'hidden', 'field' => 'fecha_inicio', 'id' => 'fecha_inicio[{#}]');

$structure['gridview']['items'][0][7][] = array('control' => 'literal', 'field' => 'observaciones');
$structure['gridview']['items'][0][7][] = array('control' => 'hidden', 'field' => 'observaciones', 'id' => 'observaciones[{#}]');

$structure['gridview']['items'][0][8][] = array('control' => 'hidden', 'field' => 'id_planificacion', 'id' => 'id_planificacion[{#}]');



$javascript['global'] = <<<'EOS'
var planificacion;
//Mostrar y ocultar boton de guardar
function mostrarBtn(){
	if($('#tsk_detallePlanificaciones_list table tbody tr').length > 0)
		$('#btn_SAVE').show();
	else
		$('#btn_SAVE').hide();
}

function actualizarJerarquia(){
	var cont=1;
	$("#tsk_detallePlanificaciones_list table tbody tr").each(function (index) 
	{
		$(this).children("td").each(function (index2) 
		{
			switch (index2) 
			{
				case 5: {
					$(this).children("label").text(cont++);
					break;
				}
			}
		})
	});
}

// Eliminar Fila
function delRowDiagTrat(btn) {
	if(confirm('Confirme que desea eliminar la Orden de trabajo')) {
		$(btn).closest('tr').remove();
		mostrarBtn();
		actualizarJerarquia();
	}
}

// Agregar Fila
function addRowDiagTrat(data) {
	//console.log(data);
	var nFila = $("#tsk_detallePlanificaciones_list table tbody tr").length;
	var jerarquia=nFila+1;
	if(nFila > 0) {
		nFila = Number($("#tsk_detallePlanificaciones_list table tbody tr").filter(':last').find('input[name=rownum]').val());
		nFila++;
	}
	
	var fila = '<tr>';
	fila += '<td><input type="hidden" name="rownum" value="' + nFila + '"/><input type="hidden" class="contar_ot" name="id_st_ot[' + nFila + ']" value="' + data.id_st_ot + '"/>' + data.id_st_ot + '</td>';
	fila += '<td><input type="hidden" name="tag_activo[' + nFila + ']" value="' + data.tag_activo + '"/>' + data.tag_activo + '</td>';
	fila += '<td><input type="hidden" name="desc_activo[' + nFila + ']" value="' + data.desc_activo + '"/>' + data.desc_activo + '</td>';
	fila += '<td><input type="hidden" name="desc_actividad[' + nFila + ']" value="' + data.desc_actividad + '"/>' + data.desc_actividad + '</td>';
	fila += '<td><input type="hidden" name="id_prioridad[' + nFila + ']" value="' + data.id_prioridad + '"/>' + data.id_prioridad + '</td>';
	fila += '<td><label>' + jerarquia + '</label></td>';
	fila += '<td><input class="fecha_inicio" type="text" name="fecha_ini[' + nFila + ']" value="' + data.fecha_inicio + '"/></td>';
	fila += '<td><input type="hidden" name="observaciones[' + nFila + ']" value="' + data.observaciones + '"/>' + data.observaciones + '</td>';
	fila += '<td><input type="hidden" name="observaciones[' + nFila + ']" value="' + data.observaciones + '"/></td>';
	fila += '<td><button type="button" onclick="delRowDiagTrat(this);" "value="Eliminar" title="Eliminar registro">Eliminar</button></td></tr>';
	
	fila = $(fila);
	fila.find('button').button({ icons: { primary: 'ui-icon ui-icon-trash' }, text: false });
	fila.find('.fecha_inicio').datepicker();
	$("#tsk_detallePlanificaciones_list table tbody").append(fila);
	mostrarBtn();
}
EOS;


$jsAddOtClick = '';

//echo "<h1>".$modo."</h1>";

if($modo == 'add1') {
	$jsAddOtClick = <<<'EOS'
		var data = { container: 'divDialog', js_func: 'addRowDiagTrat' };
		sysfwk.loadPage('task/tsk_detallePlanificaciones(dlg_jsfrm1).php', 'divDialog', data, sysfwk.POST);
EOS;
}
elseif($modo == 'mod1') {
	$jsAddOtClick = <<<'EOS'

		var data = { container: 'divDialog', id_planificacion: $('#id_planificacion').val() };
		sysfwk.loadPage('task/tsk_detallePlanificaciones(dlg_add1).php', 'divDialog', data);
EOS;
}




$javascript['exec'] = <<<EOS
// Botones
$('#btnAddOt').button({ icons: {primary:'ui-icon-circle-plus'} });

$('#btnAddOt').click(function(){
	if($('#id_ejecutor').val()!=''){
		$jsAddOtClick;
	}
	else{
		alert('Debe escoger una unidad para poder agregar una orden de trabajo');
	}
});

$('#tsk_detallePlanificaciones_list' .fecha_inicio).click(function(){
	alert('Hola');
});

EOS;

// sysfwk.loadPage('task/tsk_detallePlanificaciones(dlg_jsfrm1).php', 'divDialog');
// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'list.php');
?>


