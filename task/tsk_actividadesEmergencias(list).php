<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');


//cargar los recursos
require_once($GLOBALS['path_class'] . 'EjecutorEspecialidad.class.php');;
$params['id_ejecutor']= $_POST['id_ejecutor'];
$recursos= new EjecutorEspecialidad();
$recursos=$recursos->getRecurso($params);



// Definir la entidad
$structure['entity'] = 'ActividadEmergencia';

// Estructura del contenido
$structure['container'] = 'divActividadesEmergencias';
$structure['paginator'] = PAGINATOR_NONE; // Filas por páginas

// $structure['task_mod'] = 'tsk_materiales(dlg_mod1).php';
// $structure['task_mod_container'] = 'divDialog';
// $structure['task_del'] = 'tsk_materiales(dlg_del1).php';
// $structure['task_del_container'] = 'divDialog';

$structure['list_style'] = LST_SPL;

$_POST['estado']='TERMINADO';
$structure['where'][0]['fields'] = array('id_ejecutor','estado');
$structure['where'][0]['sql'] = "t.id_ejecutor = :id_ejecutor AND t.estado != :estado";
$structure['where'][0]['params'] = array('id_ejecutor' => 'id_ejecutor', 'estado' => 'estado');

// -> Estructura del WHERE (opcional)
// $structure['where'][0]['fields'] = array('nombre');
// $structure['where'][0]['sql'] = "t.nombre ilike ('%' || :nombre || '%')";
// $structure['where'][0]['params'] = array('nombre' => 'nombre');

// $structure['where'][1]['fields'] = array('<campo2>');
// $structure['where'][1]['sql'] = "(t.<campo2> ilike ('%' || :<campo2> || '%'))";
// $structure['where'][1]['params'] = array('<campo2>' => '<campo2>');

// $structure['where'][2]['fields'] = array('nombre');
// $structure['where'][2]['sql'] = "(t.nombre ilike ('%' || :nombre1 || '%') OR t.desc ilike ('%' || :nombre2 || '%'))";
// $structure['where'][2]['params'] = array('nombre1' => 'nombre', 'nombre2' => 'nombre');

// -> Estructura del contenido del gridview
$structure['gridview']['type'] = 'gridview';
$structure['gridview']['id'] = 'tsk_actividadesEmergencias_list';
$structure['gridview']['caption'] = 'Lista de Actividades no Planificadas';
$structure['gridview']['sorted'] = true;



$structure['gridview']['header'][0][0][] = array('control' => 'literal', 'content' => 'Area Operativa');

$structure['gridview']['header'][0][1][] = array('control' => 'literal', 'content' => 'Estado');

$structure['gridview']['header'][0][2][] = array('control' => 'literal', 'content' => 'Descripcion');

$i=2;
$auxiliar=array();
foreach ($recursos as $recurso) {
	$structure['gridview']['header'][0][++$i][] = array('control' => 'literal', 'content' => $recurso['text']);
	array_push($auxiliar, $recurso['especialidad']);
}
$cadena = '["' . implode('","',$auxiliar) . '"]';
/*var_dump(json_encode($auxiliar));
var_dump($cadena);*/
$structure['gridview']['header'][0][++$i][] = array('control' => 'literal', 'content' => 'Duracion');

$structure['gridview']['header'][0][++$i][] = array('control' => 'literal', 'content' => 'HH Trabajadas');



$structure['gridview']['items'][0][0][] = array('control' => 'literal', 'field' => 'desc_area', 'id' => 'desc_area[{#}]');
$structure['gridview']['items'][0][0][] = array('control' => 'hidden', 'field' => 'id_area_operativa', 'id' => 'id_area_operativa_e_{#}', 'name' => 'id_area_operativa_e[{#}]');
$structure['gridview']['items'][0][0][] = array('control' => 'hidden', 'field' => 'rownum', 'value' => '{#}');
$structure['gridview']['items'][0][0][] = array('control' => 'hidden', 'field' => 'emergencia[{#}]', 'value' => '{#}');
$structure['gridview']['items'][0][0][] = array('control' => 'hidden', 'field' => 'id_a_emerg', 'id' => 'id_a_emerg_{#}', 'name' => 'id_a_emerg[{#}]');
$structure['gridview']['items'][0][0][] = array('control' => 'hidden', 'field' => 'id_t_actividad', 'id' => 'id_t_actividad_{#}', 'name' => 'id_t_actividad[{#}]');

$structure['gridview']['items'][0][1][] = array('control' => 'literal','field' => 'estado','id' => 'estado_e_{#}', 'name' => 'estado_e[{#}]','velue' => 'estado', 'content' => "<select id='estado_e_{#}' name='estado_e[{#}]'><option value=''>Seleccione una Opcion</option><option value='TERMINADO'>Terminado</option><option value='EN_PROCESO'>Sin terminar</option><option value='PENDIENTE'>Pendiente</option></select>");
$structure['gridview']['items'][0][1][] = array('control' => 'hidden', 'field' => 'estado', 'id' => 'estado_p_{#}', 'name' => 'estado_p[{#}]');

$structure['gridview']['items'][0][2][] = array('control' => 'literal', 'field' => 'descripcion', 'id' => 'descripcion[{#}]');
$structure['gridview']['items'][0][2][] = array('control' => 'hidden', 'field' => 'descripcion', 'id' => 'descripcion{#}' ,
	'name' => 'descripcion[{#}]');

$i=2;
foreach ($recursos as $recurso) {
	$structure['gridview']['items'][0][++$i][] = array('control' => 'textbox', 'field' => 'recursos_e', 'id' => '{#}_'.$recurso['especialidad'], 'class' => 'result_{#} cant_r_{#}' ,'name' => 'recursos_e['.$recurso['especialidad'].'][{#}]');
}

$structure['gridview']['items'][0][++$i][] = array('control' => 'textbox', 'field' => 'duracion_e', 'id' => 'duracion_e_{#}','class' => 'result_{#} duracion', 'name' => 'duracion_e[{#}]');
$structure['gridview']['items'][0][++$i][] = array('control' => 'textbox', 'field' => 'hh_total_e', 'id' => 'hh_total_e_{#}', 'name' => 'hh_total_e[{#}]', 'class' => 'hh_em', 'disabled' => 'y');



$structure['gridview']['footer'][0][0]['td'] = array('colspan' => $i-1);
$structure['gridview']['footer'][0][0][] = array('control' => 'button', 'content' => 'Agregar',
'id' => 'btnAddEmergencia', 'name' => 'btnAddEmergencia');

$structure['gridview']['footer'][0][1][] = array('control' => 'literal', 'content' => 'Horas Hombre Totales');
$structure['gridview']['footer'][0][2][] = array('control' => 'textbox', 'field' => 'hh_t_em', 'disabled' => 'y', 'class' => 'hh');
$structure['gridview']['footer'][0][2][] = array('control' => 'hidden', 'field' => 'cantidad_emergencia', 'disabled' => 'y', 'value' => 0);
$structure['gridview']['footer'][0][2][] = array('control' => 'hidden', 'field' => 'recursos_eme','disabled' => 'y', 'value' => json_encode($auxiliar));


$javascript['global'] = <<<'EOS'
function contarEmergencia(){
	var nFila = $("#tsk_actividadesEmergencias_list table tbody tr").length;
	$("#cantidad_emergencia").val(nFila);
}

function contarHorasEmergencia()
{
	var resultado=0;
	$(".hh_em").each(function(){
        if ($(this).val()) {
        	resultado+=parseInt($(this).val());
        }else{
        	resultado+=0;
        }
    });
    $("#hh_t_em").val(resultado);
    contarHhEj();
}

contarEmergencia();

$("#tsk_actividadesEmergencias_list table tbody tr").each(function (index) 
{
	$(".result_"+index).change(function(){
		var resultado=0;
		$(".cant_r_"+index).each(function(){
			if($(this).val())
				resultado+=parseInt($(this).val());
		});
		$("#hh_total_e_"+index).val($("#duracion_e_"+index).val()*resultado);
		contarHorasEmergencia();
	});
	$(this).children("td").each(function (index2) 
		{
			switch (index2) 
			{
				case 1: {
					$(this).children("select").val($(this).children("input").val());
					break;
				}
			}
		})
});



$("#tsk_actividadesEmergencias_list table").attr("id","tabla_emergencia");
// Cargar lista de areas
var param = { extra: { value: '', text: '(Todos)' } };

var recursos = JSON.parse($('#recursos_eme').val());
//console.log(recursos);


// Eliminar Fila
function delRowEmergenciaTrat(btn) {
	if(confirm('Confirme que desea eliminar la Orden de trabajo')) {
		$(btn).closest('tr').remove();
		contarHorasEmergencia();
		contarEmergencia();
	}
}

// Editar Fila
function modRowEmergenciaTrat(btn) {
	alert('en construccion');
}

// Agregar Fila
function addRowEmergenciaTrat() {
	
	var nFila = $("#tsk_actividadesEmergencias_list table tbody tr").length;
	if(nFila > 0) {
		nFila = Number($("#tsk_actividadesEmergencias_list table tbody tr").filter(':last').find('input[name=rownum]').val());
		nFila++;
	}
	
	var fila = '<tr>';
	fila += '<td><select id="id_area_operativa_e_' + nFila + '" name="id_area_operativa_e['+nFila+']"></select></td>';
	fila += '<td><select id="estado_e_'+nFila+'" name="estado_e['+nFila+']"><option value="">Seleccione una Opcion</option><option value="TERMINADO">Terminado</option><option value="EN_PROCESO">Sin terminar</option><option value="PENDIENTE">Pendiente</option></select></td>';
	fila += '<td><input type="hidden" name="emergencia[' + nFila + ']" value="' + nFila + '"/><input type="hidden" name="rownum" value="' + nFila + '"/><textarea name="descripcion[' + nFila + ']"></textarea></td>';
	$.each(recursos, function( index, value ) {
		fila += '<td><input class="result_'+nFila+' cant_r_'+nFila+'" id="'+nFila+'_' + value + '" name="recursos_e['+ value + ']['+nFila+']"/></td>';
	});
	fila += '<td><input class="result_'+nFila+' duracion" name="duracion_e[' + nFila + ']" id="duracion_e_' + nFila + '" /></td>';
	fila += '<td><input name="hh_total_e[' + nFila + ']" class="hh_em" id="hh_total_e_' + nFila + '" disabled="disabled" /></td>';
	fila += '<td><button type="button" class="eliminar" onclick="delRowEmergenciaTrat(this);" "value="Eliminar" title="Eliminar registro">Eliminar</button></td></tr>';
	
	fila = $(fila);
	fila.find('.eliminar').button({ icons: { primary: 'ui-icon ui-icon-trash' }, text: false });
	fila.find('#id_area_operativa_'+nFila).empty().append('<option value="">cargando...</option>');
	fila.find(".result_"+nFila).change(function(){
		var resultado=0;
		$(".cant_r_"+nFila).each(function(){
			if($(this).val())
				resultado+=parseInt($(this).val());
		});
		$("#hh_total_e_"+nFila).val($("#duracion_e_"+nFila).val()*resultado);
		contarHorasEmergencia();
	});
	$("#tsk_actividadesEmergencias_list table tbody").append(fila);
	contarEmergencia();
	sysfwk.loadSelect('EjecutorArea', 'getAllArea', param, 'id_area_operativa_e_'+nFila);
}


EOS;

$javascript['exec'] = <<<EOS

$('#msg_actividademergencia_list').hide();
// Botones
$('#btnAddEmergencia').button({ icons: {primary:'ui-icon-circle-plus'} });
//console.log($('#id_ejecutor').val());
$('#btnAddEmergencia').click(function(){
	addRowEmergenciaTrat();
});

EOS;


// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'list.php');
?>
