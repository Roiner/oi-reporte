<!-- <?php echo $_GET['id_a_diaria']?> -->
	<?php 
	header('Content-Type: text/html; charset=UTF-8');
	require_once('../main.php');
	require_once($GLOBALS['path_class'] . 'ActividadDiaria.class.php');
	require_once($GLOBALS['path_class'] . 'DetalleAsistencia.class.php');
	require_once($GLOBALS['path_class'] . 'EjecutorEspecialidad.class.php');
	require_once($GLOBALS['path_class'] . 'ActividadOT.class.php');
	require_once($GLOBALS['path_class'] . 'ActividadRutinaria.class.php');
	require_once($GLOBALS['path_class'] . 'ActividadEmergencia.class.php');
	require_once($GLOBALS['path_class'] . 'TrabajadorDetalle.class.php');


	$params['id_a_diaria']= $_GET['id_a_diaria'];
	$actividadDiaria= new ActividadDiaria();
	$detalleAsistencia = new DetalleAsistencia();
	$actividadesOt=  new ActividadOT();
	$actividadesRutinarias=  new ActividadRutinaria();
	$actividadesEmergencias=  new ActividadEmergencia();
	$trabajadorDetalle = new TrabajadorDetalle();

	$arregloActividadDiaria=$actividadDiaria->getActividadDiaria($params);
	$arregloDetalleAsistencia=$detalleAsistencia->getDetalleAsistencia($params);
	$arregloActividadesOt=$actividadesOt->getActividadOt($params);
	$arregloActividadesRutinarias=$actividadesRutinarias->getActividadRutinaria($params);
	$arregloActividadesEmergencias=$actividadesEmergencias->getActividadEmergencia($params);
	$arregloTrabajadorDetalle=array();

	$params['id_ejecutor']= $arregloActividadDiaria['id_ejecutor'];
	$recursos= new EjecutorEspecialidad();
	$recursos=$recursos->getRecurso($params);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<style type="text/css">
		body{
			font-size: 11px;
			font-family: Verdana, Arial, Helvetica, sans-serif;
			color: #303030;
		}

		h1, h2{
			text-align: center;
			color:#0072FF;
		}
		.tabla-encabezado{
			width: 90%;
		}
		h3 label{
			text-decoration: underline;
		}
		.titulo-tabla{
			text-align: center;
		}
		.tabla-bordes{
			width: 100%;
			border-collapse: collapse;
			
		}
		table tbody{
			text-align: center;

		}
		.tabla-bordes th, .borde-tr td, .borde-td{
			border:1px solid black;
		}

		.tabla-bordes thead{
			background-color: #0ACDDF;
		}

		.sin_borde{
			border:0px;
		}
	</style>
</head>
<body>
	<h1>GERENCIA DE SERVICIOS GENERALES</h1>
	<h2>Reporte Diario de Actividades</h2>

	<table class="tabla-encabezado">
		<thead>
			<th><h3>Fecha: <label><?php echo $arregloActividadDiaria['fecha']; ?></label></h3></th>
			<th><h3>Turno: <label><?php echo $arregloActividadDiaria['id_turno']; ?></label></h3></th>
			<th><h3>Grupo: <label><?php echo $arregloActividadDiaria['id_turno']; ?></h3></label></th>
		</thead>
	</table>

	<div>
		<table class="tabla-bordes">
			<thead>
				<tr><th colspan="5">Asistencia de Personal</th></tr>
				<tr>
					<th>Ficha</th>
					<th>Nombre y Apellido</th>
					<th>Cargo</th>
					<th>Asistencia</th>
					<th>HH Disponibles</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($arregloDetalleAsistencia as $detallePersonal) { ?>
					<tr class="borde-tr">
						<td><?php echo $detallePersonal["id_personal"]; ?></td>
						<td><?php echo $detallePersonal["nombre"]; ?></td>
						<td><?php echo $detallePersonal["desc_cargo"]; ?></td>
						<td><?php echo $detallePersonal["tipoasistencia"]; ?></td>
						<td><?php echo $detallePersonal["hh_disponibles"]; ?></td>
					</tr>
				<?php } ?>
				<tr>
					<td></td>
					<td style="text-align: right; padding-right: 10px">Personal Disponible:</td>
					<td class="borde-td"><?php echo $arregloActividadDiaria["hh_disp"]/6?></td>
					<td style="text-align: right; padding-right: 10px">Horas Disponibles:</td>
					<td class="borde-td"><?php echo $arregloActividadDiaria["hh_disp"] ?></td>
				</tr>
			</tbody>
		</table>
	</div>
	<br>
	<br>
	<br>

	<div>
		<table class="tabla-bordes">
			<thead>
				<tr><th colspan="<?php echo count($recursos)+6?>">Actividades No Rutinarias</th></tr>
				<tr>
					<th rowspan="2">OT</th>
					<th rowspan="2">Prioridad</th>
					<th rowspan="2">Codigo Equipo</th>
					<th rowspan="2">Actividad</th>
					<th colspan="<?php echo count($recursos)?>">Recursos</th>
					<th rowspan="2">Duracion</th>
					<th rowspan="2">HH Trabajadas</th>
				</tr>
				<tr class="borde-tr">
					<?php foreach ($recursos as $recurso => $value) {?>
					<td><?php echo $value['text'] ?></td>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php $rNoRutinario=0; foreach ($arregloActividadesOt as $aDiaria) { ?>
					<tr class="borde-tr">
						<td><?php echo $aDiaria["id_st_ot"]; ?></td>
						<td><?php echo $aDiaria["id_prioridad"]; ?></td>
						<td><?php echo $aDiaria["tag_activo"]; ?></td>
						<td><?php echo $aDiaria["trab_solicitado"]; ?></td>
						<?php 
							$params["id_t_actividad"]=$aDiaria["id_t_actividad"];
							$arregloTrabajadorDetalle=$trabajadorDetalle->getTrabajadorDetalle($params);
							foreach ($arregloTrabajadorDetalle as $tDetalles => $value) {?>
								<td><?php echo $value['cantidad'] ?></td>
						<?php } ?>	
						<td><?php echo $aDiaria["duracion"]; ?></td>
						<td><?php $rNoRutinario+=$aDiaria["hh_total"]; echo $aDiaria["hh_total"]; ?></td>
					</tr>
				<?php } ?>
				<tr>
					<?php foreach ($recursos as $recurso => $value) {?>
						<td></td>
					<?php } ?>
					<td></td>
					<td></td>
					<td colspan="3" style="text-align: right; padding-right: 10px">Total de HH No Rutinarias Ejecutadas:</td>
					<td class="borde-td"><?php echo $rNoRutinario ?></td>
				</tr>
			</tbody>
		</table>
	</div>
	<br>
	<br>
	<br>
	<div>
		<table class="tabla-bordes">
			<thead>
				<tr><th colspan="5">Actividades Rutinarias</th></tr>
				<tr>
					<th>Area</th>
					<th>Cantidad de Rutinas</th>
					<th>Cantidad de Personal</th>
					<th>Duracion</th>
					<th>HH Trabajadas</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($arregloActividadesRutinarias as $aRutinas) { ?>
					<tr class="borde-tr">
						<td><?php echo $aRutinas["desc_area"]; ?></td>
						<td><?php echo $aRutinas["cantidad_rutinas"]; ?></td>
						<td><?php echo $aRutinas["cantidad"]; ?></td>
						<td><?php echo $aRutinas["duracion"]; ?></td>
						<td><?php echo $aRutinas["hh_total"]; ?></td>
					</tr>
				<?php } ?>
				<tr>
					<td></td>
					<td></td>
					<td colspan="2" style="text-align: right; padding-right: 10px">Total de HH No Rutinarias Ejecutadas: </td>
					<td class="borde-td"><?php echo $arregloActividadDiaria["hh_rutinas"] ?></td>
				</tr>
			</tbody>
		</table>
	</div>
	<br>
	<br>
	<br>
	<div>
		<table class="tabla-bordes">
			<thead>
				<tr><th colspan="<?php echo count($recursos)+5?>">Actividades No Planificadas</th></tr>
				<tr>
					<th rowspan="2">Area</th>
					<th rowspan="2">Estado</th>
					<th rowspan="2">Descripcion</th>
					<th colspan="<?php echo count($recursos)?>">Recursos</th>
					<th rowspan="2">Duracion</th>
					<th rowspan="2">HH Trabajadas</th>
				</tr>
				<tr class="borde-tr">
					<?php foreach ($recursos as $recurso => $value) {?>
					<td><?php echo $value['text'] ?></td>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php $rEmergencia=0; foreach ($arregloActividadesEmergencias as $aEmergencia) { ?>
					<tr class="borde-tr">
						<td><?php echo $aEmergencia["desc_area"]; ?></td>
						<td><?php echo $aEmergencia["estado"]; ?></td>
						<td><?php echo $aEmergencia["descripcion"]; ?></td>
						<?php 
							$params["id_t_actividad"]=$aEmergencia["id_t_actividad"];
							$arregloTrabajadorDetalle=$trabajadorDetalle->getTrabajadorDetalle($params);
							foreach ($arregloTrabajadorDetalle as $tDetalles => $value) {?>
								<td><?php echo $value['cantidad'] ?></td>
						<?php } ?>	
						<td><?php echo $aEmergencia["duracion"]; ?></td>
						<td><?php $rEmergencia+=$aEmergencia["hh_total"]; echo $aEmergencia["hh_total"]; ?></td>
					</tr>
				<?php } ?>
				<tr>
					<?php foreach ($recursos as $recurso => $value) {?>
						<td></td>
					<?php } ?>
					<td></td>
					<td colspan="3" style="text-align: right; padding-right: 10px">Total de HH No Planificadas Ejecutadas:</td>
					<td class="borde-td"><?php echo $rEmergencia ?></td>
				</tr>
			</tbody>
		</table>
	</div>
	<br>
	<br>
	<br>
	<div>
		<table class="tabla-bordes">
			<thead>
				<tr>
					<th>Observaciones</th>
				</tr>
			</thead>
			<tbody>
				<tr class="borde-tr">
					<td>NOTA: <?php echo " ".$arregloActividadDiaria['observacion'] ?></td>
				</tr>
			</tbody>
		</table>
	</div>

</body>
</html>

