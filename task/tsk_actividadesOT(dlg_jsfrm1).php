<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'ActividadOT';

// Estructura del contenido
$structure['title'] = 'Nueva Orden de trabajo';
$structure['container'] = 'divDialog';
$structure['form_action'] = 'tsk_st_ot(dlg_add1).php';


// -> Estructura del panel de botones (opcional)
/*$structure['buttons'] = array(
	BTN_SAVE => BTN_SAVE,
	BTN_CANCEL => BTN_CANCEL,
	'BTN_BUSCAR' => array('id' => 'btn_BUSCAR', 'label' => 'Buscar', 'title' => 'Botón Buscar', 'icon_p' => 'search',
		'javascript' => "sysfwk.loadPage('task/tsk_st_ot(dlg_acmp).php', 'divDialog1', { container: 'divDialog1', js_func: 'ActualizarOt', ejecutor: $('#id_ejecutor').val()});"),
	BTN_COPY => BTN_COPY,
	BTN_PASTE => BTN_PASTE,
	BTN_OK => BTN_OK,
);*/

// Cargar configuración del screen
include('../screen/scr_actividadesOT(abm1).php');
$structure['form']['fs'][0]['fields'][0][1][] = array('control' => 'button', 'id' => 'btnSelOt', 'content' => '...',
	'title' => 'Seleccionar Ot');



$javascript['global'] = <<<'EOS'


// Actualizar datos OT
function ActualizarDatosOt(id_st_ot) {
	if(id_st_ot.indexOf("¬") != -1 ){
		var ot = id_st_ot.split('¬');
		console.log(ot);
		entidad="VistaDetallePlanificacion";
	}else{
		var ot = id_st_ot;
		entidad="OT";
	}
	sysfwk.ajaxRequestJson({"entity": entidad, "method": "getOtInfo", "params": {id_st_ot: ot}}, function(data) {
		console.log(data);
		if (data.id_planificacion) {
			$('#id_planificacion').val(data.id_planificacion);
		}else{
			$('#id_planificacion').val(-1);
		}
		$('#id_st_ot').val(data.id_st_ot);
		$('#tag_activo').val(data.tag_activo);
		$('#desc_actividad').val(data.desc_actividad);
		$('#id_prioridad').val(data.id_prioridad);
	});
}

//Actualizar datos Ot
function ActualizarOt(datos) {
	ActualizarDatosOt(datos.value);
}


EOS;


if (empty($_POST['recursos'])) {
	$javascript['exec'] = <<<'EOS'
	sysfwk.loadPage('task/tsk_ejecutorEspecialidad(list).php', 'divEjecutorEspecialidad', {id_ejecutor: $('#id_ejecutor').val()},sysfwk.POST);
	// Botones
	$('#btnSelOt').button({ icons: {primary:'ui-icon-search'}, text: false });
	$('#btnSelOt').click(function() {
		var arregloOt=[];
		var cadenaOt="";
		$(".contar_ot").each(function(){
			arregloOt.push($(this).val());
		});
		cadenaOt = '"'+arregloOt.join('","')+'"';
		sysfwk.loadPage('task/tsk_st_ot(dlg_acmp).php', 'divDialog1', { container: 'divDialog1', js_func: 'ActualizarOt', ejecutor: $('#id_ejecutor').val(), arregloOt : arregloOt , cadenaOt : cadenaOt , actividad:'true'},sysfwk.POST);
	});

EOS;
}else{
	$structure['form']['fs'][0]['fields'][0][7][] = array('control' => 'hidden', 'field' => 'tr', 'value' => $_POST['tr']);
	$javascript['exec'] = <<<'EOS'
	console.log('el recurso es= '+$('#recursos').val());
	sysfwk.loadPage('task/tsk_ejecutorEspecialidad(list).php', 'divEjecutorEspecialidad', {id_ejecutor: $('#id_ejecutor').val(), recursos:$('#recursos').val()},sysfwk.POST);
	// Botones
	$('#btnSelOt').hide();

EOS;
}

// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'dlg_jsfrm1.php');
?>