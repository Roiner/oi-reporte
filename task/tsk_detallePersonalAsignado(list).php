<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'Personal';

// Estructura del contenido
$structure['container'] = 'divDetallePersonalAsignado';
$structure['page_rows'] = 20; // Filas por páginas


$structure['task_del'] = 'tsk_detallePersonalAsignado_list(dlg_del1).php';
$structure['task_del_container'] = 'divDialog';

$structure['list_style'] = LST_SPL;

// -> Estructura del WHERE (opcional)

// $structure['where'][1]['fields'] = array('<campo2>');
// $structure['where'][1]['sql'] = "(t.<campo2> ilike ('%' || :<campo2> || '%'))";
// $structure['where'][1]['params'] = array('<campo2>' => '<campo2>');

// $structure['where'][2]['fields'] = array('nombre');
// $structure['where'][2]['sql'] = "(t.nombre ilike ('%' || :nombre1 || '%') OR t.desc ilike ('%' || :nombre2 || '%'))";
// $structure['where'][2]['params'] = array('nombre1' => 'nombre', 'nombre2' => 'nombre');
$modo="";
// var_dump($_POST);
if(empty($_POST['id_supervisor'])) {
	$modo="add1";
	$_POST['id_supervisor'] = 0;
	$structure['where'][0]['fields'] = array('id_supervisor');
	$structure['where'][0]['sql'] = "t.id_supervisor IS NULL";
	$structure['where'][0]['params'] = array();
} 
else {
	$modo="mod1";
	$structure['where'][0]['fields'] = array('id_supervisor');
	$structure['where'][0]['sql'] = "t.id_supervisor = :id_supervisor";
	$structure['where'][0]['params'] = array('id_supervisor' => 'id_supervisor');
}


// -> Estructura del contenido del gridview
$structure['gridview']['type'] = 'gridview';
$structure['gridview']['id'] = 'tsk_detallePersonalAsignado_list';
$structure['gridview']['caption'] = 'Personal Asignado';
$structure['gridview']['sorted'] = true;



$structure['gridview']['header'][0][0][] = array('control' => 'literal', 'content' => 'Ficha');

$structure['gridview']['header'][0][1][] = array('control' => 'literal', 'content' => 'Nombre');


$structure['gridview']['items'][0][0][] = array('control' => 'literal', 'field' => 'id_personal');
$structure['gridview']['items'][0][0][] = array('control' => 'hidden', 'field' => 'id_personal', 'id' => 'id_personal[{#}]');

$structure['gridview']['items'][0][1][] = array('control' => 'literal', 'field' => 'personal');
$structure['gridview']['items'][0][1][] = array('control' => 'hidden', 'field' => 'personal', 'id' => 'personal[{#}]');

$structure['gridview']['footer'][0][0]['td'] = array('colspan' => 2);
$structure['gridview']['footer'][0][0][] = array('control' => 'button', 'content' => 'Agregar',
'id' => 'btnAddOt', 'name' => 'btnAddOt');



$javascript['global'] = <<<'EOS'
var planificacion;
//Mostrar y ocultar boton de guardar
function mostrarBtn(){
	if($('#tsk_detallePersonalAsignado_list table tbody tr').length > 0)
		$('#btn_SAVE').show();
	else
		$('#btn_SAVE').hide();
}


// Eliminar Fila
function delRowDiagTrat(btn) {
	if(confirm('Confirme que desea eliminar la Orden de trabajo')) {
		$(btn).closest('tr').remove();
		mostrarBtn();
	}
}

// Agregar Fila
function addRowDiagTrat(data) {
	console.log(data);
	var nFila = $("#tsk_detallePersonalAsignado_list table tbody tr").length;
	var jerarquia=nFila+1;
	if(nFila > 0) {
		nFila = Number($("#tsk_detallePersonalAsignado_list table tbody tr").filter(':last').find('input[name=rownum]').val());
		nFila++;
	}
	
	var fila = '<tr>';
	fila += '<td><input type="hidden" name="rownum" value="' + nFila + '"/><input type="hidden" name="id_personal[' + nFila + ']" value="' + data.id_personal + '"/>' + data.id_personal + '</td>';
	fila += '<td><input type="hidden" name="id_supervisor2[' + nFila + ']" value="' + $('#id_sup').val() + '"/><input type="hidden" name="nombre_personal[' + nFila + ']" value="' + data.nombre_personal + '"/>' + data.nombre_personal + '</td>';
	fila += '<td><button type="button" onclick="delRowDiagTrat(this);" "value="Eliminar" title="Eliminar registro">Eliminar</button></td></tr>';
	
	fila = $(fila);
	fila.find('button').button({ icons: { primary: 'ui-icon ui-icon-trash' }, text: false });
	$("#tsk_detallePersonalAsignado_list table tbody").append(fila);
	mostrarBtn();
}
EOS;


$jsAddOtClick = '';

//echo "<h1>".$modo."</h1>";

if($modo == 'add1') {
	$jsAddOtClick = <<<'EOS'
		var data = { container: 'divDialog', js_func: 'addRowDiagTrat' };
		if($('#id_sup').val())
			sysfwk.loadPage('task/tsk_detallePersonalAsignado(dlg_jsfrm1).php', 'divDialog', data, sysfwk.POST);
		else
			alert('debe ingresar un Supervisor Primero');
EOS;
}
elseif($modo == 'mod1') {
	$jsAddOtClick = <<<'EOS'

		var data = { container: 'divDialog', id_planificacion: $('#id_planificacion').val() };
		sysfwk.loadPage('task/tsk_detallePersonalAsignado(dlg_add1).php', 'divDialog', data);
EOS;
}




$javascript['exec'] = <<<EOS
// Botones
$('#btnAddOt').button({ icons: {primary:'ui-icon-circle-plus'} });

$('#btnAddOt').click(function(){
	$jsAddOtClick;
});


EOS;

// sysfwk.loadPage('task/tsk_detallePersonalAsignado_list(dlg_jsfrm1).php', 'divDialog');
// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'list.php');
?>


