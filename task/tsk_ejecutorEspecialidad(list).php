<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'EjecutorEspecialidad';

// Estructura del contenido
$structure['container'] = 'listContainer';
$structure['paginator'] = PAGINATOR_NONE;


$structure['list_style'] = LST_NOS;


// -> Estructura del WHERE (opcional)

if (!empty($_POST['id_ejecutor'])){
    $_POST['supervisor']='Supervisor';
    $_POST['jefe']='Jefe';
	$structure['where'][0]['fields'] = array('id_ejecutor','supervisor','jefe');
	$structure['where'][0]['sql'] = "t.id_ejecutor = :id_ejecutor AND t.desc_especialidad != :supervisor AND t.desc_especialidad != :jefe";
	$structure['where'][0]['params'] = array('id_ejecutor' => 'id_ejecutor', 'supervisor' => 'supervisor', 'jefe' => 'jefe');
}
/*$structure['where'][0]['fields'] = array('nombre');
$structure['where'][0]['sql'] = "t.nombre ilike ('%' || :nombre || '%')";
$structure['where'][0]['params'] = array('nombre' => 'nombre');*/

// $structure['where'][1]['fields'] = array('<campo2>');
// $structure['where'][1]['sql'] = "(t.<campo2> ilike ('%' || :<campo2> || '%'))";
// $structure['where'][1]['params'] = array('<campo2>' => '<campo2>');

// $structure['where'][2]['fields'] = array('nombre');
// $structure['where'][2]['sql'] = "(t.nombre ilike ('%' || :nombre1 || '%') OR t.desc ilike ('%' || :nombre2 || '%'))";
// $structure['where'][2]['params'] = array('nombre1' => 'nombre', 'nombre2' => 'nombre');

// -> Estructura del contenido del gridview
$structure['gridview']['type'] = 'gridview';
$structure['gridview']['id'] = 'tsk_ejecutorEspecialidad_list';
$structure['gridview']['caption'] = 'Lista de Recurso';
$structure['gridview']['sorted'] = true;

$structure['gridview']['header'][0][0]['th'] = array('sort_field' => 'desc_especialidad');
$structure['gridview']['header'][0][0][] = array('control' => 'literal', 'content' => 'Especialidad');

$structure['gridview']['header'][0][1][] = array('control' => 'literal', 'content' => 'Cantidad');

$structure['gridview']['items'][0][0][] = array('control' => 'literal', 'field' => 'desc_especialidad');

$structure['gridview']['items'][0][1][] = array('control' => 'textbox', 'field' => 'cantidad', 'id' => 'cantidad_{#}', 'class' => 'cantidad', 'name' => 'cantidad[{#}]');

$structure['gridview']['items'][0][1][] = array('control' => 'hidden', 'field' => 'id_especialidad', 'id' => 'id_especialidad_{#}', 'class' => 'cantidad_{#}', 'name' => 'id_especialidad[{#}]');

$structure['gridview']['footer'][0][0]['td'] = array('colspan' => 1);
$structure['gridview']['footer'][0][0][] = array('control' => 'literal', 'content' => '&nbsp;');

$javascript['exec'] = <<<'EOS'


$(".cantidad").change(function(){
    var recurso_t=[];
    var recurso=[];
	var resultado=0;
    $(".cantidad").each(function(){
        recurso.push($(this).val());
        var indice=$(this).attr("id");
        var rec={"id_especialidad": $("."+indice).val(),
                 "cantidad": $(this).val()};
        recurso_t.push(rec);
    	if($(this).val())
    		resultado+=parseInt($(this).val());
    });
    var JsonRecurso = JSON.stringify(recurso);
    var JsonRecursoT= JSON.stringify(recurso_t);
    console.log(JsonRecursoT);
    $('#cantidad').val(resultado);
    $('#recursos').val(JsonRecurso);
    $('#recursos_t').val(JsonRecursoT);
    
});
EOS;

if (!empty($_POST['recursos'])) {
    $javascript['exec'] .= <<<'EOS'
    var recursos = JSON.parse($('#recursos').val());
    console.log(' los recursos que llegaron fueron= '+recursos);
    result=0;
    $.each(recursos, function( index, value ) {
        console.log (index);
        $('#cantidad_'+index).val(value);
        result+=parseInt(value);
    });
    $('#cantidad').val(result);
EOS;
}

// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'list.php');
?>