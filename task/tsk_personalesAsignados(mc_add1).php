<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'Personal';

// Estructura del contenido
$structure['location'] = 'SISGREL &raquo; Asistencias &raquo; Personal Asignado a un Supervisor';
$structure['title'] = 'Agregar Planificación';
$structure['form_action'] = 'tsk_personalesAsignados(mc_add1).php';
$structure['task_mode'] = 'add1';

// Cargar configuración del screen
include('../screen/scr_personalesAsignados(abm1).php');


// -> Estructura del panel de botones (opcional)
 $structure['buttons'] = array(
	 BTN_SAVE => BTN_SAVE,
	 BTN_CANCEL => BTN_CANCEL,
	 BTN_OK => BTN_OK
 );


function beforeSubmit(){
	echo "var data2 = sysfwk.getControlData('tsk_detallePersonalAsignado_list');";
	echo "$.extend(data, data2);";
	echo "console.log(data);";
}

// Javascript

$javascript['global'] = <<<'EOS'
// Actualizar personal
function ActualizarPersonal(datos) {
	$('#id_sup').val(datos.value);
}
EOS;

$javascript['exec'] .= <<<'EOS'
$('#btn_OT').hide();
$('#btn_SAVE').hide();
sysfwk.loadPage('task/tsk_detallePersonalAsignado(list).php', 'divDetallePersonalAsignado');

$('#btn_CREAR').click(function() {
	$('#btn_OT').show();
});


// Botones
$('#btnSelPersonalABM').button({ icons: {primary:'ui-icon-search'}, text: false });
$('#btnSelPersonalABM').click(function() {
	sysfwk.loadPage('task/tsk_personal_sel(dlg_acmp).php', 'divDialog1', { container: 'divDialog1', js_func: 'ActualizarPersonal'});
});
EOS;



// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'mc_add1.php');
echo "<div id='divDetallePersonalAsignado'></div>";
?>
