<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'AsistenciaTipo';

// Estructura del contenido
$structure['title'] = 'Agregar Tipo';
$structure['container'] = 'divDialog';
$structure['form_action'] = 'tsk_asistenciaTipos(dlg_add1).php';

// -> Estructura del panel de botones (opcional)
// $structure['buttons'] = array(
	// BTN_SAVE => BTN_SAVE,
	// BTN_CANCEL => BTN_CANCEL,
	// 'MI_BOTON' => array('id' => 'btn_'.MI_BOTON, 'label' => 'Mi Boton', 'title' => 'Botón personalizado', 'icon_p' => 'comment',
		// 'javascript' => "alert($(event.target).button('disable'));"),
	// BTN_COPY => BTN_COPY,
	// BTN_PASTE => BTN_PASTE
// );

// Cargar configuración del screen
include('../screen/scr_asistenciaTipos(abm1).php');

// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'dlg_add1.php');
?>