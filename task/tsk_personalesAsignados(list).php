<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'Personal';

// Estructura del contenido
$structure['container'] = 'listContainer';
$structure['page_rows'] = 10; // Filas por páginas

$structure['task_mod'] = 'tsk_personalesAsignados(mc_mod1).php';
$structure['task_mod_container'] = 'mainContent';

$structure['list_style'] = LST_SPL;

// -> Estructura del WHERE (opcional)
$structure['where'][0]['fields'] = array('id_supervisor');
$structure['where'][0]['sql'] = "distinct id_supervisor = :id_supervisor";
$structure['where'][0]['params'] = array('id_supervisor' => 'id_supervisor');
 // var_dump($structure['where'][0]);

// $structure['where'][1]['fields'] = array('<campo2>');
// $structure['where'][1]['sql'] = "(t.<campo2> ilike ('%' || :<campo2> || '%'))";
// $structure['where'][1]['params'] = array('<campo2>' => '<campo2>');

// $structure['where'][2]['fields'] = array('nombre');
// $structure['where'][2]['sql'] = "(t.nombre ilike ('%' || :nombre1 || '%') OR t.desc ilike ('%' || :nombre2 || '%'))";
// $structure['where'][2]['params'] = array('nombre1' => 'nombre', 'nombre2' => 'nombre');

// -> Estructura del contenido del gridview
$structure['gridview']['type'] = 'gridview';
$structure['gridview']['id'] = 'tsk_planificaciones_list';
$structure['gridview']['caption'] = 'Lista de Planificaciones';
$structure['gridview']['sorted'] = true;


$structure['gridview']['header'][0][0][] = array('control' => 'literal', 'content' => 'Ficha');
$structure['gridview']['header'][0][1][] = array('control' => 'literal', 'content' => 'Nombre');


$structure['gridview']['items'][0][0][] = array('control' => 'literal', 'field' => 'id_supervisor');
$structure['gridview']['items'][0][1][] = array('control' => 'literal', 'field' => 'supervisor');
$structure['gridview']['items'][0][0][] = array('control' => 'hidden', 'field' => 'id_supervisor', 'id' => 'id_supervisor[{#}]');


// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'list.php');
?>