<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';

// Definir la entidad

$structure['container'] = 'divDialog1';

if (!empty($_POST['actividad'])) {
	$structure['entity'] = 'VistaDetallePlanificacion';
	$structure['method'] = 'getOtPlanificacion';
}else{
	$structure['entity'] = 'OT';
	$structure['method'] = 'getOt';
}

// Estructura del contenido
$structure['title'] = 'Seleccionar Orden de trabajo';
$structure['search']['id'] = 'search_' . strtolower($structure['entity']) . '_acmp';
$structure['search']['name'] = 'search';
$structure['result']['id'] = 'result_' . strtolower($structure['entity']) . '_acmp';
$structure['result']['name'] = $structure['result']['id'];


/*$prueba = "<script> document.write(ejecutor) </script>";
echo $prueba;*/

// Mensaje por defecto (opcional)
// $messages['title'] = 'Información:';
// $messages['info'] = 'Ingrese la ficha, cédula o nombre del personal';
// $messages['extra'] = 'En la lista de resultado apareceran las coincidencias ya sea por ficha, cédula o nombre del personal';
// $messages['state'] = SC_OK;

// var_dump($_POST);
function beforeSearch() {
	echo "param['ejecutor'] = '{$_POST['ejecutor']}';";
	echo "param['arregloOt'] = '{$_POST['arregloOt']}';";
	echo "param['cadenaOt'] = '{$_POST['cadenaOt']}';";
}

// echo "<input type='text' name='search_ejecutor'>";

// Opciones del dialogo
$structure['dlg_options'] = array(
	'width' => 900
);


$javascript['exec'] .= <<<EOS
// Evento para buscar
$('#{$structure['search']['id']}').keyup(function(e) {
	if(acmp_timeout == null) {
		acmp_timeout = setTimeout(acmp_search, 300);
	}
});


// Busqueda inicial
if($('#{$structure['search']['id']}').val().length >= 0)
	acmp_search();

// Permitir seleccionar con doubleclick
$('#{$structure['result']['id']}').dblclick(function() {
	acmp_result();
	$('#{$structure['container']}').dialog('close');
});

// Seleccionar el campo de busqueda
$('#{$structure['search']['id']}').focus();
EOS;

// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'dlg_acmp.php');
?>

