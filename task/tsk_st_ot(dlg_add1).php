<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'DetallePlanificacion';

// Estructura del contenido
$structure['title'] = 'Lista de Ordenes de trabajo';
$structure['container'] = 'divDialog1';
$structure['form_action'] = 'tsk_st_ot(dlg_add1).php';

// -> Estructura del panel de botones (opcional)
// $structure['buttons'] = array(
	// BTN_SAVE => BTN_SAVE,
	// BTN_CANCEL => BTN_CANCEL,
	// 'MI_BOTON' => array('id' => 'btn_'.MI_BOTON, 'label' => 'Mi Boton', 'title' => 'Botón personalizado', 'icon_p' => 'comment',
		// 'javascript' => "alert($(event.target).button('disable'));"),
	// BTN_COPY => BTN_COPY,
	// BTN_PASTE => BTN_PASTE
// );

// Cargar configuración del screen
include('../screen/scr_st_ot(abm1).php');

$javascript['exec'] = <<<'EOS'

	// Actualizar lista de ejecutores
	$('#id_area_operativa').change(function(){
	var param = { extra: { value: '', text: '(Todos)' } };
	param['id_area_operativa'] = $('#id_area_operativa').val();
	$('#con_id_role').empty().append('<option value="">cargando...</option>');
	sysfwk.loadSelect('EjecutorArea', 'getEjecutor', param, 'id_ejecutor');
	});

 	// Cargar lista de areas
 	var param = { extra: { value: '', text: '(Todos)' } };
 	$('#id_area_operativa').empty().append('<option value="">cargando...</option>');
 	sysfwk.loadSelect('EjecutorArea', 'getArea', param, 'id_area_operativa');
EOS;

// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'dlg_add1.php');
?>