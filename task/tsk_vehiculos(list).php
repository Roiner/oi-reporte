<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'Vehiculo';

// Estructura del contenido
$structure['container'] = 'listContainer';
$structure['page_rows'] = 10; // Filas por páginas

$structure['task_mod'] = 'tsk_vehiculos(dlg_mod1).php';
$structure['task_mod_container'] = 'divDialog';
$structure['task_del'] = 'tsk_vehiculos(dlg_del1).php';
$structure['task_del_container'] = 'divDialog';

$structure['list_style'] = LST_SPL;

// -> Estructura del WHERE (opcional)
$structure['where'][0]['fields'] = array('nombre');
$structure['where'][0]['sql'] = "t.nombre ilike ('%' || :nombre || '%')";
$structure['where'][0]['params'] = array('nombre' => 'nombre');

// $structure['where'][1]['fields'] = array('<campo2>');
// $structure['where'][1]['sql'] = "(t.<campo2> ilike ('%' || :<campo2> || '%'))";
// $structure['where'][1]['params'] = array('<campo2>' => '<campo2>');

// $structure['where'][2]['fields'] = array('nombre');
// $structure['where'][2]['sql'] = "(t.nombre ilike ('%' || :nombre1 || '%') OR t.desc ilike ('%' || :nombre2 || '%'))";
// $structure['where'][2]['params'] = array('nombre1' => 'nombre', 'nombre2' => 'nombre');

// -> Estructura del contenido del gridview
$structure['gridview']['type'] = 'gridview';
$structure['gridview']['id'] = 'tsk_vehiculo_list';
$structure['gridview']['caption'] = 'Lista de Vehiculos';
$structure['gridview']['sorted'] = true;

$structure['gridview']['header'][0][0]['th'] = array('sort_field' => 'nombre');
$structure['gridview']['header'][0][0][] = array('control' => 'literal', 'content' => 'Nombre del Vehiculo');

$structure['gridview']['footer'][0][0]['td'] = array('colspan' => 1);
$structure['gridview']['footer'][0][0][] = array('control' => 'literal', 'content' => '&nbsp;');

$structure['gridview']['items'][0][0][] = array('control' => 'literal', 'field' => 'nombre');
$structure['gridview']['items'][0][0][] = array('control' => 'hidden', 'field' => 'id_vehiculo', 'id' => 'id_vehiculo[{#}]');


// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'list.php');
?>