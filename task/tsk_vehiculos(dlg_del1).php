<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'Vehiculo';

// Estructura del contenido
$structure['title'] = 'Eliminar Vehiculo';
$structure['container'] = 'divDialog';
$structure['form_action'] = 'tsk_vehiculos(dlg_del1).php';

// Cargar configuración del screen
include('../screen/scr_vehiculos(abm1).php');
include('../screen/scr_ra(bm1).php');

// -> Estructura del panel de botones (opcional)
// $structure['buttons'] = array(
	// BTN_SAVE => BTN_SAVE,
	// BTN_CANCEL => BTN_CANCEL,
	// 'MI_BOTON' => array('id' => 'btn_'.MI_BOTON, 'label' => 'Mi Boton', 'title' => 'Botón personalizado', 'icon_p' => 'comment',
		// 'javascript' => "alert('Mi boton propio')"),
	// BTN_COPY => BTN_COPY,
	// BTN_PASTE => BTN_PASTE
// );

// Javascript
// $javascript['exec'] = <<<'EOS'
	// $('#siglas').datepicker();
// EOS;

// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'dlg_del1.php');
?>