<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'ActividadDiaria';

// Estructura del contenido
$structure['container'] = 'listContainer';
$structure['page_rows'] = 10; // Filas por páginas

$structure['list_style'] = LST_SPL;

// -> Estructura del WHERE (opcional)
$structure['where'][0]['fields'] = array('id_area_operativa');
$structure['where'][0]['sql'] = "id_area_operativa = :id_area_operativa";
$structure['where'][0]['params'] = array('id_area_operativa' => 'id_area_operativa');


$structure['where'][1]['fields'] = array('id_ejecutor');
$structure['where'][1]['sql'] = "t.id_ejecutor = :id_ejecutor";
$structure['where'][1]['params'] = array('id_ejecutor' => 'id_ejecutor');

$structure['where'][2]['fields'] = array('fecha_inicio','fecha_fin');
$structure['where'][2]['sql'] = "t.fecha >= :fecha_inicio AND t.fecha <= :fecha_fin";
$structure['where'][2]['params'] = array('fecha_inicio' => 'fecha_inicio', 'fecha_fin' => 'fecha_fin');

// $structure['where'][1]['fields'] = array('<campo2>');
// $structure['where'][1]['sql'] = "(t.<campo2> ilike ('%' || :<campo2> || '%'))";
// $structure['where'][1]['params'] = array('<campo2>' => '<campo2>');

// $structure['where'][2]['fields'] = array('nombre');
// $structure['where'][2]['sql'] = "(t.nombre ilike ('%' || :nombre1 || '%') OR t.desc ilike ('%' || :nombre2 || '%'))";
// $structure['where'][2]['params'] = array('nombre1' => 'nombre', 'nombre2' => 'nombre');

// -> Estructura del contenido del gridview
$structure['gridview']['type'] = 'gridview';
$structure['gridview']['id'] = 'tsk_actividadesDiarias_list';
$structure['gridview']['caption'] = 'Lista de Planificaciones';
$structure['gridview']['sorted'] = true;


$structure['gridview']['header'][0][0]['th'] = array('sort_field' => 'fecha');
$structure['gridview']['header'][0][0][] = array('control' => 'literal', 'content' => 'Fecha');

$structure['gridview']['header'][0][1]['th'] = array('sort_field' => 'ejecutor');
$structure['gridview']['header'][0][1][] = array('control' => 'literal', 'content' => 'Ejecutor');

$structure['gridview']['header'][0][2]['th'] = array('sort_field' => 'area');
$structure['gridview']['header'][0][2][] = array('control' => 'literal', 'content' => 'Superintendencia');

$structure['gridview']['header'][0][3]['th'] = array('sort_field' => 'id_turno');
$structure['gridview']['header'][0][3][] = array('control' => 'literal', 'content' => 'Turno');

$structure['gridview']['header'][0][4][] = array('control' => 'literal', 'content' => '&nbsp;');

$structure['gridview']['footer'][0][0]['td'] = array('colspan' => 5);
$structure['gridview']['footer'][0][0][] = array('control' => 'literal', 'content' => '&nbsp;');
$structure['gridview']['footer'][0][0][] = array('control' => 'hidden', 'value' => $GLOBALS['url_task']."tsk_reporte.php", 'id' => 'reporteDiario');

$structure['gridview']['items'][0][0][] = array('control' => 'literal', 'field' => 'fecha');
$structure['gridview']['items'][0][1][] = array('control' => 'literal', 'field' => 'ejecutor');
$structure['gridview']['items'][0][2][] = array('control' => 'literal', 'field' => 'area');
$structure['gridview']['items'][0][3][] = array('control' => 'literal', 'field' => 'id_turno');
$structure['gridview']['items'][0][4][] = array('control' => 'button', 'content' => 'Ver Reporte', 'id' => 'btnEditar_{#}', 'value' => '{#}');
$structure['gridview']['items'][0][0][] = array('control' => 'hidden', 'field' => 'id_a_diaria', 'id' => 'id_a_diaria_{#}');


$javascript['exec'] = <<<'EOS'

$('#tsk_actividadesDiarias_list button[id^=btnEditar]').each(function(){
	$(this).button({ icons: {primary:'ui-icon-document'}, text: false });
	var id_a_diaria="";
	$(this).click(function(){
		var boton= $(this).val();
		$("#tsk_actividadesDiarias_list table tbody tr").each(function (index) 
		{
			if(index==boton){
				$(this).children("td").each(function (index2) 
				{
					switch (index2) 
					{
						case 0: {
							id_a_diaria= $(this).children("input").val();
							break;
						}
					}
				})	
			}
			
		});

		// console.log($(this).val());
		window.open($('#reporteDiario').val()+'?id_a_diaria='+id_a_diaria, "nuevo" );
	});

});

EOS;
// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'list.php');
?>