<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'Personal';

// Estructura del contenido
$structure['location'] = 'SISGREL &raquo; Asistencias &raquo; Asignar Personal';
$structure['title'] = 'Modificar Planificacion';
$structure['form_action'] = 'tsk_tsk_personalesAsignados(mc_mod1).php';
$structure['task_mode'] = 'mod1';

// Cargar configuración del screen
include('../screen/scr_personalesAsignados(abm1).php');

$structure['buttons'] = array(
	 BTN_SAVE => BTN_SAVE,
	 BTN_CANCEL => BTN_CANCEL,
	 BTN_OK => BTN_OK
 );

// Javascript
$javascript['exec'] = <<<'EOS'
	$('#id_sup').attr('disabled','disabled');
	sysfwk.loadPage('task/tsk_detallePersonalAsignado(list).php', 'divDetallePlanificacion',{id_supervisor: $('#id_sup').val() },sysfwk.POST);

	// Botones
	$('#btnSelPersonalABM').button({ icons: {primary:'ui-icon-search'}, text: false });
	$('#btnSelPersonalABM').click(function() {
		sysfwk.loadPage('task/tsk_personal_sel(dlg_acmp).php', 'divDialog1', { container: 'divDialog1', js_func: 'ActualizarPersonal'});
	});
EOS;

// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'mc_mod1.php');
echo "<div id='divDetallePlanificacion'></div>";
?>