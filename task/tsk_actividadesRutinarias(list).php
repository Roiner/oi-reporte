<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'ActividadRutinaria';

// Estructura del contenido
$structure['container'] = 'divActividadesRutinarias';
$structure['page_rows'] = 10; // Filas por páginas

// $structure['task_mod'] = 'tsk_materiales(dlg_mod1).php';
// $structure['task_mod_container'] = 'divDialog';
// $structure['task_del'] = 'tsk_materiales(dlg_del1).php';
// $structure['task_del_container'] = 'divDialog';

$structure['list_style'] = LST_SPL;


$_POST['id_rutina'] = 0;
$structure['where'][0]['fields'] = array('id_rutina');
$structure['where'][0]['sql'] = "t.id_rutina IS NULL";
$structure['where'][0]['params'] = array();
// -> Estructura del WHERE (opcional)
// $structure['where'][0]['fields'] = array('nombre');
// $structure['where'][0]['sql'] = "t.nombre ilike ('%' || :nombre || '%')";
// $structure['where'][0]['params'] = array('nombre' => 'nombre');

// $structure['where'][1]['fields'] = array('<campo2>');
// $structure['where'][1]['sql'] = "(t.<campo2> ilike ('%' || :<campo2> || '%'))";
// $structure['where'][1]['params'] = array('<campo2>' => '<campo2>');

// $structure['where'][2]['fields'] = array('nombre');
// $structure['where'][2]['sql'] = "(t.nombre ilike ('%' || :nombre1 || '%') OR t.desc ilike ('%' || :nombre2 || '%'))";
// $structure['where'][2]['params'] = array('nombre1' => 'nombre', 'nombre2' => 'nombre');

error_reporting(E_ERROR | E_PARSE);

// -> Estructura del contenido del gridview
$structure['gridview']['type'] = 'gridview';
$structure['gridview']['id'] = 'tsk_actividadesRutinarias_list';
$structure['gridview']['caption'] = 'Lista de Actividades Rutinarias';
$structure['gridview']['sorted'] = true;



$structure['gridview']['header'][0][0][] = array('control' => 'literal', 'content' => 'Area Operativa');

$structure['gridview']['header'][0][1][] = array('control' => 'literal', 'content' => 'Cantidad de Rutinas');

$structure['gridview']['header'][0][2][] = array('control' => 'literal', 'content' => 'Cantidad de Personal');

$structure['gridview']['header'][0][3][] = array('control' => 'literal', 'content' => 'Duracion');

$structure['gridview']['header'][0][4][] = array('control' => 'literal', 'content' => 'HH Trabajadas');

$structure['gridview']['items'][0][0][] = array('control' => 'literal', 'field' => 'id_a_rutina');
$structure['gridview']['items'][0][0][] = array('control' => 'hidden', 'field' => 'id_a_rutina', 'id' => 'id_a_rutina[{#}]');

$structure['gridview']['footer'][0][0]['td'] = array('colspan' => 3);
$structure['gridview']['footer'][0][0][] = array('control' => 'button', 'content' => 'Agregar',
'id' => 'btnAddRutina', 'name' => 'btnAddRutina');

$structure['gridview']['footer'][0][1][] = array('control' => 'literal', 'content' => 'Horas Hombre Totales');
$structure['gridview']['footer'][0][2][] = array('control' => 'textbox', 'field' => 'hh_rutinas', 'disabled' => 'y', 'class' => 'hh');
$structure['gridview']['footer'][0][2][] = array('control' => 'hidden', 'field' => 'cantidad_rutinas','value' => 0 , 'disabled' => 'y');



$javascript['global'] = <<<'EOS'

function contarHorasRutinas()
{
	var resultado=0;
	$(".hh_r").each(function(){
        resultado+=parseInt($(this).val());
    });
    $("#hh_rutinas").val(resultado);
    contarHhEj();
}

function contarRutinas()
{
	var resultado=0;
	$(".cant_r").each(function(){
        resultado+=parseInt($(this).val());
    });
    $("#cantidad_rutinas").val(resultado);
}

// Cargar lista de areas
var param = { extra: { value: '', text: '(Todos)' } };


// Eliminar Fila
function delRowRutinaTrat(btn) {
	if(confirm('Confirme que desea eliminar la Orden de trabajo')) {
		$(btn).closest('tr').remove();
		contarHorasRutinas();
		contarRutinas();
	}
}

// Editar Fila
function modRowRutinaTrat(btn) {
	alert('en construccion');
}

// Agregar Fila
function addRowRutinaTrat() {
	
	var nFila = $("#tsk_actividadesRutinarias_list table tbody tr").length;
	if(nFila > 0) {
		nFila = Number($("#tsk_actividadesRutinarias_list table tbody tr").filter(':last').find('input[name=rownum]').val());
		nFila++;
	}
	
	var fila = '<tr>';
	fila += '<td><select id="id_area_operativa_' + nFila + '" name="id_area_operativa_r[' + nFila + ']"></select></td>';
	fila += '<td><input type="hidden" name="rutina[' + nFila + ']" value="' + nFila + '"/><input type="hidden" name="rownum" value="' + nFila + '"/><input name="cant_rutinas[' + nFila + ']" class="cant_r"/></td>';
	fila += '<td><input name="cant_personal[' + nFila + ']" id="cant_personal_' + nFila + '" class="'+nFila+'" /></td>';
	fila += '<td><input name="duracion_r[' + nFila + ']" id="duracion_r_' + nFila + '" class="'+nFila+'" /></td>';
	fila += '<td><input name="hh_total_r[' + nFila + ']" class="hh_r" id="hh_total_r_' + nFila + '" disabled="disabled" /></td>';
	fila += '<td><button type="button" class="eliminar" onclick="delRowRutinaTrat(this);" "value="Eliminar" title="Eliminar registro">Eliminar</button></td></tr>';
	
	fila = $(fila);
	fila.find('.eliminar').button({ icons: { primary: 'ui-icon ui-icon-trash' }, text: false });
	fila.find('#id_area_operativa_'+nFila).empty().append('<option value="">cargando...</option>');
	fila.find("input").change(function(){
		n=$(this).attr("class");
		resultado=$("#cant_personal_"+n).val()*$("#duracion_r_"+n).val();
		$("#hh_total_r_"+n).val(resultado);
		contarHorasRutinas();
	});
	fila.find('.cant_r').change(function(){
		contarRutinas();
	});
	$("#tsk_actividadesRutinarias_list table tbody").append(fila);
	sysfwk.loadSelect('EjecutorArea', 'getAllArea', param, 'id_area_operativa_'+nFila);
}


EOS;

$javascript['exec'] = <<<EOS

$('#msg_actividadrutinaria_list').hide();
// Botones
$('#btnAddRutina').button({ icons: {primary:'ui-icon-circle-plus'} });
//console.log($('#id_ejecutor').val());
$('#btnAddRutina').click(function(){
	addRowRutinaTrat();
});

EOS;


// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'list.php');
?>
