<!-- 
significado de cada elemento del arreglo de indicadores
desc_ejecutor = nombre de la unidad
ot_ej_tb_sc = Cantidad de Ot ejecutada por trabajo social 
hh_ej_tb_sc = Horas Hombre ejecutadas por trabajo social
ot_p_tb_sc = Cantidad de Ot planificadas por trabajo social 
hh_p_tb_sc = Horas Hombre ejecutadas por trabajo social
cump_tb_sc = Porcentaje de Cumplimiento social
ot_ej = Cantidad de Ot ejecutadas
hh_ej = Horas Hombre ejecutadas
ot_p = Cantidad de Horas Hombre Programadas
hh_p = Horas Hombre ejecutadas
ot_pend = Ot pendientes
cump_ot = Cumplimiento de ordenes de trabajo
act_ad = Cantidad de Ot de prioridad 1 y de emergencia
hh_act_ad = Horas Hombre de Ot de prioridad 1 y de emergencia
ot_ej_y_ot_ad= Suma de las ordenes de trabajo ejecutadas y las ordenes de trabajo adicionales
rut_ej = Cantidad de Ritinas Ejecutadas
hh_rut_ej = Horas Hombre de rutinas ejecutadas
sum_ot_soc_ej = Suma de Horas Hombre de trabajo social y ejecutadas
sum_p1_p_ej = Suma de Horas Hombre de Ot de Prioridad 1 y de Ot programadas y ejecutadas
hh_t_real = Horas hombre reales traidas de sismanio
hh_t = Suma de todas las horas hombre
reactividad = porcentaje de reactividad

-->

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<style type="text/css">
		h1, h2{
			text-align: center;
			color:#0072FF;
		}
		.tabla-encabezado{
			width: 90%;
		}
		h3 label{
			text-decoration: underline;
		}
		.titulo-tabla{
			text-align: center;
		}
		.tabla-bordes{
			width: 100%;
			border-collapse: collapse;
			
		}
		table tbody{
			text-align: center;

		}
		.tabla-bordes th, .borde-tr td, .borde-td{
			border:1px solid black;
		}

		.tabla-bordes thead{
			background-color: #0ACDDF;
		}

		.sin_borde{
			border:0px;
		}

		table thead{
			font-size: xx-small;
		}
	</style>
</head>
<body>
	
<?php
	header('Content-Type: text/html; charset=UTF-8');

	require_once('../main.php');
	//	 echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
	include($GLOBALS['path_fwk_script'] . 'check_session.php');
	require_once($GLOBALS['path_class'] . 'ActividadDiaria.class.php');
	require_once($GLOBALS['path_class'] . 'DetalleAsistencia.class.php');
	require_once($GLOBALS['path_class'] . 'EjecutorArea.class.php');
	require_once($GLOBALS['path_class'] . 'EjecutorEspecialidad.class.php');
	require_once($GLOBALS['path_class'] . 'ActividadOT.class.php');
	require_once($GLOBALS['path_class'] . 'ActividadRutinaria.class.php');
	require_once($GLOBALS['path_class'] . 'ActividadEmergencia.class.php');
	require_once($GLOBALS['path_class'] . 'TrabajadorDetalle.class.php');
	require_once($GLOBALS['path_class'] . 'DetallePlanificacion.class.php');
	require_once($GLOBALS['path_class'] . 'RecursoRutina.class.php');
	require_once($GLOBALS['path_class'] . 'RecursoOt.class.php');


	if (!empty($_POST['fecha_inicio']) && !empty($_POST['fecha_fin'])) {
		$indicadores=array();
		$params['fecha_inicio']=$_POST['fecha_inicio'];
		$params['fecha_fin']=$_POST['fecha_fin'];
		$params['id_area_operativa']=$_POST['id_area_operativa'];
		$actividadDiaria= new ActividadDiaria();
		$actividadOt= new ActividadOt();
		$ejecutorArea= new EjecutorArea();
		$actividadEmergencia= new ActividadEmergencia();
		$actividadRutinaria= new ActividadRutinaria();
		$detallePlanificacion = new DetallePlanificacion();
		$recursoRutina=new RecursoRutina();
		$recursoOt= new RecursoOt;
		$arregloUnidad=$ejecutorArea->getEjecutor($params);
		$fechas = array();
		$i=0;

		$diaInicio="Monday";
		$diaFin="Sunday";
		$fecha = change_date_format($_POST['fecha_inicio'], F_DMY, F_YMD);
		$fecha = new DateTime($fecha);
		$fecha=$fecha->format("Y-m-d");
		$fecha_fin = change_date_format($_POST['fecha_fin'], F_DMY, F_YMD);
		$fecha_fin = new DateTime($fecha_fin);
		$fecha_fin=$fecha_fin->format("Y-m-d");

		// while ($fecha <= $fecha_fin) {
			
		// 	// $fecha=date("Y-m-d");

		// 	$strFecha = strtotime($fecha);
		// 	// echo $strFecha;
		// 	$fechaInicio = date('Y-m-d',strtotime('next '.$diaInicio,$strFecha));
		// 	// $fechaFin = date('Y-m-d',strtotime('next '.$diaFin,$strFecha));


		// 	if(date("l",$strFecha)==$diaInicio){
		// 		$fechaInicio= date("Y-m-d",$strFecha);
		// 	}

		// 	$strFecha = strtotime($fechaInicio);
		// 	$fechaFin = date('Y-m-d',strtotime('next '.$diaFin,$strFecha));

		// 	if(date("l",$strFecha)==$diaFin){
		// 		$fechaFin= date("Y-m-d",$strFecha);
		// 	}
		// 	$fechas[]=Array("fechaInicio"=>$fechaInicio,"fechaFin"=>$fechaFin);
		// 	$i++;
		// 	$fecha=$fechaFin;
		// }
		
		// var_dump($fechas);
		// echo "<br><br>";
		
		// foreach ($fechas as $key => $valor) {
			// $params['fecha_inicio']=$valor['fechaInicio'];
			// $params['fecha_fin']=$valor['fechaFin'];
			foreach ($arregloUnidad as $key => $value) {
			// var_dump($value);
				$params['id_ejecutor']=$value['value'];
				$indicadores[$value['value']]['desc_ejecutor']=$value['text'];
				$trabajoSocial= $actividadOt->getTotalHorasTrabajoSocialOt($params);
				$indicadores[$value['value']]['ot_ej_tb_sc']=$trabajoSocial[0]['count'];
				$indicadores[$value['value']]['hh_ej_tb_sc']=$trabajoSocial[0]['sum'];
				$trabajoSocialPlanificado= $detallePlanificacion->getTotalHorasTrabajoSocialOt($params);
				$indicadores[$value['value']]['ot_p_tb_sc']=$trabajoSocialPlanificado[0]['count'];
				$indicadores[$value['value']]['hh_p_tb_sc']=$trabajoSocialPlanificado[0]['sum'];
				if ($trabajoSocialPlanificado[0]['count']>0) {
					$indicadores[$value['value']]['cump_tb_sc']=($trabajoSocial[0]['count']/$trabajoSocialPlanificado[0]['count'])*100;
				}else{
					$indicadores[$value['value']]['cump_tb_sc']=0;
				}
				$ot_ej=$actividadOt->getTotalHoraslOt($params);
				$indicadores[$value['value']]['ot_ej']=$ot_ej[0]['count'];
				$indicadores[$value['value']]['hh_ej']=$ot_ej[0]['sum'];
				$ot_p=$detallePlanificacion->getTotalHorasOt($params);
				$indicadores[$value['value']]['ot_p']=$ot_p[0]['count'];
				$indicadores[$value['value']]['hh_p']=$ot_p[0]['sum'];
				$indicadores[$value['value']]['ot_pend']=($ot_p[0]['count']-$ot_ej[0]['count']);

				if ($ot_p[0]['count']>0) {
					$indicadores[$value['value']]['cump_ot']=($ot_ej[0]['count']/$ot_p[0]['count'])*100;
				}else{
					$indicadores[$value['value']]['cump_ot']=0;
				}

				$ot_p1=$actividadOt->getTotalHoraslOtP1($params);
				$act_eme=$actividadEmergencia->getTotalHorasEmg($params);
				$indicadores[$value['value']]['act_ad']=$ot_p1[0]['count']+$act_eme[0]['count'];
				$indicadores[$value['value']]['hh_act_ad']=$ot_p1[0]['sum']+$act_eme[0]['sum'];
				$indicadores[$value['value']]['ot_ej_y_ot_ad']=$ot_ej[0]['count']+$ot_p1[0]['count']+$act_eme[0]['count'];

				$rutinas=$actividadRutinaria->getTotalHorasRutinas($params);
				$indicadores[$value['value']]['rut_ej']=$rutinas[0]['count'];
				$indicadores[$value['value']]['hh_rut_ej']=$rutinas[0]['sum'];
				$indicadores[$value['value']]['sum_ot_soc_ej']=$trabajoSocial[0]['sum']+$ot_ej[0]['sum'];
				$indicadores[$value['value']]['sum_p1_p_ej']=($trabajoSocial[0]['sum']+$ot_ej[0]['sum'])+($ot_p1[0]['sum']+$act_eme[0]['sum']);	
				$hh_r_rutina=$recursoRutina->getHorasRecurso($params);
				$hh_r_ot=$recursoOt->getHorasRecurso($params);
				$indicadores[$value['value']]['hh_t_real']=$hh_r_rutina[0]['sum']+$hh_r_ot[0]['sum'];
				$indicadores[$value['value']]['hh_t']=$indicadores[$value['value']]['sum_p1_p_ej']+$indicadores[$value['value']]['hh_t_real']+$indicadores[$value['value']]['hh_rut_ej'];
				if ($indicadores[$value['value']]['hh_t']>0) {
					$indicadores[$value['value']]['reactividad']=($indicadores[$value['value']]['sum_p1_p_ej']/$indicadores[$value['value']]['hh_t'])*100;
				}else{
					$indicadores[$value['value']]['reactividad']=0;
				}
				// var_dump($hh_r_ot);
			}

			// foreach ($indicadores as $key => $value) {
			// 	echo $key;
			// 	print_r($value);
			// 	echo "<br>";
			// }
			// echo "<br><br>Semana finalizada<br><br>";

		// }

?>

<h1>GERENCIA DE SERVICIOS GENERALES</h1>
	<h2>Reporte de Indicadores del <?php echo $POST['fecha_inicio'];  ?> al <?php echo $POST['fecha_fin'];  ?></h2>

	<div>
		<table class="tabla-bordes">
			<thead>
				<tr>
					<th>Unidad</th>
					<th>OT Prog Trab Soc</th>
					<th>OT Ejec Trab Soc</th>
					<th>% Cump Trab Soc</th>
					<th>OT Prog</th>
					<th>OT Ej</th>
					<th>OT Pend</th>
					<th>% Cump OT</th>
					<th>OT Ad</th>
					<th>HH Ad</th>
					<th>OT Ej + Ad</th>
					<th>HH OT Trab Soc</th>
					<th>HH OT Prog Ej</th>
					<th>HH Rut Ej</th>
					<th>HHP1+ HH Prog Ej</th>
					<th>HH Rutinas+HHP1+HH Real+HH OT TRAB SOC</th>
					<th>Reactividad</th>
				</tr>
			</thead>
			<tbody>
<?php
		foreach ($indicadores as $key => $value) {
?>
			<tr class="borde-tr">
				<td><?php echo $value['desc_ejecutor']; ?></td>
				<td><?php echo $value['ot_p_tb_sc']; ?></td>
				<td><?php echo $value['ot_ej_tb_sc']; ?></td>
				<td><?php echo round($value['cump_tb_sc'],2); ?></td>
				<td><?php echo $value['ot_p']; ?></td>
				<td><?php echo $value['ot_ej']; ?></td>
				<td><?php echo $value['ot_pend']; ?></td>
				<td><?php echo round($value['cump_ot'],2); ?></td>
				<td><?php echo $value['act_ad']; ?></td>
				<td><?php echo $value['hh_act_ad']; ?></td>
				<td><?php echo $value['ot_ej_y_ot_ad']; ?></td>
				<td><?php echo $value['hh_ej_tb_sc']; ?></td>
				<td><?php echo $value['sum_ot_soc_ej']; ?></td>
				<td><?php echo $value['hh_rut_ej']; ?></td>
				<td><?php echo $value['sum_p1_p_ej']; ?></td>
				<td><?php echo $value['hh_t']; ?></td>
				<td><?php echo round($value['reactividad'],2); ?></td>
			</tr>			
<?php
		}

	}
?>

</body>


		