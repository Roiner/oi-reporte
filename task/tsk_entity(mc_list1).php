<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = '<Entity>';

// Estructura del contenido
$structure['location'] = 'SISGREL &raquo; ADMINISTRAR &raquo; Tablas Maestras &raquo; <Entity>';
$structure['title'] = 'Administrar <Entity>s';
$structure['task_list'] = 'tsk_<Entity>(list).php'; // Tarea para listar/buscar
if(USE_DLG) {
	$structure['task_add'] = 'tsk_<Entity>(dlg_add1).php'; // Tarea para agregar
	$structure['task_add_container'] = 'divDialog'; // Contenedor de la tarea para agregar
}
else {
	$structure['task_add'] = 'tsk_<Entity>(mc_add1).php'; // Tarea para agregar
}

// -> Estructura del contenido del form de búsqueda (opcional)
$structure['search']['form']['type'] = 'fieldsets';
$structure['search']['form']['fs'][0]['type'] = 'fieldset';
$structure['search']['form']['fs'][0]['legend'] = 'Filtro de la búsqueda';
$structure['search']['form']['fs'][0]['table'] = 'y';

$structure['search']['form']['fs'][0]['fields'][0][0]['td'] = array('class' => 'label');
$structure['search']['form']['fs'][0]['fields'][0][0][] = array('control' => 'label', 'field' => '<campo1>');
$structure['search']['form']['fs'][0]['fields'][0][1]['td'] = array('class' => 'field');
$structure['search']['form']['fs'][0]['fields'][0][1][] = array('control' => 'textbox', 'field' => '<campo1>');
$structure['search']['form']['fs'][0]['fields'][0][2]['td'] = array('class' => 'label');
$structure['search']['form']['fs'][0]['fields'][0][2][] = array('control' => 'label', 'field' => '<campo2>');
$structure['search']['form']['fs'][0]['fields'][0][3]['td'] = array('class' => 'field');
$structure['search']['form']['fs'][0]['fields'][0][3][] = array('control' => 'textbox', 'field' => '<campo2>');
$structure['search']['form']['fs'][0]['fields'][0][4]['td'] = array('class' => 'label');
$structure['search']['form']['fs'][0]['fields'][0][4][] = array('control' => 'label', 'field' => '<campo3>');
$structure['search']['form']['fs'][0]['fields'][0][5]['td'] = array('class' => 'field');
$structure['search']['form']['fs'][0]['fields'][0][5][] = array('control' => 'textbox', 'field' => '<campo3>');

// -> Estructura del panel de botones (opcional)
// $structure['buttons'] = array(
	// BTN_SAVE => BTN_SAVE,
	// BTN_CANCEL => BTN_CANCEL,
	// 'MI_BOTON' => array('id' => 'btn_'.MI_BOTON, 'label' => 'Mi Boton', 'title' => 'Botón personalizado', 'icon_p' => 'comment',
		// 'javascript' => "alert('Mi boton propio')"),
	// BTN_COPY => BTN_COPY,
	// BTN_PASTE => BTN_PASTE
// );

// Javascript
// $javascript['exec'] = <<<'EOS'
	// $('#siglas').datepicker();
// EOS;

// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'mc_list1.php');
?>