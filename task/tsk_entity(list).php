<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = '<Entity>';

// Estructura del contenido
$structure['container'] = 'listContainer';
$structure['page_rows'] = 10; // Filas por páginas
if(USE_DLG) {
	$structure['task_enq'] = 'tsk_<Entity>(dlg_enq1).php';
	$structure['task_enq_container'] = 'divDialog';
	$structure['task_mod'] = 'tsk_<Entity>(dlg_mod1).php';
	$structure['task_mod_container'] = 'divDialog';
	$structure['task_del'] = 'tsk_<Entity>(dlg_del1).php';
	$structure['task_del_container'] = 'divDialog';
}
else {
	$structure['task_enq'] = 'tsk_<Entity>(mc_enq1).php';
	$structure['task_mod'] = 'tsk_<Entity>(mc_mod1).php';
	$structure['task_del'] = 'tsk_<Entity>(mc_del1).php';
}
$structure['list_style'] = LST_SPL;

// -> Estructura del WHERE (opcional)
$structure['where'][0]['fields'] = array('<campo1>');
$structure['where'][0]['sql'] = "t.<campo1> = :<campo1>";
$structure['where'][0]['params'] = array('<campo1>' => '<campo1>');

$structure['where'][1]['fields'] = array('<campo2>');
$structure['where'][1]['sql'] = "(t.<campo2> ilike ('%' || :<campo2> || '%'))";
$structure['where'][1]['params'] = array('<campo2>' => '<campo2>');

$structure['where'][2]['fields'] = array('<campo3>');
$structure['where'][2]['sql'] = "(t.<campo3> ilike ('%' || :<campo3> || '%') OR t.<campo4> ilike ('%' || :<campo4> || '%'))";
$structure['where'][2]['params'] = array('<campo3>' => '<campo3>', '<campo4>' => '<campo3>');

// -> Estructura del contenido del gridview
$structure['gridview']['type'] = 'gridview';
$structure['gridview']['id'] = 'tsk_<Entity>_list';
$structure['gridview']['caption'] = 'Lista de <Entity>';
$structure['gridview']['sorted'] = true;

$structure['gridview']['header'][0][0]['th'] = array('sort_field' => '<campo1>');
$structure['gridview']['header'][0][0][] = array('control' => 'literal', 'content' => 'Campo1');
$structure['gridview']['header'][0][1]['th'] = array('sort_field' => '<campo2>');
$structure['gridview']['header'][0][1][] = array('control' => 'literal', 'content' => 'Campo2');
$structure['gridview']['header'][0][2]['th'] = array('sort_field' => '<campo3>');
$structure['gridview']['header'][0][2][] = array('control' => 'literal', 'content' => '<campo3>');

$structure['gridview']['footer'][0][0]['td'] = array('colspan' => 3);
$structure['gridview']['footer'][0][0][] = array('control' => 'literal', 'content' => '&nbsp;');

$structure['gridview']['items'][0][0][] = array('control' => 'literal', 'field' => '<campo1>');
$structure['gridview']['items'][0][0][] = array('control' => 'hidden', 'field' => '<campo1>', 'id' => '<campo1>[{#}]');
$structure['gridview']['items'][0][1][] = array('control' => 'literal', 'field' => '<campo2>');
$structure['gridview']['items'][0][2]['td'] = array('class' => 'text_left');
$structure['gridview']['items'][0][2][] = array('control' => 'literal', 'field' => '<campo3>');

// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'list.php');
?>