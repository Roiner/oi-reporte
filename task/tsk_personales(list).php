<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'Personal';

// Estructura del contenido
$structure['container'] = 'divAsistencia';
$structure['paginator'] = PAGINATOR_NONE;
//$structure['page_rows'] = 10; // Filas por páginas

/*$structure['task_mod'] = 'tsk_materiales(dlg_mod1).php';
$structure['task_mod_container'] = 'divDialog';
$structure['task_del'] = 'tsk_materiales(dlg_del1).php';
$structure['task_del_container'] = 'divDialog';*/

$structure['list_style'] = LST_NOS;

// -> Estructura del WHERE (opcional)
if(!empty($_POST['id_supervisor'])) {
	$structure['where'][0]['fields'] = array('id_supervisor'); 
	$structure['where'][0]['sql'] = "t.id_supervisor = :id_supervisor";
	$structure['where'][0]['params'] = array('id_supervisor' => 'id_supervisor');
} 

// $structure['where'][1]['fields'] = array('<campo2>');
// $structure['where'][1]['sql'] = "(t.<campo2> ilike ('%' || :<campo2> || '%'))";
// $structure['where'][1]['params'] = array('<campo2>' => '<campo2>');

// $structure['where'][2]['fields'] = array('nombre');
// $structure['where'][2]['sql'] = "(t.nombre ilike ('%' || :nombre1 || '%') OR t.desc ilike ('%' || :nombre2 || '%'))";
// $structure['where'][2]['params'] = array('nombre1' => 'nombre', 'nombre2' => 'nombre');

// -> Estructura del contenido del gridview
$structure['gridview']['type'] = 'gridview';
$structure['gridview']['id'] = 'tsk_personales_list';
$structure['gridview']['caption'] = 'Personal';
$structure['gridview']['sorted'] = true;

$structure['gridview']['header'][0][0]['th'] = array('sort_field' => 'id_personal');
$structure['gridview']['header'][0][0][] = array('control' => 'literal', 'content' => 'Ficha:');
$structure['gridview']['header'][0][1][] = array('control' => 'literal', 'content' => 'Nombre:');
$structure['gridview']['header'][0][2][] = array('control' => 'literal', 'content' => 'Asistencia:');

$structure['gridview']['footer'][0][0]['td'] = array('colspan' => 1);
$structure['gridview']['footer'][0][0][] = array('control' => 'literal', 'content' => '&nbsp;');

$structure['gridview']['items'][0][0][] = array('control' => 'literal', 'field' => 'id_personal');
$structure['gridview']['items'][0][0][] = array('control' => 'hidden', 'field' => 'id_supervisor', 'id' => 'id_supervisor[{#}]');
$structure['gridview']['items'][0][0][] = array('control' => 'hidden', 'field' => 'id_personal' ,'name' => 'id_p[{#}]');

$structure['gridview']['items'][0][1][] = array('control' => 'literal', 'field' => 'personal');

$structure['gridview']['items'][0][2][] = array('control' => 'dropdown', 'field' => 'id_a_tipo', 'id' => 'id_a_tipo{#}','name' =>'id_a_tipo[{#}]','class' => 'a_tipos' );

$javascript['exec'] = <<<'EOS'
	$("#cantidad_trab").val($("#tsk_personales_list table tbody tr").length);

	// $('#hh_disp').val($('.a_tipos').size()*6)

	$('.a_tipos').change(function(){
		resultado=0;
		$(".a_tipos").each(function(){
			if ($(this).val()==1) {
				resultado+=6;
			}
		});
		console.log(resultado);
		$('#hh_disp').val(resultado);
	});

	// var n= $('.a_tipos');

	$('tfoot').hide();
	for (var i = 0; i < $('.a_tipos').size() ; i++) {
		// console.log($('#id_a_tipo'+i).attr('id'));
		// Cargar tipos de asistencia
		var param = { extra: { value: '', text: 'Seleccione una opcion' } };
		$('#id_a_tipo'+i).empty().append('<option value="">cargando...</option>');
		sysfwk.loadSelect('AsistenciaTipo', 'getTipos', param, 'id_a_tipo'+i);
	}

EOS;


// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'list.php');
?>