<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

// Definir la entidad
$structure['entity'] = 'Personal';

// Estructura del contenido
$structure['title'] = 'Personal';
$structure['container'] = 'divDialog';
$structure['form_action'] = 'tsk_detallePersonalAsigado(dlg_add1).php';

// -> Estructura del panel de botones (opcional)
/*$structure['buttons'] = array(
	BTN_SAVE => BTN_SAVE,
	BTN_CANCEL => BTN_CANCEL,
	'BTN_BUSCAR' => array('id' => 'btn_BUSCAR', 'label' => 'Buscar', 'title' => 'Botón Buscar', 'icon_p' => 'search',
		'javascript' => "sysfwk.loadPage('task/tsk_st_ot(dlg_acmp).php', 'divDialog1', { container: 'divDialog1', js_func: 'ActualizarOt', ejecutor: $('#id_ejecutor').val()});"),
	BTN_COPY => BTN_COPY,
	BTN_PASTE => BTN_PASTE,
	BTN_OK => BTN_OK,
);*/

// Cargar configuración del screen
include('../screen/scr_detallePersonalAsignado(abm1).php');
$structure['form']['fs'][0]['fields'][0][1][] = array('control' => 'button', 'id' => 'btnSelPersonalAsignado', 'content' => '...',
	'title' => 'Seleccionar Personal');



$javascript['global'] = <<<'EOS'
// Actualizar personal
function ActualizarPersonal(datos) {
	var nombre = datos.text.split(' - ');
	console.log(nombre);
	$('#id_personal').val(datos.value);
	$('#nombre_personal').val(nombre[1]);
}
EOS;

$javascript['exec'] .= <<<'EOS'

// Botones
$('#btnSelPersonalAsignado').button({ icons: {primary:'ui-icon-search'}, text: false });
$('#btnSelPersonalAsignado').click(function() {
	sysfwk.loadPage('task/tsk_personal_sel(dlg_acmp).php', 'divDialog1', { container: 'divDialog1', js_func: 'ActualizarPersonal'});
});
EOS;

// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'dlg_jsfrm1.php');
?>