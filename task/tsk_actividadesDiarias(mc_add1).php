<?php
header('Content-Type: text/html; charset=UTF-8');

require_once('../main.php');
// echo '<pre>'; var_dump($GLOBALS); echo '</pre>';
include($GLOBALS['path_fwk_script'] . 'check_session.php');

/*require_once($GLOBALS['path_class'] . 'Turno.class.php' );
$turnos= new Turno();
$turnos=$turnos->getTurno();
var_dump($turnos);*/


// Definir la entidad
$structure['entity'] = 'ActividadDiaria';
// Estructura del contenido
$structure['location'] = 'SISGREL &raquo; Actividades diarios';
$structure['title'] = 'Gestion de Actividades';
$structure['form_action'] = 'tsk_actividadesDiarias(mc_add1).php';
$structure['task_mode'] = 'add1';

// Cargar configuración del screen
include('../screen/scr_actividadesDiarias(abm1).php');

// -> Estructura del panel de botones (opcional)
/* $structure['buttons'] = array(
	 BTN_SAVE => BTN_SAVE,
	 BTN_CANCEL => BTN_CANCEL,
	 'BTN_CREAR' => array('id' => 'btn_CREAR', 'label' => 'Cargar Planificacion', 'title' => 'Nueva Planificacion', 'icon_p' => 'document',
		 'javascript' => "sysfwk.loadPage('task/tsk_detallePlanificaciones(list).php', 'divDetallePlanificacion',{},sysfwk.POST);"),
	 BTN_OK => BTN_OK
 );
*/


// Javascript
include('../lib_js/script_planificacion.php');

$javascript['global'] = <<<'EOS'

//Contar horas hombre ejecutadas
function contarHhEj(){
	var resultado=0;
	$(".hh").each(function(){
		if ($(this).val()) {
			resultado+=parseInt($(this).val());
		}
	});
	console.log(resultado);
	$("#hh_ej").val(resultado);
}

EOS;



$javascript['exec'] .= <<<'EOS'
	// Cargar lista de turnos
 	var param = { extra: { value: '', text: '(Todos)' } };
 	$('#id_turno').empty().append('<option value="">cargando...</option>');
 	sysfwk.loadSelect('Turno', 'getTurno', param, 'id_turno');

	$('#fecha').datepicker();
	$('#btnAddActividad').button({ icons: {primary:'ui-icon-circle-plus'} }).click(function(){
		if($('#id_ejecutor').val()==null || $('#id_ejecutor').val()=='')
			alert('debe ingresar una unidad')
		else{
			if($('#fecha').val()==null || $('#fecha').val()==''){
				alert('debe ingresar una fecha');
				// console.log('hola desde hoy');
				// var hoy = new Date();
				// var fecha_ejecucion= hoy.getFullYear() + "-" + (hoy.getMonth() +1) + "-" + hoy.getDate();
			}else{
				fecha=$('#fecha').val();
			}
			sysfwk.loadPage('task/tsk_actividadesOT(list).php', 'divActividadesOT', {id_ejecutor: $('#id_ejecutor').val() },sysfwk.POST );
			sysfwk.loadPage('task/tsk_actividadesRutinarias(list).php', 'divActividadesRutinarias', {id_ejecutor: $('#id_ejecutor').val() },sysfwk.POST );
			sysfwk.loadPage('task/tsk_actividadesEmergencias(list).php', 'divActividadesEmergencias', {id_ejecutor: $('#id_ejecutor').val() },sysfwk.POST );
			//sysfwk.loadPage('task/tsk_actividadesRutinarias(list).php', 'divActividadesRutinarias', { container: 'divActividadesRutinarias', js_func: 'ActualizarRecursos', id_ejecutor: $('#id_ejecutor').val(), fecha_ejecucion: fecha },sysfwk.POST);
		}
	});

	$(".cantidad").change(function(){
		var resultado=0;
		resultado = $("#hh").val()*$("#cantidad_personal").val();
		$('#hh_rutinas').val(resultado);
	});	

	sysfwk.loadPage('task/tsk_personales(list).php', 'divAsistencia', {id_supervisor: $('#id_personal').val() },sysfwk.POST );



EOS;


// Cargar patron
include($GLOBALS['path_fwk_pattern'] . 'mc_add1.php');

//echo "<div id='divAsistencia'></div>";
?>
