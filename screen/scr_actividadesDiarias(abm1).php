<?php

$i=0;
// Configuración de pantalla para la vista mc.abm1
$structure['form_id'] = 'frm_actividadesDiarias_abm1';
$structure['msg_id'] = 'msg_actividadesDiarias_abm1';

$structure['form']['fs'][$i]['type'] = 'div';
$structure['form']['fs'][$i]['table'] = 'n';
$structure['form']['fs'][$i]['fields'][0][] = array('control' => 'literal', 'content' => "<div id='divAsistencia'></div>");

$i++;
// -> Estructura del contenido del form
$structure['form']['type'] = 'fieldsets';
$structure['form']['fs'][$i]['type'] = 'fieldset';
$structure['form']['fs'][$i]['legend'] = 'Seleccione la Unidad:';
$structure['form']['fs'][$i]['table'] = 'y';


$structure['form']['fs'][$i]['fields'][0][0]['td'] = array('class' => 'label superintendencia');
$structure['form']['fs'][$i]['fields'][0][0][] = array('control' => 'label', 'label' => 'Superintendencia:');

$structure['form']['fs'][$i]['fields'][0][1]['td'] = array('class' => 'field superintendencia');
$structure['form']['fs'][$i]['fields'][0][1][] = array('control' => 'dropdown', 'field' => 'id_area_operativa');

$structure['form']['fs'][$i]['fields'][0][2]['td'] = array('class' => 'label');
$structure['form']['fs'][$i]['fields'][0][2][] = array('control' => 'label', 'label' => 'Unidad:');

$structure['form']['fs'][$i]['fields'][0][3]['td'] = array('class' => 'field');
$structure['form']['fs'][$i]['fields'][0][3][] = array('control' => 'dropdown', 'field' => 'id_ejecutor');

$structure['form']['fs'][$i]['fields'][1][0]['td'] = array('class' => 'label');
$structure['form']['fs'][$i]['fields'][1][0][] = array('control' => 'label', 'label' => 'Fecha de ejecucion:');

$structure['form']['fs'][$i]['fields'][1][1]['td'] = array('class' => 'field');
$structure['form']['fs'][$i]['fields'][1][1][] = array('control' => 'textbox', 'field' => 'fecha');

$structure['form']['fs'][$i]['fields'][1][2]['td'] = array('class' => 'label');
$structure['form']['fs'][$i]['fields'][1][2][] = array('control' => 'label', 'label' => 'Turno:');

$structure['form']['fs'][$i]['fields'][1][3]['td'] = array('class' => 'field');
$structure['form']['fs'][$i]['fields'][1][3][] = array('control' => 'dropdown', 'field' => 'id_turno');

$structure['form']['fs'][$i]['fields'][2][0][] = array('control' => 'button', 'content' => 'Cargar Actividades',
'id' => 'btnAddActividad', 'name' => 'btnAddActividad');

$i++;
// -> Estructura del contenido de las actividades no rutinarias
$structure['form']['fs'][$i]['type'] = 'div';
$structure['form']['fs'][$i]['table'] = 'n';
$structure['form']['fs'][$i]['fields'][0][] = array('control' => 'literal', 'content' => "<div id='divActividadesOT'></div>");
$i++;
// -> Estructura del contenido de las actividades rutinarias
$structure['form']['fs'][$i]['type'] = 'div';
$structure['form']['fs'][$i]['table'] = 'n';
$structure['form']['fs'][$i]['fields'][0][] = array('control' => 'literal', 'content' => "<div id='divActividadesRutinarias'></div>");

$i++;

// -> Estructura del contenido de las actividades no planificadas
$structure['form']['fs'][$i]['type'] = 'div';
$structure['form']['fs'][$i]['table'] = 'n';
$structure['form']['fs'][$i]['fields'][0][] = array('control' => 'literal', 'content' => "<div id='divActividadesEmergencias'></div>");

$i++;

// -> Estructura del contenido del form
// $structure['form']['type'] = 'fieldsets';
// $structure['form']['fs'][$i]['type'] = 'fieldset';
// $structure['form']['fs'][$i]['legend'] = 'Actividades Rutinarias:';
// $structure['form']['fs'][$i]['table'] = 'y';


// $structure['form']['fs'][$i]['fields'][0][0]['td'] = array('class' => 'label');
// $structure['form']['fs'][$i]['fields'][0][0][] = array('control' => 'label', 'field' => 'cantidad_rutinas');

// $structure['form']['fs'][$i]['fields'][0][1]['td'] = array('class' => 'field');
// $structure['form']['fs'][$i]['fields'][0][1][] = array('control' => 'textbox', 'field' => 'cantidad_rutinas');

// $structure['form']['fs'][$i]['fields'][0][2]['td'] = array('class' => 'label');
// $structure['form']['fs'][$i]['fields'][0][2][] = array('control' => 'label', 'field' => 'hh_rutinas');

// $structure['form']['fs'][$i]['fields'][0][3]['td'] = array('class' => 'field');
// $structure['form']['fs'][$i]['fields'][0][3][] = array('control' => 'textbox', 'field' => 'hh', 'class' => 'cantidad');

// $structure['form']['fs'][$i]['fields'][1][0]['td'] = array('class' => 'label');
// $structure['form']['fs'][$i]['fields'][1][0][] = array('control' => 'label', 'label' => 'Cantidad de Personal');

// $structure['form']['fs'][$i]['fields'][1][1]['td'] = array('class' => 'field');
// $structure['form']['fs'][$i]['fields'][1][1][] = array('control' => 'textbox', 'field' => 'cantidad_personal', 'class' => 'cantidad');

// $structure['form']['fs'][$i]['fields'][1][2]['td'] = array('class' => 'label');
// $structure['form']['fs'][$i]['fields'][1][2][] = array('control' => 'label', 'label' => 'HH Totales');

// $structure['form']['fs'][$i]['fields'][1][3]['td'] = array('class' => 'field');
// $structure['form']['fs'][$i]['fields'][1][3][] = array('control' => 'textbox', 'field' => 'hh_rutinas', 'disabled' => 'y');


$i++;
// -> Estructura del contenido del form
$structure['form']['type'] = 'fieldsets';
$structure['form']['fs'][$i]['type'] = 'fieldset';
$structure['form']['fs'][$i]['legend'] = 'Nueva actividad:';
$structure['form']['fs'][$i]['table'] = 'y';


$structure['form']['fs'][$i]['fields'][0][0]['td'] = array('class' => 'label');
$structure['form']['fs'][$i]['fields'][0][0][] = array('control' => 'label', 'field' => 'id_personal');

$structure['form']['fs'][$i]['fields'][0][1]['td'] = array('class' => 'field');
$structure['form']['fs'][$i]['fields'][0][1][] = array('control' => 'textbox', 'field' => 'id_personal', 'disabled' => 'y', 'value' => $_SESSION[USER_REAL_ID]);

$structure['form']['fs'][$i]['fields'][0][2]['td'] = array('class' => 'label');
$structure['form']['fs'][$i]['fields'][0][2][] = array('control' => 'label', 'field' => 'hh_disp');

$structure['form']['fs'][$i]['fields'][0][3]['td'] = array('class' => 'field');
$structure['form']['fs'][$i]['fields'][0][3][] = array('control' => 'textbox', 'field' => 'hh_disp', 'disabled' => 'y');

$structure['form']['fs'][$i]['fields'][1][0]['td'] = array('class' => 'label');
$structure['form']['fs'][$i]['fields'][1][0][] = array('control' => 'label', 'field' => 'observacion');

$structure['form']['fs'][$i]['fields'][1][1]['td'] = array('class' => 'field');
$structure['form']['fs'][$i]['fields'][1][1][] = array('control' => 'textarea', 'field' => 'observacion');

$structure['form']['fs'][$i]['fields'][1][1][] = array('control' => 'hidden', 'field' => 'hh_ej');

$structure['form']['fs'][$i]['fields'][1][1][] = array('control' => 'hidden', 'field' => 'cantidad_trab');



/*$structure['search']['form']['fs'][0]['fields'][0][0]['td'] = array('class' => 'label');
$structure['search']['form']['fs'][0]['fields'][0][0][] = array('control' => 'label', 'label' => 'Superintendencia:');
$structure['search']['form']['fs'][0]['fields'][0][1]['td'] = array('class' => 'field');
$structure['search']['form']['fs'][0]['fields'][0][1][]= array('control' => 'dropdown', 'field' => 'id_area_operativa');
$structure['search']['form']['fs'][0]['fields'][0][2]['td'] = array('class' => 'label');
$structure['search']['form']['fs'][0]['fields'][0][2][] = array('control' => 'label', 'label' => 'Unidad:');

$structure['search']['form']['fs'][0]['fields'][0][3]['td'] = array('class' => 'field');
$structure['search']['form']['fs'][0]['fields'][0][3][]= array('control' => 'dropdown', 'field' => 'id_ejecutor');

$structure['search']['form']['fs'][0]['fields'][1][0]['td'] = array('class' => 'label');
$structure['search']['form']['fs'][0]['fields'][1][0][] = array('control' => 'label', 'label' => 'Fecha Inicio:');

$structure['search']['form']['fs'][0]['fields'][1][1]['td'] = array('class' => 'field');
$structure['search']['form']['fs'][0]['fields'][1][1][]= array('control' => 'textbox', 'field' => 'fecha_inicio', 'class'=>'datepicker');

$structure['search']['form']['fs'][0]['fields'][1][2]['td'] = array('class' => 'label');
$structure['search']['form']['fs'][0]['fields'][1][2][] = array('control' => 'label', 'label' => 'Fecha final:');

$structure['search']['form']['fs'][0]['fields'][1][3]['td'] = array('class' => 'field');
$structure['search']['form']['fs'][0]['fields'][1][3][]= array('control' => 'textbox', 'field' => 'fecha_fin');*/
?>

