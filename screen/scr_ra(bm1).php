<?php
isset($raFS) or $raFS = count($structure['form']['fs']);

// -> Estructura del fieldset de auditoria
$structure['form']['fs'][$raFS]['type'] = 'fieldset';
$structure['form']['fs'][$raFS]['legend'] = 'Datos de auditoría:';
$structure['form']['fs'][$raFS]['table'] = 'y';

$structure['form']['fs'][$raFS]['fields'][0][0]['td'] = array('class' => 'label');
$structure['form']['fs'][$raFS]['fields'][0][0][] = array('control' => 'label', 'field' => 'ra_add_user', 'for' => 'creado_por');
$structure['form']['fs'][$raFS]['fields'][0][1]['td'] = array('class' => 'field');
$structure['form']['fs'][$raFS]['fields'][0][1][] = array('control' => 'literal', 'field' => 'creado_por');
$structure['form']['fs'][$raFS]['fields'][0][1][] = array('control' => 'literal', 'field' => 'ra_add_date');

$structure['form']['fs'][$raFS]['fields'][1][0]['td'] = array('class' => 'label');
$structure['form']['fs'][$raFS]['fields'][1][0][] = array('control' => 'label', 'field' => 'ra_mod_user', 'for' => 'modificado_por');
$structure['form']['fs'][$raFS]['fields'][1][1]['td'] = array('class' => 'field');
$structure['form']['fs'][$raFS]['fields'][1][1][] = array('control' => 'literal', 'field' => 'modificado_por');
$structure['form']['fs'][$raFS]['fields'][1][1][] = array('control' => 'literal', 'field' => 'ra_mod_date');
?>