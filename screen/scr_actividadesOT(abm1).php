<?php
// Configuración de pantalla para la vista mc.abm1
$structure['form_id'] = 'frm_material_abm1';
$structure['msg_id'] = 'msg_material_abm1';

// Opciones del dialogo
$structure['dlg_options'] = array(
	'width' => 650,
	'heigth' => 900,
);

// -> Estructura del contenido del form
$structure['form']['type'] = 'fieldsets';
$structure['form']['fs'][0]['type'] = 'fieldset';
$structure['form']['fs'][0]['legend'] = 'Datos de Orden de Trabajo';
$structure['form']['fs'][0]['table'] = 'y';

$structure['form']['fs'][0]['fields'][0][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][0][0][] = array('control' => 'label', 'field' => 'id_st_ot');

$structure['form']['fs'][0]['fields'][0][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][0][1][] = array('control' => 'textbox', 'field' => 'id_st_ot', 'disabled' => 'y');

$structure['form']['fs'][0]['fields'][1][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][1][0][] = array('control' => 'label', 'field' => 'hh_total');

$structure['form']['fs'][0]['fields'][1][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][1][1][] = array('control' => 'textbox', 'field' => 'hh_total');

$structure['form']['fs'][0]['fields'][2][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][2][0][] = array('control' => 'label', 'label' => 'estado');

$structure['form']['fs'][0]['fields'][2][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][2][1][] = array('control' => 'literal','field' => 'estado', 'content' => "<select required='required' name='estado'>
	<option value=''>--Seleccione una opcion--</option>
	<option value='SIN_COMENZAR'>Sin Comenzar</option>
	<option value='EN_PROCESO'>En Proceso</option>
	<option value='TERMINADO'>Terminada</option>
</select>");


$structure['form']['fs'][0]['fields'][0][2][] = array('control' => 'hidden', 'field' => 'id_prioridad');

$structure['form']['fs'][0]['fields'][0][3][] = array('control' => 'hidden', 'field' => 'tag_activo');

$structure['form']['fs'][0]['fields'][0][4][] = array('control' => 'hidden', 'field' => 'desc_actividad');

$structure['form']['fs'][0]['fields'][0][5][] = array('control' => 'hidden', 'field' => 'cantidad');

/*$structure['form']['fs'][0]['fields'][0][5][] = array('control' => 'hidden', 'field' => 'estado', 'value' => 'SIN_COMENZAR');*/

$structure['form']['fs'][0]['fields'][0][6][] = array('control' => 'hidden', 'field' => 'recursos');


/*$structure['form']['fs'][0]['fields'][4][0][] = array('control' => 'hidden', 'field' => 'id_d_planificaciones');*/

$structure['form']['fs'][0]['fields'][0][7][] = array('control' => 'hidden', 'field' => 'recursos_t');

$structure['form']['fs'][0]['fields'][0][8][] = array('control' => 'hidden', 'field' => 'id_planificacion');

$structure['form']['fs'][1]['type'] = 'div';
$structure['form']['fs'][1]['table'] = 'n';
$structure['form']['fs'][1]['fields'][0][] = array('control' => 'literal', 'content' => "<div id='divEjecutorEspecialidad'></div>");



?>

