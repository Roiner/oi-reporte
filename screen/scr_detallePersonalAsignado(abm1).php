<?php
// Configuración de pantalla para la vista mc.abm1
$structure['form_id'] = 'frm_personalAsignado_abm1';
$structure['msg_id'] = 'msg_personalAsignado_abm1';

// Opciones del dialogo
$structure['dlg_options'] = array(
	'width' => 580,
	'heigth' => 900,
);

// -> Estructura del contenido del form
$structure['form']['type'] = 'fieldsets';
$structure['form']['fs'][0]['type'] = 'fieldset';
$structure['form']['fs'][0]['legend'] = 'Datos del Personal';
$structure['form']['fs'][0]['table'] = 'y';

$structure['form']['fs'][0]['fields'][0][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][0][0][] = array('control' => 'label', 'label' => 'Ficha');

$structure['form']['fs'][0]['fields'][0][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][0][1][] = array('control' => 'textbox', 'field' => 'id_personal', 'disabled' => 'y');

$structure['form']['fs'][0]['fields'][1][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][1][0][] = array('control' => 'label', 'label' => 'Nombre');

$structure['form']['fs'][0]['fields'][1][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][1][1][] = array('control' => 'textbox', 'field' => 'nombre_personal', 'disabled' => 'y');

$structure['form']['fs'][0]['fields'][0][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][0][1][] = array('control' => 'hidden', 'field' => 'id_supervisor');

?>