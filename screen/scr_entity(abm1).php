<?php
// Configuración de pantalla para la vista mc.abm1
$structure['form_id'] = 'frm_<entity>_abm1';
$structure['msg_id'] = 'msg_<entity>_abm1';

// Opciones del dialogo
$structure['dlg_options'] = array(
	'width' => 580
);

// -> Estructura del contenido del form
$structure['form']['type'] = 'fieldsets';
$structure['form']['fs'][0]['type'] = 'fieldset';
$structure['form']['fs'][0]['legend'] = 'Datos de <entity>';
$structure['form']['fs'][0]['table'] = 'y';

$structure['form']['fs'][0]['fields'][0][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][0][0][] = array('control' => 'label', 'field' => '<campo1>');
$structure['form']['fs'][0]['fields'][0][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][0][1][] = array('control' => 'textbox', 'field' => '<campo1>');
$structure['form']['fs'][0]['fields'][0][2]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][0][2][] = array('control' => 'label', 'field' => '<campo2>');
$structure['form']['fs'][0]['fields'][0][3]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][0][3][] = array('control' => 'textbox', 'field' => '<campo2>');

$structure['form']['fs'][0]['fields'][1][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][1][0][] = array('control' => 'label', 'field' => '<campo3>');
$structure['form']['fs'][0]['fields'][1][1]['td'] = array('class' => 'field', 'colspan' => 3);
$structure['form']['fs'][0]['fields'][1][1][] = array('control' => 'textbox', 'field' => '<campo3>');
?>