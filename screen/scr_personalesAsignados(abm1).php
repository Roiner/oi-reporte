<?php
// Configuración de pantalla para la vista mc.abm1
$structure['form_id'] = 'frm_personalesAsignados_abm1';
$structure['msg_id'] = 'msg_personalesAsignados_abm1';

// -> Estructura del contenido del form
$structure['form']['type'] = 'fieldsets';
$structure['form']['fs'][0]['type'] = 'fieldset';
$structure['form']['fs'][0]['legend'] = 'Asignar Nuevos Personales:';
$structure['form']['fs'][0]['table'] = 'y';

$structure['form']['fs'][0]['fields'][0][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][0][0][] = array('control' => 'label', 'label' => 'Ficha Supervisor:');

$structure['form']['fs'][0]['fields'][0][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][0][1][] = array('control' => 'textbox', 'field' => 'id_supervisor', 'disabled' => 'y', 'id' => 'id_sup');

$structure['form']['fs'][0]['fields'][0][1][] = array('control' => 'button', 'id' => 'btnSelPersonalABM', 'content' => '...',
	'title' => 'Buscar Supervisor');





/*$structure['search']['form']['fs'][0]['fields'][0][0]['td'] = array('class' => 'label');
$structure['search']['form']['fs'][0]['fields'][0][0][] = array('control' => 'label', 'label' => 'Superintendencia:');
$structure['search']['form']['fs'][0]['fields'][0][1]['td'] = array('class' => 'field');
$structure['search']['form']['fs'][0]['fields'][0][1][]= array('control' => 'dropdown', 'field' => 'id_area_operativa');
$structure['search']['form']['fs'][0]['fields'][0][2]['td'] = array('class' => 'label');
$structure['search']['form']['fs'][0]['fields'][0][2][] = array('control' => 'label', 'label' => 'Unidad:');

$structure['search']['form']['fs'][0]['fields'][0][3]['td'] = array('class' => 'field');
$structure['search']['form']['fs'][0]['fields'][0][3][]= array('control' => 'dropdown', 'field' => 'id_ejecutor');

$structure['search']['form']['fs'][0]['fields'][1][0]['td'] = array('class' => 'label');
$structure['search']['form']['fs'][0]['fields'][1][0][] = array('control' => 'label', 'label' => 'Fecha Inicio:');

$structure['search']['form']['fs'][0]['fields'][1][1]['td'] = array('class' => 'field');
$structure['search']['form']['fs'][0]['fields'][1][1][]= array('control' => 'textbox', 'field' => 'fecha_inicio', 'class'=>'datepicker');

$structure['search']['form']['fs'][0]['fields'][1][2]['td'] = array('class' => 'label');
$structure['search']['form']['fs'][0]['fields'][1][2][] = array('control' => 'label', 'label' => 'Fecha final:');

$structure['search']['form']['fs'][0]['fields'][1][3]['td'] = array('class' => 'field');
$structure['search']['form']['fs'][0]['fields'][1][3][]= array('control' => 'textbox', 'field' => 'fecha_fin');*/
?>

