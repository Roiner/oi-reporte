<?php
// Configuración de pantalla para la vista dlg.abm1
$structure['form_id'] = 'frm_st_ot_abm1';
$structure['msg_id'] = 'msg_st_ot_abm1';

// Opciones del dialogo
$structure['dlg_options'] = array(
	'width' => 730
);

// -> Estructura del contenido del form
$structure['form']['type'] = 'fieldsets';
$structure['form']['fs'][0]['type'] = 'fieldset';
$structure['form']['fs'][0]['legend'] = 'Lista de Ordenes de Trabajo';
$structure['form']['fs'][0]['table'] = 'y';

$structure['form']['fs'][0]['fields'][0][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][0][0][] = array('control' => 'label', 'field' => 'id_st_ot');
$structure['form']['fs'][0]['fields'][0][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][0][1][] = array('control' => 'hidden', 'field' => 'id_st_ot');
$structure['form']['fs'][0]['fields'][0][1][] = array('control' => 'textbox', 'field' => 'trab_solicitado', 'disabled' => 'y');

$structure['form']['fs'][0]['fields'][1][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][1][0][] = array('control' => 'label', 'field' => 'id_sistema');
$structure['form']['fs'][0]['fields'][1][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][1][1][] = array('control' => 'dropdown', 'field' => 'id_sistema');

$structure['form']['fs'][0]['fields'][2][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][2][0][] = array('control' => 'label', 'field' => 'id_role');
$structure['form']['fs'][0]['fields'][2][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][2][1][] = array('control' => 'dropdown', 'field' => 'id_role');

?>