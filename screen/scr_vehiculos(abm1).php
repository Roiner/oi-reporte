<?php
// Configuración de pantalla para la vista mc.abm1
$structure['form_id'] = 'frm_vehiculo_abm1';
$structure['msg_id'] = 'msg_vehiculo_abm1';

// Opciones del dialogo
$structure['dlg_options'] = array(
	'width' => 580
);

// -> Estructura del contenido del form
$structure['form']['type'] = 'fieldsets';
$structure['form']['fs'][0]['type'] = 'fieldset';
$structure['form']['fs'][0]['legend'] = 'Datos de Vehiculos';
$structure['form']['fs'][0]['table'] = 'y';

$structure['form']['fs'][0]['fields'][0][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][0][0][] = array('control' => 'label', 'field' => 'nombre');
$structure['form']['fs'][0]['fields'][0][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][0][1][] = array('control' => 'textbox', 'field' => 'nombre');
$structure['form']['fs'][0]['fields'][0][1][] = array('control' => 'hidden', 'field' => 'id_vehiculo');
?>