<?php
// Configuración de pantalla para la vista mc.abm1
$structure['form_id'] = 'frm_material_abm1';
$structure['msg_id'] = 'msg_material_abm1';

// Opciones del dialogo
$structure['dlg_options'] = array(
	'width' => 580,
	'heigth' => 900,
);

// -> Estructura del contenido del form
$structure['form']['type'] = 'fieldsets';
$structure['form']['fs'][0]['type'] = 'fieldset';
$structure['form']['fs'][0]['legend'] = 'Datos de Orden de Trabajo';
$structure['form']['fs'][0]['table'] = 'y';

$structure['form']['fs'][0]['fields'][0][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][0][0][] = array('control' => 'label', 'field' => 'id_st_ot');

$structure['form']['fs'][0]['fields'][0][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][0][1][] = array('control' => 'textbox', 'field' => 'id_st_ot', 'disabled' => 'y');

$structure['form']['fs'][0]['fields'][1][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][1][0][] = array('control' => 'label', 'field' => 'fecha_inicio');

$structure['form']['fs'][0]['fields'][1][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][1][1][] = array('control' => 'textbox', 'field' => 'fecha_inicio','id' => 'f_inicio');

$structure['form']['fs'][0]['fields'][2][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][2][0][] = array('control' => 'label', 'field' => 'hh_p');

$structure['form']['fs'][0]['fields'][2][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][2][1][] = array('control' => 'textbox', 'field' => 'hh_p');

$structure['form']['fs'][0]['fields'][3][0]['td'] = array('class' => 'label');
$structure['form']['fs'][0]['fields'][3][0][] = array('control' => 'label', 'field' => 'observaciones');

$structure['form']['fs'][0]['fields'][3][1]['td'] = array('class' => 'field');
$structure['form']['fs'][0]['fields'][3][1][] = array('control' => 'textarea', 'field' => 'observaciones');


$structure['form']['fs'][0]['fields'][3][1][] = array('control' => 'hidden', 'field' => 'id_prioridad');

$structure['form']['fs'][0]['fields'][3][1][] = array('control' => 'hidden', 'field' => 'tag_activo');

$structure['form']['fs'][0]['fields'][3][1][] = array('control' => 'hidden', 'field' => 'desc_activo');

$structure['form']['fs'][0]['fields'][3][1][] = array('control' => 'hidden', 'field' => 'desc_actividad');

$structure['form']['fs'][0]['fields'][3][1][] = array('control' => 'hidden', 'field' => 'id_planificador');

$structure['form']['fs'][0]['fields'][3][1][] = array('control' => 'hidden', 'field' => 'trab_solicitado');

$structure['form']['fs'][0]['fields'][3][1][] = array('control' => 'hidden', 'field' => 'estado', 'value' => 'SIN_COMENZAR');
/*$structure['form']['fs'][0]['fields'][4][0][] = array('control' => 'hidden', 'field' => 'id_d_planificaciones');*/

$javascript['exec'] .= <<<'EOS'
	$('#f_inicio').datepicker();
EOS;
?>