<?php 
$javascript['exec'] = <<<'EOS'
	$('#fecha_inicio').datepicker();
	$('#fecha_fin').datepicker();
	$('#fecha_reunion').datepicker();

	// Actualizar lista de ejecutores
	$('#id_area_operativa').change(function(){
	var param = { extra: { value: '', text: '(Todos)' } };
	param['id_area_operativa'] = $('#id_area_operativa').val();
	$('#id_ejecutor').empty().append('<option value="">cargando...</option>');
	sysfwk.loadSelect('EjecutorArea', 'getEjecutor', param, 'id_ejecutor');
	});

 	// Cargar lista de areas
 	var param = { extra: { value: '', text: '(Todos)' } };
 	$('#id_area_operativa').empty().append('<option value="">cargando...</option>');
 	sysfwk.loadSelect('EjecutorArea', 'getArea', param, 'id_area_operativa');
EOS;
?>